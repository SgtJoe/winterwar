
class WWVehicle_T20 extends WWVehicleTransport
	abstract;

var repnotify byte PassengerOneCurrentPositionIndex;
var repnotify bool bDrivingPassengerOne;
var repnotify byte PassengerTwoCurrentPositionIndex;
var repnotify bool bDrivingPassengerTwo;
var repnotify byte PassengerThreeCurrentPositionIndex;
var repnotify bool bDrivingPassengerThree;
var repnotify byte PassengerFourCurrentPositionIndex;
var repnotify bool bDrivingPassengerFour;

var repnotify TakeHitInfo DeathHitInfo_ProxyDriver;
var repnotify TakeHitInfo DeathHitInfo_ProxyHullMG;
var repnotify TakeHitInfo DeathHitInfo_ProxyPassOne;
var repnotify TakeHitInfo DeathHitInfo_ProxyPassTwo;
var repnotify TakeHitInfo DeathHitInfo_ProxyPassThree;
var repnotify TakeHitInfo DeathHitInfo_ProxyPassFour;

replication
{
	if (bNetDirty)
		PassengerOneCurrentPositionIndex, PassengerTwoCurrentPositionIndex,
		PassengerThreeCurrentPositionIndex, PassengerFourCurrentPositionIndex,
		bDrivingPassengerOne,bDrivingPassengerTwo,bDrivingPassengerThree,
		bDrivingPassengerFour;
	
	if (bNetDirty)
		DeathHitInfo_ProxyDriver, DeathHitInfo_ProxyHullMG, DeathHitInfo_ProxyPassOne,
		DeathHitInfo_ProxyPassTwo, DeathHitInfo_ProxyPassThree, DeathHitInfo_ProxyPassFour;
}

simulated event ReplicatedEvent(name VarName)
{
	if (VarName == 'DeathHitInfo_ProxyDriver')
	{
		PlaySeatProxyDeathHitEffects(0, DeathHitInfo_ProxyDriver);
	}
	else if (VarName == 'DeathHitInfo_ProxyHullMG')
	{
		PlaySeatProxyDeathHitEffects(1, DeathHitInfo_ProxyHullMG);
	}
	else if (VarName == 'DeathHitInfo_ProxyPassOne')
	{
		PlaySeatProxyDeathHitEffects(2, DeathHitInfo_ProxyPassOne);
	}
	else if (VarName == 'DeathHitInfo_ProxyPassTwo')
	{
		PlaySeatProxyDeathHitEffects(3, DeathHitInfo_ProxyPassTwo);
	}
	else if (VarName == 'DeathHitInfo_ProxyPassThree')
	{
		PlaySeatProxyDeathHitEffects(4, DeathHitInfo_ProxyPassThree);
	}
	else if (VarName == 'DeathHitInfo_ProxyPassFour')
	{
		PlaySeatProxyDeathHitEffects(5, DeathHitInfo_ProxyPassFour);
	}
	else
	{
		super.ReplicatedEvent(VarName);
	}
}

function DamageSeatProxy(int SeatProxyIndex, int Damage, Controller InstigatedBy, vector HitLocation, vector Momentum, class<DamageType> DamageType, optional Actor DamageCauser)
{
	switch (SeatProxyIndex)
	{
	case 0:
		DeathHitInfo_ProxyDriver.Damage = Damage;
		DeathHitInfo_ProxyDriver.HitLocation = HitLocation;
		DeathHitInfo_ProxyDriver.Momentum = Momentum;
		DeathHitInfo_ProxyDriver.DamageType = DamageType;
		break;
	case 1:
		DeathHitInfo_ProxyHullMG.Damage = Damage;
		DeathHitInfo_ProxyHullMG.HitLocation = HitLocation;
		DeathHitInfo_ProxyHullMG.Momentum = Momentum;
		DeathHitInfo_ProxyHullMG.DamageType = DamageType;
		break;
	case 2:
		DeathHitInfo_ProxyPassOne.Damage = Damage;
		DeathHitInfo_ProxyPassOne.HitLocation = HitLocation;
		DeathHitInfo_ProxyPassOne.Momentum = Momentum;
		DeathHitInfo_ProxyPassOne.DamageType = DamageType;
		break;
	case 3:
		DeathHitInfo_ProxyPassTwo.Damage = Damage;
		DeathHitInfo_ProxyPassTwo.HitLocation = HitLocation;
		DeathHitInfo_ProxyPassTwo.Momentum = Momentum;
		DeathHitInfo_ProxyPassTwo.DamageType = DamageType;
		break;
	case 4:
		DeathHitInfo_ProxyPassThree.Damage = Damage;
		DeathHitInfo_ProxyPassThree.HitLocation = HitLocation;
		DeathHitInfo_ProxyPassThree.Momentum = Momentum;
		DeathHitInfo_ProxyPassThree.DamageType = DamageType;
		break;
	case 5:
		DeathHitInfo_ProxyPassFour.Damage = Damage;
		DeathHitInfo_ProxyPassFour.HitLocation = HitLocation;
		DeathHitInfo_ProxyPassFour.Momentum = Momentum;
		DeathHitInfo_ProxyPassFour.DamageType = DamageType;
		break;
	}
	
	super.DamageSeatProxy(SeatProxyIndex, Damage, InstigatedBy, HitLocation, Momentum, DamageType, DamageCauser);
}

simulated event PostBeginPlay()
{
	super.PostBeginPlay();
	
	if (WorldInfo.NetMode != NM_DedicatedServer)
	{
		// Mesh.MinLodModel = 1;
		Mesh.AttachComponentToSocket(MGAmbientSound, 'MG_Barrel');
	}
}

simulated event TornOff()
{
	MGAmbientSound.Stop();
	
	Super.TornOff();
}

simulated function StopVehicleSounds()
{
	Super.StopVehicleSounds();
	
	MGAmbientSound.Stop();
}

simulated function VehicleWeaponFireEffects(vector HitLocation, int SeatIndex)
{
	Super.VehicleWeaponFireEffects(HitLocation, SeatIndex);
	
	if (SeatIndex == GetHullMGSeatIndex() && SeatFiringMode(SeatIndex,,true) == 0 && !MGAmbientSound.bWasPlaying)
	{
		MGAmbientSound.Play();
	}
}

simulated function VehicleWeaponStoppedFiring(bool bViaReplication, int SeatIndex)
{
	Super.VehicleWeaponStoppedFiring(bViaReplication, SeatIndex);
	
	if ( SeatIndex == GetHullMGSeatIndex() )
	{
		if ( MGAmbientSound.bWasPlaying || !MGAmbientSound.bFinished )
		{
			MGAmbientSound.Stop();
			PlaySound(MGStopSound, TRUE, FALSE, FALSE, MGAmbientSound.CurrentLocation, FALSE);
		}
	}
}

DefaultProperties
{
	Team=`ALLIES_TEAM_INDEX
	
	bOpenVehicle=true
	
	ExitRadius=110
	
	Begin Object Name=CollisionCylinder
		CollisionHeight=+60.0
		CollisionRadius=+90.0
		Translation=(X=0.0,Y=0.0,Z=10.0)
	End Object
	CylinderComponent=CollisionCylinder
	
	bDontUseCollCylinderForRelevancyCheck=true
	RelevancyHeight=70.0
	RelevancyRadius=130.0
	
	HUDBodyTexture=Texture2D'WinterWar_UI.Vehicle.UI_HUD_VH_T20'
	
	DriverOverlayTexture=Texture2D'WinterWar_UI.VehicleOptics.UI_HUD_VH_Optics_SquareOverlay'
	
	Seats(0)={(
		CameraTag=None,
		CameraOffset=-420,
		SeatAnimBlendName=DriverPositionNode,
		SeatPositions=(
			(
				bDriverVisible=true,
				bAllowFocus=false,
				PositionCameraTag=Driver_Camera,
				ViewFOV=70.0,
				bCamRotationFollowSocket=true,
				bViewFromCameraTag=true,
				bDrawOverlays=true,
				// bDrawOverlays=false,
				PositionIdleAnim=Driver_leanIN_idle,
				DriverIdleAnim=Driver_leanIN_idle,
				AlternateIdleAnim=Driver_leanIN_idle_AI,
				SeatProxyIndex=0,
				bIsExterior=false
			)
		),
		bSeatVisible=false,
		SeatBone=driver_player,
		DriverDamageMult=1.0,
		InitialPositionIndex=0,
		SeatRotation=(Pitch=0,Yaw=0,Roll=0)
	)}
	
	Seats(1)={(
		GunClass=class'WWVehicleWeapon_T20_HullMG',
		VignetteOverlayTexture=Texture2D'WinterWar_UI.VehicleOptics.UI_HUD_VH_Optics_CircularOverlay',
		GunSocket=(MG_Barrel),
		GunPivotPoints=(MG_Pitch),
		TurretVarPrefix="HullMG",
		TurretControls=(Hull_MG_Yaw,Hull_MG_Pitch),
		CameraTag=None,
		CameraOffset=-420,
		SeatBone=Hullgunner_bone,
		SeatAnimBlendName=HullMGPositionNode,
		SeatPositions=(
			(
				bDriverVisible=true,
				bAllowFocus=false,
				PositionCameraTag=MG_Camera,
				ViewFOV=55.0,
				bCamRotationFollowSocket=true,
				bViewFromCameraTag=true,
				bDrawOverlays=true,
				bUseDOF=true,
				bIsExterior=false,
				PositionIdleAnim=MG_ironIdle,
				bConstrainRotation=false,
				YawContraintIndex=0,
				PitchContraintIndex=1,
				DriverIdleAnim=MG_ironIdle,
				AlternateIdleAnim=MG_ironIdle_AI,
				SeatProxyIndex=1
			)
		),
		bSeatVisible=false,
		DriverDamageMult=1.0,
		InitialPositionIndex=0,
		FiringPositionIndex=0,
		SeatRotation=(Pitch=0,Yaw=0,Roll=0),
		TracerFrequency=5,
		WeaponTracerClass=(class'WWProjectile_DT_T20_Tracer',class'WWProjectile_DT_T20_Tracer'),
		MuzzleFlashLightClass=(none,none)
	)}
	
	Seats(2)={(
		CameraTag=None,
		CameraOffset=-420,
		SeatAnimBlendName=Pass1PositionNode,
		SeatPositions=(
			(
				bDriverVisible=true,
				bAllowFocus=true,
				PositionCameraTag=none,
				ViewFOV=70.0,
				PositionUpAnim=Passenger1_SitUp,
				PositionIdleAnim=Passenger1_idle,
				DriverIdleAnim=Passenger1_idle,
				AlternateIdleAnim=Passenger1_idle_AI,
				SeatProxyIndex=2,
				bIsExterior=true,
				PositionDeathAnims=(Passenger1_Death)
			)
		),
		TurretVarPrefix="PassengerOne",
		bSeatVisible=true,
		SeatRotation=(Pitch=0,Yaw=0,Roll=0),
		SeatBone=passenger_l_1,
		DriverDamageMult=1.0
	)}
	
	Seats(3)={(
		CameraTag=None,
		CameraOffset=-420,
		SeatAnimBlendName=Pass2PositionNode,
		SeatPositions=(
			(
				bDriverVisible=true,
				bAllowFocus=true,
				PositionCameraTag=none,
				ViewFOV=70.0,
				PositionUpAnim=Passenger3_SitUp,
				PositionIdleAnim=Passenger3_idle,
				DriverIdleAnim=Passenger3_idle,
				AlternateIdleAnim=Passenger3_idle_AI,
				SeatProxyIndex=3,
				bIsExterior=true,
				PositionDeathAnims=(Passenger3_Death)
			)
		),
		TurretVarPrefix="PassengerTwo",
		bSeatVisible=true,
		SeatRotation=(Pitch=0,Yaw=0,Roll=0),
		SeatBone=passenger_r_1,
		DriverDamageMult=1.0
	)}
	
	Seats(4)={(
		CameraTag=None,
		CameraOffset=-420,
		SeatAnimBlendName=Pass3PositionNode,
		SeatPositions=(
			(
				bDriverVisible=true,
				bAllowFocus=true,
				PositionCameraTag=none,
				ViewFOV=70.0,
				PositionUpAnim=Passenger2_SitUp,
				PositionIdleAnim=Passenger2_idle,
				DriverIdleAnim=Passenger2_idle,
				AlternateIdleAnim=Passenger2_idle_AI,
				SeatProxyIndex=4,
				bIsExterior=true,
				PositionDeathAnims=(Passenger2_Death)
			)
		),
		TurretVarPrefix="PassengerThree",
		bSeatVisible=true,
		SeatRotation=(Pitch=0,Yaw=0,Roll=0),
		SeatBone=passenger_l_2,
		DriverDamageMult=1.0
	)}
	
	Seats(5)={(
		CameraTag=None,
		CameraOffset=-420,
		SeatAnimBlendName=Pass4PositionNode,
		SeatPositions=(
			(
				bDriverVisible=true,
				bAllowFocus=true,
				PositionCameraTag=none,
				ViewFOV=70.0,
				PositionUpAnim=Passenger4_SitUp,
				PositionIdleAnim=Passenger4_idle,
				DriverIdleAnim=Passenger4_idle,
				AlternateIdleAnim=Passenger4_idle_AI,
				SeatProxyIndex=5,
				bIsExterior=true,
				PositionDeathAnims=(Passenger4_Death)
			)
		),
		TurretVarPrefix="PassengerFour",
		bSeatVisible=true,
		SeatRotation=(Pitch=0,Yaw=0,Roll=0),
		SeatBone=passenger_r_2,
		DriverDamageMult=1.0
	)}
	
	PassengerAnimTree=AnimTree'CHR_Playeranimtree_Master.CHR_Tanker_animtree'
	CrewAnimSet=AnimSet'WinterWar_VH_USSR_T-20.Anim.CHR_UC_Anim_Master'
	
	LeftWheels(0)="FacadeWheel_L1"
	LeftWheels(1)="FacadeWheel_L2"
	LeftWheels(2)="FacadeWheel_L3"
	LeftWheels(3)="FacadeWheel_L4"
	LeftWheels(4)="FacadeWheel_L5"
	LeftWheels(5)="FacadeWheel_L6"
	LeftWheels(6)="FacadeWheel_L7"
	LeftWheels(7)="FacadeWheel_L8"
	
	RightWheels(0)="FacadeWheel_R1"
	RightWheels(1)="FacadeWheel_R2"
	RightWheels(2)="FacadeWheel_R3"
	RightWheels(3)="FacadeWheel_R4"
	RightWheels(4)="FacadeWheel_R5"
	RightWheels(5)="FacadeWheel_R6"
	RightWheels(6)="FacadeWheel_R7"
	RightWheels(7)="FacadeWheel_R8"
	
	Begin Object Name=RRWheel
		BoneName="Wheel_T_R_4"
		BoneOffset=(X=-10.0,Y=0,Z=0.0)
		WheelRadius=20
	End Object
	
	Begin Object Name=RMWheel
		BoneName="Wheel_T_R_3"
		BoneOffset=(X=0.0,Y=0,Z=0.0)
		WheelRadius=20
	End Object
	
	Begin Object Name=RFWheel
		BoneName="Wheel_T_R_2"
		BoneOffset=(X=20.0,Y=0,Z=2.0)
		WheelRadius=20
	End Object
	
	Begin Object Name=LRWheel
		BoneName="Wheel_T_L_4"
		BoneOffset=(X=-10.0,Y=0,Z=0.0)
		WheelRadius=20
	End Object
	
	Begin Object Name=LMWheel
		BoneName="Wheel_T_L_3"
		BoneOffset=(X=0.0,Y=0,Z=0.0)
		WheelRadius=20
	End Object
	
	Begin Object Name=LFWheel
		BoneName="Wheel_T_L_2"
		BoneOffset=(X=20.0,Y=0,Z=2.0)
		WheelRadius=20
	End Object
	
	Begin Object Name=SimObject
		WheelSuspensionStiffness=325
		WheelSuspensionDamping=25.0
		GearArray(0)={(
			GearRatio=-5.64,
			AccelRate=10.25,
			TorqueCurve=(Points={(
				(InVal=0,OutVal=-2100),
				(InVal=300,OutVal=-700),
				(InVal=2800,OutVal=-2100),
				(InVal=3000,OutVal=-700),
				(InVal=3200,OutVal=-0.0)
				)}),
			TurningThrottle=1.0
			)}
		GearArray(1)={()}
		GearArray(2)={(
			// Real world - [4.22] ~10.0 kph
			GearRatio=4.22,
			AccelRate=7.60,
			TorqueCurve=(Points={(
				(InVal=0,OutVal=2500),
				(InVal=300,OutVal=1800),
				(InVal=2800,OutVal=4000),
				(InVal=3000,OutVal=1000),
				(InVal=3200,OutVal=0.0)
				)}),
			TurningThrottle=1.0
			)}
		GearArray(3)={(
			// Real world - [2.1] ~20.0 kph
			GearRatio=2.1,
			AccelRate=9.20,
			TorqueCurve=(Points={(
				(InVal=0,OutVal=3000),
				(InVal=2800,OutVal=6200),
				(InVal=3000,OutVal=2000),
				(InVal=3200,OutVal=0.0)
				)}),
			TurningThrottle=1.0
			)}
		GearArray(4)={(
			// Real world - [1.4] ~30.0 kph
			GearRatio=1.4,
			AccelRate=9.70,
			TorqueCurve=(Points={(
				(InVal=0,OutVal=3500),
				(InVal=2800,OutVal=9000),
				(InVal=3000,OutVal=3500),
				(InVal=3200,OutVal=0.0)
				)}),
			TurningThrottle=1.0
			)}
		/* GearArray(5)={(
			// Real world - [1.05] ~40.0 kph
			GearRatio=1.05,
			AccelRate=10.20,
			TorqueCurve=(Points={(
				(InVal=0,OutVal=5000),
				(InVal=2800,OutVal=11700),
				(InVal=3000,OutVal=6000),
				(InVal=3200,OutVal=0.0)
				)}),
			TurningThrottle=1.0
			)} */
		FirstForwardGear=2
	End Object
	
	EngineIdleRPM=100
	EngineNormalRPM=2000
	EngineMaxRPM=4000
	
	VehicleEffects(TankVFX_Firing1)=(EffectStartTag=UCHullMG,EffectTemplate=ParticleSystem'FX_VN_Weapons.MuzzleFlashes.FX_VN_MuzzleFlash_1stP_Rifles_split',EffectSocket=MG_Barrel)
	VehicleEffects(TankVFX_Exhaust)=(EffectStartTag=EngineStart,EffectEndTag=EngineStop,EffectTemplate=ParticleSystem'WinterWar_FX.ParticleSystems.FX_VEH_exhaust',EffectSocket=Exhaust)
	VehicleEffects(TankVFX_TreadWing)=(EffectStartTag=EngineStart,EffectEndTag=EngineStop,bStayActive=true,EffectTemplate=ParticleSystem'WinterWar_FX.ParticleSystems.FX_VEH_LightTank_A_Wing_Snow',EffectSocket=attachments_body_ground)
	
	FireEmitterSocketName=attachments_engine
	
	BigExplosionSocket=FX_Fire
	ExplosionTemplate=ParticleSystem'WinterWar_FX.ParticleSystems.FX_VEH_Tank_C_Explosion'
	
	EngineStartOffsetSecs=2.0
	EngineStopOffsetSecs=0.5
	
	EngineTextureOffset=(PositionOffset=(X=58,Y=83,Z=0),MySizeX=24,MYSizeY=24)
	
	TreadTextureOffsets(0)=(PositionOffset=(X=36,Y=34,Z=0),MySizeX=8,MYSizeY=70)
	TreadTextureOffsets(1)=(PositionOffset=(X=95,Y=34,Z=0),MySizeX=8,MYSizeY=70)
	
	SeatTextureOffsets(0)=(PositionOffSet=(X=-8,Y=-20,Z=0),bTurretPosition=0)
	SeatTextureOffsets(1)=(PositionOffSet=(X=8,Y=-20,Z=0),bTurretPosition=0)
	
	SeatTextureOffsets(2)=(PositionOffSet=(X=-11,Y=2,Z=0),bTurretPosition=0)
	SeatTextureOffsets(3)=(PositionOffSet=(X=11,Y=2,Z=0),bTurretPosition=0)
	SeatTextureOffsets(4)=(PositionOffSet=(X=-11,Y=24,Z=0),bTurretPosition=0)
	SeatTextureOffsets(5)=(PositionOffSet=(X=11,Y=24,Z=0),bTurretPosition=0)
	
	SpeedoMinDegree=5461
	SpeedoMaxDegree=56000
	SpeedoMaxSpeed=1365
	
	VehHitZones(0)=(ZoneName=ENGINE,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Engine)
	VehHitZones(1)=(ZoneName=Root_Track_R,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Track)
	VehHitZones(2)=(ZoneName=Root_Track_L,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Track)
	
	CrewHitZoneStart=3
	VehHitZones(3) =(ZoneName=PASS1BODY,DamageMultiplier=1.0,VehicleHitZoneType=VHT_CrewBody,CrewSeatIndex=2,SeatProxyIndex=2,CrewBoneName=passenger_l1_HITBOX)
	VehHitZones(4) =(ZoneName=PASS1HEAD,DamageMultiplier=1.0,VehicleHitZoneType=VHT_CrewHead,CrewSeatIndex=2,SeatProxyIndex=2,CrewBoneName=passenger_l1_head_HITBOX)
	VehHitZones(5) =(ZoneName=PASS2BODY,DamageMultiplier=1.0,VehicleHitZoneType=VHT_CrewBody,CrewSeatIndex=3,SeatProxyIndex=3,CrewBoneName=passenger_r1_HITBOX)
	VehHitZones(6) =(ZoneName=PASS2HEAD,DamageMultiplier=1.0,VehicleHitZoneType=VHT_CrewHead,CrewSeatIndex=3,SeatProxyIndex=3,CrewBoneName=passenger_r1_head_HITBOX)
	VehHitZones(7) =(ZoneName=PASS3BODY,DamageMultiplier=1.0,VehicleHitZoneType=VHT_CrewBody,CrewSeatIndex=4,SeatProxyIndex=4,CrewBoneName=passenger_l2_HITBOX)
	VehHitZones(8) =(ZoneName=PASS3HEAD,DamageMultiplier=1.0,VehicleHitZoneType=VHT_CrewHead,CrewSeatIndex=4,SeatProxyIndex=4,CrewBoneName=passenger_l2_head_HITBOX)
	VehHitZones(9) =(ZoneName=PASS4BODY,DamageMultiplier=1.0,VehicleHitZoneType=VHT_CrewBody,CrewSeatIndex=5,SeatProxyIndex=5,CrewBoneName=passenger_r2_HITBOX)
	VehHitZones(10)=(ZoneName=PASS4HEAD,DamageMultiplier=1.0,VehicleHitZoneType=VHT_CrewHead,CrewSeatIndex=5,SeatProxyIndex=5,CrewBoneName=passenger_r2_head_HITBOX)
	VehHitZones(11)=(ZoneName=DRIVERBODY,DamageMultiplier=1.0,VehicleHitZoneType=VHT_CrewBody,CrewSeatIndex=0,SeatProxyIndex=0)
	VehHitZones(12)=(ZoneName=DRIVERHEAD,DamageMultiplier=1.0,VehicleHitZoneType=VHT_CrewHead,CrewSeatIndex=0,SeatProxyIndex=0)
	VehHitZones(13)=(ZoneName=MGUNNERBODY,DamageMultiplier=1.0,VehicleHitZoneType=VHT_CrewBody,CrewSeatIndex=1,SeatProxyIndex=1)
	VehHitZones(14)=(ZoneName=MGUNNERHEAD,DamageMultiplier=1.0,VehicleHitZoneType=VHT_CrewHead,CrewSeatIndex=1,SeatProxyIndex=1)
	CrewHitZoneEnd=14

	// Non armour hit zones such as the crew, componants and tracks.
	VehHitZones(16)=(ZoneName=ENGINEBLOCK,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Engine,ZoneHealth=100,VisibleFrom=15)
	VehHitZones(17)=(ZoneName=ENGINECORE,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Engine,ZoneHealth=300,VisibleFrom=15)
	VehHitZones(18)=(ZoneName=GEARBOX,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Mechanicals,ZoneHealth=100,VisibleFrom=1)
	VehHitZones(19)=(ZoneName=GEARBOXCORE,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Mechanicals,ZoneHealth=200,VisibleFrom=1)
	VehHitZones(20)=(ZoneName=LEFTBRAKES,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Mechanicals,ZoneHealth=25,VisibleFrom=5)
	VehHitZones(21)=(ZoneName=RIGHTBRAKES,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Mechanicals,ZoneHealth=25,VisibleFrom=9)
	VehHitZones(22)=(ZoneName=LEFTTRACKONE,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Track,ZoneHealth=200,VisibleFrom=5)
	VehHitZones(23)=(ZoneName=LEFTTRACKTWO,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Track,ZoneHealth=200)
	VehHitZones(24)=(ZoneName=LEFTTRACKTHREE,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Track,ZoneHealth=200)
	VehHitZones(25)=(ZoneName=LEFTTRACKFOUR,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Track,ZoneHealth=200)
	VehHitZones(26)=(ZoneName=LEFTTRACKFIVE,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Track,ZoneHealth=200)
	VehHitZones(27)=(ZoneName=LEFTTRACKSIX,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Track,ZoneHealth=200)
	VehHitZones(28)=(ZoneName=LEFTTRACKSEVEN,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Track,ZoneHealth=200)
	VehHitZones(29)=(ZoneName=LEFTTRACKEIGHT,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Track,ZoneHealth=200)
	VehHitZones(30)=(ZoneName=LEFTTRACKNINE,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Track,ZoneHealth=200,VisibleFrom=9)
	VehHitZones(31)=(ZoneName=LEFTTRACKTEN,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Track,ZoneHealth=200)
	VehHitZones(32)=(ZoneName=LEFTTRACKELEVEN,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Track,ZoneHealth=200)
	VehHitZones(33)=(ZoneName=RIGHTTRACKONE,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Track,ZoneHealth=200)
	VehHitZones(34)=(ZoneName=RIGHTTRACKTWO,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Track,ZoneHealth=200)
	VehHitZones(35)=(ZoneName=RIGHTTRACKTHREE,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Track,ZoneHealth=200)
	VehHitZones(36)=(ZoneName=RIGHTTRACKFOUR,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Track,ZoneHealth=200)
	VehHitZones(37)=(ZoneName=RIGHTTRACKEIVE,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Track,ZoneHealth=200)
	VehHitZones(38)=(ZoneName=RIGHTDRIVESIX,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Track,ZoneHealth=200)
	VehHitZones(39)=(ZoneName=RIGHTTRACKSEVEN,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Track,ZoneHealth=200)
	VehHitZones(40)=(ZoneName=RIGHTTRACKEIGHT,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Track,ZoneHealth=200)
	VehHitZones(41)=(ZoneName=RIGHTTRACKNINE,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Track,ZoneHealth=200)
	VehHitZones(42)=(ZoneName=RIGHTTRACKTEN,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Track,ZoneHealth=200)
	VehHitZones(43)=(ZoneName=RIGHTTRACKELEVEN,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Track,ZoneHealth=200)
	VehHitZones(44)=(ZoneName=RIGHTFUELTANK,DamageMultiplier=10.0,VehicleHitZoneType=VHT_Fuel,ZoneHealth=200,KillPercentage=0.3,VisibleFrom=15)
	VehHitZones(45)=(ZoneName=LEFTFUELTANK,DamageMultiplier=5.0,VehicleHitZoneType=VHT_Fuel,ZoneHealth=200,KillPercentage=0.3,VisibleFrom=15)
	

	// All the armour hit zones organised into plate classes
	ArmorHitZones(0)=(ZoneName=COMPARTMENTREARONE,PhysBodyBoneName=Chassis,ArmorPlateName=COMPARTMENTREAR)
	ArmorHitZones(1)=(ZoneName=CHASSISFRONTLOWER,PhysBodyBoneName=Chassis,ArmorPlateName=FRONTGLACIS)
	ArmorHitZones(2)=(ZoneName=CHASSISFRONTUPPWER,PhysBodyBoneName=Chassis,ArmorPlateName=FRONTGLACIS)
	ArmorHitZones(3)=(ZoneName=HULLFRONTONE,PhysBodyBoneName=Chassis,ArmorPlateName=HULLFRONT)
	ArmorHitZones(4)=(ZoneName=HULLFRONTTWO,PhysBodyBoneName=Chassis,ArmorPlateName=HULLFRONT)
	ArmorHitZones(5)=(ZoneName=HULLFRONTTHREE,PhysBodyBoneName=Chassis,ArmorPlateName=HULLFRONT)
	ArmorHitZones(6)=(ZoneName=HULLFRONTFOUR,PhysBodyBoneName=Chassis,ArmorPlateName=HULLFRONT)
	ArmorHitZones(7)=(ZoneName=HULLFRONTFIVE,PhysBodyBoneName=Chassis,ArmorPlateName=HULLFRONT)
	ArmorHitZones(8)=(ZoneName=HULLFRONTSIX,PhysBodyBoneName=Chassis,ArmorPlateName=HULLFRONT)
	ArmorHitZones(9)=(ZoneName=MGFRONTPLATE,PhysBodyBoneName=Chassis,ArmorPlateName=MGPLATEFRONT)
	ArmorHitZones(10)=(ZoneName=MGTOPPLATE,PhysBodyBoneName=Chassis,ArmorPlateName=MGPLATE)
	ArmorHitZones(11)=(ZoneName=MGRIGHTPLATE,PhysBodyBoneName=Chassis,ArmorPlateName=MGPLATE)
	ArmorHitZones(12)=(ZoneName=MGLEFTPLATE,PhysBodyBoneName=Chassis,ArmorPlateName=MGPLATE)
	ArmorHitZones(13)=(ZoneName=HULLROOF,PhysBodyBoneName=Chassis,ArmorPlateName=ROOF)
	ArmorHitZones(14)=(ZoneName=CHASSISFLOOR,PhysBodyBoneName=Chassis,ArmorPlateName=FLOOR)
	ArmorHitZones(15)=(ZoneName=CHASSISREAR,PhysBodyBoneName=Chassis,ArmorPlateName=CHASSISREAR)
	ArmorHitZones(16)=(ZoneName=HULLREARONE,PhysBodyBoneName=Chassis,ArmorPlateName=HULLREAR)
	ArmorHitZones(17)=(ZoneName=HULLREARTWO,PhysBodyBoneName=Chassis,ArmorPlateName=HULLREAR)
	ArmorHitZones(18)=(ZoneName=HULLREARTHREE,PhysBodyBoneName=Chassis,ArmorPlateName=HULLREAR)
	ArmorHitZones(19)=(ZoneName=HULLRIGHTONE,PhysBodyBoneName=Chassis,ArmorPlateName=HULLRIGHT)
	ArmorHitZones(20)=(ZoneName=HULLRIGHTTWO,PhysBodyBoneName=Chassis,ArmorPlateName=HULLRIGHT)
	ArmorHitZones(21)=(ZoneName=HULLRIGHTTHREE,PhysBodyBoneName=Chassis,ArmorPlateName=HULLRIGHT)
	ArmorHitZones(22)=(ZoneName=HULLLEFTONE,PhysBodyBoneName=Chassis,ArmorPlateName=HULLLEFT)
	ArmorHitZones(23)=(ZoneName=HULLLEFTTWO,PhysBodyBoneName=Chassis,ArmorPlateName=HULLLEFT)
	ArmorHitZones(24)=(ZoneName=HULLLEFTTHREE,PhysBodyBoneName=Chassis,ArmorPlateName=HULLLEFT)
	ArmorHitZones(25)=(ZoneName=COMPARTMENTREARTWO,PhysBodyBoneName=Chassis,ArmorPlateName=COMPARTMENTREAR)
	ArmorHitZones(26)=(ZoneName=COMPARTMENTREARTHREE,PhysBodyBoneName=Chassis,ArmorPlateName=COMPARTMENTREAR)
	ArmorHitZones(27)=(ZoneName=TOPCOVER,PhysBodyBoneName=Chassis,ArmorPlateName=COVER)
	ArmorHitZones(28)=(ZoneName=RIGHTPLATEONE,PhysBodyBoneName=Chassis,ArmorPlateName=RIGHTPLATE)
	ArmorHitZones(29)=(ZoneName=RIGHTPLATETWO,PhysBodyBoneName=Turret,ArmorPlateName=RIGHTPLATE)
	ArmorHitZones(30)=(ZoneName=RIGHTPLATETHREE,PhysBodyBoneName=Turret,ArmorPlateName=RIGHTPLATE)
	ArmorHitZones(31)=(ZoneName=RIGHTPLATEFOUR,PhysBodyBoneName=Turret,ArmorPlateName=RIGHTPLATE)
	ArmorHitZones(32)=(ZoneName=LEFTPLATEONE,PhysBodyBoneName=Turret,ArmorPlateName=LEFTPLATE)
	ArmorHitZones(33)=(ZoneName=LEFTPLATETWO,PhysBodyBoneName=Turret,ArmorPlateName=LEFTPLATE)
	ArmorHitZones(34)=(ZoneName=LEFTPLATETHREE,PhysBodyBoneName=Turret,ArmorPlateName=LEFTPLATE)
	ArmorHitZones(35)=(ZoneName=LEFTPLATEFOUR,PhysBodyBoneName=Turret,ArmorPlateName=LEFTPLATE)

	// Actual data for the plates
	ArmorPlates(0)=(PlateName=COMPARTMENTREAR,ArmorZoneType=AZT_Front,PlateThickness=7,OverallHardness=450,bHighHardness=true)
	ArmorPlates(1)=(PlateName=FRONTGLACIS,ArmorZoneType=AZT_Front,PlateThickness=10,OverallHardness=450,bHighHardness=true)
	ArmorPlates(2)=(PlateName=HULLFRONT,ArmorZoneType=AZT_Left,PlateThickness=10,OverallHardness=450,bHighHardness=true)
	ArmorPlates(3)=(PlateName=MGPLATEFRONT,ArmorZoneType=AZT_Front,PlateThickness=12,OverallHardness=450,bHighHardness=true)
	ArmorPlates(4)=(PlateName=MGPLATE,ArmorZoneType=AZT_Floor,PlateThickness=10,OverallHardness=450,bHighHardness=true)
	ArmorPlates(5)=(PlateName=ROOF,ArmorZoneType=AZT_Back,PlateThickness=5,OverallHardness=450,bHighHardness=true)
	ArmorPlates(6)=(PlateName=FLOOR,ArmorZoneType=AZT_Roof,PlateThickness=5,OverallHardness=450,bHighHardness=true)
	ArmorPlates(7)=(PlateName=CHASSISREAR,ArmorZoneType=AZT_Roof,PlateThickness=7,OverallHardness=450,bHighHardness=true)
	ArmorPlates(8)=(PlateName=HULLREAR,ArmorZoneType=AZT_Roof,PlateThickness=7,OverallHardness=450,bHighHardness=true)
	ArmorPlates(9)=(PlateName=HULLRIGHT,ArmorZoneType=AZT_Left,PlateThickness=7,OverallHardness=450,bHighHardness=true)
	ArmorPlates(10)=(PlateName=HULLLEFT,ArmorZoneType=AZT_Right,PlateThickness=7,OverallHardness=450,bHighHardness=true)
	ArmorPlates(11)=(PlateName=COVER,ArmorZoneType=AZT_Back,PlateThickness=5,OverallHardness=330,bHighHardness=false)
	ArmorPlates(12)=(PlateName=RIGHTPLATE,ArmorZoneType=AZT_Left,PlateThickness=7,OverallHardness=450,bHighHardness=true)
	ArmorPlates(13)=(PlateName=LEFTPLATE,ArmorZoneType=AZT_Right,PlateThickness=7,OverallHardness=450,bHighHardness=true)

	
	GroundSpeed=655
	MaxSpeed=655	// 48 km/h
	
	RelatedDamageTypes(0)=class'WWDmgType_DT_T20'
}
