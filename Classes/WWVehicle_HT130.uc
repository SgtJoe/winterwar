
class WWVehicle_HT130 extends WWVehicle_T26
	abstract;

var Texture2D FlamethrowerAmmoTex;
var localized string FlamethrowerAmmoString;

simulated function VehicleLoadMainGun() {}

function OnMainCannonReloadComplete() {}

simulated function VehicleWeaponFired( bool bViaReplication, vector HitLocation, int SeatIndex )
{
	// Don't animate the barrel or add any impulse when firing
	
	super(WWVehicleTransport).VehicleWeaponFired(bViaReplication, HitLocation, SeatIndex);
}

DefaultProperties
{
	FlamethrowerAmmoTex=Texture2D'WinterWar_UI.Vehicle.UI_HUD_VH_FireAmmo'
	
	Seats(1)={(
		CameraOffset=-420,
		SeatAnimBlendName=CommanderPositionNode,
		GunClass=class'WWVehicleWeapon_HT130_Turret',
		RangeOverlayTexture=none,
		PeriscopeRangeTexture=none,
		VignetteOverlayTexture=Texture2D'WinterWar_UI.VehicleOptics.UI_HUD_VH_Optics_CircularOverlay',
		GunSocket=(Barrel,CoaxMG),
		GunPivotPoints=(gun_base,gun_base),
		TurretVarPrefix="Turret",
		TurretControls=(Turret_Gun,Turret_Main),
		SeatPositions=(
			(
				bDriverVisible=false,
				bAllowFocus=false,
				PositionCameraTag=Camera_Gunner,
				bViewFromCameraTag=true,
				ViewFOV=70.0,
				bCamRotationFollowSocket=true,
				bDrawOverlays=true,
				// bDrawOverlays=false,
				PositionUpAnim=MG_Hull_Idle,
				PositionDownAnim=MG_Hull_Idle,
				PositionIdleAnim=MG_Hull_Idle,
				DriverIdleAnim=MG_Hull_Idle,
				AlternateIdleAnim=MG_Hull_Idle,
				SeatProxyIndex=1,
				bIsExterior=false,
				PositionFlinchAnims=(MG_Hull_Idle),
				PositionDeathAnims=(MG_Hull_Idle)
			)
		),
		bSeatVisible=false,
		SeatBone=Turret,
		DriverDamageMult=1.0,
		InitialPositionIndex=0,
		FiringPositionIndex=0,
		TracerFrequency=5,
		WeaponTracerClass=(none,class'WWProjectile_DT_HT130_Tracer'),
		MuzzleFlashLightClass=(none,none),
	)}
	
	ExplosionDamageType=class'RODmgType_VehicleExplosion'
	ExplosionDamage=300.0
	ExplosionRadius=500.0
	ExplosionMomentum=80000
	ExplosionInAirAngVel=1.5
	InnerExplosionShakeRadius=800.0
	OuterExplosionShakeRadius=1400.0
	MaxExplosionLightDistance=6000.0
	TimeTilSecondaryVehicleExplosion=0.5f
	TurretExplosiveForce=25000
	
	BurnOutPhase1Timer=5.0
	BurnOutPhase2Timer=10.0
	BurnOutPhase3Timer=15.0
	
	RelatedDamageTypes(0)=class'WWDmgType_DT_HT130'
	RelatedDamageTypes(6)=class'WWDmgType_HT130Flamethrower'
}
