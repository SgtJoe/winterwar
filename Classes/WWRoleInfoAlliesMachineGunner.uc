
class WWRoleInfoAlliesMachineGunner extends WWRoleInfoAllies;

DefaultProperties
{
	RoleType=RORIT_MachineGunner
	ClassTier=2
	ClassIndex=`RI_MACHINE_GUNNER
	
	Items[RORIGM_Default]={(
		PrimaryWeapons=(class'WWWeapon_DP28'),
		
		SecondaryWeapons=(class'WWWeapon_NagantRevolver'),
		
		OtherItems=(class'WWWeapon_RGD33')
	)}
	
	bAllowPistolsInRealism=true
	
	ClassIcon=Texture2D'VN_UI_Textures.menu.class_icon_mg'
}
