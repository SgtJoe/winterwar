
class WWVoicePack_FIN_C extends ROAnnouncerPack;

DefaultProperties
{
	// CMD_RadioAbilityConfirm
	RadioComs[EROARC_Ability1Confirm]=(Sound=AkEvent'WW_AUD_VOX_FIN_C.Play_CMD_FIN_RadioAbilityConfirm')
	RadioComs[EROARC_Ability2Confirm]=(Sound=AkEvent'WW_AUD_VOX_FIN_C.Play_CMD_FIN_RadioAbilityConfirm')
	RadioComs[EROARC_Ability3Confirm]=(Sound=AkEvent'WW_AUD_VOX_FIN_C.Play_CMD_FIN_RadioAbilityConfirm')
	
	// CMD_RadioAbilityConfirm_CancelRequest
	RadioComs[EROARC_StrikeCancel]=(Sound=AkEvent'WW_AUD_VOX_FIN_C.Play_CMD_FIN_RadioAbilityConfirm_CancelRequest')
}
