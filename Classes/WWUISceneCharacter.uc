
class WWUISceneCharacter extends ROUISceneCharacter;

function InitPlayerConfig()
{
	local LocalPlayer Player;
	local bool bMainMenu;
	
	Player = GetPlayerOwner();
	if ( Player != none && Player.Actor != none )
	{
		ROPC = ROPlayerController(Player.Actor);
		if(ROPC != none)
		{
			bMainMenu = ROPC.WorldInfo.GRI.GameClass.static.GetGameType() == ROGT_Default;
			
			if( bMainMenu )
			{
				TeamIndexActual = ROPC.LastDisplayedTeam;
				ArmyIndexActual = 0;
			}
			else
			{
				TeamIndexActual = ROPC.GetTeamNum();
				ArmyIndexActual = 0;
			}
			
			ROPRI = ROPlayerReplicationInfo(ROPC.PlayerReplicationInfo);
		}
		
		if(ROPRI != none && ROPRI.RoleInfo != none)
		{
			ClassIndexActual = ROPRI.RoleInfo.ClassIndex;
			bPilotActual = ROPRI.RoleInfo.bIsPilot || ClassIndexActual == `RI_TANK_CREW;
			bCombatPilotActual = bPilotActual;
		}
		else
		{
			if( bMainMenu )
			{
				bPilotActual = ROPC.bLastDisplayedPilot;
				ClassIndexActual = ROPC.LastDisplayedClass;
			}
			else
			{
				bPilotActual = false;
				ClassIndexActual = `RI_NOGEAR;
			}
		}
		
		ROPC.StatsWrite.UpdateHonorLevel();
		HonorLevel = byte(ROPC.StatsWrite.HonorLevel);
	}
}

function PopulateArmyList()
{
	ROCharCustStringsDataStore.Empty('ROCharCustArmyType');
	ROCharCustStringsDataStore.AddStr('ROCharCustArmyType', class'WWMapInfo'.default.NorthernArmyShortNames[0]);
	ROCharCustStringsDataStore.AddStr('ROCharCustArmyType', class'WWMapInfo'.default.SouthernArmyShortNames[0]);
	
	FirstSouthIndex = 1;
	
	ArmyComboBox.ComboList.RefreshSubscriberValue();
	ArmyComboBox.ComboList.SetRowCount(2);
	ArmyComboBox.SetSelection(TeamIndexActual);
}

function PopulateCurrentPanel()
{
	local byte SelectedItemThisPanel;
	
	if( ActivePanel == none )
		return;
	
	switch( ActivePanel )
	{
		case TunicSelectionWidget:
			SelectedItemThisPanel = CurrentCharConfig.TunicMesh == 255 ? CurrentCharConfig.TunicMesh : PreviewConfig.TunicMesh;
			// 1.2.2 New Customization
			// if (ArmyComboBox.ComboList.GetCurrentItem() == `ALLIES_TEAM_INDEX && bPilot)
			// {
				// SecondaryPanel.SetVisibility(true);
				// UIImage(ActivePanel.FindChild('UIImage_CharacterTunicMatsBG', true)).SetVisibility(true);
				// SecondaryPanel = TunicMatSelectionWidget;
				// PopulateSecondaryPanel();
			// }
			// else
			// {
				SecondaryPanel.SetVisibility(false);
				UIImage(ActivePanel.FindChild('UIImage_CharacterTunicMatsBG', true)).SetVisibility(false);
				SecondaryPanel = none;
			// }
			break;
		
		case HeadSelectionWidget:
			SelectedItemThisPanel = CurrentCharConfig.HeadMesh == 255 ? CurrentCharConfig.HeadMesh : PreviewConfig.HeadMesh;
			SecondaryPanel.SetVisibility(false);
			UIImage(ActivePanel.FindChild('UIImage_CharacterHairColourBG', true)).SetVisibility(false);
			SecondaryPanel = none;
			break;
		
		case HeadgearSelectionWidget:
			SelectedItemThisPanel = CurrentCharConfig.HeadgearMesh == 255 ? CurrentCharConfig.HeadgearMesh : PreviewConfig.HeadgearMesh;
			SecondaryPanel = HeadgearMatSelectionWidget;
			PopulateSecondaryPanel();
			break;
		
		case FaceItemSelectionWidget:
			SelectedItemThisPanel = CurrentCharConfig.FaceItemMesh == 255 ? CurrentCharConfig.FaceItemMesh : PreviewConfig.FaceItemMesh;
			break;
		
		case FacialHairSelectionWidget:
			SelectedItemThisPanel = CurrentCharConfig.FacialHairMesh == 255 ? CurrentCharConfig.FacialHairMesh : PreviewConfig.FacialHairMesh;
			break;
	}
	
	if( bPilot )
	{
		ActivePanel.InitializeGrid(TeamIndex * FirstSouthIndex + ArmyIndex + 128, SelectedItemThisPanel, HonorLevel, ROPRI.OwnedDLCPacks);
	}
	else
	{
		ActivePanel.InitializeGrid(TeamIndex * FirstSouthIndex + ArmyIndex, SelectedItemThisPanel, HonorLevel, ROPRI.OwnedDLCPacks);
	}
}

function PopulateRoleList()
{
	local int i, ArrayLength;
	local string ClassName;
	
	ROCharCustStringsDataStore.Empty('ROCharCustRoleType');
	ClassListIndexArray.length = 0;
	
	for (i = 0; i <= `RI_TANK_CREW; i++)
	{
		ClassName = class'WWMapInfo'.static.GetClassNameByIndex(ArmyComboBox.ComboList.GetCurrentItem(), i);
		
		if (ClassName != "Error!")
		{
			ClassListIndexArray.AddItem(i);
			ROCharCustStringsDataStore.AddStr('ROCharCustRoleType', ClassName);
		}
	}
	
	ArrayLength = ClassListIndexArray.length;
	
	RoleComboBox.ComboList.RefreshSubscriberValue();
	RoleComboBox.ComboList.SetRowCount(ArrayLength);
	RoleComboBox.SetSelection(ClassListIndexArray.Find(ClassIndexActual));
}

function PopulateCopyRoleList()
{
	local int ArrayLength;
	// local int i;
	// local string ClassName;
	
	ROCharCustStringsDataStore.Empty('ROCharCustCopyRoleType');
	ROCharCustStringsDataStore.AddStr('ROCharCustCopyRoleType', AllRoleStrings[0]);
	CopyRoleListIndexArray.length = 1;
	CopyRoleListIndexArray[0] = -1;
	
	/* for (i = 0; i < `RI_TANK_CREW; i++)
	{
		ClassName = class'WWMapInfo'.static.GetClassNameByIndex(ArmyComboBox.ComboList.GetCurrentItem(), i);
		
		if (ClassName != "Error!")
		{
			CopyRoleListIndexArray.AddItem(i);
			ROCharCustStringsDataStore.AddStr('ROCharCustCopyRoleType', ClassName);
		}
	} */
	
	ArrayLength = CopyRoleListIndexArray.length;
	
	CopyRoleComboBox.ComboList.RefreshSubscriberValue();
	CopyRoleComboBox.ComboList.SetRowCount(ArrayLength);
	CopyRoleComboBox.SetSelection(0);
}

// BE VERY CAREFUL WITH THIS FUNCTION, IT CAN CAUSE INSTANT CTD UPON OPENING THE CHAR MENU
function RoleComboUpdated()
{
	local int ArmyIndexRaw;
	
	ClassIndex = ClassListIndexArray[Max(0,RoleComboBox.ComboList.GetCurrentItem())];
	
	ArmyIndexRaw = ArmyComboBox.ComboList.GetCurrentItem() - FirstSouthIndex * TeamIndex;
	
	bPilot = ClassIndex == `RI_TANK_CREW;
	
	ArmyIndex = ArmyIndexRaw;
	
	GetCurrentConfig();
	
	bUseBaseConfig = 0;
	UseBaselineCheckbox.SetValue(bool(bUseBaseConfig));
	
	bConfigChanged = false;
	ApplyButton.SetEnabled(false);
	CopyRoleButton.SetEnabled(true);
	
	if( bPostFirstRender && bInitialised )
	{
		PopulateCurrentPanel();
		UpdatePreviewMesh();
	}
	
	PopulateCopyRoleList();
}

function SetPawnHandler()
{
	PawnHandlerClass = class'WWPawnHandler';
	
	if (ROCCM != none)
	{
		TunicSelectionWidget.PawnHandlerClass = PawnHandlerClass;
		TunicMatSelectionWidget.PawnHandlerClass = PawnHandlerClass;
		ShirtSelectionWidget.PawnHandlerClass = PawnHandlerClass;
		HeadSelectionWidget.PawnHandlerClass = PawnHandlerClass;
		HairColourSelectionWidget.PawnHandlerClass = PawnHandlerClass;
		HeadgearSelectionWidget.PawnHandlerClass = PawnHandlerClass;
		HeadgearMatSelectionWidget.PawnHandlerClass = PawnHandlerClass;
		FaceItemSelectionWidget.PawnHandlerClass = PawnHandlerClass;
		FacialHairSelectionWidget.PawnHandlerClass = PawnHandlerClass;
		TattooSelectionWidget.PawnHandlerClass = PawnHandlerClass;
		
		TunicSelectionWidget.TeamSplitValue = 1;
		TunicMatSelectionWidget.TeamSplitValue = 1;
		ShirtSelectionWidget.TeamSplitValue = 1;
		HeadSelectionWidget.TeamSplitValue = 1;
		HairColourSelectionWidget.TeamSplitValue = 1;
		HeadgearSelectionWidget.TeamSplitValue = 1;
		HeadgearMatSelectionWidget.TeamSplitValue = 1;
		FaceItemSelectionWidget.TeamSplitValue = 1;
		FacialHairSelectionWidget.TeamSplitValue = 1;
		TattooSelectionWidget.TeamSplitValue = 1;
	}
}

event bool CloseScene(optional UIScene SceneToClose=self, optional bool bCloseChildScenes=true, optional bool bForceCloseImmediately)
{
	local CharacterConfig LastPreviewConfig;
	local bool bMainMenu;
	
	if (ROPC != none)
	{
		bMainMenu = ROPC.WorldInfo.GRI.GameClass.static.GetGameType() == ROGT_Default;
		
		if( bMainMenu && ROPC.ROCCC != none )
		{
			ROPC.ROCCC.SetTargetLocation(ROPC.Location);
			ROPC.ROCCC.SetTargetRotation(ROPC.Rotation);
			ROPC.ROCCC.bDisableOnComplete = true;
		}
		
		ROPC.CloseCharacterMenu();
	}
	
	ROPlayerReplicationInfo(ROPC.PlayerReplicationInfo).ClientSetCustomCharConfig();
	
	if (bMainMenu)
	{
		ROPC.LastDisplayedTeam = TeamIndex;
		ROPC.LastDisplayedArmy = ArmyIndex;
		ROPC.LastDisplayedClass = ClassIndex;
		ROPC.bLastDisplayedPilot = bPilot;
		ROPC.SaveConfig();
		
		LastPreviewConfig = PreviewConfig;
		
		GetCurrentConfig();
		
		if( CurrentCharConfig.TunicMesh == 255 )
			PreviewConfig.TunicMesh = LastPreviewConfig.TunicMesh;
		if( CurrentCharConfig.TunicMaterial == 255 )
			PreviewConfig.TunicMaterial = LastPreviewConfig.TunicMaterial;
		if( CurrentCharConfig.ShirtTexture == 255 )
			PreviewConfig.ShirtTexture = LastPreviewConfig.ShirtTexture;
		if( CurrentCharConfig.HeadMesh == 255 )
			PreviewConfig.HeadMesh = LastPreviewConfig.HeadMesh;
		if( CurrentCharConfig.HairMaterial == 255 )
			PreviewConfig.HairMaterial = LastPreviewConfig.HairMaterial;
		if( CurrentCharConfig.HeadgearMesh == 255 )
			PreviewConfig.HeadgearMesh = LastPreviewConfig.HeadgearMesh;
		if( CurrentCharConfig.FaceItemMesh == 255 )
			PreviewConfig.FaceItemMesh = LastPreviewConfig.FaceItemMesh;
		if( CurrentCharConfig.TattooTex == 255 )
			PreviewConfig.TattooTex = LastPreviewConfig.TattooTex;
		
		UpdatePreviewMesh(bMainMenu);
		ROCCM.PlayTransitionAnim();
		
		if( ROUISceneMainMenu(ROGameViewportClient(GetPlayerOwner().ViewportClient).MainMenuScene) != none && ROPC.bUseMenuMovieBG )
			ROUISceneMainMenu(ROGameViewportClient(GetPlayerOwner().ViewportClient).MainMenuScene).BackgroundMovie.Fade(0.0, 1.0, 0.5);
	}
	
	ROPC = none;
	ROCCM = none;
	ROPRI = none;
	
	return super(ROUIScene).CloseScene(SceneToClose, bCloseChildScenes, bForceCloseImmediately);
}
