
class WWWeapon_Skis extends ROItemPlaceable
	config(Game_WinterWar_Client)
	abstract;

var WWVehicle_Skis SpawnedSkis;

`include(WinterWar\Classes\WWWeaponPickupMessagesOverride.uci)

simulated function BeginFire(Byte FireModeNum)
{
	if (IsTimerActive('WeaponEquipped'))
	{
		return;
	}
	
	super.BeginFire(FireModeNum);
}

simulated function bool CanPhysicallyPlace(optional bool bIsInitialCheck = true)
{
	local Actor HitActor, HitActorSecondary;
	local vector HitNormal, HitLocation, HitLocationFloatingPoint, TraceEnd, TraceStart, TraceExtent;
	local vector HitNormalSecondary, HitLocationSecondary, TraceStartSecondary, TraceEndSecondary;
	local vector ViewDirection, ViewDirectionFlattened;
	local float VerticalAngle, LimitedVerticalAngle, TempFloat, TraceLength, SurfaceNormalDegrees;
	local rotator TempRot;
	local TraceHitInfo HitInfo, HitInfoSecondary;
	local ROPlayerController ROPC;
	local ROPhysicalMaterialProperty PhysicalProperty;
	local byte AdditionalFailType;
	
	ROPC = ROPlayerController(Instigator.Controller);
	bLastCheckWasHardFail = false;
	bLastTraceWasDirect = true;
	bLastTraceWasExtent = true;
	
	if ( !CanPlace(bIsInitialCheck) )
	{
		bLastCheckWasHardFail = true;
		return false;
	}
	
	TraceStart = Instigator.GetPawnViewLocation();
	ViewDirection = Vector(Instigator.GetViewRotation());
	VerticalAngle = acos(-ViewDirection.Z);
	
	PlaceRot = Instigator.GetViewRotation();
	PlaceRot.Pitch = 0.0f;
	PlaceRot.Roll = 0.0f;
	
	ViewDirectionFlattened = Vector(PlaceRot);
	
	if ( !bUsesAltTraceMethod )
	{
		if( Instigator.bIsCrouched )
		{
			TraceLength = PlaceCrouchDist;
			LimitedVerticalAngle = FMin(VerticalAngle, MaxAngleForViewDirForCrouching * DegToRad);
		}
		else
		{
			TraceLength = PlaceStandingDist;
			LimitedVerticalAngle = FMin(VerticalAngle, MaxAngleForViewDirForStanding * DegToRad);
		}
		
		if (LimitedVerticalAngle < VerticalAngle)
		{
			ViewDirection.Z = -cos(LimitedVerticalAngle);
			TempFloat = sqrt((1 - ViewDirection.Z * ViewDirection.Z) / (ViewDirection.X * ViewDirection.X + ViewDirection.Y * ViewDirection.Y));
			ViewDirection.X *= TempFloat;
			ViewDirection.Y *= TempFloat;
			bLastTraceWasDirect = false;
		}
		
		TraceEnd = TraceStart + ViewDirection * TraceLength;
		HitActor = Trace(HitLocation, HitNormal, TraceEnd, TraceStart, false,, HitInfo, TRACEFLAG_PhysicsVolumes );
		
		if( HitActor != None && WaterVolume(HitActor) != None )
		{
			bLastCheckWasHardFail = true;
			return false;
		}
		
		HitActor = Trace(HitLocation, HitNormal, TraceEnd, TraceStart, false,, HitInfo, TRACEFLAG_Bullet);
		
		if( HitActor == none )
		{
			TraceStart = TraceEnd;
			TraceEnd = TraceEnd - (vect(0,0,1)*DownTraceDist);
			HitActor = Trace(HitLocation, HitNormal, TraceEnd, TraceStart, false,, HitInfo, TRACEFLAG_Bullet);
		}
	}
	else
	{
		TraceStart = Instigator.GetPawnViewLocation();
		TraceEnd = Instigator.GetPawnViewLocation() + (ViewDirectionFlattened*TraceForwardOffset);
		HitActor = Trace(HitLocation, HitNormal, TraceEnd, TraceStart, false,, HitInfo, TRACEFLAG_PhysicsVolumes );
		
		if( WaterVolume(HitActor) != None )
		{
			bLastCheckWasHardFail = true;
			return false;
		}
		
		HitActor = Trace(HitLocation, HitNormal, TraceEnd, TraceStart, false,, HitInfo, TRACEFLAG_Bullet);
		
		if ( HitActor != none )
		{
			bLastCheckWasHardFail = true;
			return false;
		}
		
		TraceStart = Instigator.GetPawnViewLocation() + (ViewDirectionFlattened*TraceForwardOffset);
		TraceEnd = TraceStart + (vect(0,0,-1)*TraceDownOffset);
		HitActor = Trace(HitLocation, HitNormal, TraceEnd, TraceStart, false,, HitInfo, TRACEFLAG_PhysicsVolumes );
		
		if( WaterVolume(HitActor) != None )
		{
			bLastCheckWasHardFail = true;
			return false;
		}
		
		TraceExtent = vect(1,1,0) * TraceExtentSize;
		TraceExtent.Z = 0.1;
		HitActor = Trace(HitLocation, HitNormal, TraceEnd, TraceStart, false, TraceExtent, HitInfo);
		
		if ( HitActor == none )
		{
			bLastTraceWasExtent = false;
			HitActor = Trace(HitLocation, HitNormal, TraceEnd, TraceStart, false,, HitInfo);
		}
		
		if ( VSizeSq(HitLocation-TraceStart) <= Square(MinDistFromTraceLocation) )
		{
			bLastCheckWasHardFail = true;
			return false;
		}
	}
	
	if( HitActor != none && HitActor.bWorldGeometry )
	{
		HitLocationFloatingPoint = TraceLocFloatingPointFix(HitActor, TraceStart, TraceEnd, HitLocation);
		SetPlaceLocWithOffsets(HitLocationFloatingPoint);
		SurfaceNormalDegrees = (RadToDeg * acos(vect(0,0,1) dot HitNormal));
		
		if ( WaterVolume(HitActor) != None )
		{
			bLastCheckWasHardFail = true;
			return false;
		}
		else if( SurfaceNormalDegrees > MaxPlacingSurfaceDegrees  )
		{
			return false;
		}
		
		if ( HitInfo.PhysMaterial != None )
		{
			PhysicalProperty = ROPhysicalMaterialProperty(HitInfo.PhysMaterial.GetPhysicalMaterialProperty(class'ROPhysicalMaterialProperty'));
			CurMatType = (PhysicalProperty != None) ? PhysicalProperty.MaterialType : EMT_Default;
		}
		else
		{
			CurMatType = EMT_Default;
		}
		
		TraceStartSecondary = HitLocationFloatingPoint + (vect(0,0,1)*2.0f);
		TraceEndSecondary = TraceStartSecondary + (vect(0,0,1)*GetHeightOffset());
		
		if ( !bUsesAltTraceMethod )
		{
			HitActorSecondary = Trace(HitLocationSecondary, HitNormalSecondary, TraceEndSecondary, TraceStartSecondary, true,, HitInfoSecondary);
			
			if( HitActorSecondary != none )
			{
				return false;
			}
		}
		
		TraceStart = TraceEndSecondary;
		TempRot.Yaw = Instigator.GetViewRotation().Yaw;
		TraceEndSecondary = TraceEndSecondary + (Vector(TempRot)*ForwardPlaceOffset);
		HitActorSecondary = Trace(HitLocationSecondary, HitNormalSecondary, TraceEndSecondary, TraceStart, true,, HitInfoSecondary, TRACEFLAG_Bullet);
		
		if( HitActorSecondary != none )
		{
			return false;
		}
		
		if ( !AdditionalCanPhysicallyPlaceTraces(HitLocation, AdditionalFailType) )
		{
			if ( AdditionalFailType == 2 )
			{
				bLastCheckWasHardFail = true;
			}
			return false;
		}
		
		if ( ROPC != none )
		{
			ROPC.ReceiveLocalizedMessage(class'WWLocalMessagePlaceSkis', , , , ROPC);
		}
		
		return true;
	}
	
	bLastCheckWasHardFail = true;
	return false;
}

simulated function PlayPlacingItem()
{
	local float TotalAnimDuration, PlacingTime;
	
	HidePreviewMesh();
	
	if ( Mesh != none && SkeletalMeshComponent(Mesh) != none )
	{
		TotalAnimDuration = SkeletalMeshComponent(Mesh).GetAnimLength(WeaponPlaceAnim)/*  * 0.98f */;
	}
	
	PlacingTime = (PlacingLength);
	
	if ( Instigator.IsLocallyControlled() )
	{
		if ( WeaponPlaceAnim != '' )
		{
			PlayAnimation(WeaponPlaceAnim, TotalAnimDuration, true);
		}
	}
	
	if ( WeaponPlaceAnimCameraAnim != none && ROPlayerController(Instigator.Controller) != none )
	{
		ROPlayerController(Instigator.Controller).PlayCameraAnim(WeaponPlaceAnimCameraAnim,,1,0.1,0.1,false, true );
	}
	
	SetTimer(PlacingTime, false, 'SpawnPlaceable');
	SetTimer(TotalAnimDuration, false, 'GotoActiveState');
}

simulated function AlertPlacingTime(float PlacingTime){}

simulated function SpawnPlaceable()
{
	HidePreviewMesh();
	
	if ( Role == ROLE_Authority )
	{
		if ( bRedoPlacementCheckBeforeSpawn || bPlantedWhileMovingFast )
		{
			if ( !CanPhysicallyPlace(false) )
			{
				if ( Instigator != none && PlayerController(Instigator.Controller) != none )
				{
					PlayerController(Instigator.Controller).ClientMessage("Failed To Spawn Placeable In Location");
				}
				
				CancelWeaponAction(true, true);
				GotoActiveState();
				return;
			}
		}
		
		if ( DoActualSpawn() )
		{
			ForceEndFire();
			
			// Micro delay to avoid online desync
			SetTimer(0.08, false, 'PostPlacement');
		}
		else
		{
			if ( Instigator != none && PlayerController(Instigator.Controller) != none )
			{
				PlayerController(Instigator.Controller).ClientMessage("Failed To Spawn Placeable");
			}
			GotoActiveState();
		}
	}
}

function bool DoActualSpawn()
{
	SpawnedSkis = Spawn(class'WWVehicle_Skis_ActualContent',,, PlaceLoc, PlaceRot,,true);
	
	if (SpawnedSkis != none)
	{
		return true;
	}
	
	return false;
}

function PostPlacement()
{
	HidePreviewMesh(); // Just in case
	
	if (ROPawn(Instigator) != none)
	{
		SpawnedSkis.TryToDrive(Instigator);
	}
}

DefaultProperties
{
	WeaponContentClass(0)="WinterWar.WWWeapon_Skis_ActualContent"
	
	RoleSelectionImage(0)=Texture2D'WinterWar_UI.WP_Render.WP_Render_Skis'
	
	bCanThrow=false
	DroppedPickupMesh=None
	PickupFactoryMesh=None
	
	AmmoClass=class'ROAmmo_Placeable_HMG'
	
	TeamIndex=`AXIS_TEAM_INDEX
	
	InvIndex=`WI_SKIS
	
	ROTM_PlacingMessage=ROTMSG_PlaceHMG
	
	EquipTime=+2.0
	PutDownTime=+1.8
	
	Weight=0
	
	bExemptFromCategoryLimits=true
	bExemptFromWeightLimits=true
	
	PlacingLength=0.5
	
	bPlacingUsesSingleAnim=true
	bPlacingLengthBasedOnAnims=false
	
	WeaponIdleAnims(DEFAULT_FIREMODE)=PickMattock_idle
	
	WeaponPutDownAnim=PickMattock_Putaway
	WeaponEquipAnim=PickMattock_Pullout
	WeaponDownAnim=PickMattock_Down
	WeaponUpAnim=PickMattock_Up
	
	WeaponCrawlingAnims(0)=PickMattock_CrawlF
	WeaponCrawlStartAnim=PickMattock_Crawl_into
	WeaponCrawlEndAnim=PickMattock_Crawl_out
	
	WeaponSprintStartAnim=PickMattock_sprint_into
	WeaponSprintLoopAnim=PickMattock_Sprint
	WeaponSprintEndAnim=PickMattock_sprint_out
	
	WeaponMantleOverAnim=PickMattock_Mantle
	
	WeaponPlaceAnim=PickMattock_Putaway
	
	WeaponPlaceAnimCameraAnim=CameraAnim'1stperson_Cameras.Anim.TunnelTool_Dig'
}
