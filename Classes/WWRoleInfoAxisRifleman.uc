
class WWRoleInfoAxisRifleman extends WWRoleInfoAxis;

DefaultProperties
{
	RoleType=RORIT_Rifleman
	ClassTier=1
	ClassIndex=`RI_RIFLEMAN
	
	Items[RORIGM_Default]={(
		PrimaryWeapons=(class'WWWeapon_MN91',class'WWWeapon_MN27'),
		
		OtherItems=(class'WWWeapon_Molotov')
	)}
	
	ClassIcon=Texture2D'WinterWar_UI.RoleIcons.RoleIcon_Rifleman'
}
