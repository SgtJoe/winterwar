
class WWVehicleWeapon_T28_MG_Right extends WWVehicleWeapon_T28_MG
	abstract;

function BeginReload()
{
	local WWVehicle_T28 T;
	T = WWVehicle_T28(MyVehicle);
	
	TimeReloading();
	
	if (T != none)
	{
		if (T.RightMGReloadCount < 255)
		{
			T.RightMGReloadCount++;
		}
		else
		{
			T.RightMGReloadCount = 0;
		}
	}
}

defaultproperties
{
	WeaponContentClass(0)="WinterWar.WWVehicleWeapon_T28_MG_Right_ActualContent"
	
	SeatIndex=`T28_MG_H_R
	
	FireTriggerTags=(T28_RightMG)
}
