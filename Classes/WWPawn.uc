
class WWPawn extends ROPawn
	abstract;

var StaticMeshComponent BadgeComponent;
var StaticMesh BadgeMesh;

var name BadgeAttachSocket;

var CharacterConfig DefaultConfigs[2];

var Controller			ShotOffSkisInstigator;
var vector				ShotOffSkisMomentum;
var class<DamageType>	ShotOffSkisDamageType;
var Actor				ShotOffSkisDamageCauser;

var byte TunicID; // Moved up here for easy access by WWVehicle_Skis

simulated event PreBeginPlay()
{
	PawnHandlerClass = class'WWPawnHandler';
	
	super.PreBeginPlay();
}

simulated function SetPawnElementsByConfig(bool bViaReplication, optional ROPlayerReplicationInfo OverrideROPRI)
{
	local int TeamNum, ArmyIndex, ClassIndex, HonorLevel, isSL;
	local ROPlayerReplicationInfo ROPRI;
	local byte IsHelmet, /* TunicID, */ TunicMatID, ShirtID, HeadID, HairID, HeadgearID, HeadgearMatID, SkinID, FaceItemID, FacialHairID, TattooID, bPilot, bNoHeadgear, byteDisposal;
	
	PawnHandlerClass = class'WWPawnHandler';
	
	TeamNum = GetTeamNum();
	
	if( OverrideROPRI != none )
		ROPRI = OverrideROPRI;
	else
		ROPRI = ROPlayerReplicationInfo(PlayerReplicationInfo);
	
	ArmyIndex = 0;
	isSL = 0;
	
	if( ROPRI != none )
	{
		if( ROPRI.RoleInfo != none )
		{
			ClassIndex = ROPRI.RoleInfo.ClassIndex;
		}
		
		if (ROPRI.bIsSquadLeader)
		{
			isSL = 1;
			// bSetFinalMesh = false;
		}
		
		if( !ROPRI.bBot )
		{
			if( bViaReplication )
			{
				HonorLevel = int(ROPRI.HonorLevel);
			}
			else if( ROPlayerController(Controller) != none )
			{
				ROPlayerController(Controller).StatsWrite.UpdateHonorLevel();
				HonorLevel = ROPlayerController(Controller).StatsWrite.HonorLevel;
			}
		}
	}
	
	if (TeamNum == 255)
	{
		TeamNum = ScriptGetTeamNum();
	}
	
	bPilot = (ClassIndex == `RI_TANK_CREW) ? 1 : 0;
	
	if( !bViaReplication && IsHumanControlled() )
	{
		PawnHandlerClass.static.GetCharConfig(TeamNum, ArmyIndex, bPilot, ClassIndex, HonorLevel, TunicID, TunicMatID, ShirtID, HeadID, HairID, HeadgearID, HeadgearMatID, FaceItemID, FacialHairID, TattooID, ROPRI);
		
		ROPRI.bUsesAltVoicePacks = false;
	}
	else if( ROPRI != none )
	{
		TunicID = ROPRI.CurrentCharConfig.TunicMesh;
		TunicMatID = ROPRI.CurrentCharConfig.TunicMaterial;
		ShirtID = ROPRI.CurrentCharConfig.ShirtTexture;
		HeadID = ROPRI.CurrentCharConfig.HeadMesh;
		HairID = 0;
		HeadgearID = ROPRI.CurrentCharConfig.HeadgearMesh;
		HeadgearMatID = ROPRI.CurrentCharConfig.HeadgearMaterial;
		FaceItemID = ROPRI.CurrentCharConfig.FaceItemMesh;
		FacialHairID = ROPRI.CurrentCharConfig.FacialHairMesh;
		TattooID = 0;
		
		PawnHandlerClass.static.ValidateCharConfig(TeamNum, ArmyIndex, bPilot, HonorLevel, TunicID, TunicMatID, ShirtID, HeadID, HairID, HeadgearID, HeadgearMatID, FaceItemID, FacialHairID, TattooID, ROPRI);
	}
	else
	{
		TunicID = 0;
		TunicMatID = 0;
		ShirtID = 0;
		HeadID = 0;
		HairID = 0;
		HeadgearID = 0;
		HeadgearMatID = 0;
		FaceItemID = 0;
		FacialHairID = 0;
		TattooID = 0;
	}
	
	if (TunicID == 0 && TunicMatID == 0 && HeadID == 0 && HeadgearID == 0 && FaceItemID == 0 && FacialHairID == 0)
	{
		// Client hasn't saved their config yet, set it to default random values to avoid a bunch of 0-everything clones
		TunicID = DefaultConfigs[TeamNum].TunicMesh;
		TunicMatID = DefaultConfigs[TeamNum].TunicMaterial;
		HeadID = DefaultConfigs[TeamNum].HeadMesh;
		HeadgearID = DefaultConfigs[TeamNum].HeadgearMesh;
		FaceItemID = DefaultConfigs[TeamNum].FaceItemMesh;
		FacialHairID = DefaultConfigs[TeamNum].FacialHairMesh;
		
		// Need to call this function to convert 255 to an actual item ID
		PawnHandlerClass.static.ValidateCharConfig(TeamNum, ArmyIndex, bPilot, HonorLevel, TunicID, TunicMatID, ShirtID, HeadID, HairID, HeadgearID, HeadgearMatID, FaceItemID, FacialHairID, TattooID, ROPRI);
	}
	
	TunicMesh = PawnHandlerClass.static.GetTunicMeshes(TeamNum, ArmyIndex, bPilot, TunicID, bNoHeadgear);
	BodyMICTemplate = PawnHandlerClass.static.GetBodyMIC(TeamNum, ArmyIndex, bPilot, TunicID, TunicMatID);
	
	PawnMesh_SV = PawnHandlerClass.static.GetTunicMeshSV(TeamNum, ArmyIndex, ClassIndex, bPilot, bHasFlamethrower, BodyMICTemplate_SV);
	
	FieldgearMesh = PawnHandlerClass.static.GetFieldgearMesh(TeamNum, isSL, TunicID, ClassIndex, TunicMatID);
	
	HeadAndArmsMesh = PawnHandlerClass.static.GetHeadAndArmsMesh(TeamNum, ArmyIndex, bPilot, HeadID, SkinID);
	
	if( bUseSingleCharacterVariant )
		HeadAndArmsMICTemplate = PawnHandlerClass.static.GetHeadMIC(TeamNum, ArmyIndex, HeadID, 1, bPilot);
	else
		HeadAndArmsMICTemplate = PawnHandlerClass.static.GetHeadMIC(TeamNum, ArmyIndex, HeadID, TunicID, bPilot);
	
	if( !bUseSingleCharacterVariant )
	{
		UberGoreMesh = PawnHandlerClass.static.GetGoreMeshes(TeamNum, ArmyIndex, TunicID, SkinID, GoreMIC, Gore_LeftHand.GibClass, Gore_RightHand.GibClass, Gore_LeftLeg.GibClass, Gore_RightLeg.GibClass);
	}
	
	ArmsOnlyMeshFP = PawnHandlerClass.static.GetFPArmsMesh(TeamNum, ArmyIndex, bPilot, TunicID, TunicMatID, SkinID, FPArmsSkinMaterialTemplate, FPArmsSleeveMaterialTemplate);
	
	if( bNoHeadgear == 0 )
		HeadgearMesh = PawnHandlerClass.static.GetHeadgearMesh(TeamNum, ArmyIndex, bPilot, HeadID, HairID, HeadgearID, HeadgearMatID, HeadgearMICTemplate, HairMICTemplate, HeadgearAttachSocket, IsHelmet);
	else
		HeadgearMesh = none;
	
	if( IsHelmet != 0 )
		bHeadGearIsHelmet = true;
	else
		bHeadGearIsHelmet = false;
	
	FaceItemMesh = PawnHandlerClass.static.GetFaceItemMesh(TeamNum, ArmyIndex, bPilot, HeadgearID, FaceItemID, FaceItemAttachSocket, byteDisposal);
	FacialHairMesh = PawnHandlerClass.static.GetFacialHairMesh(TeamNum, ArmyIndex, FacialHairID, FacialHairAttachSocket);
	
	if (TeamNum == `AXIS_TEAM_INDEX && TunicID == 4)
	{
		FaceItemMesh = none;
		FacialHairMesh = none;
	}
	
	BadgeMesh = class'WWPawnHandler'.static.GetBadgeMesh(TeamNum, TunicID, ClassIndex, isSL);
	
	if( Role == ROLE_Authority && ROPRI != none )
	{
		ROPRI.VoicePackIndex = rand(3);
	}
}

simulated function SetPawnElementsForPosedPawn(int TeamNum, int ClassIndex, int HonorLevel)
{
	local int ArmyIndex, isSL;
	local byte IsHelmet, /* TunicID, */ TunicMatID, ShirtID, HeadID, HairID, HeadgearID, HeadgearMatID, SkinID, FaceItemID, FacialHairID, TattooID, bPilot, bNoHeadgear, byteDisposal;
	local ROPlayerReplicationInfo FakePRI;
	
	PawnHandlerClass = class'WWPawnHandler';
	
	// ValidateCharConfig needs a PRI, but all it checks for is DLC packs, which we obviously don't have
	FakePRI = Spawn(class'ROPlayerReplicationInfo', self, , vect(0,0,0), rot(0,0,0));
	
	ArmyIndex = 0;
	
	if (ClassIndex == 8 /*SQUAD_LEADER*/)
	{
		isSL = 1;
	}
	
	bPilot = (ClassIndex == `RI_TANK_CREW) ? 1 : 0;
	
	TunicID = 255;
	TunicMatID = 255;
	ShirtID = 0;
	HeadID = 255;
	HairID = 0;
	HeadgearID = 255;
	FaceItemID = 255;
	FacialHairID = 0;
	TattooID = 0;
	
	// Need to call this function to convert 255 to an actual item ID
	PawnHandlerClass.static.ValidateCharConfig(TeamNum, ArmyIndex, bPilot, HonorLevel, TunicID, TunicMatID, ShirtID, HeadID, HairID, HeadgearID, HeadgearMatID, FaceItemID, FacialHairID, TattooID, FakePRI);
	
	// We don't need this anymore
	FakePRI.Destroy();
	
	TunicMesh = PawnHandlerClass.static.GetTunicMeshes(TeamNum, ArmyIndex, bPilot, TunicID, bNoHeadgear);
	BodyMICTemplate = PawnHandlerClass.static.GetBodyMIC(TeamNum, ArmyIndex, bPilot, TunicID, TunicMatID);
	
	FieldgearMesh = PawnHandlerClass.static.GetFieldgearMesh(TeamNum, isSL, TunicID, ClassIndex, TunicMatID);
	
	HeadAndArmsMesh = PawnHandlerClass.static.GetHeadAndArmsMesh(TeamNum, ArmyIndex, bPilot, HeadID, SkinID);
	
	HeadAndArmsMICTemplate = PawnHandlerClass.static.GetHeadMIC(TeamNum, ArmyIndex, HeadID, TunicID, bPilot);
	
	if( bNoHeadgear == 0 )
		HeadgearMesh = PawnHandlerClass.static.GetHeadgearMesh(TeamNum, ArmyIndex, bPilot, HeadID, HairID, HeadgearID, HeadgearMatID, HeadgearMICTemplate, HairMICTemplate, HeadgearAttachSocket, IsHelmet);
	else
		HeadgearMesh = none;
	
	FaceItemMesh = PawnHandlerClass.static.GetFaceItemMesh(TeamNum, ArmyIndex, bPilot, HeadgearID, FaceItemID, FaceItemAttachSocket, byteDisposal);
	
	FacialHairMesh = none;
	
	if (TeamNum == `AXIS_TEAM_INDEX && TunicID == 4)
	{
		FaceItemMesh = none;
	}
	
	BadgeMesh = class'WWPawnHandler'.static.GetBadgeMesh(TeamNum, TunicID, ClassIndex, isSL);
}

simulated function CreatePawnMesh()
{
	local ROMapInfo ROMI;
	
	if( Health <= 0 )
		return;
	
	if( HeadAndArmsMIC == none )
		HeadAndArmsMIC = new class'MaterialInstanceConstant';
	if( BodyMIC == none )
		BodyMIC = new class'MaterialInstanceConstant';
	if( HeadgearMIC == none )
		HeadgearMIC = new class'MaterialInstanceConstant';
	if( HairMIC == none && HairMICTemplate != none )
		HairMIC = new class'MaterialInstanceConstant';
	if( FPArmsSleeveMaterial == none && FPArmsSleeveMaterialTemplate != none )
		FPArmsSleeveMaterial = new class'MaterialInstanceConstant';
	
	if( bUseSingleCharacterVariant && BodyMICTemplate_SV != none )
		BodyMIC.SetParent(BodyMICTemplate_SV);
	else
		BodyMIC.SetParent(BodyMICTemplate);
	
	HeadAndArmsMIC.SetParent(HeadAndArmsMICTemplate);
	HeadgearMIC.SetParent(HeadgearMICTemplate);
	
	if( FPArmsSleeveMaterial != none )
		FPArmsSleeveMaterial.SetParent(FPArmsSleeveMaterialTemplate);
	
	if( HairMIC != none )
		HairMIC.SetParent(HairMICTemplate);
	
	MeshMICs.Length = 0;
	MeshMICs.AddItem(BodyMIC);
	MeshMICs.AddItem(HeadAndArmsMIC);
	MeshMICs.AddItem(HeadgearMIC);
	
	if( HairMIC != none )
		MeshMICs.AddItem(HairMIC);
	
	if( ThirdPersonHeadgearMeshComponent.AttachedToSkelComponent != none )
		mesh.DetachComponent(ThirdPersonHeadgearMeshComponent);
	if( FaceItemMeshComponent.AttachedToSkelComponent != none )
		mesh.DetachComponent(FaceItemMeshComponent);
	if( FacialHairMeshComponent.AttachedToSkelComponent != none )
		mesh.DetachComponent(FacialHairMeshComponent);
	if( ThirdPersonHeadAndArmsMeshComponent.AttachedToSkelComponent != none )
		DetachComponent(ThirdPersonHeadAndArmsMeshComponent);
	if( TrapDisarmToolMeshTP.AttachedToSkelComponent != none )
		mesh.DetachComponent(TrapDisarmToolMeshTP);
	if( BadgeComponent.bAttached )
		mesh.DetachComponent(BadgeComponent);
	
	ROMI = ROMapInfo(WorldInfo.GetMapInfo());
	
	if (!bUseSingleCharacterVariant && ROMI != none && FieldgearMesh != none)
	{
		CompositedBodyMesh = ROMI.GetCachedCompositedPawnMesh(TunicMesh, FieldgearMesh);
	}
	else
	{
		CompositedBodyMesh = PawnMesh_SV;
	}
	
	CompositedBodyMesh.Characterization = PlayerHIKCharacterization;
	
	ROSkeletalMeshComponent(mesh).ReplaceSkeletalMesh(CompositedBodyMesh);
	
	mesh.SetMaterial(0, BodyMIC);
	
	/* 1.2.2 New Customization
	
	// Be careful here. Soviets need to have their BodyMic applied to their gear mesh
	// for proper wrappings/boot textures but it also seems to run on Finns sometimes.
	// Hopefully this will mitigate that.
	
//	if (GetTeamNum() == `ALLIES_TEAM_INDEX)
	if (self.IsA('WWPawnAllies'))
	{
		mesh.SetMaterial(1, BodyMIC);
	}
	*/
	
	ROSkeletalMeshComponent(mesh).GenerateAnimationOverrideBones(HeadAndArmsMesh);
	
	ThirdPersonHeadAndArmsMeshComponent.SetSkeletalMesh(HeadAndArmsMesh);
	ThirdPersonHeadAndArmsMeshComponent.SetMaterial(0, HeadAndArmsMIC);
	ThirdPersonHeadAndArmsMeshComponent.SetParentAnimComponent(mesh);
	ThirdPersonHeadAndArmsMeshComponent.SetShadowParent(mesh);
	ThirdPersonHeadAndArmsMeshComponent.SetLODParent(mesh);
	
	AttachComponent(ThirdPersonHeadAndArmsMeshComponent);
	
	if( HeadgearMesh != none )
	{
		AttachNewHeadgear(HeadgearMesh);
	}
	
	if( FaceItemMesh != none )
	{
		AttachNewFaceItem(FaceItemMesh);
	}
	
	if( FacialHairMesh != none )
	{
		AttachNewFacialHair(FacialHairMesh);
	}
	
	if ( BadgeMesh != none )
	{
		AttachBadge(BadgeMesh);
	}
	
	if ( ClothComponent != None )
	{
		ClothComponent.SetParentAnimComponent(mesh);
		ClothComponent.SetShadowParent(mesh);
		AttachComponent(ClothComponent);
		ClothComponent.SetEnableClothSimulation(true);
		ClothComponent.SetAttachClothVertsToBaseBody(true);
	}
	
	if ( ArmsMesh != None )
	{
		ArmsMesh.SetSkeletalMesh(ArmsOnlyMeshFP);
		
		if( FPArmsSleeveMaterial != none )
		{
			ArmsMesh.SetMaterial(0, FPArmsSleeveMaterial);
		}
		/* else
		{
			`wwdebug("Null FPArmsSleeveMaterial!", 'P');
			`wwdebug("FPArmsSleeveMaterialTemplate:" @ FPArmsSleeveMaterialTemplate, 'P');
		} */
	}
	
	if ( BandageMesh != none )
	{
		BandageMesh.SetSkeletalMesh(BandageMeshFP);
		BandageMesh.SetHidden(true);
	}
	
	if ( ROMI != none )
	{
		if ( TrapDisarmToolMesh != none )
		{
			TrapDisarmToolMesh.SetSkeletalMesh(GetTrapDisarmToolMesh(true));
		}
		if ( TrapDisarmToolMeshTP != none )
		{
			TrapDisarmToolMeshTP.SetSkeletalMesh(GetTrapDisarmToolMesh(false));
		}
	}
	
	if ( TrapDisarmToolMesh != none )
	{
		TrapDisarmToolMesh.SetHidden(true);
	}
	
	if ( TrapDisarmToolMeshTP != none )
	{
		Mesh.AttachComponentToSocket(TrapDisarmToolMeshTP, GrenadeSocket);
		TrapDisarmToolMeshTP.SetHidden(true);
	}
	
	if ( bOverrideLighting )
	{
		ThirdPersonHeadAndArmsMeshComponent.SetLightingChannels(LightingOverride);
		ThirdPersonHeadgearMeshComponent.SetLightingChannels(LightingOverride);
	}
	
	if( WorldInfo.NetMode == NM_DedicatedServer )
	{
		mesh.ForcedLODModel = 1000;
		ThirdPersonHeadAndArmsMeshComponent.ForcedLodModel = 1000;
		ThirdPersonHeadgearMeshComponent.ForcedLodModel = 1000;
		FaceItemMeshComponent.ForcedLodModel = 1000;
		FacialHairMeshComponent.ForcedLodModel = 1000;
	}
}

simulated function AttachNewHeadgear(SkeletalMesh NewHeadgearMesh)
{
	local SkeletalMeshSocket HeadSocket;
	
	ThirdPersonHeadgearMeshComponent.SetSkeletalMesh(NewHeadgearMesh);
	ThirdPersonHeadgearMeshComponent.SetMaterial(0, HeadgearMIC);
	
	HeadSocket = ThirdPersonHeadAndArmsMeshComponent.GetSocketByName(HeadgearAttachSocket);
	
	if( HeadSocket != none )
	{
		if( mesh.MatchRefBone(HeadSocket.BoneName) != INDEX_NONE )
		{
			ThirdPersonHeadgearMeshComponent.SetShadowParent(mesh);
			ThirdPersonHeadgearMeshComponent.SetLODParent(mesh);
			mesh.AttachComponent( ThirdPersonHeadgearMeshComponent, HeadSocket.BoneName, HeadSocket.RelativeLocation, HeadSocket.RelativeRotation, HeadSocket.RelativeScale);
		}
	}
}

simulated function AttachBadge(StaticMesh NewBadgeMesh)
{
	if ( Mesh.GetSocketByName(BadgeAttachSocket) != none )
	{
		BadgeComponent.SetStaticMesh(NewBadgeMesh);
		
		BadgeComponent.SetShadowParent(Mesh);
		Mesh.AttachComponentToSocket(BadgeComponent, BadgeAttachSocket);
	}
}

simulated function SetMeshVisibility(bool bVisible)
{
	super.SetMeshVisibility(bVisible);
	
	if (BadgeComponent != none)
	{
		BadgeComponent.SetOwnerNoSee(!bVisible);
	}
}

simulated function ShotOffSkis()
{
	local TraceHitInfo VictimHitInfo;
	local vector HitLocation;
	
	`wwdebug("",'S');
	
	VictimHitInfo.BoneName = default.PlayerHitZones[23].ZoneName;
	
	TakeDamage(150, ShotOffSkisInstigator, HitLocation, ShotOffSkisMomentum, ShotOffSkisDamageType, VictimHitInfo, ShotOffSkisDamageCauser);
}

simulated function UpdateWetnessValue(float DeltaTime) {}

function JumpOffVehicle() {}

event EncroachedBy(Actor Other)
{
	`wwdebug(""$Other,'P');
	
	// Don't do anything here, we don't want to kill people just by touching them
}

function bool IsInCamo()
{
	return false;
}

// The T-20 Komsomolets is the only vehicle players are visible in, and it has plenty of space for gear
simulated function HideGear(optional bool bHideGear) {}

// Moved from ROPawnTanker
simulated function LoaderLHCannon1()
{
	VehicleIKNode.HandleDriverAction(DAct_CannonReload_LH1);
}

simulated function LoaderRHCannon1()
{
	VehicleIKNode.HandleDriverAction(DAct_CannonReload_RH1);
}

simulated function LoaderLHCannon2()
{
	VehicleIKNode.HandleDriverAction(DAct_CannonReload_LH2);
}

simulated function LoaderRHCannonMG1()
{
	VehicleIKNode.HandleDriverAction(DAct_CannonReload_RH2);
}

simulated function LoaderLHCannonBreech1()
{
	VehicleIKNode.HandleDriverAction(DAct_CannonReload_LH3);
}

simulated function LoaderRHIKoff()
{
	VehicleIKNode.HandleDriverAction(DAct_CannonReload_RHOff);
}

simulated function LoaderLHIKoff()
{
	VehicleIKNode.HandleDriverAction(DAct_CannonReload_LHOff);
}

reliable client function ClientPossessed()
{
	local RORoleInfo RORI;
	
	if(ROPlayerController(Controller) != none && ROPlayerController(Controller).IsLocalPlayerController())
	{
		NFTrapsArray.Remove(0, NFTrapsArray.Length);
		
		if(PlayerReplicationInfo != none)
		{
			RORI = ROPlayerReplicationInfo(PlayerReplicationInfo).RoleInfo;
			
			if(GetTeamNum() == `ALLIES_TEAM_INDEX && RORI != none && RORI.RoleType == RORIT_Engineer)
			{
				`wwdebug("we are a Soviet Engineer - starting minesweeping operations",'T');
				
				if(!IsTimerActive('CacheAndHighlightTrapsTimerDelegate'))
					SetTimer(0.5, true, 'CacheAndHighlightTrapsTimerDelegate');
			}
		}
	}
}

function CacheAndHighlightTrapsTimerDelegate()
{
	local WWAntiTankMine tempNFTrap;
	local array<WWAntiTankMine> tempNFTrapsArray;
	local int i;
	local bool bFoundNew;
	
	tempNFTrapsArray.Remove(0, tempNFTrapsArray.Length);
	
	foreach OverlappingActors(class'WWAntiTankMine', tempNFTrap, 500, self.Location)
	{
		tempNFTrapsArray.AddItem(tempNFTrap);
	}
	
	for (i = 0; i < NFTrapsArray.Length; i++)
	{
		if (NFTrapsArray[i] == none)
		{
			NFTrapsArray.Remove(i, 1);
			i--;
			continue;
		}
		
		if (tempNFTrapsArray.Find(NFTrapsArray[i]) == INDEX_NONE)
		{
			NFTrapsArray[i].ThrowableMesh.SetMaterial(0, MD82MICTemplate);
			NFTrapsArray[i].ThrowableMesh.SetDepthPriorityGroup(SDPG_World);
			
			if(GlowingMICs[i] != None)
			{
				GlowingMICs[i] = None;
				GlowingMICs.Remove(i, 1);
			}
			
			NFTrapsArray.Remove(i, 1);
			i--;
		}
	}
	
	for (i = 0; i < tempNFTrapsArray.Length; i++)
	{
		if(NFTrapsArray.Find(tempNFTrapsArray[i]) == INDEX_None)
		{
			NFTrapsArray.AddItem(tempNFTrapsArray[i]);
			bFoundNew = true;
			
			GlowingMICs.AddItem(new class'MaterialInstanceConstant');
			GlowingMICs[i].SetParent(MD82GlowMICTemplate);
			NFTrapsArray[NFTrapsArray.Find(tempNFTrapsArray[i])].ThrowableMesh.SetMaterial(0, GlowingMICs[i]);
			
			NFTrapsArray[NFTrapsArray.Find(tempNFTrapsArray[i])].ThrowableMesh.SetDepthPriorityGroup(SDPG_Foreground);
		}
	}
	
	if (bFoundNew && WorldInfo.TimeSeconds >= LastTrapSpotTime + 8.0)
	{
		PlaySpottedExplosiveSound();
		LastTrapSpotTime = WorldInfo.TimeSeconds;
	}
}

function PlaySpottedExplosiveSound()
{
	`wwdebug("",'T');
	super.PlaySpottedExplosiveSound();
	
	// Since we unfortunately don't have any voicelines for this, just show a text message
	if (WWPlayerController(Controller) != none)
	{
		WWPlayerController(Instigator.Controller).ReceiveLocalizedMessage(class'WWLocalMessageGameMineDetected');
	}
}

defaultproperties
{
	MD82MICTemplate=	MaterialInstanceConstant'WinterWar_WP_FIN_M39.MIC.M39_Mine'
	MD82GlowMICTemplate=MaterialInstanceConstant'WinterWar_WP_FIN_M39.MIC.M39_Mine_GLOW'
	
	TunicMesh=SkeletalMesh'WinterWar_CHR_ALL.CharRef_Full'
	
	ArmsOnlyMesh=none
	
	BulletHitHelmetSound=none
	BulletHitMyHeadSound=none
	BulletHitMyHelmetSound=none
	MyBulletHitHelmetSound=none
	
	bCanCamouflage=false
	
	Begin Object class=StaticMeshComponent name=BadgeComponent0
		LightEnvironment=MyLightEnvironment
		CastShadow=FALSE
		MaxDrawDistance=1000
		CollideActors=false
		BlockActors=false
		BlockZeroExtent=false
		BlockNonZeroExtent=false
	End Object
	BadgeComponent=BadgeComponent0
	
	BadgeAttachSocket=TorsoSocket
	
	Begin Object Name=ROPawnSkeletalMeshComponent
		AnimSets(0)=AnimSet'CHR_Playeranim_Master.Anim.CHR_Stand_anim'
		AnimSets(1)=AnimSet'CHR_Playeranim_Master.Anim.CHR_ChestCover_anim'
		AnimSets(2)=AnimSet'CHR_Playeranim_Master.Anim.CHR_WaistCover_anim'
		AnimSets(3)=AnimSet'CHR_Playeranim_Master.Anim.CHR_StandCover_anim'
		AnimSets(4)=AnimSet'CHR_Playeranim_Master.Anim.CHR_Crouch_anim'
		AnimSets(5)=AnimSet'CHR_Playeranim_Master.Anim.CHR_Prone_anim'
		AnimSets(6)=AnimSet'CHR_Playeranim_Master.Anim.CHR_Hand_Poses_Master'
		AnimSets(7)=AnimSet'CHR_Playeranim_Master.Anim.CHR_Death_anim'
		AnimSets(8)=AnimSet'CHR_Playeranim_Master.Anim.CHR_Tripod_anim'
		AnimSets(9)=AnimSet'CHR_Playeranim_Master.Anim.Special_Actions'
		AnimSets(10)=AnimSet'CHR_Playeranim_Master.Anim.CHR_Melee'
		AnimSets(11)=none // Team specific sprinting animations
		AnimSets(12)=none // Reserved for weapon specific animations
		AnimSets(13)=AnimSet'WinterWar_VH_USSR_ZIS.Anim.CHR_QuadMaxim'
		AnimSets(14)=AnimSet'CHR_VN_Playeranim_Master.Anim.CHR_VN_Stand_anim'
		AnimSets(15)=AnimSet'CHR_VN_Playeranim_Master.Anim.CHR_VN_ChestCover_anim'
		AnimSets(16)=AnimSet'CHR_VN_Playeranim_Master.Anim.CHR_VN_WaistCover_anim'
		AnimSets(17)=AnimSet'CHR_VN_Playeranim_Master.Anim.CHR_VN_StandCover_anim'
		AnimSets(18)=AnimSet'CHR_VN_Playeranim_Master.Anim.CHR_VN_Crouch_anim'
		AnimSets(19)=AnimSet'CHR_VN_Playeranim_Master.Anim.CHR_VN_Prone_anim'
		AnimSets(20)=AnimSet'CHR_VN_Playeranim_Master.Anim.CHR_VN_Hand_Poses_Master'
		AnimSets(21)=AnimSet'CHR_VN_Playeranim_Master.Anim.VN_Special_Actions'
		AnimSets(22)=AnimSet'WinterWar_CHR_ALL.Anim.CHR_RS_Hand_Poses_Master'
	End Object
	
	Gore_LeftHand=(GibClass=class'ROGameContent.ROGib_HumanArm_Gore_BareArm')
	Gore_RightHand=(GibClass=class'ROGameContent.ROGib_HumanArm_Gore_BareArm')
	Gore_LeftLeg=(GibClass=class'ROGameContent.ROGib_HumanLeg_Gore_BareLeg')
	Gore_RightLeg=(GibClass=class'ROGameContent.ROGib_HumanLeg_Gore_BareLeg')
	
	STAMINA_STANDSTILL_REGEN_MOD=2.8
	STAMINA_CROUCH_REGEN_MOD=2.8
	STAMINA_PRONE_REGEN_MOD=2.8
	STAMINA_WALKING_REGEN_MOD=2.5
	STAMINA_CRAWLING_REGEN_MOD=2.5
	STAMINA_DRAIN_RATE_SPRINT=4.5
	STAMINA_DRAIN_JUMP=15.0
	STAMINA_STD_SPRINT_MOD=0.85
	STAMINA_EXHAUSTED_SPRINT_MOD=0.75
	STAMINA_JUMP_SPEED_PENALTY=0.4
	STAMINA_JUMP_PENALTY_DURATION=1.0
	
	FootstepSounds.Add((MaterialType=EMT_Default,Sound=AkEvent'WW_FOL_US.Play_FS_US_Jog_Dirt')()
	FootstepSounds.Add((MaterialType=EMT_Rock,Sound=AkEvent'WW_FOL_US.Play_FS_US_Jog_Gravel'))
	FootstepSounds.Add((MaterialType=EMT_Dirt,Sound=AkEvent'WW_FOL_US.Play_FS_US_Jog_Dirt'))
	FootstepSounds.Add((MaterialType=EMT_Metal,Sound=AkEvent'WW_FOL_US.Play_FS_US_Jog_Metal'))
	FootstepSounds.Add((MaterialType=EMT_Wood,Sound=AkEvent'WW_FOL_US.Play_FS_US_Jog_Wood'))
	FootstepSounds.Add((MaterialType=EMT_Asphalt,Sound=AkEvent'WW_FOL_US.Play_FS_US_Jog_Rock'))
	FootstepSounds.Add((MaterialType=EMT_RedBrick,Sound=AkEvent'WW_FOL_US.Play_FS_US_Jog_Rock'))
	FootstepSounds.Add((MaterialType=EMT_WhiteBrick,Sound=AkEvent'WW_FOL_US.Play_FS_US_Jog_Rock'))
	FootstepSounds.Add((MaterialType=EMT_Plant,Sound=AkEvent'WW_FOL_US.Play_FS_US_Jog_Grass'))
	FootstepSounds.Add((MaterialType=EMT_HollowMetal,Sound=AkEvent'WW_FOL_US.Play_FS_US_Jog_Metal'))
	FootstepSounds.Add((MaterialType=EMT_HollowWood,Sound=AkEvent'WW_FOL_US.Play_FS_US_Jog_Wood'))
	FootstepSounds.Add((MaterialType=EMT_Mud,Sound=AkEvent'WW_FOL_US.Play_FS_US_Jog_Mud'))
	FootstepSounds.Add((MaterialType=EMT_Dirt,Sound=AkEvent'WW_FOL_US.Play_FS_US_Jog_Dirt'))
	FootstepSounds.Add((MaterialType=EMT_Water,Sound=AkEvent'WW_FOL_US.Play_FS_US_Jog_Water'))
	FootstepSounds.Add((MaterialType=EMT_ShallowWater,Sound=AkEvent'WW_FOL_US.Play_FS_US_Jog_Mud'))
	FootstepSounds.Add((MaterialType=EMT_Gravel,Sound=AkEvent'WW_FOL_US.Play_FS_US_Jog_Gravel'))
	FootstepSounds.Add((MaterialType=EMT_Plaster,Sound=AkEvent'WW_FOL_US.Play_FS_US_Jog_Rock'))
	FootstepSounds.Add((MaterialType=EMT_Concrete,Sound=AkEvent'WW_FOL_US.Play_FS_US_Jog_Rock'))
	FootstepSounds.Add((MaterialType=EMT_Poop,Sound=AkEvent'WW_FOL_US.Play_FS_US_Jog_Mud'))
	FootstepSounds.Add((MaterialType=EMT_Plastic,Sound=AkEvent'WW_FOL_US.Play_FS_US_Jog_Rock'))
	
	CrawlFootStepSounds.Add((MaterialType=EMT_Default,Sound=AkEvent'WW_FOL_US.Play_FS_US_Crawl_Dirt'))
	CrawlFootStepSounds.Add((MaterialType=EMT_Rock,Sound=AkEvent'WW_FOL_US.Play_FS_US_Crawl_Gravel'))
	CrawlFootStepSounds.Add((MaterialType=EMT_Dirt,Sound=AkEvent'WW_FOL_US.Play_FS_US_Crawl_Dirt'))
	CrawlFootStepSounds.Add((MaterialType=EMT_Metal,Sound=AkEvent'WW_FOL_US.Play_FS_US_Crawl_Metal'))
	CrawlFootStepSounds.Add((MaterialType=EMT_Wood,Sound=AkEvent'WW_FOL_US.Play_FS_US_Crawl_Wood'))
	CrawlFootStepSounds.Add((MaterialType=EMT_Asphalt,Sound=AkEvent'WW_FOL_US.Play_FS_US_Crawl_Rock'))
	CrawlFootStepSounds.Add((MaterialType=EMT_RedBrick,Sound=AkEvent'WW_FOL_US.Play_FS_US_Crawl_Rock'))
	CrawlFootStepSounds.Add((MaterialType=EMT_WhiteBrick,Sound=AkEvent'WW_FOL_US.Play_FS_US_Crawl_Rock'))
	CrawlFootStepSounds.Add((MaterialType=EMT_Plant,Sound=AkEvent'WW_FOL_US.Play_FS_US_Crawl_Grass'))
	CrawlFootStepSounds.Add((MaterialType=EMT_HollowMetal,Sound=AkEvent'WW_FOL_US.Play_FS_US_Crawl_Metal'))
	CrawlFootStepSounds.Add((MaterialType=EMT_HollowWood,Sound=AkEvent'WW_FOL_US.Play_FS_US_Crawl_Wood'))
	CrawlFootStepSounds.Add((MaterialType=EMT_Mud,Sound=AkEvent'WW_FOL_US.Play_FS_US_Crawl_Mud'))
	CrawlFootStepSounds.Add((MaterialType=EMT_Dirt,Sound=AkEvent'WW_FOL_US.Play_FS_US_Crawl_Dirt'))
	CrawlFootStepSounds.Add((MaterialType=EMT_Water,Sound=AkEvent'WW_FOL_US.Play_FS_US_Crawl_Water'))
	CrawlFootStepSounds.Add((MaterialType=EMT_ShallowWater,Sound=AkEvent'WW_FOL_US.Play_FS_US_Crawl_Water'))
	CrawlFootStepSounds.Add((MaterialType=EMT_Gravel,Sound=AkEvent'WW_FOL_US.Play_FS_US_Crawl_Gravel'))
	CrawlFootStepSounds.Add((MaterialType=EMT_Plaster,Sound=AkEvent'WW_FOL_US.Play_FS_US_Crawl_Rock'))
	CrawlFootStepSounds.Add((MaterialType=EMT_Concrete,Sound=AkEvent'WW_FOL_US.Play_FS_US_Crawl_Rock'))
	CrawlFootStepSounds.Add((MaterialType=EMT_Poop,Sound=AkEvent'WW_FOL_US.Play_FS_US_Crawl_Mud'))
	CrawlFootStepSounds.Add((MaterialType=EMT_Plastic,Sound=AkEvent'WW_FOL_US.Play_FS_US_Crawl_Rock'))
	
	SprintFootStepSounds.Add((MaterialType=EMT_Default,Sound=AkEvent'WW_FOL_US.Play_FS_US_Sprint_Dirt'))
	SprintFootStepSounds.Add((MaterialType=EMT_Rock,Sound=AkEvent'WW_FOL_US.Play_FS_US_Sprint_Gravel'))
	SprintFootStepSounds.Add((MaterialType=EMT_Dirt,Sound=AkEvent'WW_FOL_US.Play_FS_US_Sprint_Dirt'))
	SprintFootStepSounds.Add((MaterialType=EMT_Metal,Sound=AkEvent'WW_FOL_US.Play_FS_US_Sprint_Metal'))
	SprintFootStepSounds.Add((MaterialType=EMT_Wood,Sound=AkEvent'WW_FOL_US.Play_FS_US_Sprint_Wood'))
	SprintFootStepSounds.Add((MaterialType=EMT_Asphalt,Sound=AkEvent'WW_FOL_US.Play_FS_US_Sprint_Rock'))
	SprintFootStepSounds.Add((MaterialType=EMT_RedBrick,Sound=AkEvent'WW_FOL_US.Play_FS_US_Sprint_Rock'))
	SprintFootStepSounds.Add((MaterialType=EMT_WhiteBrick,Sound=AkEvent'WW_FOL_US.Play_FS_US_Sprint_Rock'))
	SprintFootStepSounds.Add((MaterialType=EMT_Plant,Sound=AkEvent'WW_FOL_US.Play_FS_US_Sprint_Grass'))
	SprintFootStepSounds.Add((MaterialType=EMT_HollowMetal,Sound=AkEvent'WW_FOL_US.Play_FS_US_Sprint_Metal'))
	SprintFootStepSounds.Add((MaterialType=EMT_HollowWood,Sound=AkEvent'WW_FOL_US.Play_FS_US_Sprint_Wood'))
	SprintFootStepSounds.Add((MaterialType=EMT_Mud,Sound=AkEvent'WW_FOL_US.Play_FS_US_Sprint_Mud'))
	SprintFootStepSounds.Add((MaterialType=EMT_Dirt,Sound=AkEvent'WW_FOL_US.Play_FS_US_Sprint_Dirt'))
	SprintFootStepSounds.Add((MaterialType=EMT_Water,Sound=AkEvent'WW_FOL_US.Play_FS_US_Sprint_Water'))
	SprintFootStepSounds.Add((MaterialType=EMT_ShallowWater,Sound=AkEvent'WW_FOL_US.Play_FS_US_Sprint_Mud'))
	SprintFootStepSounds.Add((MaterialType=EMT_Gravel,Sound=AkEvent'WW_FOL_US.Play_FS_US_Sprint_Gravel'))
	SprintFootStepSounds.Add((MaterialType=EMT_Plaster,Sound=AkEvent'WW_FOL_US.Play_FS_US_Sprint_Rock'))
	SprintFootStepSounds.Add((MaterialType=EMT_Concrete,Sound=AkEvent'WW_FOL_US.Play_FS_US_Sprint_Rock'))
	SprintFootStepSounds.Add((MaterialType=EMT_Poop,Sound=AkEvent'WW_FOL_US.Play_FS_US_Sprint_Mud'))
	SprintFootStepSounds.Add((MaterialType=EMT_Plastic,Sound=AkEvent'WW_FOL_US.Play_FS_US_Sprint_Rock'))
	
	WalkFootStepSounds.Add((MaterialType=EMT_Default,Sound=AkEvent'WW_FOL_US.Play_FS_US_Walk_Dirt'))
	WalkFootStepSounds.Add((MaterialType=EMT_Rock,Sound=AkEvent'WW_FOL_US.Play_FS_US_Walk_Gravel'))
	WalkFootStepSounds.Add((MaterialType=EMT_Dirt,Sound=AkEvent'WW_FOL_US.Play_FS_US_Walk_Dirt'))
	WalkFootStepSounds.Add((MaterialType=EMT_Metal,Sound=AkEvent'WW_FOL_US.Play_FS_US_Walk_Metal'))
	WalkFootStepSounds.Add((MaterialType=EMT_Wood,Sound=AkEvent'WW_FOL_US.Play_FS_US_Walk_Wood'))
	WalkFootStepSounds.Add((MaterialType=EMT_Asphalt,Sound=AkEvent'WW_FOL_US.Play_FS_US_Walk_Rock'))
	WalkFootStepSounds.Add((MaterialType=EMT_RedBrick,Sound=AkEvent'WW_FOL_US.Play_FS_US_Walk_Rock'))
	WalkFootStepSounds.Add((MaterialType=EMT_WhiteBrick,Sound=AkEvent'WW_FOL_US.Play_FS_US_Walk_Rock'))
	WalkFootStepSounds.Add((MaterialType=EMT_Plant,Sound=AkEvent'WW_FOL_US.Play_FS_US_Walk_Grass'))
	WalkFootStepSounds.Add((MaterialType=EMT_HollowMetal,Sound=AkEvent'WW_FOL_US.Play_FS_US_Walk_Metal'))
	WalkFootStepSounds.Add((MaterialType=EMT_HollowWood,Sound=AkEvent'WW_FOL_US.Play_FS_US_Walk_Wood'))
	WalkFootStepSounds.Add((MaterialType=EMT_Mud,Sound=AkEvent'WW_FOL_US.Play_FS_US_Walk_Mud'))
	WalkFootStepSounds.Add((MaterialType=EMT_Dirt,Sound=AkEvent'WW_FOL_US.Play_FS_US_Walk_Dirt'))
	WalkFootStepSounds.Add((MaterialType=EMT_Water,Sound=AkEvent'WW_FOL_US.Play_FS_US_Walk_Water'))
	WalkFootStepSounds.Add((MaterialType=EMT_ShallowWater,Sound=AkEvent'WW_FOL_US.Play_FS_US_Walk_Mud'))
	WalkFootStepSounds.Add((MaterialType=EMT_Gravel,Sound=AkEvent'WW_FOL_US.Play_FS_US_Walk_Gravel'))
	WalkFootStepSounds.Add((MaterialType=EMT_Plaster,Sound=AkEvent'WW_FOL_US.Play_FS_US_Walk_Rock'))
	WalkFootStepSounds.Add((MaterialType=EMT_Concrete,Sound=AkEvent'WW_FOL_US.Play_FS_US_Walk_Rock'))
	WalkFootStepSounds.Add((MaterialType=EMT_Poop,Sound=AkEvent'WW_FOL_US.Play_FS_US_Walk_Mud'))
	WalkFootStepSounds.Add((MaterialType=EMT_Plastic,Sound=AkEvent'WW_FOL_US.Play_FS_US_Walk_Rock'))
	
	CrouchFootStepSounds.Add((MaterialType=EMT_Default,Sound=AkEvent'WW_FOL_US.Play_FS_US_Crouch_Dirt'))
	CrouchFootStepSounds.Add((MaterialType=EMT_Rock,Sound=AkEvent'WW_FOL_US.Play_FS_US_Crouch_Gravel'))
	CrouchFootStepSounds.Add((MaterialType=EMT_Dirt,Sound=AkEvent'WW_FOL_US.Play_FS_US_Crouch_Dirt'))
	CrouchFootStepSounds.Add((MaterialType=EMT_Metal,Sound=AkEvent'WW_FOL_US.Play_FS_US_Crouch_Metal'))
	CrouchFootStepSounds.Add((MaterialType=EMT_Wood,Sound=AkEvent'WW_FOL_US.Play_FS_US_Crouch_Wood'))
	CrouchFootStepSounds.Add((MaterialType=EMT_Asphalt,Sound=AkEvent'WW_FOL_US.Play_FS_US_Crouch_Rock'))
	CrouchFootStepSounds.Add((MaterialType=EMT_RedBrick,Sound=AkEvent'WW_FOL_US.Play_FS_US_Crouch_Rock'))
	CrouchFootStepSounds.Add((MaterialType=EMT_WhiteBrick,Sound=AkEvent'WW_FOL_US.Play_FS_US_Crouch_Rock'))
	CrouchFootStepSounds.Add((MaterialType=EMT_Plant,Sound=AkEvent'WW_FOL_US.Play_FS_US_Crouch_Grass'))
	CrouchFootStepSounds.Add((MaterialType=EMT_HollowMetal,Sound=AkEvent'WW_FOL_US.Play_FS_US_Crouch_Metal'))
	CrouchFootStepSounds.Add((MaterialType=EMT_HollowWood,Sound=AkEvent'WW_FOL_US.Play_FS_US_Crouch_Wood'))
	CrouchFootStepSounds.Add((MaterialType=EMT_Mud,Sound=AkEvent'WW_FOL_US.Play_FS_US_Crouch_Mud'))
	CrouchFootStepSounds.Add((MaterialType=EMT_Dirt,Sound=AkEvent'WW_FOL_US.Play_FS_US_Crouch_Dirt'))
	CrouchFootStepSounds.Add((MaterialType=EMT_Water,Sound=AkEvent'WW_FOL_US.Play_FS_US_Walk_Water'))
	CrouchFootStepSounds.Add((MaterialType=EMT_ShallowWater,Sound=AkEvent'WW_FOL_US.Play_FS_US_Crouch_Mud'))
	CrouchFootStepSounds.Add((MaterialType=EMT_Gravel,Sound=AkEvent'WW_FOL_US.Play_FS_US_Crouch_Gravel'))
	CrouchFootStepSounds.Add((MaterialType=EMT_Plaster,Sound=AkEvent'WW_FOL_US.Play_FS_US_Crouch_Rock'))
	CrouchFootStepSounds.Add((MaterialType=EMT_Concrete,Sound=AkEvent'WW_FOL_US.Play_FS_US_Crouch_Rock'))
	CrouchFootStepSounds.Add((MaterialType=EMT_Poop,Sound=AkEvent'WW_FOL_US.Play_FS_US_Crouch_Mud'))
	CrouchFootStepSounds.Add((MaterialType=EMT_Plastic,Sound=AkEvent'WW_FOL_US.Play_FS_US_Crouch_Rock'))
	
	CrouchWalkFootStepSounds.Add((MaterialType=EMT_Default,Sound=AkEvent'WW_FOL_US.Play_FS_US_Crouch_Dirt'))
	CrouchWalkFootStepSounds.Add((MaterialType=EMT_Rock,Sound=AkEvent'WW_FOL_US.Play_FS_US_Crouch_Gravel'))
	CrouchWalkFootStepSounds.Add((MaterialType=EMT_Dirt,Sound=AkEvent'WW_FOL_US.Play_FS_US_Crouch_Dirt'))
	CrouchWalkFootStepSounds.Add((MaterialType=EMT_Metal,Sound=AkEvent'WW_FOL_US.Play_FS_US_Crouch_Metal'))
	CrouchWalkFootStepSounds.Add((MaterialType=EMT_Wood,Sound=AkEvent'WW_FOL_US.Play_FS_US_Crouch_Wood'))
	CrouchWalkFootStepSounds.Add((MaterialType=EMT_Asphalt,Sound=AkEvent'WW_FOL_US.Play_FS_US_Crouch_Rock'))
	CrouchWalkFootStepSounds.Add((MaterialType=EMT_RedBrick,Sound=AkEvent'WW_FOL_US.Play_FS_US_Crouch_Rock'))
	CrouchWalkFootStepSounds.Add((MaterialType=EMT_WhiteBrick,Sound=AkEvent'WW_FOL_US.Play_FS_US_Crouch_Rock'))
	CrouchWalkFootStepSounds.Add((MaterialType=EMT_Plant,Sound=AkEvent'WW_FOL_US.Play_FS_US_Crouch_Grass'))
	CrouchWalkFootStepSounds.Add((MaterialType=EMT_HollowMetal,Sound=AkEvent'WW_FOL_US.Play_FS_US_Crouch_Metal'))
	CrouchWalkFootStepSounds.Add((MaterialType=EMT_HollowWood,Sound=AkEvent'WW_FOL_US.Play_FS_US_Crouch_Wood'))
	CrouchWalkFootStepSounds.Add((MaterialType=EMT_Mud,Sound=AkEvent'WW_FOL_US.Play_FS_US_Crouch_Mud'))
	CrouchWalkFootStepSounds.Add((MaterialType=EMT_Dirt,Sound=AkEvent'WW_FOL_US.Play_FS_US_Crouch_Dirt'))
	CrouchWalkFootStepSounds.Add((MaterialType=EMT_Water,Sound=AkEvent'WW_FOL_US.Play_FS_US_Walk_Water'))
	CrouchWalkFootStepSounds.Add((MaterialType=EMT_ShallowWater,Sound=AkEvent'WW_FOL_US.Play_FS_US_Crouch_Mud'))
	CrouchWalkFootStepSounds.Add((MaterialType=EMT_Gravel,Sound=AkEvent'WW_FOL_US.Play_FS_US_Crouch_Gravel'))
	CrouchWalkFootStepSounds.Add((MaterialType=EMT_Plaster,Sound=AkEvent'WW_FOL_US.Play_FS_US_Crouch_Rock'))
	CrouchWalkFootStepSounds.Add((MaterialType=EMT_Concrete,Sound=AkEvent'WW_FOL_US.Play_FS_US_Crouch_Rock'))
	CrouchWalkFootStepSounds.Add((MaterialType=EMT_Poop,Sound=AkEvent'WW_FOL_US.Play_FS_US_Crouch_Mud'))
	CrouchWalkFootStepSounds.Add((MaterialType=EMT_Plastic,Sound=AkEvent'WW_FOL_US.Play_FS_US_Crouch_Rock'))
	
	DefaultConfigs(`ALLIES_TEAM_INDEX)=	(TunicMesh=1,	TunicMaterial=255,	HeadMesh=255,	HeadgearMesh=255,	FaceItemMesh=0,FacialHairMesh=0)
	DefaultConfigs(`AXIS_TEAM_INDEX)=	(TunicMesh=2,	TunicMaterial=0,	HeadMesh=255,	HeadgearMesh=255,	FaceItemMesh=0,FacialHairMesh=0)
}
