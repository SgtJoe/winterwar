
class WWWeapon_F1Grenade_Attach extends ROWeapAttach_EggGrenade;

DefaultProperties
{
	ThirdPersonHandsAnim=F1_Grenade_Handpose
	IKProfileName=F1
	
	Begin Object Name=SkeletalMeshComponent0
		SkeletalMesh=SkeletalMesh'WinterWar_WP_SOV_F1_GRENADE.Mesh.SOV_F1_Grenade_3rd'
		PhysicsAsset=PhysicsAsset'WinterWar_WP_SOV_F1_GRENADE.Phy.F1_Grenade_3rd_Bounds_Physics'
		CullDistance=5000
	End Object
	
	WeaponClass=class'WWWeapon_F1Grenade'
}
