
class WWVehicleProjectile_T28_HE extends WWVehicleProjectile_HE;

defaultproperties
{
	ImpactDamageType=	class'WWDmgType_T28Shell_HEImpact'
	GeneralDamageType=	class'WWDmgType_T28Shell_HEImpactGeneral'
	MyDamageType=		class'WWDmgType_T28Shell_HE'

	BallisticCoefficient=1.55
	Speed=19350 //387 M/S
	MaxSpeed=16750
	Damage=350
	DamageRadius=600
	MomentumTransfer=45000
	
	Caliber=76
	ActualRHA=20
	TestPlateHardness=400
	SlopeEffect=0.82472
	ShatterNumber=1.0
	ShatterTd=0.65
	ShatteredPenEffectiveness=0.8
}
