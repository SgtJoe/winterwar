
class WWProjectile_NagantRevolverBullet extends ROBullet;

DefaultProperties
{
	BallisticCoefficient=0.132
	
	Damage=112
	MyDamageType=class'WWDmgType_NagantRevolverBullet'
	Speed=16350		// 327 m/s
	MaxSpeed=16350  // 327 m/s
	
	VelocityDamageFalloffCurve=(Points=((InVal=291726400,OutVal=0.8),(InVal=595360000,OutVal=0.36)))
}
