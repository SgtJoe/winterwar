
class WWWeapon_F1Grenade_ActualContent extends WWWeapon_F1Grenade;

DefaultProperties
{
	ArmsAnimSet=AnimSet'WinterWar_WP_SOV_F1_GRENADE.Anim.WP_F1Hands'
	
	Begin Object Name=FirstPersonMesh
		DepthPriorityGroup=SDPG_Foreground
		SkeletalMesh=SkeletalMesh'WinterWar_WP_SOV_F1_GRENADE.Mesh.SOV_F1_Grenade'
		PhysicsAsset=None
		AnimSets(0)=AnimSet'WinterWar_WP_SOV_F1_GRENADE.Anim.WP_F1Hands'
		AnimTreeTemplate=AnimTree'WinterWar_WP_SOV_F1_GRENADE.Anim.Sov_F1_Tree'
		Scale=1.0
		FOV=70
	End Object
	
	Begin Object Name=PickupMesh
		SkeletalMesh=SkeletalMesh'WinterWar_WP_SOV_F1_GRENADE.Mesh.SOV_F1_Grenade_3rd'
		PhysicsAsset=PhysicsAsset'WinterWar_WP_SOV_F1_GRENADE.Phy.F1_Grenade_3rd_Master_Physics'
		CollideActors=true
		BlockActors=true
		BlockZeroExtent=true
		BlockNonZeroExtent=true//false
		BlockRigidBody=true
		bHasPhysicsAssetInstance=false
		bUpdateKinematicBonesFromAnimation=false
		PhysicsWeight=1.0
		RBChannel=RBCC_GameplayPhysics
		RBCollideWithChannels=(Default=TRUE,GameplayPhysics=TRUE,EffectPhysics=TRUE)
		bSkipAllUpdateWhenPhysicsAsleep=TRUE
		bSyncActorLocationToRootRigidBody=true
	End Object
	
	AttachmentClass=class'WWWeapon_F1Grenade_Attach'
}
