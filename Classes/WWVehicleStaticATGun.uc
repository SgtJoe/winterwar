
class WWVehicleStaticATGun extends WWVehicleTank
	abstract;

var vector	LastExplosionLoc;
var float	LastExplosionDist;

simulated function PostBeginPlay()
{
	super.PostBeginPlay();
	
	EntryPoints[0].EntryActor.MySeatType = VST_Gunner;
}

// Now that the suspension is fixed we can add impulse again
/* simulated function VehicleWeaponFired( bool bViaReplication, vector HitLocation, int SeatIndex )
{
	// Don't add any impulse when firing, only animate the barrel
	// Otherwise we get weird physics fuckups for some reason
	
	super(WWVehicleTransport).VehicleWeaponFired(bViaReplication, HitLocation, SeatIndex);
	
	if (MainGunAnimInfo.AnimNode != none)
	{
		MainGunAnimInfo.AnimNode.PlayCustomAnim(MainGunAnimInfo.FiringAnimName, 1.0, , , , true);
	}
} */

function HandleMomentum( vector Momentum, Vector HitLocation, class<DamageType> DamageType, optional TraceHitInfo HitInfo )
{
	// No momentum at all, this should help with the damn physics
}

simulated function Tick(float DeltaTime)
{
	// We don't have any exhaust effects or engine audio
	super(ROVehicle).Tick(DeltaTime);
}

simulated function SetInputs(float InForward, float InStrafe, float InUp)
{
	InForward = 0;
	InStrafe = 0;
	InUp = 0;
	
	super.SetInputs(InForward, InStrafe, InUp);
}

function bool TryToDriveSeat(Pawn P, optional byte SeatIdx = 255)
{
	local bool bEnteredVehicle;
	
	if (!CanEnterVehicle(P) || (Vehicle(P) != None))
	{
		return false;
	}
	
	if (!bIsDisabled)
	{
		if(!AnySeatAvailable())
		{
			return false;
		}
		
		// Avoid calling DriverEnter, we want only seat 1
		bEnteredVehicle = PassengerEnter(P, 1);
		
		if (bEnteredVehicle)
		{
			SetTexturesToBeResident( true );
		}
		
		return bEnteredVehicle;
	}
	
	VehicleLocked(P);
	return false;
}

simulated function StartEngineSound()
{
	ClearTimer('StartEngineSound');
	ClearTimer('StopEngineSound');
}

function OnMainCannonReloadComplete()
{
	// Don't make any callouts, we're loading ourselves and can clearly see it
	`wwdebug("main cannon reloaded", 'VH');
}

// This function is called from WWVehicleWeapon_TankTurret, but
// WWVehicleWeapon_ATCannon overrides the call with something else
function float GetMainCannonReloadDuration()
{
	`wwdebug("UNUSED FUNCTION", 'VH');
	return 0.0;
}

simulated function VehicleLoadMainGun()
{
	local ROPawn ROP;
	
	`wwdebug("playing main cannon reload", 'VH');
	
	if (WorldInfo.NetMode != NM_DedicatedServer)
	{
		if ( SeatProxies[GetSeatProxyIndexForSeatIndex(1)].Health <= 0 )
		{
			return;
		}
		
		ROP = GetDriverForSeatIndex(1);
		
		if( ROP != none )
		{
			ROP.PlayFullBodyAnimation('Reload');
		}
		else
		{
			ProxyAnimAction.AnimActionName = 'Reload';
			ProxyAnimAction.SeatProxyIndex = Seats[1].SeatPositions[SeatPositionIndex(1,,true)].SeatProxyIndex;
			
			if( ProxyAnimAction.ProxyActionCount < 255 )
			{
				ProxyAnimAction.ProxyActionCount += 1;
			}
			else
			{
				ProxyAnimAction.ProxyActionCount = 0;
			}
			
			if( WorldInfo.NetMode != NM_DedicatedServer )
			{
				HandleSeatProxyAnimAction();
			}
		}
		
		if( Seats[1].PositionBlend != none )
		{
			Seats[1].PositionBlend.HandleAnimPlay('Reload', false);
		}
	}
	
	if (MainCannonReloadSound != none && !MainCannonReloadSound.IsPlaying())
	{
		MainCannonReloadSound.Play();
	}
}

simulated function RequestPosition(byte SeatIndex, byte DesiredIndex, optional bool bViaInteraction)
{
	// Don't change to the scoped or standing position while we're in the middle of reloading
	if (Seats[SeatIndex].bPerformingReload || WWVehicleWeapon_ATCannon(Seats[SeatIndex].Gun).bCannonReloading)
	{
		return;
	}
	
	`wwdebug("SeatIndex" @ SeatIndex @ "DesiredPositionIndex" @ DesiredIndex, 'VH');
	
	super.RequestPosition(SeatIndex, DesiredIndex, bViaInteraction);
}

simulated function bool CanEnterVehicle(Pawn P)
{
	return super(ROVehicle).CanEnterVehicle(P);
}

simulated function PlayerLookingAtMe(ROPlayerController PlayerController)
{
	local ROPawn P;
	P = ROPawn(PlayerController.Pawn);
	
	if (
		bUseRootLocForEntry &&
//		Team == PlayerController.GetTeamNum() &&
		!bDeadVehicle &&
		VSizeSq( Velocity ) < class'ROVehicle'.const.MAX_SPEED_FOR_ENTRY_SQ &&
		P != none && !P.bIsProning && !P.IsProneTransitioning()
		)
	{
		PlayerController.bInVehicleRange = true;
		PlayerController.InVehicleSeatRange = VST_Passenger;
		
		if( !HasEmptySeat() )
			PlayerController.bTargetVehicleFull = true;
		else
			PlayerController.bTargetVehicleFull = false;
	}
}

function bool PassengerEnter(Pawn P, int SeatIndex)
{
	local bool bNewPawnControllerHuman;
	
	if( Vehicle(P) != none )
	{
		return false;
	}
	
	// Skip the team-based check here
	
	if (SeatIndex <= 0 || SeatIndex >= Seats.Length)
	{
		return false;
	}
	
	if ( PlayerController(P.Controller) != None )
	{
		bNewPawnControllerHuman = true;
	}
	
	if ( !Seats[SeatIndex].bNonEnterable && !Seats[SeatIndex].SeatPawn.DriverEnter(p) )
	{
		return false;
	}
	else if( bNewPawnControllerHuman )
	{
		bHadHumanCrew = true;
		ClearTimer('DestroyIfEmpty');
	}
	
	if (Seats[SeatIndex].Gun != none)
	{
		Seats[SeatIndex].Gun.SetClientAmmoCount(Seats[SeatIndex].Gun.AmmoCount);
	}
	
	SetSeatStoragePawn(SeatIndex, P);
	bHasBeenDriven = true;
	return true;
}

function EvaluateBackSeatDrivers() {}

simulated function TakeRadiusDamage(Controller InstigatedBy, float BaseDamage, float DamageRadius, class<DamageType> DamageType, float Momentum, vector HurtOrigin, bool bFullDamage, Actor DamageCauser, optional float DamageFalloffExponent=1.f)
{
	LastExplosionLoc = HurtOrigin - Location;
	LastExplosionDist = VSize(LastExplosionLoc) / 50.0;
	
	`wwdebug("taking" @ DamageType @ "from" @ LastExplosionDist @ "meters away" @ "("$LastExplosionLoc.X @ LastExplosionLoc.Y$")", 'VH');
	
	super.TakeRadiusDamage(InstigatedBy, BaseDamage, DamageRadius, DamageType, Momentum, HurtOrigin, bFullDamage, DamageCauser, DamageFalloffExponent);
}

simulated event TakeDamage(int Damage, Controller EventInstigator, vector HitLocation, vector Momentum, class<DamageType> DamageType, optional TraceHitInfo HitInfo, optional Actor DamageCauser)
{
	local int HitIndex;
	local WWPawn Victim;
	local bool ExplodeVictim;
	
	super.TakeDamage(Damage, EventInstigator, HitLocation, Momentum, DamageType, HitInfo, DamageCauser);
	
	if (ShouldInstaKill(DamageType, Damage))
	{
		Died(EventInstigator, DamageType, HitLocation);
		return;
	}
	
	if (LastExplosionDist != 0.0)
	{
		if (LastExplosionLoc.X < -30)
		{
			// Explosion was behind the shield, max range
			ExplodeVictim = (LastExplosionDist < 6.25);
		}
		else
		{
			// Explosion was in front of the shield, needs to be closer to do anything
			ExplodeVictim = (LastExplosionDist < 2.0);
		}
	}
	else
	{
		ExplodeVictim = false;
	}
	
	if (HitInfo.BoneName != '')
	{
		HitIndex = VehHitZones.Find('ZoneName', HitInfo.BoneName);
		
		if (HitIndex < 0)
		{
			HitIndex = VehHitZones.Find('CrewBoneName', HitInfo.BoneName);
		}
		
		`wwdebug("taking" @ Damage @ DamageType @ "in" @ HitInfo.BoneName @ "Zone" @ HitIndex @ "ExplodeVictim" @ ExplodeVictim, 'VH');
		
		if (HitIndex >= 0 || ExplodeVictim)
		{
			if (VehHitZones[HitIndex].VehicleHitZoneType >= VHT_CrewBody || ExplodeVictim)
			{
				`wwdebug("Hit Passenger", 'VH');
				
				Victim = WWPawn(Seats[1].SeatPawn.Driver);
				
				if (Victim == none)
				{
					`wwdebug("Nobody in seat", 'VH');
					LastExplosionDist = 0.0;
					return;
				}
				
				if (ExplodeVictim)
				{
					`wwdebug(Victim @ "taking" @ Damage @ DamageType, 'VH');
				}
				else
				{
					`wwdebug(Victim @ "taking" @ Damage @ DamageType @ "in" @ HitInfo.BoneName @ "Zone" @ HitIndex, 'VH');
				}
				
				Seats[1].SeatPawn.DriverLeave(true);
				
				// These aren't skis, but the intended effect is exactly the same
				Victim.ShotOffSkisInstigator = EventInstigator;
				Victim.ShotOffSkisMomentum = Momentum;
				Victim.ShotOffSkisDamageType = DamageType;
				Victim.ShotOffSkisDamageCauser = DamageCauser;
				
				Victim.SetTimer(0.2, false, 'ShotOffSkis');
			}
		}
	}
	
	// After we're all finished here, reset this var in case our next TakeDamage is not an explosion
	LastExplosionDist = 0.0;
}

function bool ShouldInstakill(class<DamageType> DamageType, int Damage)
{
	// Lowered satchel threshold to compensate for smaller hitbox
	if (class<RODmgType_Satchel>(DamageType) != none && Damage >= 430)
	{
		return true;
	}
	
	return super.ShouldInstakill(DamageType, Damage);
}

simulated function TracksDestroyed(bool RightSide) {}

simulated function EngineCaughtFire() {}

simulated function BurnOutPhase1() {}
simulated function BurnOutPhase2() {}
simulated function BurnOutPhase3() {}

simulated function SpawnTurretGib() {}

function OneSecondLoopingTimer() {}

function bool FindAutoExit(Pawn ExitingDriver)
{
	local vector X, Y, Z;
	local float PlaceDist;
	
	GetAxes(ExitRotation(), X,Y,Z);
	// Y *= -1;
	
	if ( ExitRadius == 0 )
	{
		ExitRadius = CylinderComponent.CollisionRadius + 2*ExitingDriver.GetCollisionRadius();
	}
	
	PlaceDist = ExitRadius - Rand(ExitRadius / 2) + ExitingDriver.GetCollisionRadius();
	/*
	if ( Controller != None )
	{
		if ( (Y dot vector(Controller.Rotation)) < 0 )
		{
			Y *= -1;
		}
	}
	
	if ( VSize(Velocity) > MinCrushSpeed )
	{
		//avoid running driver over by placing in direction away from velocity
		if ( (Velocity Dot X) < 0 )
			X *= -1;
		// check if going sideways fast enough
		if ( (Velocity dot Y) > MinCrushSpeed )
			Y *= -1;
		// Randomise which side we check first so that players don't always exit on the left
		else if( Rand(2) - 1 < 0 )
			Y *= -1;
	}
	// Randomise which side we check first so that players don't always exit on the left
	else if( Rand(2) - 1 < 0 )
	{
		Y *= -1;
	}
	
	if ( TryExitPos(ExitingDriver, GetTargetLocation() + (ExitOffset >> Rotation) + (PlaceDist * Y), bFindGroundExit) )
		return true;
	if ( TryExitPos(ExitingDriver, GetTargetLocation() + (ExitOffset >> Rotation) - (PlaceDist * Y), bFindGroundExit) )
		return true;
	*/
	// We don't want to exit to the sides or front, only behind
	if ( TryExitPos(ExitingDriver, GetTargetLocation() + (ExitOffset >> Rotation) - (PlaceDist * X), false) )
		return true;
	/*
	if ( TryExitPos(ExitingDriver, GetTargetLocation() + (ExitOffset >> Rotation) + (PlaceDist * X), false) )
		return true;
	if ( !bFindGroundExit )
		return false;
	if ( TryExitPos(ExitingDriver, GetTargetLocation() + (ExitOffset >> Rotation) + (PlaceDist * Y), false) )
		return true;
	if ( TryExitPos(ExitingDriver, GetTargetLocation() + (ExitOffset >> Rotation) - (PlaceDist * Y), false) )
		return true;
	if ( TryExitPos(ExitingDriver, GetTargetLocation() + (ExitOffset >> Rotation) + (PlaceDist * Z), false) )
		return true;
	*/
	return false;
}

defaultproperties
{
	// Anyone can enter the vehicle, this is only for overhead map
	Team=`AXIS_TEAM_INDEX
	
	Health=600
	
	// This is a dummy non-enterable seat purely so things don't glitch out
	Seats(0)={(
		bNonEnterable=true,
		bSeatVisible=false,
		SeatPositions=(
			(
				bDriverVisible=false,
				ViewFOV=70,
				SeatProxyIndex=0,
				bIsExterior=false
			)
		),
		InitialPositionIndex=0,
		SeatBone=Chassis
	)}
	
	// Even though we don't have tracks we should still keep these (albiet invisible) so things don't bounce around.
	Begin Object Name=RRWheel
		BoneName="Wheel_T_R_4"
		BoneOffset=(X=-10.0,Y=0,Z=0.0)
		WheelRadius=20
	End Object
	
	Begin Object Name=RMWheel
		BoneName="Wheel_T_R_3"
		BoneOffset=(X=0.0,Y=0,Z=0.0)
		WheelRadius=20
	End Object
	
	Begin Object Name=RFWheel
		BoneName="Wheel_T_R_2"
		BoneOffset=(X=20.0,Y=0,Z=2.0)
		WheelRadius=20
	End Object
	
	Begin Object Name=LRWheel
		BoneName="Wheel_T_L_4"
		BoneOffset=(X=-10.0,Y=0,Z=0.0)
		WheelRadius=20
	End Object
	
	Begin Object Name=LMWheel
		BoneName="Wheel_T_L_3"
		BoneOffset=(X=0.0,Y=0,Z=0.0)
		WheelRadius=20
	End Object
	
	Begin Object Name=LFWheel
		BoneName="Wheel_T_L_2"
		BoneOffset=(X=20.0,Y=0,Z=2.0)
		WheelRadius=20
	End Object
	
	bOpenVehicle=true
	
	// For the HUD icon only
	bHasTurret=true
	
	CannonFireImpulseMag=0
	CannonFireTorqueMag=1//12500
	
	LargeChunkImpactSound=none
	MediumChunkImpactSound=none
	SmallChunkImpactSound=none
}
