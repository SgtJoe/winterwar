
class WWWeapon_Kasapanos extends ROWeap_Type67_Grenade
	abstract;

`include(WinterWar\Classes\WWWeaponPickupMessagesOverride.uci)

defaultproperties
{
	WeaponContentClass(0)="WinterWar.WWWeapon_Kasapanos_Improvised_ActualContent"
	
	RoleSelectionImage(0)=Texture2D'WinterWar_UI.WP_Render.WP_Render_Kasapanos_IM'
	
	InvIndex=`WI_KASAPANOS_IM
	
	AmmoClass=class'ROAmmo_Type67_Grenade'
	
	WeaponProjectiles(DEFAULT_FIREMODE)=class'WWProjectile_Kasapanos_IM'
	WeaponProjectiles(ALTERNATE_FIREMODE)=class'WWProjectile_Kasapanos_IM'
	
	InitialNumPrimaryMags=1
	
	ThrowingBattleChatterIndex=`BATTLECHATTER_ThrowingGrenade
	
	EquipTime=+1.3
	PutDownTime=+0.7
	
	bCanBeQuickThrown=false
}
