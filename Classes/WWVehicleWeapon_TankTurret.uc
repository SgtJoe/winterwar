
class WWVehicleWeapon_TankTurret extends ROVWeap_TankTurret
	abstract;

simulated exec function SwitchFireMode()
{
	if( DesiredProjectileIndex < (MainGunProjectiles.Length - 1) )
	{
		DesiredProjectileIndex += 1;
	}
	else
	{
		DesiredProjectileIndex = 0;
	}
	
	// change immediately if out of ammo, else wait for next fire
	if ( !HasMainGunAmmo(MainGunProjectileIndex) )
	{
		SetMainGunProjectile();
	}
}

simulated function StartFire(byte FireModeNum)
{
	local ROWeaponPawn ROWP;
	local byte PositionIdx;
	
	if( FireModeNum == DEFAULT_FIREMODE && MyVehicle != None )
	{
		PositionIdx = MyVehicle.SeatPositionIndex(SeatIndex,,true);
		if ( MyVehicle.Seats[SeatIndex].SeatPositions[PositionIdx].bRotateGunOnCommand )
		{
			ROWP = ROWeaponPawn(MyVehicle.Seats[SeatIndex].SeatPawn);
			if ( ROWP != None && MyVehicle.IsInPeriscope(SeatIndex, PositionIdx) )
			{
				ROWP.UsePeriscope();
				return;
			}
		}
	}
	
	Super.StartFire(FireModeNum);
}

simulated function DrawRangeOverlay( Hud HUD )
{
	local byte PositionIdx;
	
	HUD.Canvas.SetPos((HUD.Canvas.SizeX - HUD.Canvas.SizeY) / 2.0, 0);
	
	PositionIdx = MyVehicle.SeatPositionIndex(SeatIndex,,true);
	// if ( MyVehicle.IsInPeriscope(SeatIndex, PositionIdx) )
	// {
		// HUD.Canvas.DrawTile(MyVehicle.Seats[SeatIndex].PeriscopeRangeTexture, HUD.Canvas.SizeY, HUD.Canvas.SizeY, 0, 0, MyVehicle.Seats[SeatIndex].PeriscopeRangeTexture.SizeX, MyVehicle.Seats[SeatIndex].PeriscopeRangeTexture.SizeY);
	// }
	// else
	if (PositionIdx == MyVehicle.Seats[SeatIndex].FiringPositionIndex)
	{
		HUD.Canvas.DrawTile(MyVehicle.Seats[SeatIndex].RangeOverlayTexture, HUD.Canvas.SizeY, HUD.Canvas.SizeY, 0, 0, MyVehicle.Seats[SeatIndex].RangeOverlayTexture.SizeX, MyVehicle.Seats[SeatIndex].RangeOverlayTexture.SizeY);
	}
}

simulated function DrawCenterSights( Hud HUD ) {}

function ConsumeAmmo( byte FireModeNum )
{
	local WWVehicleTank VT;
	
	if ( FireModeNum == DEFAULT_FIREMODE )
	{
		ConsumeMainGunAmmo(MainGunProjectileIndex);
		return;
	}
	
	Super.ConsumeAmmo( FireModeNum );
	if( !HasAmmo(FireModeNum) && CurrentMagCount > 0 )
	{
		if( Role == ROLE_Authority )
		{
			TimeReloading();
			
			VT = WWVehicleTank(MyVehicle);
			if( VT != none )
			{
				if( VT.CoaxialMGReloadCount < 255 )
				{
					VT.CoaxialMGReloadCount++;
				}
				else
				{
					VT.CoaxialMGReloadCount=0;
				}
			}
		}
	}
}

function TryToLoadCannon( byte AmmoIndex )
{
	local WWVehicleTank VT;
	
	if ( HasMainGunAmmo(AmmoIndex) )
	{
		VT = WWVehicleTank(MyVehicle);
		
		`wwdebug("Cannon ReloadDuration:" @ VT.GetMainCannonReloadDuration(),'VH');
		
		SetTimer(VT.GetMainCannonReloadDuration(), false, 'MainCannonDelayTimer');
		
		if ( VT != None )
		{
			if (WorldInfo.NetMode == NM_Standalone)
			{
				// This function already gets called when MainGunReloadCount changes, but replication doesn't exist offline
				VT.VehicleLoadMainGun();
			}
			
			VT.MainGunReloadCount++;
		}
	}
	else
	{
		EndReloadTime();
	}
	
	bMainCannonLoaded = false;
}

simulated function TimeReloading()
{
	local float ReloadDuration;
	
	if (WWVehicleTank(MyVehicle) != none)
	{
		ReloadDuration = WWVehicleTank(MyVehicle).GetCoaxMGReloadDuration();
	}
	
	`wwdebug("MG ReloadDuration:" @ ReloadDuration,'VH');
	
	if( Role == ROLE_Authority )
	{
		bCoaxialReloading = true;
	}
	
	if ( WorldInfo.NetMode == NM_DedicatedServer || (WorldInfo.NetMode == NM_ListenServer && !Instigator.IsLocallyControlled()) )
	{
		ReloadDuration *= 0.9;
	}
	
	if( WorldInfo.NetMode != NM_DedicatedServer )
	{
		PlayReload(ReloadDuration);
	}
	
	SetTimer(ReloadDuration, false, 'ReloadComplete');
	SetTimer(ReloadDuration-0.5, false, 'ReloadAlmostComplete');
}

simulated function PlayReload(float ReloadDuration)
{
	if (WWVehicleTank(MyVehicle) != none)
	{
		WWVehicleTank(MyVehicle).VehicleCoaxMGReload();
	}
}

function ReloadComplete()
{
	if( Role == ROLE_Authority )
	{
		bCoaxialReloading = false;
		PerformReload();
	}
	
	`wwdebug("MG reloaded",'VH');
}

/* defaultproperties
{
	MainGunProjectiles(MAINGUN_SMOKE_INDEX)=class'WWVehicleProjectile_Smoke'
	
	SmokeAmmoCount=`TANK_AMMO_SMOKE
} */
