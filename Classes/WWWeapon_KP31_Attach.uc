
class WWWeapon_KP31_Attach extends ROWeapAttach_PPSH41_SMG_Drum;

defaultproperties
{
	Begin Object Name=SkeletalMeshComponent0
		SkeletalMesh=SkeletalMesh'WinterWar_WP_FIN_KP31.Mesh.FIN_KP-31_3rd'
		AnimSets(0)=AnimSet'WP_VN_3rd_Master.Anim.PPSH_3rd_anim'
		PhysicsAsset=PhysicsAsset'WP_VN_3rd_Master.Phy_Bounds.PPSH_3rd_Bounds_Physics'
		CullDistance=5000
	End Object
	
	WeaponClass=class'WWWeapon_KP31'
	
	ReloadAnims(0)=Reload_Half
	CH_ReloadAnims(0)=CH_Reload_Half
	Prone_ReloadAnims(0)=Prone_Reload_Half
}
