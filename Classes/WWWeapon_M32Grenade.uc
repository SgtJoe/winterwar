
class WWWeapon_M32Grenade extends ROWeap_Type67_Grenade
	abstract;

`include(WinterWar\Classes\WWWeaponPickupMessagesOverride.uci)

defaultproperties
{
	WeaponContentClass(0)="WinterWar.WWWeapon_M32Grenade_ActualContent"
	
	RoleSelectionImage(0)=Texture2D'WinterWar_UI.WP_Render.WP_Render_M32'
	
	InvIndex=`WI_M32GRENADE
	
	AmmoClass=class'ROAmmo_Type67_Grenade'
	
	WeaponProjectiles(DEFAULT_FIREMODE)=class'Type67GrenadeProjectile'
	WeaponProjectiles(ALTERNATE_FIREMODE)=class'Type67GrenadeProjectile'
	
	InitialNumPrimaryMags=2
}
