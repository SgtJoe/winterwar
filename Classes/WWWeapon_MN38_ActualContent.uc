
class WWWeapon_MN38_ActualContent extends WWWeapon_MN38;

defaultproperties
{
	ArmsAnimSet=AnimSet'WinterWar_WP_COMMON.Anim.MNHands_Fast'
	
	Begin Object Name=FirstPersonMesh
		DepthPriorityGroup=SDPG_Foreground
		SkeletalMesh=SkeletalMesh'WinterWar_WP_SOV_MN38.Mesh.SOV_MN38'
		PhysicsAsset=PhysicsAsset'WinterWar_WP_SOV_MN38.Phy.MN38_Physics'
		AnimSets(0)=AnimSet'WinterWar_WP_COMMON.Anim.MNHands_Fast'
		AnimTreeTemplate=AnimTree'WP_VN_VC_MN9130_rifle.animation.Sov_MN9130_Tree'
		Scale=1.0
		FOV=70
	End Object
	
	Begin Object Name=PickupMesh
		SkeletalMesh=SkeletalMesh'WinterWar_WP_SOV_MN38.Mesh.SOV_MN38_3rd'
		PhysicsAsset=PhysicsAsset'WinterWar_WP_SOV_MN38.Phy.MN38_3rd_Master_Physics'
		AnimTreeTemplate=AnimTree'WP_VN_3rd_Master.AnimTree.MN9130_3rd_Tree'
		CollideActors=true
		BlockActors=true
		BlockZeroExtent=true
		BlockNonZeroExtent=true//false
		BlockRigidBody=true
		bHasPhysicsAssetInstance=false
		bUpdateKinematicBonesFromAnimation=false
		PhysicsWeight=1.0
		RBChannel=RBCC_GameplayPhysics
		RBCollideWithChannels=(Default=TRUE,GameplayPhysics=TRUE,EffectPhysics=TRUE)
		bSkipAllUpdateWhenPhysicsAsleep=TRUE
		bSyncActorLocationToRootRigidBody=true
	End Object
	
	AttachmentClass=class'WWWeapon_MN38_Attach'
}
