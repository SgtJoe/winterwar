
class WWLocalMessageGameTankFire extends ROLocalMessageGameRedAlert;

var localized string TankOnFire;

static function string GetString(optional int Switch, optional bool bPRI1HUD, optional PlayerReplicationInfo RelatedPRI_1, optional PlayerReplicationInfo RelatedPRI_2, optional Object OptionalObject)
{
	return default.TankOnFire;
}

static function ClientReceive(PlayerController PC, optional int Switch, optional PlayerReplicationInfo RelatedPRI_1, optional PlayerReplicationInfo RelatedPRI_2, optional Object OptionalObject)
{
	super.ClientReceive(PC, Switch, RelatedPRI_1, RelatedPRI_2, OptionalObject);
	
	PC.PlaySoundBase(default.TankWillScuttleSound, true);
}
