
class WWWeapon_SVT38 extends ROProjectileWeapon
	abstract;

`include(WinterWar\Classes\WWWeaponPickupMessagesOverride.uci)

simulated function EROReloadType SelectSmartReloadType()
{
	// Can't reload if we don't have another mag to put in(or pull bullets from)
	if ( CurrentMagCount > 0 && AmmoCount < MaxAmmoCount )
	{
		// Must have another Magazine to switch to if we're gonna swap magazines
		if ( bUsesMagazines && !HasAnyAmmo() )
		{
			return RORT_Magazine;
		}
		// Make sure there's room for a stripper clip(some weapons lose the bullet in the chamber during stripper reload)
		else if ( bCanLoadStripperClip && ShouldLoadStripperClip() )
		{
			// Reload with a magazine if it would be more efficient
			if ( NewMagWouldBeFaster() )
			{
				return RORT_Magazine;
			}

			return RORT_StripperClip;
		}
		// We know at least one round is gone
		else if ( bCanLoadSingleBullet )
		{
			return RORT_SingleBullet;
		}
		// All else fails, load a new magazine, even though this one isn't empty
		else if ( bCanReloadNonEmptyMag )
		{
			return RORT_Magazine;
		}
	}

	return RORT_None;
}

/** For 'SelectSmartReloadType' */
// @Override to prevent putting more than 20 rounds into a magazine
simulated function bool ShouldLoadStripperClip()
{
	if( NumFullStripperClips <= 0 )
	{
		return false;
	}

	if ( (MaxAmmoCount - AmmoCount - 1) >= StripperClipBulletCount )
	{
		return TRUE;
	}

	return FALSE;
}

// Check whether it would be more efficient to simply swap mags and if so, whether we have a mag with enough rounds in it
simulated function bool NewMagWouldBeFaster()
{
	local int DesiredRounds;

	if( AmmoCount < MaxAmmoCount / 2 )
	{
		// Only use a mag reload if we're getting enough new rounds to make it worth while
		DesiredRounds = Max(MaxAmmoCount / 2, AmmoCount + StripperClipBulletCount + 1);

		if( FindMagWithBulletsAvailable(DesiredRounds) > -1 )
		{
			return true;
		}
	}

	return false;
}

/**
 * Do a magazine style reload
 */
function PerformMagazineReload()
{
	local bool bRemovedMag;
	local bool bDidPlusOneReload;

	if( AmmoArray.Length == 0 || CurrentReloadStatus == RORS_OpeningBolt || CurrentReloadStatus == RORS_ClosingBolt )
	{
		return;
	}

	if ( AmmoCount <= 0 )
	{
		AmmoArray.Remove(CurrentMagIndex, 1);
		bRemovedMag = true;
	}
	else
	{
		if( bPlusOneLoading )
		{
		    //If there's only one bullet left(the one in the chamber), discard the clip
		    if ( AmmoCount == 1 )
		    {
				AmmoArray.Remove(CurrentMagIndex, 1);
				bRemovedMag = true;
		    }
		    // Take a bullet away from the current mag
		    else
		    {
				AmmoArray[CurrentMagIndex] = AmmoCount - 1;
			}

			bDidPlusOneReload = true;
		}
		else
		{
			// update the current mag before we reload and switch mags
			AmmoArray[CurrentMagIndex] = AmmoCount;
		}
	}

	// We're done reloading
	CurrentReloadStatus = RORS_Complete;

 	if( AmmoArray.Length > 0 )
	{
		// Select another mag as our current mag index
		// Overridden for the M14 to make sure we always get the fullest one. Done to make sure that when we mag reload,
		// it's always better than the stripper reload that we're foregoing
		CurrentMagIndex = FindMagWithBulletsAvailable(MaxAmmoCount, true);

		// Set the current ammo for our weapon
		if( bDidPlusOneReload )
		{
			SetAmmo(AmmoArray[CurrentMagIndex] + 1);
		}
		else
		{
			SetAmmo(AmmoArray[CurrentMagIndex]);
		}

		CurrentMagCount = AmmoArray.Length - 1;
	}
	else
	{
	   // Should never get here I think, remove later - Ramm
	   `log("Reload with AmmoArray length of 0");

	   if( bDidPlusOneReload )
	   {
			SetAmmo(1);
	   }
	}

	// Update the inventory/encumbrance system
	if( bRemovedMag )
	{
		// Remove the clip from encumbrance calculations
		if ( ROPawn(Instigator) != none )
		{
			ROPawn(Instigator).ModifyEncumbrance(-1 * AmmoClass.default.Weight, self);
		}

		// Remove the clip from the category limitations
		if ( ROInventoryManager(Instigator.InvManager) != none )
		{
			ROInventoryManager(Instigator.InvManager).CategoryCounts[ROIC_Ammo] -= 1.0 / AmmoClass.default.ClipsPerSlot;
		}
	}
}


simulated function float GetSpreadMod()
{
	return 3 * super.GetSpreadMod();
}

defaultproperties
{
	WeaponContentClass(0)="WinterWar.WWWeapon_SVT38_ActualContent"
	
	RoleSelectionImage(0)=Texture2D'WinterWar_UI.WP_Render.WP_Render_SVT'
	
	WeaponClassType=ROWCT_SemiAutoRifle
	
	TeamIndex=`ALLIES_TEAM_INDEX
	
	InvIndex=`WI_SVT38
	
	Category=ROIC_Primary
	Weight=3.9 // kg
	InventoryWeight=1

	PlayerIronSightFOV=40.0

	// MAIN FIREMODE
	FiringStatesArray(0)=WeaponSingleFiring
	WeaponFireTypes(0)=EWFT_Custom
	WeaponProjectiles(0)=class'WWProjectile_SVT'
	FireInterval(0)=+0.15
	DelayedRecoilTime(0)=0.01
	Spread(0)=0.0005 // ~ 1.5 MOA, Probably. 

	// ALT FIREMODE
	FiringStatesArray(ALTERNATE_FIREMODE)=none
	//WeaponFireTypes(ALTERNATE_FIREMODE)=EWFT_Custom
	WeaponProjectiles(ALTERNATE_FIREMODE)=none
	FireInterval(ALTERNATE_FIREMODE)=+0.15
	DelayedRecoilTime(ALTERNATE_FIREMODE)=0.01
	Spread(ALTERNATE_FIREMODE)=0

	PreFireTraceLength=2500 //50 Meters
	FireTweenTime=0.08

	//ShoulderedSpreadMod=6.0
	//HippedSpreadMod=10.0

	// AI
	MinBurstAmount=1
	MaxBurstAmount=3
	BurstWaitTime=1.0

	// Recoil
	maxRecoilPitch=450
	minRecoilPitch=450
	maxRecoilYaw=100
	minRecoilYaw=-100
	RecoilRate=0.1
	RecoilMaxYawLimit=500
	RecoilMinYawLimit=65035
	RecoilMaxPitchLimit=2000
	RecoilMinPitchLimit=63535
	RecoilISMaxYawLimit=500
	RecoilISMinYawLimit=65035
	RecoilISMaxPitchLimit=1500
	RecoilISMinPitchLimit=64035
	RecoilBlendOutRatio=0.6
	RecoilViewRotationScale=1.0

	InstantHitDamage(0)=179
	InstantHitDamage(1)=179

	InstantHitDamageTypes(0)=class'WWDmgType_SVT'
	InstantHitDamageTypes(1)=class'WWDmgType_SVT'

	MuzzleFlashSocket=MuzzleFlashSocket
	MuzzleFlashPSCTemplate=ParticleSystem'WinterWar_FX.ParticleSystems.FX_WW_MuzzleFlash_1stP_Rifles_round'
	MuzzleFlashDuration=0.33
	MuzzleFlashLightClass=class'ROGame.RORifleMuzzleFlashLight'

	// Shell eject FX
	ShellEjectSocket=ShellEjectSocket
	ShellEjectPSCTemplate=ParticleSystem'FX_VN_Weapons.ShellEjects.FX_Wep_ShellEject_VC_MN9130'

	bHasIronSights=true;

	//Equip and putdown
	WeaponPutDownAnim=Svt40_putaway
	WeaponEquipAnim=Svt40_pullout
	WeaponDownAnim=Svt40_Down
	WeaponUpAnim=Svt40_Up

	// Fire Anims
	//Hip fire
	WeaponFireAnim(0)=Svt40_shoot
	WeaponFireAnim(1)=Svt40_shoot
	WeaponFireLastAnim=Svt40_shootLAST
	//Shouldered fire
	WeaponFireShoulderedAnim(0)=Svt40_shoot
	WeaponFireShoulderedAnim(1)=Svt40_shoot
	WeaponFireLastShoulderedAnim=Svt40_shootLAST
   //Fire using iron sights
	WeaponFireSightedAnim(0)=Svt40_iron_shoot
	WeaponFireSightedAnim(1)=Svt40_iron_shoot
	WeaponFireLastSightedAnim=Svt40_iron_shootLAST

	// Idle Anims
	//Hip Idle
	WeaponIdleAnims(0)=Svt40_shoulder_idle
	WeaponIdleAnims(1)=Svt40_shoulder_idle
	// Shouldered idle
	WeaponIdleShoulderedAnims(0)=Svt40_shoulder_idle
	WeaponIdleShoulderedAnims(1)=Svt40_shoulder_idle
	//Sighted Idle
	WeaponIdleSightedAnims(0)=Svt40_iron_idle
	WeaponIdleSightedAnims(1)=Svt40_iron_idle

	// Prone Crawl
	WeaponCrawlingAnims(0)=Svt40_CrawlF
	WeaponCrawlStartAnim=Svt40_Crawl_into
	WeaponCrawlEndAnim=Svt40_Crawl_out

	//Reloading
	WeaponReloadEmptyMagAnim=SVT40_reloadempty
	WeaponReloadNonEmptyMagAnim=SVT40_reloadhalf
	WeaponReloadStripperAnim=SVT40_reloadStriper_half
	
	// Ammo check
	WeaponAmmoCheckAnim=Svt40_ammocheck

	// Sprinting
	WeaponSprintStartAnim=Svt40_sprint_into
	WeaponSprintLoopAnim=SVT38_Sprint
	WeaponSprintEndAnim=Svt40_sprint_out
	Weapon1HSprintStartAnim=Svt40_ger_sprint_into
	Weapon1HSprintLoopAnim=Svt40_ger_sprint
	Weapon1HSprintEndAnim=Svt40_ger_sprint_out

	// Mantling
	WeaponMantleOverAnim=Svt40_Mantle

	// Melee anims
	WeaponMeleeAnims(0)=Svt40_Bash
	WeaponMeleeHardAnim=Svt40_BashHard
	MeleePullbackAnim=Svt40_Pullback
	MeleeHoldAnim=Svt40_Pullback_Hold

	EquipTime=+0.75
	PutDownTime=+0.50

	bDebugWeapon = false

  	BoltControllerNames[0]=BoltSlide_SVT40

	ISFocusDepth=30
	ISFocusBlendRadius=12

	// Ammo
	AmmoClass=class'ROAmmo_762x39_SKSStripper'
	MaxAmmoCount=11
	bUsesMagazines=true
	InitialNumPrimaryMags=8
	bLosesRoundOnReload=true
	bPlusOneLoading=true
	bCanReloadNonEmptyMag=true
	bCanLoadStripperClip=true
	bCanLoadSingleBullet=false
	StripperClipBulletCount=5
	PenetrationDepth=22.25
	MaxPenetrationTests=3
	MaxNumPenetrations=2

	PlayerViewOffset=(X=0.0,Y=8.0,Z=-5)
	ZoomInRotation=(Pitch=-910,Yaw=0,Roll=2910)
	ShoulderedTime=0.35
	ShoulderedPosition=(X=1.5,Y=4.5,Z=-2.0)//(X=2.0,Y=5.0,Z=-2.5)// (X=0,Y=1,Z=-1.4) (X=2.0,Y=5.0,Z=-2.5)
	ShoulderRotation=(Pitch=-500,Yaw=0,Roll=2500)
	IronSightPosition=(X=0,Y=0,Z=-0.03)

	// Free Aim variables
	bUsesFreeAim=true
	FullFreeAimISMaxYaw=350
	FullFreeAimISMinYaw=65185
	FullFreeAimISMaxPitch=250
	FullFreeAimISMinPitch=65285

	Begin Object Class=ForceFeedbackWaveform Name=ForceFeedbackWaveformShooting1
		Samples(0)=(LeftAmplitude=30,RightAmplitude=50,LeftFunction=WF_Constant,RightFunction=WF_Constant,Duration=0.150)
	End Object
	WeaponFireWaveForm=ForceFeedbackWaveformShooting1

	CollisionCheckLength=53.0

	FireCameraAnim[0]=CameraAnim'1stperson_Cameras.Anim.Camera_MN9130_Shoot'
	FireCameraAnim[1]=CameraAnim'1stperson_Cameras.Anim.Camera_MN9130_Shoot'

	SightSlideControlName=Sight_Slide
	SightRotControlName=Sight_Rotation

	SightRanges[0]=(SightRange=100,SightPitch=10,SightSlideOffset=0.06,SightPositionOffset=-0.03)
	SightRanges[1]=(SightRange=200,SightPitch=105,SightSlideOffset=0.175,SightPositionOffset=-0.075)
	SightRanges[2]=(SightRange=300,SightPitch=215,SightSlideOffset=0.3,SightPositionOffset=-0.13)
	SightRanges[3]=(SightRange=400,SightPitch=350,SightSlideOffset=0.42,SightPositionOffset=-0.19)
	SightRanges[4]=(SightRange=500,SightPitch=510,SightSlideOffset=0.55,SightPositionOffset=-0.265)
	
	SuppressionPower=20
}
