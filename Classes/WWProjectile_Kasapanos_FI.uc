
class WWProjectile_Kasapanos_FI extends ROSatchelChargeProjectile;

defaultproperties
{
	Speed=600
	MinSpeed=300
	MaxSpeed=700
	MinTossSpeed=300
	MaxTossSpeed=500
	MomentumTransfer=8000
	
	MyDamageType=class'WWDmgType_Kasapanos'
	
	Begin Object Name=ThowableMeshComponent
		SkeletalMesh=SkeletalMesh'WinterWar_WP_FIN_KASAPANOS.Mesh.FIN_Kasapanos_FactoryIssue_3rd'
		PhysicsAsset=PhysicsAsset'WP_VN_3rd_Projectile.Phy.Type67_Projectile_3rd_Physics'
	End Object
}
