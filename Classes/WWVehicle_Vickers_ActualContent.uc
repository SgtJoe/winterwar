
class WWVehicle_Vickers_ActualContent extends WWVehicle_T26_ActualContent
	placeable;

DefaultProperties
{
	Team=`AXIS_TEAM_INDEX
	
	Begin Object Name=ROSVehicleMesh
		SkeletalMesh=SkeletalMesh'WinterWar_VH_USSR_T-26.Mesh.T-26_Rig'
		Materials[0]=MaterialInstanceConstant'WinterWar_VH_FIN_VICKERS.MIC.VICKERS_M'
	End Object
	
	DestroyedMats(0)=MaterialInstanceConstant'WinterWar_VH_FIN_VICKERS.MIC.VICKERS_WRECK_M'
	
	DestroyedMatsTurret(0)=MaterialInstanceConstant'WinterWar_VH_FIN_VICKERS.MIC.VICKERS_WRECK_M'
	
}
