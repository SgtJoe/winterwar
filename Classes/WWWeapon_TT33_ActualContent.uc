
class WWWeapon_TT33_ActualContent extends WWWeapon_TT33;

simulated function SetupArmsAnim()
{
	super.SetupArmsAnim();
	
	// ArmsMesh.AnimSets has slots 0-2-3 filled, so we need to back fill slot 1 and then move to slot 4
	ROPawn(Instigator).ArmsMesh.AnimSets[1] = SkeletalMeshComponent(Mesh).AnimSets[0];
	ROPawn(Instigator).ArmsMesh.AnimSets[4] = SkeletalMeshComponent(Mesh).AnimSets[1];
}

DefaultProperties
{
	ArmsAnimSet=AnimSet'WP_VN_VC_Tokarev_TT33.Animation.WP_TT33Hands'
	
	Begin Object Name=FirstPersonMesh
		DepthPriorityGroup=SDPG_Foreground
		SkeletalMesh=SkeletalMesh'WP_VN_VC_Tokarev_TT33.Mesh.Sov_TT_33'
		PhysicsAsset=PhysicsAsset'WP_VN_VC_Tokarev_TT33.Phys.Sov_TT_33_Physics'
		AnimSets(0)=AnimSet'WP_VN_VC_Tokarev_TT33.Animation.WP_TT33Hands'
		AnimSets(1)=AnimSet'WinterWar_WP_SOV_TT33.Anim.WP_TT33Hands_New'
		AnimTreeTemplate=AnimTree'WP_VN_VC_Tokarev_TT33.Animation.Sov_TT33_Tree'
		Scale=1.0
		FOV=70
	End Object
	
	Begin Object Name=PickupMesh
		SkeletalMesh=SkeletalMesh'WP_VN_3rd_Master.Mesh.TT33_3rd_master'
		PhysicsAsset=PhysicsAsset'WP_VN_3rd_Master.Phy.TT33_3rd_Master_Physics'
		CollideActors=true
		BlockActors=true
		BlockZeroExtent=true
		BlockNonZeroExtent=true//false
		BlockRigidBody=true
		bHasPhysicsAssetInstance=false
		bUpdateKinematicBonesFromAnimation=false
		PhysicsWeight=1.0
		RBChannel=RBCC_GameplayPhysics
		RBCollideWithChannels=(Default=TRUE,GameplayPhysics=TRUE,EffectPhysics=TRUE)
		bSkipAllUpdateWhenPhysicsAsleep=TRUE
		bSyncActorLocationToRootRigidBody=true
	End Object
	
	AttachmentClass=class'WWWeapon_TT33_Attach'
}
