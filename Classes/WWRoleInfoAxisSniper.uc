
class WWRoleInfoAxisSniper extends WWRoleInfoAxis;

DefaultProperties
{
	RoleType=RORIT_Marksman
	ClassTier=3
	ClassIndex=`RI_SNIPER
	
	Items[RORIGM_Default]={(
		PrimaryWeapons=(class'WWWeapon_MN9130_Scoped'),
		
		SecondaryWeapons=(class'WWWeapon_Luger')
	)}
	
	bAllowPistolsInRealism=true
	
	// For some reason bot snipers don't work, they just stand in spawn twitching.
	// We don't have time to investigate so just do this.
	bBotSelectable=false
	
	ClassIcon=Texture2D'VN_UI_Textures.menu.class_icon_sniper'
}
