
class WWGameInfo extends ROGameInfo
	config(Game_WinterWar_GameInfo)
	HideDropDown;

var localized string FinnishBotNames[32];
var localized string RussianBotNames[32];

static event class<GameInfo> SetGameType(string MapName, string Options, string Portal)
{
	local class<GameInfo> NewGameType;
	local array<ROUIResourceDataProvider> ProviderList;
	local ROUIDataProvider_GameModeInfo Provider;
	local string ThisMapPrefix;
	local int i, MapPrefixPos;
	
	ReplaceText(MapName, "UEDPIE", "");
	
	if (Left(MapName, InStr(MapName, "-")) ~= "WWTE")
	{
		return class'WWGameInfoTerritories';
	}
	else if ( Left(MapName, InStr(MapName, "-")) ~= "WWSU")
	{
		return class'WWGameInfoSupremacy';
	}
	
	MapPrefixPos = InStr(MapName,"-");
	ThisMapPrefix = left(MapName,MapPrefixPos);
	
	class'ROUIDataStore_MenuItems'.static.GetAllResourceDataProviders(class'ROUIDataProvider_GameModeInfo', ProviderList);
	for (i = 0; i < ProviderList.Length; i++)
	{
		Provider = ROUIDataProvider_GameModeInfo(ProviderList[i]);
		if ( Provider.Prefixes ~= ThisMapPrefix )
		{
			NewGameType = class<GameInfo>(DynamicLoadObject(Provider.GameMode,class'Class'));
			if ( NewGameType != None )
			{
				return NewGameType;
			}
		}
	}
	
	`wwlog("CANNOT SET GAMETYPE", 'ERROR');
	return default.class;
}

defaultproperties
{
	MenuMusicTrack=none
}
