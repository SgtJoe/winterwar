
class WWWeapon_SVT38_ActualContent extends WWWeapon_SVT38;

simulated function SetupArmsAnim()
{
	super.SetupArmsAnim();
	
	// ArmsMesh.AnimSets has slots 0-2-3 filled, so we need to back fill slot 1 and then move to slot 4
	ROPawn(Instigator).ArmsMesh.AnimSets[1] = SkeletalMeshComponent(Mesh).AnimSets[0];
	ROPawn(Instigator).ArmsMesh.AnimSets[4] = SkeletalMeshComponent(Mesh).AnimSets[1];
}

DefaultProperties
{
	ArmsAnimSet=AnimSet'WinterWar_WP_SOV_SVT38.Anim.WP_Svt40Hands'
	
	Begin Object Name=FirstPersonMesh
		DepthPriorityGroup=SDPG_Foreground
		SkeletalMesh=SkeletalMesh'WinterWar_WP_SOV_SVT38.Mesh.SOV_SVT38'
		PhysicsAsset=None
		AnimSets(0)=AnimSet'WinterWar_WP_SOV_SVT38.Anim.WP_Svt40Hands'
		AnimSets(1)=AnimSet'WinterWar_WP_SOV_SVT38.Anim.WP_SVT38Hands'
		Animations=AnimTree'WinterWar_WP_SOV_SVT38.Anim.Sov_Svt40_Tree'
		Scale=1.0
		FOV=70
	End Object
	
	Begin Object Name=PickupMesh
		SkeletalMesh=SkeletalMesh'WinterWar_WP_SOV_SVT38.Mesh.SOV_SVT38_3rd'
		PhysicsAsset=PhysicsAsset'WinterWar_WP_SOV_SVT38.Phy.SVT40_3rd_Master_Physics'
		AnimTreeTemplate=AnimTree'WinterWar_WP_SOV_SVT38.Anim.SOV_SVT38_3rd_Tree'
		CollideActors=true
		BlockActors=true
		BlockZeroExtent=true
		BlockNonZeroExtent=true//false
		BlockRigidBody=true
		bHasPhysicsAssetInstance=false
		bUpdateKinematicBonesFromAnimation=false
		PhysicsWeight=1.0
		RBChannel=RBCC_GameplayPhysics
		RBCollideWithChannels=(Default=TRUE,GameplayPhysics=TRUE,EffectPhysics=TRUE)
		bSkipAllUpdateWhenPhysicsAsleep=TRUE
		bSyncActorLocationToRootRigidBody=true
	End Object
	
	AttachmentClass=class'WWWeapon_SVT38_Attach'
	
	WeaponFireSnd(DEFAULT_FIREMODE)=(DefaultCue=AkEvent'WW_WEP_L1A1.Play_WEP_L1A1_Single_3P', FirstPersonCue=AkEvent'WW_WEP_L1A1.Play_WEP_L1A1_Fire_Single')
	WeaponFireSnd(ALTERNATE_FIREMODE)=(DefaultCue=AkEvent'WW_WEP_L1A1.Play_WEP_L1A1_Single_3P', FirstPersonCue=AkEvent'WW_WEP_L1A1.Play_WEP_L1A1_Fire_Single')
}
