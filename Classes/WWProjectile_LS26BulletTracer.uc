
class WWProjectile_LS26BulletTracer extends ROBulletTracer;

defaultproperties
{
	BallisticCoefficient=0.451 // Steel Core Ball ammo. 148.151 grain, .309 cal, 0.925 inch nose length. 
	
	MyDamageType=class'WWDmgType_LS26Bullet'
	
	Speed=40000 // 800 M/S
	MaxSpeed=40000 // 800 M/S
	
	ProjFlightTemplate=ParticleSystem'FX_VN_Weapons.Tracers.FX_Wep_Gun_A_MGTracer_North'
	ProjExplosionTemplate=ParticleSystem'FX_VN_Weapons.Tracers.FX_Wep_Gun_A_MGTracer_Explode_North'
	DeflectionTemplate=ParticleSystem'FX_VN_Weapons.Tracers.FX_Wep_Gun_A_MGTracer_Deflect_North'
	
	TracerLightClass=none
}
