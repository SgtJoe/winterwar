
class WWVehicleTransport extends ROVehicleTransport
	abstract;

var array<MaterialInstanceConstant> DestroyedMats, DestroyedMatsTurret;

var repnotify bool			CaughtFire;
var repnotify Controller	EngineFireInstigator;

var repnotify bool DetrackedRight, DetrackedBoth;

var float BurnOutPhase1Timer, BurnOutPhase2Timer, BurnOutPhase3Timer;

var array< class<DamageType> > RelatedDamageTypes;

var	ParticleSystemComponent SmokeEmitter, FireEmitter;
var ParticleSystem SmokeEmitterTemplate, FireEmitterTemplate;
var name SmokeEmitterSocketName, FireEmitterSocketName;

var AudioComponent EngineStartupAudio, EngineRunAudio, EngineShiftUpAudio, EngineShiftDownAudio, EngineShutdownAudio, TracksDestroyedAudio;

var AudioComponent MGAmbientSound, MGReloadSound;
var SoundCue MGStopSound;

replication
{
	if (Role == ROLE_Authority && bNetDirty)
		CaughtFire, EngineFireInstigator;
	
	if (bNetDirty)
		DetrackedRight, DetrackedBoth;
}

simulated event ReplicatedEvent(name VarName)
{
	if (VarName == 'CaughtFire')
	{
		EngineCaughtFire();
	}
	else if (VarName == 'DetrackedRight')
	{
		TracksDestroyed(DetrackedRight);
	}
	else if (VarName == 'DetrackedBoth')
	{
		TracksDestroyed(true);
		TracksDestroyed(false);
	}
	else
	{
		super.ReplicatedEvent(VarName);
	}
}

simulated function PostBeginPlay()
{
	local int i, NewSeatIndexHealths, LoopMax;
	local ROTeamInfo ROTI;
	local ROGameReplicationInfo ROGRI;
	
	// ROVehicleTreaded::PostBeginPlay tries to set a bunch of audio components that we don't have, so skip it
	super(ROVehicle).PostBeginPlay();
	
	if (bDeleteMe)
	{
		return;
	}
	
	SeatIndexHullMG = GetHullMGSeatIndex();
	
	if (WorldInfo.NetMode != NM_DedicatedServer && Mesh != None)
	{
		LeftTreadMaterialInstance = Mesh.CreateAndSetMaterialInstanceConstant(1);
		RightTreadMaterialInstance = Mesh.CreateAndSetMaterialInstanceConstant(2);
		Mesh.MinLodModel = 0;
	}
	
	for (i = 0; i < VehHitZones.length; i++)
	{
		VehHitZoneHealths[i] = 255;
		
		if( VehHitZones[i].VehicleHitZoneType == VHT_CrewHead || VehHitZones[i].VehicleHitZoneType == VHT_CrewBody )
		{
			CrewVehHitZoneIndexes[CrewVehHitZoneIndexes.Length] = i;
		}
	}
	
	if (Role == ROLE_Authority)
	{
		if(SeatProxies.Length > 7)
			LoopMax = 7;
		else
			LoopMax = SeatProxies.Length;
		
		for ( i = 0; i < LoopMax; i++ )
		{
			NewSeatIndexHealths = (NewSeatIndexHealths - (NewSeatIndexHealths & (15 << (4 * i)))) | (int(SeatProxies[i].Health / 6.6666666) << (4 * i));
		}
		
		ReplicatedSeatProxyHealths = NewSeatIndexHealths;
		
		if( LoopMax < SeatProxies.Length )
		{
			LoopMax = SeatProxies.Length;
			NewSeatIndexHealths = 0;
			
			for ( i = 7; i < LoopMax; i++ )
			{
				NewSeatIndexHealths = (NewSeatIndexHealths - (NewSeatIndexHealths & (15 << (4 * (i - 7))))) | (int(SeatProxies[i].Health / 6.6666666) << (4 * (i - 7)));
			}
			ReplicatedSeatProxyHealths2 = NewSeatIndexHealths;
		}
		
		if (!self.IsTank())
		{
			ROGRI = ROGameReplicationInfo(WorldInfo.Game.GameReplicationInfo);
			ROTI = ROTeamInfo( ROGRI.Teams[Team] );
			ROTI.AddTransportToTeam(self);
		}
	}
	
	for( i = CrewHitZoneStart; i <= CrewHitZoneEnd; i++ )
	{
		Mesh.HideBoneByName(VehHitZones[i].CrewBoneName, PBO_Disable);
	}
	
	Mesh.UnHideBoneByName('Root_Track_R');
	Mesh.UnHideBoneByName('Root_Track_L');
	
	Mesh.HideBoneByName('R_TRACK_DAM', PBO_Disable);
	Mesh.HideBoneByName('L_TRACK_DAM', PBO_Disable);
}

function OneSecondLoopingTimer() {}

simulated function Tick(float DeltaTime)
{
	super.Tick(DeltaTime);
	
	if (EngineRunAudio != none && EngineRunAudio.IsPlaying())
	{
		EngineRunAudio.SetFloatParameter('RPMParam', (ROVehicleSimTreaded(SimObj).EngineRPM / 2800.0));
		
		if (OutputGear > DelayedOutputGear)
		{
			if (EngineShiftUpAudio != none && !EngineShiftUpAudio.IsPlaying())
			{
				`wwdebug("Shift Up",'VH');
				EngineShiftUpAudio.Play();
			}
		}
		else if (OutputGear < DelayedOutputGear)
		{
			if (EngineShiftDownAudio != none && !EngineShiftDownAudio.IsPlaying())
			{
				`wwdebug("Shift Down",'VH');
				EngineShiftDownAudio.Play();
			}
		}
	}
}

simulated function StartEngineSoundTimed()
{
	super.StartEngineSoundTimed();
	
	if (EngineStartupAudio != none && !EngineStartupAudio.IsPlaying())
	{
		EngineStartupAudio.Play();
	}
}

simulated function StartEngineSound()
{
	if (EngineRunAudio != none && !EngineRunAudio.IsPlaying())
	{
		EngineRunAudio.Play();
	}
	
	ClearTimer('StartEngineSound');
	ClearTimer('StopEngineSound');
}

simulated function StopEngineSound()
{
	if (EngineRunAudio != none)
	{
		EngineRunAudio.Stop();
	}
	
	if (EngineShutdownAudio != none && !EngineShutdownAudio.IsPlaying())
	{
		EngineShutdownAudio.Play();
	}
	
	ClearTimer('StartEngineSound');
	ClearTimer('StopEngineSound');
}

simulated function StopVehicleSounds()
{
	`wwdebug("",'VH');
	
	super.StopVehicleSounds();
	
	if (EngineStartupAudio != none)
	{
		EngineStartupAudio.Stop();
	}
	
	if (EngineRunAudio != none)
	{
		EngineRunAudio.Stop();
	}
	
	if (EngineShutdownAudio != none)
	{
		EngineShutdownAudio.Stop();
	}
}

function float GetHullMGReloadDuration()
{
	if (MGReloadSound.SoundCue != none)
	{
		return MGReloadSound.SoundCue.GetCueDuration();
	}
	
	return 7.0;
}

simulated function VehicleHullMGReload()
{
	`wwdebug("playing MG reload sound", 'VH');
	
	if (MGReloadSound != None && !MGReloadSound.IsPlaying())
	{
		MGReloadSound.Play();
	}
}

function EvaluateBackSeatDrivers()
{
`ifndef(RELEASE)
	super(ROVehicle).EvaluateBackSeatDrivers();
`endif
}

function HandleMomentum( vector Momentum, Vector HitLocation, class<DamageType> DamageType, optional TraceHitInfo HitInfo )
{
	if (class<RODmgType_SmallArmsBullet>(DamageType) != none)
	{
		AddVelocity( Momentum, HitLocation, DamageType, HitInfo );
	}
}

static function bool IsDamageTypeRelated(class<DamageType> DamageType)
{
	local int i;
	
	for (i = 0; i < default.RelatedDamageTypes.length; i++)
	{
		if (ClassIsChildOf(DamageType, default.RelatedDamageTypes[i]))
		{
			return true;
		}
	}
	
	return false;
}

simulated function bool HasDamagedPart()
{
	return bEngineDamaged || bLeftTrackDamaged || bRightTrackDamaged || bLeftBrakeDamaged || bRightBrakeDamaged || bRightSteerWheelDamaged || bLeftSteerWheelDamaged;
}

function bool HasDestroyedPart()
{
	return bEngineDestroyed || bLeftTrackDestroyed || bRightTrackDestroyed || bLeftBrakeDestroyed || bRightBrakeDestroyed || bMainCannonDestroyed || 
		bCoaxMGDestroyed || bTraverseMoterDestroyed || bTurretRingDisabled || bLeftAmmoDestroyed || bRightAmmoDestroyed || bLeftFuelTankDestroyed || 
		bRightFuelTankDestroyed || bCenterFuelTankDestroyed || bRightSteerWheelDestroyed || bLeftSteerWheelDestroyed;
}

simulated event TakeDamage(int Damage, Controller EventInstigator, vector HitLocation, vector Momentum, class<DamageType> DamageType, optional TraceHitInfo HitInfo, optional Actor DamageCauser)
{
	local int HitIndex, HitSeatIndex;
	local TraceHitInfo DriverHitInfo;
	local ROPawn ROP;
	local bool hitMine;
	
	super.TakeDamage(Damage, EventInstigator, HitLocation, Momentum, DamageType, HitInfo, DamageCauser);
	
	if (ShouldInstaKill(DamageType, Damage))
	{
		Died(EventInstigator, DamageType, HitLocation);
	}
	
	// Track hitboxes are super unreliable, so check for mines here
	hitMine = (class<WWDmgType_ATMine>(DamageType) != none);
	
	if (hitMine)
	{
		`wwdebug("Taking Mine damage", 'VH');
	}
	
	if (HitInfo.BoneName != '' || hitMine)
	{
		HitIndex = VehHitZones.Find('ZoneName', HitInfo.BoneName);
		
		if (HitIndex < 0)
		{
			HitIndex = VehHitZones.Find('CrewBoneName', HitInfo.BoneName);
		}
		
		if (Damage > 30) // Stops spam from SpotFires
		{
			`wwdebug("taking" @ Damage @ DamageType @ "in" @ HitInfo.BoneName @ "Zone" @ HitIndex, 'VH');
		}
		
		if (HitIndex >= 0 || hitMine)
		{
			if (VehHitZones[HitIndex].VehicleHitZoneType == VHT_Engine && class<RODmgType_Molotov>(DamageType) != none && !CaughtFire)
			{
				if (Role == ROLE_Authority)
				{
					EngineFireInstigator = EventInstigator;
					CaughtFire = true;
					bNetDirty = true;
					
					if (WorldInfo.NetMode == NM_Standalone)
					{
						EngineCaughtFire();
					}
					
					`wwdebug("Setting Timers", 'VH');
					SetTimer(BurnOutPhase1Timer, false, 'BurnOutPhase1');
					SetTimer(BurnOutPhase2Timer, false, 'BurnOutPhase2');
					SetTimer(BurnOutPhase3Timer, false, 'BurnOutPhase3');
				}
			}
			
			// if (/* VehHitZones[HitIndex].VehicleHitZoneType == VHT_Track && */ class<WWDmgType_ATMine>(DamageType) != none)
			if (hitMine)
			{
				bLeftTrackDestroyed = true;
				bRightTrackDestroyed = true;
				
				// Give the minelayer a point or two
				// TODO: eventually add a custom message
				if (EventInstigator != none && Role == ROLE_Authority)
				{
					ROGameInfo(WorldInfo.Game).AddDelayedKillMessage(ROKMT_VehicleKillAssist, ROGameReplicationInfo(WorldInfo.GRI).ElapsedTime, EventInstigator.PlayerReplicationInfo);
				}
				
				if (TracksDestroyedAudio != none)
				{
					TracksDestroyedAudio.Play();
				}
				
				if (HitInfo.BoneName == 'Root_Track_R')
				{
					DetrackedRight = true;
					TracksDestroyed(true);
				}
				else if (HitInfo.BoneName == 'Root_Track_L')
				{
					// DetrackedLeft = true;
					DetrackedRight = false;
					TracksDestroyed(false);
				}
				else
				{
					`wwdebug("COULD NOT DETERMINE TRACKS POSITION!", 'VH');
					DetrackedBoth = true;
					TracksDestroyed(true);
					TracksDestroyed(false);
				}
			}
			
			if (VehHitZones[HitIndex].VehicleHitZoneType >= VHT_CrewBody)
			{
				`wwdebug("Hit Passenger", 'VH');
				
				// Since the passengers on the T-20 are so exposed, let's try this
				// if (DamageType.default.bLocationalHit || class<RODmgType_Fire>(DamageType) != none)
				// {
					HitSeatIndex = VehHitZones[HitIndex].CrewSeatIndex;
					
					ROP = ROPawn(Seats[HitSeatIndex].SeatPawn.Driver);
					
					if (ROP != none)
					{
						DriverHitInfo = HitInfo;
						
						if (VehHitZones[HitIndex].VehicleHitZoneType == VHT_CrewBody )
						{
							DriverHitInfo.BoneName = ROP.PlayerHitZones[2].ZoneName; // Chest
						}
						else
						{
							DriverHitInfo.BoneName = ROP.PlayerHitZones[0].ZoneName; // Head
						}
						
						if (Damage > 0)
						{
							`wwdebug("Seat" @ HitSeatIndex @ "taking" @ Damage @ "from" @ DamageType, 'VH');
							
							ROP.TakeDamage(Damage, EventInstigator, HitLocation, Momentum, DamageType, DriverHitInfo, DamageCauser);
						}
					}
					else
					{
						`wwdebug("Nobody in Seat" @ HitSeatIndex, 'VH');
					}
				// }
			}
		}
	}
}

function bool ShouldInstakill(class<DamageType> DamageType, int Damage)
{
	if (class<RODmgType_Satchel>(DamageType) != none)
	{
		return (Damage >= 460);
	}
	
	return (class<RODmgTypeArtillery>(DamageType) != none ||
			class<RODmgType_AirCrash>(DamageType) != none ||
			class<RODmgTypeMineField>(DamageType) != none ||
			class<WWDmgType_VehicleFire>(DamageType) != none);
}

simulated function TracksDestroyed(bool RightSide)
{
	if (RightSide)
	{
		Mesh.HideBoneByName('Root_Track_R', PBO_Disable);
		Mesh.UnHideBoneByName('R_TRACK_DAM');
	}
	else
	{
		Mesh.HideBoneByName('Root_Track_L', PBO_Disable);
		Mesh.UnHideBoneByName('L_TRACK_DAM');
	}
}

simulated function EngineCaughtFire()
{
	local int i;
	
	if (FireEmitter == none && FireEmitterTemplate != none && FireEmitterSocketName != '')
	{
		FireEmitter = WorldInfo.MyEmitterPool.SpawnEmitterMeshAttachment(FireEmitterTemplate, Mesh, FireEmitterSocketName, true);
	}
	else
	{
		`wwdebug("Could not spawn emitter!", 'VH');
	}
	
	/* if (SmokeEmitter == none && SmokeEmitterTemplate != none && SmokeEmitterSocketName != '')
	{
		SmokeEmitter = WorldInfo.MyEmitterPool.SpawnEmitterMeshAttachment(SmokeEmitterTemplate, Mesh, SmokeEmitterSocketName, true);
	}
	else
	{
		`wwdebug("Could not spawn emitter!", 'VH');
	} */
	
	for (i = 0; i < Seats.Length; i++)
	{
		if (ROPlayerController(Seats[i].SeatPawn.Controller) != none && (Seats[i].SeatPawn) != none)
		{
			ROPlayerController(Seats[i].SeatPawn.Controller).ReceiveLocalizedMessage(class'WWLocalMessageGameTankFire');
		}
	}
}

simulated function BurnOutPhase1()
{
	/* local int i;
	
	`wwdebug("", 'VH');
	
	for (i = 0; i < Seats.Length; i++)
	{
		if (Seats[i].SeatPawn) != none && Rand(10) < 7)
		{
			`wwdebug("Killing Seat" @ i, 'VH');
			Seats[i].SeatPawn.TakeDamage(9999, EngineFireInstigator, vect(0,0,0), vect(0,0,0), class'WWDmgType_VehicleFire');
		}
	} */
}

simulated function BurnOutPhase2()
{
	`wwdebug("", 'VH');
	
	bEngineDestroyed = true;
}

simulated function BurnOutPhase3()
{
	`wwdebug("", 'VH');
	
	TakeDamage(9999, EngineFireInstigator, vect(0,0,0), vect(0,0,0), class'WWDmgType_VehicleFire');
}

simulated function BlowupVehicle()
{
	local int i;
	
	VehicleEvent('EngineStop');
	
	if (SmokeEmitter != none)
	{
		SmokeEmitter.DeactivateSystem();
		SmokeEmitter.KillParticlesForced();
	}
	
	if (FireEmitter != none)
	{
		FireEmitter.DeactivateSystem();
		FireEmitter.KillParticlesForced();
	}
	
	SmokeEmitter = none;
	FireEmitter = none;
	
	for( i = 0; i < EntryPoints.length; i++ )
	{
		if( EntryPoints[i].EntryActor != none )
		{
			EntryPoints[i].EntryActor.SetBase(none);
			EntryPoints[i].EntryActor.Destroy();
			EntryPoints[i].EntryActor = none;
		}
	}
	
	if ( WorldInfo.NetMode != NM_Client )
	{
		if (CaughtFire && bHasTurretExplosion)
		{
			DeadVehicleType = 3;
		}
		else
		{
			DeadVehicleType = 1;
		}
		
		`wwdebug(""$DeadVehicleType, 'VH');
	}
	
	bCanBeBaseForPawns = false;
	GotoState('DyingVehicle');
	AddVelocity(TearOffMomentum, TakeHitLocation, HitDamageType);
	bDeadVehicle = true;
	bStayUpright = false;
	
	if ( StayUprightConstraintInstance != None )
	{
		StayUprightConstraintInstance.TermConstraint();
	}
}

simulated state DyingVehicle
{
	simulated function SwapToDestroyedMesh()
	{
		local int i;
		
		super.SwapToDestroyedMesh();
		
		for (i = 0; i < DestroyedMats.length; i++)
		{
			Mesh.SetMaterial(i, DestroyedMats[i]);
		}
	}
}

simulated function LeaveBloodSplats(int InSeatIndex) {}

simulated exec function SwitchFireMode() {}

simulated function SpawnOrReplaceSeatProxy(int SeatIndex, ROPawn ROP, optional bool bInternalVisibility)
{
	local int i;
	local VehicleCrewProxy CurrentProxyActor;
	
	if (WorldInfo.NetMode == NM_DedicatedServer)
	{
		return;
	}
	
	for (i = 0; i < SeatProxies.Length; i++)
	{
		if (SeatIndex == i && ROP != none)
		{
			if (SeatProxies[i].ProxyMeshActor != none && SeatProxies[i].ProxyMeshActor.bIsDismembered)
			{
				SeatProxies[i].ProxyMeshActor.Destroy();
			}
			
			if (SeatProxies[i].ProxyMeshActor == none)
			{
				// Need to spawn our own proxy for custom Materials handling
				SeatProxies[i].ProxyMeshActor = Spawn(class'WWVehicleCrewProxy',self);
				SeatProxies[i].ProxyMeshActor.MyVehicle = self;
				SeatProxies[i].ProxyMeshActor.SeatProxyIndex = i;
				CurrentProxyActor = SeatProxies[i].ProxyMeshActor;
				SeatProxies[i].TunicMeshType.Characterization = class'ROPawn'.default.PlayerHIKCharacterization;
				CurrentProxyActor.Mesh.SetShadowParent(Mesh);
				CurrentProxyActor.SetLightingChannels(InteriorLightingChannels);
				CurrentProxyActor.SetLightEnvironment(InteriorLightEnvironment);
				CurrentProxyActor.SetCollision( false, false);
				CurrentProxyActor.bCollideWorld = false;
				CurrentProxyActor.SetBase(none);
				CurrentProxyActor.SetHardAttach(true);
				CurrentProxyActor.SetLocation( Location );
				CurrentProxyActor.SetPhysics( PHYS_None );
				CurrentProxyActor.SetBase( Self, , Mesh, Seats[SeatProxies[i].SeatIndex].SeatBone);
				CurrentProxyActor.SetRelativeLocation( vect(0,0,0) );
				CurrentProxyActor.SetRelativeRotation( Seats[SeatProxies[i].SeatIndex].SeatRotation );
			}
			else
			{
				CurrentProxyActor = SeatProxies[i].ProxyMeshActor;
			}
			
			CurrentProxyActor.ReplaceProxyMeshWithPawn(ROP);
			
			if (SeatProxyAnimSet != None)
			{
				CurrentProxyActor.Mesh.AnimSets[0] = SeatProxyAnimSet;
			}
			
			if (IsLocalPlayerInThisVehicle())
				CurrentProxyActor.SetVisibilityToInterior();
			else
				CurrentProxyActor.SetVisibilityToExterior();
			
			CurrentProxyActor.HideMesh(true);
		}
	}
}

function SetPendingDestroyIfEmpty(float WaitToDestroyTime) {}

event Touch(Actor Other, PrimitiveComponent OtherComp, vector HitLocation, vector HitNormal)
{
	super.Touch(Other, OtherComp, HitLocation, HitNormal);
	
	if(ROVolumeSpawnProtection(Other) != none)
	{
		if (ROVolumeSpawnProtection(Other).bEnabled && ROVolumeSpawnProtection(Other).OwningTeam != self.GetTeamNum())
		{
			OnTouchEnemySpawnVolume(ROVolumeSpawnProtection(Other));
		}
	}
}

event UnTouch(Actor Other)
{
	super.UnTouch(Other);
	
	if(ROVolumeSpawnProtection(Other) != none)
	{
		if (ROVolumeSpawnProtection(Other).OwningTeam != self.GetTeamNum())
		{
			OnUnTouchEnemySpawnVolume(ROVolumeSpawnProtection(Other));
		}
	}
}

simulated function OnTouchEnemySpawnVolume(ROVolumeSpawnProtection vol)
{
	local int i;
	
	for (i = 0; i < Seats.Length; i++)
	{
		if (Seats[i].Gun != none)
		{
			Seats[i].Gun.StopFire(0);
			Seats[i].Gun.StopFire(1);
			
			Seats[i].Gun.ClientEndFire(0);
			Seats[i].Gun.ClientEndFire(1);
			
			Seats[i].Gun.ForceEndFire();
			
			Seats[i].Gun.bPrimaryFireDisabled = true;
			Seats[i].Gun.bAlternateFireDisabled = true;
		}
		
		if (ROPlayerController(Seats[i].SeatPawn.Controller) != none)
		{
			ROPlayerController(Seats[i].SeatPawn.Controller).ReceiveLocalizedMessage(class'ROLocalMessageGameRedAlert', RORAMSG_SpawnProtectionWarning,,, vol);
		}
	}
}

simulated function OnUnTouchEnemySpawnVolume(ROVolumeSpawnProtection vol)
{
	local int i;
	
	for (i = 0; i < Seats.Length; i++)
	{
		if (Seats[i].Gun != none)
		{
			Seats[i].Gun.bPrimaryFireDisabled = false;
			Seats[i].Gun.bAlternateFireDisabled = false;
		}
		
		if (ROPlayerController(Seats[i].SeatPawn.Controller) != none)
		{
			ROPlayerController(Seats[i].SeatPawn.Controller).ReceiveLocalizedMessage(class'ROLocalMessageGameRedAlert', RORAMSG_SpawnProtectionWarningUnTouch,,, vol);
		}
	}
}

DefaultProperties
{
	Health=300
	
	WeaponPawnClass=class'WWWeaponPawn'
	
	bOpenVehicle=true
	bUseLoopedMGSound=true
	
	ExitRadius=200
	
	Begin Object Name=CollisionCylinder
		CollisionHeight=60.0
		CollisionRadius=200.0
		Translation=(X=0.0,Y=0.0,Z=0.0)
	End Object
	CylinderComponent=CollisionCylinder
	
	bDontUseCollCylinderForRelevancyCheck=true
	RelevancyHeight=70.0
	RelevancyRadius=130.0
	
	bInfantryCanUse=true
	
	DrivingPhysicalMaterial=PhysicalMaterial'VH_VN_ARVN_M113_APC.Phys.M113_PhysMat_Moving'
	DefaultPhysicalMaterial=PhysicalMaterial'VH_VN_ARVN_M113_APC.Phys.M113_PhysMat'
	
	EngineStartOffsetSecs=2.0
	EngineStopOffsetSecs=0.1
	
	SpeedoMinDegree=5461
	SpeedoMaxDegree=56000
	SpeedoMaxSpeed=1365 //100 km/h
	
	TreadSpeedParameterName=Tank_Tread_Speed
	TrackSoundParamScale=0.000033
	TreadSpeedScale=2.5
	
	ExplosionDamageType=class'RODmgType_VehicleExplosion'
	ExplosionDamage=100.0
	ExplosionRadius=300.0
	ExplosionMomentum=60000
	ExplosionInAirAngVel=1.5
	InnerExplosionShakeRadius=400.0
	OuterExplosionShakeRadius=1000.0
	ExplosionLightClass=none
	MaxExplosionLightDistance=4000.0
	TimeTilSecondaryVehicleExplosion=0//2.0f
	SecondaryExplosion=none
	bHasTurretExplosion=false
	
	CaughtFire=false
	bHitAmmo=false
	BurnOutPhase1Timer=5.0
	BurnOutPhase2Timer=10.0
	BurnOutPhase3Timer=15.0
	
	SmokeEmitterTemplate=ParticleSystem'FX_Recon.FX_Recon_Smoke'
	FireEmitterTemplate=ParticleSystem'FX_Recon.FX_Recon_Fire'
	
	ExplosionSound=AkEvent'WW_VEH_Shared.Play_VEH_Helicopter_Explode_Close'
	
	RanOverDamageType=WWDmgType_RoadRage
}
