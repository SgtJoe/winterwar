
class WWWeapon_Skis_Attach extends ROItemAttach_TunnelTool;

simulated function PickImpact3P() {}

defaultproperties
{
	Begin Object Name=SkeletalMeshComponent0
		SkeletalMesh=SkeletalMesh'WinterWar_VH_FIN_SKIS.Mesh.FIN_Skis_3rd'
		PhysicsAsset=PhysicsAsset'WP_VN_3rd_Master.Phy_Bounds.PickMattock_3rd_Bounds_Physics'
		AnimSets(0)=AnimSet'WP_VN_3rd_Master.Anim.PickMattock_3rd_Anims'
		CullDistance=5000
	End Object
	
	WeaponClass=class'WWWeapon_Skis'
}
