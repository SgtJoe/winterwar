
class WWRoleInfoAlliesAssault extends WWRoleInfoAllies;

DefaultProperties
{
	RoleType=RORIT_Rifleman
	ClassTier=2
	ClassIndex=`RI_ASSAULT
	
	Items[RORIGM_Default]={(
		PrimaryWeapons=(class'WWWeapon_AVS36',class'WWWeapon_PPD34'),
		
		OtherItems=(class'WWWeapon_RGD33')
	)}
	
	ClassIcon=Texture2D'WinterWar_UI.RoleIcons.RoleIcon_Assault'
}
