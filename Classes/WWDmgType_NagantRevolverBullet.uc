
class WWDmgType_NagantRevolverBullet extends RODmgType_SmallArmsBullet
	abstract;

defaultproperties
{
	WeaponShortName="NAGANT"
	KDamageImpulse=102.8 // kg * uu/s
	BloodSprayTemplate=ParticleSystem'FX_VN_Impacts.BloodNGore.FX_VN_BloodSpray_Clothes_small'
}
