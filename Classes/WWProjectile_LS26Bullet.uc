
class WWProjectile_LS26Bullet extends ROBullet;

defaultproperties
{
	BallisticCoefficient=0.451 // Steel Core Ball ammo. 148.151 grain, .309 cal, 0.925 inch nose length. 
	
	Damage=768
	MyDamageType=class'WWDmgType_LS26Bullet'
	
	Speed=40000 // 800 M/S
	MaxSpeed=40000 // 800 M/S
	
	// RS2. Energy transfer function
	// MN9130, DP28
	VelocityDamageFalloffCurve=(Points=((InVal=467640625,OutVal=0.5), (InVal=1870562500,OutVal=0.18)))
}
