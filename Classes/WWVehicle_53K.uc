
class WWVehicle_53K extends WWVehicleStaticATGun
	abstract;

var repnotify TakeHitInfo DeathHitInfo_ProxyDriver, DeathHitInfo_ProxyCommander;

replication
{
	if (bNetDirty)
		DeathHitInfo_ProxyDriver, DeathHitInfo_ProxyCommander;
}

simulated event ReplicatedEvent(name VarName)
{
	if (VarName == 'DeathHitInfo_ProxyDriver')
	{
		if( IsLocalPlayerInThisVehicle() )
		{
			PlaySeatProxyDeathHitEffects(0, DeathHitInfo_ProxyDriver);
		}
	}
	else if (VarName == 'DeathHitInfo_ProxyCommander')
	{
		if( IsLocalPlayerInThisVehicle() )
		{
			PlaySeatProxyDeathHitEffects(1, DeathHitInfo_ProxyCommander);
		}
	}
	else
	{
		super.ReplicatedEvent(VarName);
	}
}

function DamageSeatProxy(int SeatProxyIndex, int Damage, Controller InstigatedBy, vector HitLocation, vector Momentum, class<DamageType> DamageType, optional Actor DamageCauser)
{
	switch( SeatProxyIndex )
	{
		case 0:
			DeathHitInfo_ProxyDriver.Damage = Damage;
			DeathHitInfo_ProxyDriver.HitLocation = HitLocation;
			DeathHitInfo_ProxyDriver.Momentum = Momentum;
			DeathHitInfo_ProxyDriver.DamageType = DamageType;
			break;
		case 1:
			DeathHitInfo_ProxyCommander.Damage = Damage;
			DeathHitInfo_ProxyCommander.HitLocation = HitLocation;
			DeathHitInfo_ProxyCommander.Momentum = Momentum;
			DeathHitInfo_ProxyCommander.DamageType = DamageType;
			break;
	}
	
	super.DamageSeatProxy(SeatProxyIndex, Damage, InstigatedBy, HitLocation, Momentum, DamageType, DamageCauser);
}

simulated function int GetCommanderSeatIndex()
{
	return GetSeatIndexFromPrefix("Turret");
}

simulated function int GetGunnerSeatIndex()
{
	return GetSeatIndexFromPrefix("Turret");
}

simulated function int GetLoaderSeatIndex()
{
	return GetSeatIndexFromPrefix("Turret");
}

simulated function int GetHullMGSeatIndex()
{
	return -1;
}

simulated function bool IsInPeriscope(byte SeatIndex, byte PositionIndex)
{
	return false;
}

// For some reason the breech bone is being stupid so we have to fix it like this
simulated function FixBreechStart()
{
	SkelControlSingleBone(Mesh.FindSkelControl('BreechFix')).SetSkelControlActive(true);
}

simulated function FixBreechEnd()
{
	SkelControlSingleBone(Mesh.FindSkelControl('BreechFix')).SetSkelControlActive(false);
}

DefaultProperties
{
	HUDBodyTexture=Texture2D'WinterWar_UI.Vehicle.UI_HUD_VH_53-K_Body'
	HUDTurretTexture=Texture2D'WinterWar_UI.Vehicle.UI_HUD_VH_53-K_Cannon'
	
	ExitRadius=50
	
	Begin Object Name=CollisionCylinder
		CollisionHeight=+30.0
		CollisionRadius=+20.0
		Translation=(X=-10.0,Y=0.0,Z=35.0)
	End Object
	CylinderComponent=CollisionCylinder
	
	EntryPoints(0)=(CollisionRadius=15, CollisionHeight=25, AttachBone=Chassis, LocationOffset=(X=-10,Y=0,Z=10), SeatIndex = 1)
	
	Seats(1)={(
		CameraOffset=-420,
		SeatAnimBlendName=CommanderPositionNode,
		GunClass=class'WWVehicleWeapon_53K_Cannon',
		RangeOverlayTexture=Texture2D'WinterWar_UI.VehicleOptics.UI_HUD_VH_Optics_Range_53K',
		VignetteOverlayTexture=Texture2D'WinterWar_UI.VehicleOptics.UI_HUD_VH_Optics_CircularOverlay',
		GunSocket=(Barrel),
		GunPivotPoints=(gun_base,gun_base),
		TurretVarPrefix="Turret",
		TurretControls=(Turret_Gun,Turret_Main),
		SeatPositions=(
			(
				// Looking around
				bDriverVisible=true,
				bAllowFocus=true,
				PositionCameraTag=None,
				ViewFOV=70,
				bRotateGunOnCommand=true,
				bIgnoreWeapon=true,
				PositionUpAnim=IdleToStand,
				PositionIdleAnim=Stand,
				DriverIdleAnim=Stand,
				AlternateIdleAnim=Stand,
				SeatProxyIndex=1,
				bIsExterior=true,
				PositionFlinchAnims=(Stand),
				PositionDeathAnims=(Death)
			),
			(
				// Behind cannon
				bDriverVisible=true,
				bAllowFocus=true,
				PositionCameraTag=None,
				ViewFOV=70,
				bRotateGunOnCommand=true,
				PositionDownAnim=StandToIdle,
				PositionUpAnim=Idle,
				PositionIdleAnim=Idle,
				DriverIdleAnim=Idle,
				AlternateIdleAnim=Idle,
				LeftHandIKInfo=(IKEnabled=true,DefaultEffectorLocationTargetName=IK_LeftHandPitchControl,DefaultEffectorRotationTargetName=IK_LeftHandPitchControl),
				RightHandIKInfo=(IKEnabled=true,DefaultEffectorLocationTargetName=IK_RightHandYawControl,DefaultEffectorRotationTargetName=IK_RightHandYawControl),
				SeatProxyIndex=1,
				bIsExterior=true,
				PositionFlinchAnims=(Idle),
				PositionDeathAnims=(Death)
			),
			(
				// Scoped
				bDriverVisible=true,
				bAllowFocus=false,
				PositionCameraTag=Camera_Gunner,
				bViewFromCameraTag=true,
				ViewFOV=20,
				bCamRotationFollowSocket=true,
				bDrawOverlays=true,
				PositionDownAnim=Idle,
				PositionIdleAnim=Idle,
				DriverIdleAnim=Idle,
				AlternateIdleAnim=Idle,
				LeftHandIKInfo=
				(
					IKEnabled=true,
					DefaultEffectorLocationTargetName=IK_LeftHandPitchControl,DefaultEffectorRotationTargetName=IK_LeftHandPitchControl,
					AlternateEffectorTargets=
					(
					//	(Action=DAct_CannonReload_LH1,IKEnabled=true,PinEnabled=false,EffectorLocationTargetName=LoaderLHCannon1,EffectorRotationTargetName=LoaderLHCannon1),
					//	(Action=DAct_CannonReload_LH2,IKEnabled=true,PinEnabled=false,EffectorLocationTargetName=LoaderLHCannon2,EffectorRotationTargetName=LoaderLHCannon2),
					//	(Action=DAct_CannonReload_LH3,IKEnabled=true,PinEnabled=false,EffectorLocationTargetName=LoaderLHCannonBreech1,EffectorRotationTargetName=LoaderLHCannonBreech1),
						(Action=DAct_CannonReload_LHOff,IKEnabled=false,PinEnabled=true)
					)
				),
				RightHandIKInfo=
				(
					IKEnabled=true,
					DefaultEffectorLocationTargetName=IK_RightHandYawControl,DefaultEffectorRotationTargetName=IK_RightHandYawControl,
					AlternateEffectorTargets=
					(
					//	(Action=DAct_CannonReload_RH1,IKEnabled=true,PinEnabled=false,EffectorLocationTargetName=LoaderRHCannon1,EffectorRotationTargetName=LoaderRHCannon1),
					//	(Action=DAct_CannonReload_RH2,IKEnabled=true,PinEnabled=false,EffectorLocationTargetName=LoaderRHCannonMG1,EffectorRotationTargetName=LoaderRHCannonMG1),
						(Action=DAct_CannonReload_RHOff,IKEnabled=false,PinEnabled=true)
					)
				),
				SeatProxyIndex=1,
				bIsExterior=true,
				PositionFlinchAnims=(Idle),
				PositionDeathAnims=(Death)
			)
		),
		bSeatVisible=true,
		SeatBone=CharAttach,
		DriverDamageMult=1.0,
		InitialPositionIndex=2,
		FiringPositionIndex=2,
		WeaponTracerClass=(none,none),
		MuzzleFlashLightClass=(none,none),
	)}
	
	PassengerAnimTree=AnimTree'CHR_Playeranimtree_Master.CHR_Tanker_animtree'
	
	CrewAnimSet=		AnimSet'WinterWar_VH_USSR_53-K.Anim.CHR_53-K'
	SeatProxyAnimSet=	AnimSet'WinterWar_VH_USSR_53-K.Anim.CHR_53-K'
	
	// Crew hit zones
	CrewHitZoneStart=0
	VehHitZones(0)=(ZoneName=BODY,CrewBoneName=hitbox_body,DamageMultiplier=1.0,VehicleHitZoneType=VHT_CrewBody,CrewSeatIndex=1,SeatProxyIndex=1)
	VehHitZones(1)=(ZoneName=HEAD,CrewBoneName=hitbox_head,DamageMultiplier=1.0,VehicleHitZoneType=VHT_CrewHead,CrewSeatIndex=1,SeatProxyIndex=1)
	CrewHitZoneEnd=1
	
	// All the armour hit zones organised into plate classes
	ArmorHitZones(0)=(ZoneName=BASEPLATE,PhysBodyBoneName=Chassis,ArmorPlateName=LOWERPLATE)
	ArmorHitZones(1)=(ZoneName=FRONTPLATELOWER,PhysBodyBoneName=Chassis,ArmorPlateName=FRONTPLATE)
	ArmorHitZones(2)=(ZoneName=FRONTPLATECENTER,PhysBodyBoneName=Chassis,ArmorPlateName=FRONTPLATE)
	ArmorHitZones(3)=(ZoneName=FRONTRIGHTONE,PhysBodyBoneName=Chassis,ArmorPlateName=FRONTPLATE)
	ArmorHitZones(4)=(ZoneName=FRONTRIGHTTWO,PhysBodyBoneName=Chassis,ArmorPlateName=FRONTPLATE)
	ArmorHitZones(5)=(ZoneName=FRONTLEFTONE,PhysBodyBoneName=Chassis,ArmorPlateName=FRONTPLATE)
	ArmorHitZones(6)=(ZoneName=FRONTLEFTTWO,PhysBodyBoneName=Chassis,ArmorPlateName=FRONTPLATE)
	ArmorHitZones(7)=(ZoneName=FRONTLEFTTHREE,PhysBodyBoneName=Chassis,ArmorPlateName=FRONTPLATE)
	ArmorHitZones(8)=(ZoneName=FRONTLEFTFOUR,PhysBodyBoneName=Chassis,ArmorPlateName=FRONTPLATE)
	ArmorHitZones(9)=(ZoneName=RIGHTPLATEONE,PhysBodyBoneName=Chassis,ArmorPlateName=RIGHTPLATE)
	ArmorHitZones(10)=(ZoneName=RIGHTPLATETWO,PhysBodyBoneName=Chassis,ArmorPlateName=RIGHTPLATE)
	ArmorHitZones(11)=(ZoneName=RIGHTPLATETHREE,PhysBodyBoneName=Chassis,ArmorPlateName=RIGHTPLATE)
	ArmorHitZones(12)=(ZoneName=RIGHTPLATEFOUR,PhysBodyBoneName=Chassis,ArmorPlateName=RIGHTPLATE)
	ArmorHitZones(13)=(ZoneName=RIGHTPLATEFIVE,PhysBodyBoneName=Chassis,ArmorPlateName=RIGHTPLATE)
	ArmorHitZones(14)=(ZoneName=RIGHTPLATESIX,PhysBodyBoneName=Chassis,ArmorPlateName=RIGHTPLATE)
	ArmorHitZones(15)=(ZoneName=RIGHTPLATESEVEN,PhysBodyBoneName=Chassis,ArmorPlateName=RIGHTPLATE)
	ArmorHitZones(16)=(ZoneName=RIGHTPLATEEIGHT,PhysBodyBoneName=Chassis,ArmorPlateName=RIGHTPLATE)
	ArmorHitZones(17)=(ZoneName=LEFTPLATEONE,PhysBodyBoneName=Chassis,ArmorPlateName=LEFTPLATE)
	ArmorHitZones(18)=(ZoneName=LEFTPLATETWO,PhysBodyBoneName=Chassis,ArmorPlateName=LEFTPLATE)
	ArmorHitZones(19)=(ZoneName=LEFTPLATETHREE,PhysBodyBoneName=Chassis,ArmorPlateName=LEFTPLATE)
	ArmorHitZones(20)=(ZoneName=LEFTPLATEFOUR,PhysBodyBoneName=Chassis,ArmorPlateName=LEFTPLATE)
	ArmorHitZones(21)=(ZoneName=LEFTPLATEFIVE,PhysBodyBoneName=Chassis,ArmorPlateName=LEFTPLATE)
	ArmorHitZones(22)=(ZoneName=LEFTPLATESIX,PhysBodyBoneName=Chassis,ArmorPlateName=LEFTPLATE)
	ArmorHitZones(23)=(ZoneName=LEFTPLATESEVEN,PhysBodyBoneName=Chassis,ArmorPlateName=LEFTPLATE)
	
	// Actual data for the plates
	ArmorPlates(0)=(PlateName=LOWERPLATE,ArmorZoneType=AZT_Front,PlateThickness=7,OverallHardness=450,bHighHardness=true)
	ArmorPlates(1)=(PlateName=FRONTPLATE,ArmorZoneType=AZT_Front,PlateThickness=7,OverallHardness=450,bHighHardness=true)
	ArmorPlates(2)=(PlateName=RIGHTPLATE,ArmorZoneType=AZT_Right,PlateThickness=7,OverallHardness=450,bHighHardness=true)
	ArmorPlates(3)=(PlateName=LEFTPLATE,ArmorZoneType=AZT_Left,PlateThickness=7,OverallHardness=450,bHighHardness=true)
	
	VehicleEffects(TankVFX_Firing1)=(EffectStartTag=Cannon,EffectTemplate=ParticleSystem'WinterWar_FX.ParticleSystems.FX_WW_MuzzleFlash_T-26',EffectSocket=Barrel)
	
	ExplosionDamageType=class'RODmgType_VehicleExplosion'
	ExplosionDamage=100.0
	ExplosionRadius=300.0
	ExplosionMomentum=60000
	ExplosionInAirAngVel=1.5
	InnerExplosionShakeRadius=400.0
	OuterExplosionShakeRadius=1000.0
	ExplosionLightClass=class'ROGame.ROGrenadeExplosionLight'
	MaxExplosionLightDistance=4000.0
	TimeTilSecondaryVehicleExplosion=2.0f
	SecondaryExplosion=none
	bHasTurretExplosion=false
	TurretExplosiveForce=15000
	
	BigExplosionSocket=none
	ExplosionTemplate=none
	
	SeatTextureOffsets(0)=(PositionOffSet=(X=-100,Y=+100,Z=0),bTurretPosition=0)
	SeatTextureOffsets(1)=(PositionOffSet=(X=-6,Y=+15,Z=0),bTurretPosition=1)
	
	RelatedDamageTypes(0)=class'WWDmgType_53KShell_AP'
	RelatedDamageTypes(1)=class'WWDmgType_53KShell_AP_General'
}
