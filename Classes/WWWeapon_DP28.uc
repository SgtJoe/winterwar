
class WWWeapon_DP28 extends ROWeap_DP28_LMG
	abstract;

`include(WinterWar\Classes\WWWeaponPickupMessagesOverride.uci)

simulated function bool CanTransitionToIronSights()
{
	super.CanTransitionToIronSights();
	
	return (ROPawn(Instigator).bBipodDeployed || 
			ROPawn(Instigator).bBipodDeployedNoCover || 
			ROPawn(Instigator).bCanDeployNoCover || 
			Instigator.bIsProning || 
			bIronSightOnBringUp);
}

simulated exec function IronSights()
{
	if(!CanTransitionToIronSights())
	{
		PlayerController(Instigator.Controller).ClientMessage("Must be deployed to aim down sights");
		return;
	}
	
	super.IronSights();
}

defaultproperties
{
	WeaponContentClass(0)="WinterWar.WWWeapon_DP28_ActualContent"
	
	RoleSelectionImage(0)=Texture2D'VN_UI_Textures.WeaponTex.VN_Weap_DP28_LMG'
	
	TeamIndex=`ALLIES_TEAM_INDEX
	
	InvIndex=`WI_DP28
	
	WeaponProjectiles(0)=class'DP28Bullet'
	Spread(0)=0.0005 // ~1.75 MOA, New Barrel.
	
	AltFireModeType=ROAFT_None
	
	InstantHitDamageTypes(0)=class'RODmgType_DP28Bullet'
	InstantHitDamageTypes(1)=class'RODmgType_DP28Bullet'
	
	MuzzleFlashPSCTemplate=ParticleSystem'WinterWar_FX.ParticleSystems.FX_WW_MuzzleFlash_1stP_LMG'
	
	AmmoClass=class'ROAmmo_762x54R_DP28Drum'
	
	TracerClass=class'DP28BulletTracer'
	TracerFrequency=5
	
	SightRanges.Empty
	SightRanges[0]=(SightRange=50,SightPitch=90,SightSlideOffset=-0.18,SightPositionOffset=-0.05)
	SightRanges[1]=(SightRange=100,SightPitch=90,SightSlideOffset=-0.1,SightPositionOffset=-0.05)
	SightRanges[2]=(SightRange=200,SightPitch=155,SightSlideOffset=0.085,SightPositionOffset=-0.08)
	SightRanges[3]=(SightRange=300,SightPitch=240,SightSlideOffset=0.25,SightPositionOffset=-0.13)
	SightRanges[4]=(SightRange=400,SightPitch=330,SightSlideOffset=0.45,SightPositionOffset=-0.18)
	SightRanges[5]=(SightRange=500,SightPitch=440,SightSlideOffset=0.63,SightPositionOffset=-0.24)
}
