
class WWLocalMessageGameMineDetected extends ROLocalMessageGameReload;

var localized string MineSpotted;

static function string GetString(optional int Switch, optional bool bPRI1HUD, optional PlayerReplicationInfo RelatedPRI_1, optional PlayerReplicationInfo RelatedPRI_2, optional Object OptionalObject)
{
	return default.MineSpotted;
}

static function ClientReceive(PlayerController PC, optional int Switch, optional PlayerReplicationInfo RelatedPRI_1, optional PlayerReplicationInfo RelatedPRI_2, optional Object OptionalObject)
{
	super.ClientReceive(PC, Switch, RelatedPRI_1, RelatedPRI_2, OptionalObject);
	
	PC.PlaySoundBase(AkEvent'WW_UI.Play_UI_Neutral', true);
}
