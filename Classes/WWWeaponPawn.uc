
class WWWeaponPawn extends ROWeaponPawn
	notplaceable;

simulated function DetachDriver(Pawn P)
{
	local ROPawn ROP;
	ROP = ROPawn(P);
	
	if (ROP != None)
	{
		ROP.Mesh.SetAnimTreeTemplate(ROP.Mesh.default.AnimTreeTemplate);
		ROSkeletalMeshComponent(ROP.Mesh).AnimSets[0]=ROSkeletalMeshComponent(ROP.Mesh).default.AnimSets[0];
		ROP.ThirdPersonHeadAndArmsMeshComponent.SetSkeletalMesh(ROP.HeadAndArmsMesh);
		ROP.ThirdPersonHeadgearMeshComponent.SetHidden(false);
		ROP.FaceItemMeshComponent.SetHidden(false);
		ROP.FacialHairMeshComponent.SetHidden(false);
		ROP.HideGear(false);
	}
	
	Super.DetachDriver(P);
}

simulated function DrivingStatusChanged()
{
	local ROVWeap_Transport_HullMG ROVWT;
	
	Super.DrivingStatusChanged();
	
	if( ROVehicleTransport(MyVehicle) != none )
	{
		if( MyVehicleWeapon != none )
			ROVWT = ROVWeap_Transport_HullMG(MyVehicleWeapon);
		
		if( ROVWT != none )
		{
			if( !bDriving )
			{
				if( ROVWT.IsTimerActive('ReloadComplete') || ROVWT.bMGReloading )
				{
					ROVehicleTransport(MyVehicle).HullMGReloadInterrupt(MySeatIndex);
				}
				
				if(  ROVWT.IsTimerActive('BeginReload') )
				{
					ROVWT.ClearTimer('BeginReload');
				}
			}
			else
			{
				ROVWT.CheckReloadRequired();
			}
		}
		
		if( Role == ROLE_Authority )
			MyVehicle.ChangeCrewCollision(bDriving, MySeatIndex);
	}
}

event bool DriverLeave(bool bForceLeave)
{
	if( Super.DriverLeave(bForceLeave) )
	{
		if( !MyVehicle.Occupied() )
			MyVehicle.ResetTime = WorldInfo.TimeSeconds + MyVehicle.RespawnTime;
		
		return true;
	}
	
	return false;
}

DefaultProperties
{
	bIsTransportPawn = true
}
