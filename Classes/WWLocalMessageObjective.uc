
class WWLocalMessageObjective extends ROLocalMessageObjective;

var SoundCue ObjectiveWonAxis, ObjectiveLostAxis, ObjectiveWonAllies, ObjectiveLostAllies;

static function ClientReceive(PlayerController PC, optional int Switch, optional PlayerReplicationInfo RelatedPRI_1, optional PlayerReplicationInfo RelatedPRI_2, optional Object OptionalObject)
{
	super(ROLocalMessage).ClientReceive(PC, Switch, RelatedPRI_1, RelatedPRI_2, OptionalObject);
	
	if (PC.GetTeamNum() == `NEUTRAL_TEAM_INDEX)
	{
		return;
	}
	
	switch (switch)
	{
		case ROOBJMSG_Captured:
		// case ROOBJMSG_CapturedKey:
			if (PC.GetTeamNum() == `AXIS_TEAM_INDEX)
			{
				WWPlayerController(PC).DuckMoraleMusic(0.05, 0.4, default.ObjectiveWonAxis.GetCueDuration());
				WWPlayerController(PC).PlayStinger(default.ObjectiveWonAxis);
			}
			else
			{
				WWPlayerController(PC).DuckMoraleMusic(0.05, 0.4, default.ObjectiveLostAllies.GetCueDuration());
				WWPlayerController(PC).PlayStinger(default.ObjectiveLostAllies);
			}
			break;
		
		case ROOBJMSG_CapturedSouth:
		// case ROOBJMSG_CapturedKeySouth:
			if (PC.GetTeamNum() == `AXIS_TEAM_INDEX)
			{
				WWPlayerController(PC).DuckMoraleMusic(0.05, 0.4, default.ObjectiveLostAxis.GetCueDuration());
				WWPlayerController(PC).PlayStinger(default.ObjectiveLostAxis);
			}
			else
			{
				WWPlayerController(PC).DuckMoraleMusic(0.05, 0.4, default.ObjectiveWonAllies.GetCueDuration());
				WWPlayerController(PC).PlayStinger(default.ObjectiveWonAllies);
			}
			break;

		case ROOBJMSG_NorthCapturing:
		case ROOBJMSG_SouthCapturing:
		case ROOBJMSG_AttackObjective:
		case ROOBJMSG_DefendObjective:
		case ROOBJMSG_AttackNextObjective:
		case ROOBJMSG_DefendNextObjective:
		case ROOBJMSG_AttackNextObjectiveIsKey:
		case ROOBJMSG_DefendNextObjectiveIsKey:
			PC.PlaySoundBase(default.ObjectiveUnderAttack, true);
			break;
	}
}

defaultproperties
{
	ObjectiveWonAxis=SoundCue'WinterWar_AUD_MUS.FIN.F_Objective_Captured_Cue'
	ObjectiveLostAxis=SoundCue'WinterWar_AUD_MUS.FIN.F_Objective_Lost_Cue'
	
	ObjectiveWonAllies=SoundCue'WinterWar_AUD_MUS.SOV.R_Objective_Captured_Cue'
	ObjectiveLostAllies=SoundCue'WinterWar_AUD_MUS.SOV.R_Objective_Lost_Cue'
	
	ObjectiveUnderAttack=AkEvent'WW_UI.Play_UI_Positive'
}
