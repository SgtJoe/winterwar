
class WWRoleInfoAxisEliteRifleman extends WWRoleInfoAxis;

DefaultProperties
{
	RoleType=RORIT_Rifleman
	ClassTier=2
	ClassIndex=`RI_ELITE_RIFLEMAN
	
	Items[RORIGM_Default]={(
		PrimaryWeapons=(class'WWWeapon_MN27',class'WWWeapon_MN9130',class'WWWeapon_MN91'),
		
		OtherItems=(class'WWWeapon_M32Grenade')
	)}
	
	ClassIcon=Texture2D'WinterWar_UI.RoleIcons.RoleIcon_Rifleman'
}
