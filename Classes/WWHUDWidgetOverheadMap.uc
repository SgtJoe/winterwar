
class WWHUDWidgetOverheadMap extends ROHUDWidgetOverheadMap;

function UpdateWidget()
{
	local ROPlayerReplicationInfo ROPRI;
	local ROTeamInfo Team;
	
	super.UpdateWidget();
	
	if ( PlayerOwner != none )
	{
		ROPRI = ROPlayerReplicationInfo(PlayerOwner.PlayerReplicationInfo);
		if ( ROPRI != none )
		{
			Team = ROTeamInfo(ROPRI.Team);
		}
	}
	
	if ( Team != none && ROPRI.RoleInfo != none )
	{
		HUDComponents[ROOMT_ForwardScoutTexture].bVisible = false;
		HUDComponents[ROOMT_F4Phantom].bVisible = false;
		
		if (Team.AerialReconPlane != none && IsVisible() &&
			 Team.AerialReconPlane.Location.X > MinVisibleWorldX && Team.AerialReconPlane.Location.X < MaxVisibleWorldX &&
			 Team.AerialReconPlane.Location.Y > MinVisibleWorldY && Team.AerialReconPlane.Location.Y < MaxVisibleWorldY )
		{
			HUDComponents[ROOMT_AerialReconPlane].SetScreenLocation(HUDComponents[ROOMT_Map].X + ((Team.AerialReconPlane.Location.Y - MinVisibleWorldY) * (HUDComponents[ROOMT_Map].Width / (MaxVisibleWorldY - MinVisibleWorldY)) - (HUDComponents[ROOMT_AerialReconPlane].Width / 2.0)),
																	HUDComponents[ROOMT_Map].Y - ((Team.AerialReconPlane.Location.X - MaxVisibleWorldX) * (HUDComponents[ROOMT_Map].Height / (MaxVisibleWorldX - MinVisibleWorldX)) + (HUDComponents[ROOMT_AerialReconPlane].Height / 2.0)));
			HUDComponents[ROOMT_AerialReconPlane].Rotation.Yaw = Team.AerialReconPlane.Rotation.Yaw;
			HUDComponents[ROOMT_AerialReconPlane].bVisible = true;
		}
		else
		{
			HUDComponents[ROOMT_AerialReconPlane].bVisible = false;
		}
		
		if( PlayerOwner.GetTeamNum() == `ALLIES_TEAM_INDEX )
		{
			if ( Team.ActiveSupportAircraft != none && !Team.ActiveSupportAircraft.bShotDown && IsVisible() &&
				 Team.ActiveSupportAircraft.Location.X > MinVisibleWorldX && Team.ActiveSupportAircraft.Location.X < MaxVisibleWorldX &&
				 Team.ActiveSupportAircraft.Location.Y > MinVisibleWorldY && Team.ActiveSupportAircraft.Location.Y < MaxVisibleWorldY )
			{
				HUDComponents[ROOMT_AC47Spooky].SetScreenLocation(	HUDComponents[ROOMT_Map].X + ((Team.ActiveSupportAircraft.Location.Y - MinVisibleWorldY) * (HUDComponents[ROOMT_Map].Width / (MaxVisibleWorldY - MinVisibleWorldY)) - (HUDComponents[ROOMT_AC47Spooky].Width / 2.0)),
																	HUDComponents[ROOMT_Map].Y - ((Team.ActiveSupportAircraft.Location.X - MaxVisibleWorldX) * (HUDComponents[ROOMT_Map].Height / (MaxVisibleWorldX - MinVisibleWorldX)) + (HUDComponents[ROOMT_AC47Spooky].Height / 2.0)));
				HUDComponents[ROOMT_AC47Spooky].Rotation.Yaw = Team.ActiveSupportAircraft.Rotation.Yaw;
				HUDComponents[ROOMT_AC47Spooky].bVisible = true;
			}
			else
			{
				HUDComponents[ROOMT_AC47Spooky].bVisible = false;
			}
		}
	}
}

// function UpdateFriendlyVehicleIcons(ROTeamInfo Team, ROPlayerReplicationInfo ROPRI)

// UpdateEnemySpottedIcon(int Index, float FadeTime, vector EnemyLocation, bool EnemyLocationValid, bool bIsEnemyTank, bool bIsEnemyTransport,  optional byte EnemyHelicopterType = 255)

defaultproperties
{
	NeutralIcon=			Texture2D'WinterWar_UI.Objective.64x64.Objective_64_Neutral'
	NeutralIconCappingRed=	Texture2D'WinterWar_UI.Objective.64x64.Objective_64_Neutral_FIN_Taking'
	NeutralIconCappingBlue=	Texture2D'WinterWar_UI.Objective.64x64.Objective_64_Neutral_SOV_Taking'
	
	NorthIcons(0)={(
		ObjIconRed=					Texture2D'WinterWar_UI.Objective.64x64.Objective_64_FIN',
		ObjIconBlue=				Texture2D'WinterWar_UI.Objective.64x64.Objective_64_FIN',
		EnemyCappingIconRed=		Texture2D'WinterWar_UI.Objective.64x64.Objective_64_FIN_Losing',
		EnemyCappingIconBlue=		Texture2D'WinterWar_UI.Objective.64x64.Objective_64_FIN_Losing',
		HomeObjIconRed=				Texture2D'WinterWar_UI.Objective.64x64.Objective_64_FINHQ',
		HomeObjIconBlue=			Texture2D'WinterWar_UI.Objective.64x64.Objective_64_FINHQ',
		HomeObjNeutralIconRed=		Texture2D'WinterWar_UI.Objective.64x64.Objective_64_FINHQ_Neutral',
		HomeObjNeutralIconBlue=		Texture2D'WinterWar_UI.Objective.64x64.Objective_64_FINHQ_Neutral',
		EnemyCappingHomeIconRed=	Texture2D'WinterWar_UI.Objective.64x64.Objective_64_FINHQ_Losing',
		EnemyCappingHomeIconBlue=	Texture2D'WinterWar_UI.Objective.64x64.Objective_64_FINHQ_Losing'
	)}
	
	SouthIcons(0)={(
		ObjIconRed=					Texture2D'WinterWar_UI.Objective.64x64.Objective_64_SOV',
		ObjIconBlue=				Texture2D'WinterWar_UI.Objective.64x64.Objective_64_SOV',
		EnemyCappingIconRed=		Texture2D'WinterWar_UI.Objective.64x64.Objective_64_SOV_Losing',
		EnemyCappingIconBlue=		Texture2D'WinterWar_UI.Objective.64x64.Objective_64_SOV_Losing',
		HomeObjIconRed=				Texture2D'WinterWar_UI.Objective.64x64.Objective_64_SOVHQ',
		HomeObjIconBlue=			Texture2D'WinterWar_UI.Objective.64x64.Objective_64_SOVHQ',
		HomeObjNeutralIconRed=		Texture2D'WinterWar_UI.Objective.64x64.Objective_64_SOVHQ_Neutral',
		HomeObjNeutralIconBlue=		Texture2D'WinterWar_UI.Objective.64x64.Objective_64_SOVHQ_Neutral',
		EnemyCappingHomeIconRed=	Texture2D'WinterWar_UI.Objective.64x64.Objective_64_SOVHQ_Losing',
		EnemyCappingHomeIconBlue=	Texture2D'WinterWar_UI.Objective.64x64.Objective_64_SOVHQ_Losing'
	)}
}
