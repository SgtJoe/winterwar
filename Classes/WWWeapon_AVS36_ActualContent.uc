
class WWWeapon_AVS36_ActualContent extends WWWeapon_AVS36;

simulated function SetupArmsAnim()
{
	super.SetupArmsAnim();
	
	// ArmsMesh.AnimSets has slots 0-2-3 filled, so we need to back fill slot 1 and then move to slot 4
	ROPawn(Instigator).ArmsMesh.AnimSets[1] = SkeletalMeshComponent(Mesh).AnimSets[0];
	ROPawn(Instigator).ArmsMesh.AnimSets[4] = SkeletalMeshComponent(Mesh).AnimSets[1];
	ROPawn(Instigator).ArmsMesh.AnimSets[5] = SkeletalMeshComponent(Mesh).AnimSets[2];
}

DefaultProperties
{
	ArmsAnimSet=AnimSet'WinterWar_WP_SOV_SVT38.Anim.WP_Svt40Hands'
	
	Begin Object Name=FirstPersonMesh
		DepthPriorityGroup=SDPG_Foreground
		SkeletalMesh=SkeletalMesh'WinterWar_WP_SOV_AVS.Mesh.SOV_AVS36'
		PhysicsAsset=None
		AnimSets(0)=AnimSet'WinterWar_WP_SOV_SVT38.Anim.WP_Svt40Hands'
		AnimSets(1)=AnimSet'WinterWar_WP_SOV_SVT38.Anim.WP_SVT38Hands'
		AnimSets(2)=AnimSet'WinterWar_WP_SOV_AVS.Anim.WP_AVS36Hands'
		Animations=AnimTree'WinterWar_WP_SOV_AVS.Anim.SOV_AVS36_Tree'
		Scale=1.0
		FOV=70
	End Object
	
	Begin Object Name=PickupMesh
		SkeletalMesh=SkeletalMesh'WinterWar_WP_SOV_AVS.Mesh.SOV_AVS36_3rd'
		PhysicsAsset=PhysicsAsset'WinterWar_WP_SOV_SVT38.Phy.SVT40_3rd_Master_Physics'
		AnimTreeTemplate=AnimTree'WinterWar_WP_SOV_SVT38.Anim.SOV_SVT38_3rd_Tree'
		CollideActors=true
		BlockActors=true
		BlockZeroExtent=true
		BlockNonZeroExtent=true//false
		BlockRigidBody=true
		bHasPhysicsAssetInstance=false
		bUpdateKinematicBonesFromAnimation=false
		PhysicsWeight=1.0
		RBChannel=RBCC_GameplayPhysics
		RBCollideWithChannels=(Default=TRUE,GameplayPhysics=TRUE,EffectPhysics=TRUE)
		bSkipAllUpdateWhenPhysicsAsleep=TRUE
		bSyncActorLocationToRootRigidBody=true
	End Object
	
	AttachmentClass=class'WWWeapon_AVS36_Attach'
	
	WeaponFireSnd(DEFAULT_FIREMODE)=(DefaultCue=AkEvent'WW_WEP_MAS49.Play_WEP_MAS49_Single_3P', FirstPersonCue=AkEvent'WW_WEP_MAS49.Play_WEP_MAS49_Fire_Single')
	WeaponFireSnd(ALTERNATE_FIREMODE)=(DefaultCue=AkEvent'WW_WEP_MAS49.Play_WEP_MAS49_Single_3P', FirstPersonCue=AkEvent'WW_WEP_MAS49.Play_WEP_MAS49_Fire_Single')
}
