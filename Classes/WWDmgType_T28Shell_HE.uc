
class WWDmgType_T28Shell_HE extends RODamageType_CannonShell_HE
	abstract;

defaultproperties
{
	WeaponShortName="T28"
	VehicleDamageScaling=0.15
	RadialDamageImpulse=2000
}
