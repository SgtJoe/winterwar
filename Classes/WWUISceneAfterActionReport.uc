
class WWUISceneAfterActionReport extends ROUISceneAfterActionReport;

event PostInitialize()
{
	local LocalPlayer Player;
	local ROPlayerController ROPC;
	
	super.PostInitialize();
	
	Player = GetPlayerOwner();
	
	if (Player != none && Player.Actor != none)
	{
		ROPC = ROPlayerController(Player.Actor);
	}
	
	if(ROPC != none)
	{
		NorthIndex = 0;
		SouthIndex = 0;
		
		UnlocksCharItemList.ROMI = WWMapInfo(ROPC.WorldInfo.GetMapInfo());
		UnlocksCharItemList.TeamSplitValue = 1;
		UnlocksCharItemList.NumArmies = 2;
		UnlocksCharItemList.PawnHandlerClass = class'WWPawnHandler';
	}
}

defaultproperties
{
	TWSTeamWinIconNorthRedTextures(0)= Texture2D'WinterWar_UI.TeamLogo.TeamLogo_FIN'
	TWSTeamWinIconNorthBlueTextures(0)=Texture2D'WinterWar_UI.TeamLogo.TeamLogo_FIN'
	
	TWSTeamWinIconSouthRedTextures(0)= Texture2D'WinterWar_UI.TeamLogo.TeamLogo_SOV'
	TWSTeamWinIconSouthBlueTextures(0)=Texture2D'WinterWar_UI.TeamLogo.TeamLogo_SOV'
}
