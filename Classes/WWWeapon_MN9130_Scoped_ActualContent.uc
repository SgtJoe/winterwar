
class WWWeapon_MN9130_Scoped_ActualContent extends WWWeapon_MN9130_Scoped;

defaultproperties
{
	ArmsAnimSet=AnimSet'WinterWar_WP_SOV_MN9130_SCOPED.Anim.MNSniperHands_Normal'
	
	Begin Object Name=FirstPersonMesh
		DepthPriorityGroup=SDPG_Foreground
		SkeletalMesh=SkeletalMesh'WinterWar_WP_SOV_MN9130_SCOPED.Mesh.SOV_MN91-30_SCOPED'
		PhysicsAsset=PhysicsAsset'WP_VN_VC_MN9130_Sniper.Phys.Sov_MN9130_Sniper_Physics'
		AnimSets(0)=AnimSet'WinterWar_WP_SOV_MN9130_SCOPED.Anim.MNSniperHands_Normal'
		AnimTreeTemplate=AnimTree'WP_VN_VC_MN9130_Sniper.Animation.WP_MN9130_Scoped_Tree'
		Scale=1.0
		FOV=70
	End Object
	
	Begin Object Name=PickupMesh
		SkeletalMesh=SkeletalMesh'WP_VN_3rd_Master.Mesh.MN9130_Sniper_3rd_Master'
		PhysicsAsset=PhysicsAsset'WP_VN_3rd_Master.Phy.MN9130_Sniper_3rd_Master_Physics'
		AnimTreeTemplate=AnimTree'WP_VN_3rd_Master.AnimTree.MN9130_ScopedRifle_3rd_Tree'
		CollideActors=true
		BlockActors=true
		BlockZeroExtent=true
		BlockNonZeroExtent=true//false
		BlockRigidBody=true
		bHasPhysicsAssetInstance=false
		bUpdateKinematicBonesFromAnimation=false
		PhysicsWeight=1.0
		RBChannel=RBCC_GameplayPhysics
		RBCollideWithChannels=(Default=TRUE,GameplayPhysics=TRUE,EffectPhysics=TRUE)
		bSkipAllUpdateWhenPhysicsAsleep=TRUE
		bSyncActorLocationToRootRigidBody=true
	End Object
	
	AttachmentClass=class'WWWeapon_MN9130_Scoped_Attach'
	
	ScopeLenseMICTemplate=MaterialInstanceConstant'WP_VN_VC_MN9130_Sniper.Materials.VC_MN9130_LenseMat_PU'
}
