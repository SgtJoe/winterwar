
class WWVehicle_T28 extends WWVehicleTank
	abstract;

var AudioComponent TurretMGAmbient,		LeftMGAmbient,		RightMGAmbient,			RearMGAmbient;
var AudioComponent TurretMGReloadSound,	LeftMGReloadSound,	RightMGReloadSound,		RearMGReloadSound;

var SoundCue TurretMGStopSound, LeftMGStopSound, RightMGStopSound, RearMGStopSound;

var repnotify vector	/*TurretMGFlashLocation,*/			LeftMGFlashLocation,		RightMGFlashLocation,			RearMGFlashLocation;
var	repnotify byte		/*TurretMGFlashCount,*/				LeftMGFlashCount,			RightMGFlashCount,				RearMGFlashCount;
var repnotify rotator	/*TurretMGWeaponRotation,*/			LeftMGWeaponRotation,		RightMGWeaponRotation,			RearMGWeaponRotation;
var repnotify byte		/*TurretMGCurrentPositionIndex,*/	LeftMGCurrentPositionIndex,	RightMGCurrentPositionIndex,	RearMGCurrentPositionIndex;

var repnotify bool /*bDrivingTurretMG,*/ bDrivingLeftMG, bDrivingRightMG, bDrivingRearMG;

var repnotify byte /*TurretMGReloadCount,*/		LeftMGReloadCount,		RightMGReloadCount,		RearMGReloadCount;
var repnotify byte /*TurretMGStopReloadCount,*/	LeftMGStopReloadCount,	RightMGStopReloadCount,	RearMGStopReloadCount;

replication
{
	// Obviously a shitton of replication, good thing there's only ever 1 per map!
	// EDIT: Making the Turret MG coaxial should help with that
	if (bNetDirty)
		/*TurretMGFlashLocation,*/ LeftMGFlashLocation, RightMGFlashLocation, RearMGFlashLocation,
		/*TurretMGCurrentPositionIndex,*/ LeftMGCurrentPositionIndex, RightMGCurrentPositionIndex, RearMGCurrentPositionIndex,
		/*bDrivingTurretMG,*/ bDrivingLeftMG, bDrivingRightMG, bDrivingRearMG,
		/*TurretMGReloadCount,*/ LeftMGReloadCount, RightMGReloadCount, RearMGReloadCount,
		/*TurretMGStopReloadCount,*/ LeftMGStopReloadCount, RightMGStopReloadCount, RearMGStopReloadCount;
	
	/* if (!IsSeatControllerReplicationViewer(`T28_MG_T_F))
		TurretMGFlashCount, TurretMGWeaponRotation; */
	
	if (!IsSeatControllerReplicationViewer(`T28_MG_H_L))
		LeftMGFlashCount, LeftMGWeaponRotation;
	
	if (!IsSeatControllerReplicationViewer(`T28_MG_H_R))
		RightMGFlashCount, RightMGWeaponRotation;
	
	if (!IsSeatControllerReplicationViewer(`T28_MG_T_R))
		RearMGFlashCount, RearMGWeaponRotation;
}

simulated function int GetCommanderSeatIndex()
{
	return GetSeatIndexFromPrefix("Turret");
}

simulated function int GetGunnerSeatIndex()
{
	return GetSeatIndexFromPrefix("Turret");
}

simulated function int GetLoaderSeatIndex()
{
	return GetSeatIndexFromPrefix("Turret");
}

simulated event PostBeginPlay()
{
	super.PostBeginPlay();
	
	if( WorldInfo.NetMode != NM_DedicatedServer )
	{
		Mesh.AttachComponentToSocket(TurretMGAmbient, 'TurretMG');
		Mesh.AttachComponentToSocket(LeftMGAmbient, 'LeftMG');
		Mesh.AttachComponentToSocket(RightMGAmbient, 'RightMG');
		Mesh.AttachComponentToSocket(RearMGAmbient, 'RearMG');
	}
}

/* The Exhaust PFX has been updated to include both streams, keeping this code just for reference
simulated event Tick(float DeltaTime)
{
	local float ParamScale;
	
	super.Tick(DeltaTime);
	
	if (WorldInfo.NetMode != NM_DedicatedServer)
	{
		if (LastRenderTime > WorldInfo.TimeSeconds - 0.2)
		{
			ParamScale = FClamp(abs(OutputGas), 0.01 , 1.0);
			
			if ( VehicleEffects[TankVFX_TreadWing].EffectRef != None )
			{
				VehicleEffects[TankVFX_TreadWing].EffectRef.SetFloatParameter(ExhaustScalingParam, ParamScale);
			}
		}
	}
} */

simulated event TornOff()
{
	TurretMGAmbient.Stop();
	LeftMGAmbient.Stop();
	RightMGAmbient.Stop();
	RearMGAmbient.Stop();
	
	super.TornOff();
}

simulated function StopVehicleSounds()
{
	super.StopVehicleSounds();
	
	TurretMGAmbient.Stop();
	LeftMGAmbient.Stop();
	RightMGAmbient.Stop();
	RearMGAmbient.Stop();
}

// TODO: Clean this up to avoid the massive copypasta
simulated event ReplicatedEvent(name VarName)
{
	local int SeatIndex;
	
	/* if (VarName == 'TurretMGReloadCount')
	{
		SeatIndex = `T28_MG_T_F;
		
		if (Seats[SeatIndex].Gun != none)
		{
			Seats[SeatIndex].Gun.ClientTimeReloading();
		}
		else
		{
			VehicleMGReload(SeatIndex);
		}
	}
	else if (VarName == 'TurretMGStopReloadCount')
	{
		HullMGReloadInterrupt(SeatIndex);
	}
	else */ if (VarName == 'LeftMGReloadCount')
	{
		SeatIndex = `T28_MG_H_L;
		
		if (Seats[SeatIndex].Gun != none)
		{
			Seats[SeatIndex].Gun.ClientTimeReloading();
		}
		else
		{
			VehicleMGReload(SeatIndex);
		}
	}
	else if (VarName == 'LeftMGStopReloadCount')
	{
		HullMGReloadInterrupt(SeatIndex);
	}
	else if (VarName == 'RightMGReloadCount')
	{
		SeatIndex = `T28_MG_H_R;
		
		if (Seats[SeatIndex].Gun != none)
		{
			Seats[SeatIndex].Gun.ClientTimeReloading();
		}
		else
		{
			VehicleMGReload(SeatIndex);
		}
	}
	else if (VarName == 'RightMGStopReloadCount')
	{
		HullMGReloadInterrupt(SeatIndex);
	}
	else if (VarName == 'RearMGReloadCount')
	{
		SeatIndex = `T28_MG_T_R;
		
		if (Seats[SeatIndex].Gun != none)
		{
			Seats[SeatIndex].Gun.ClientTimeReloading();
		}
		else
		{
			VehicleMGReload(SeatIndex);
		}
	}
	else if (VarName == 'RearMGStopReloadCount')
	{
		HullMGReloadInterrupt(SeatIndex);
	}
	else
	{
		super.ReplicatedEvent(VarName);
	}
}

simulated function VehicleMGReload(int SeatIndex)
{
	`wwdebug("playing MG reload sound for seat" @ SeatIndex, 'VH');
	
	switch (SeatIndex)
	{
		/* case `T28_MG_T_F:
			if (TurretMGReloadSound != None && !TurretMGReloadSound.IsPlaying())
			{
				TurretMGReloadSound.Play();
			}
			break; */
		
		case `T28_MG_H_L:
			if (LeftMGReloadSound != None && !LeftMGReloadSound.IsPlaying())
			{
				LeftMGReloadSound.Play();
			}
			break;
		
		case `T28_MG_H_R:
			if (RightMGReloadSound != None && !RightMGReloadSound.IsPlaying())
			{
				RightMGReloadSound.Play();
			}
			break;
		
		case `T28_MG_T_R:
			if (RearMGReloadSound != None && !RearMGReloadSound.IsPlaying())
			{
				RearMGReloadSound.Play();
			}
			break;
		
		default:
			`wwdebug("BAD SEAT!",'VH');
	}
}

function float GetCoaxMGReloadDuration()
{
	if (TurretMGReloadSound.SoundCue != none)
	{
		return TurretMGReloadSound.SoundCue.GetCueDuration();
	}
	
	return 7.0;
}

simulated function VehicleCoaxMGReload()
{
	`wwdebug("playing MG reload sound (T-28 Custom)", 'VH');
	
	if (TurretMGReloadSound != None && !TurretMGReloadSound.IsPlaying())
	{
		TurretMGReloadSound.Play();
	}
}

simulated function VehicleWeaponFireEffects(vector HitLocation, int SeatIndex)
{
	super.VehicleWeaponFireEffects(HitLocation, SeatIndex);
	
	switch (SeatIndex)
	{
		case `T28_GUNNER:
			// Main cannon already handled by super
			
			// But now we need handling for the new coax MG
			// Keep in mind the SeatFiringMode
			if (TurretMGAmbient != none && !TurretMGAmbient.IsPlaying() && SeatFiringMode(SeatIndex,,true) == 1)
			{
				TurretMGAmbient.Play();
			}
			break;
		
		/* case `T28_MG_T_F:
			if (TurretMGAmbient != none && !TurretMGAmbient.IsPlaying() && SeatFiringMode(SeatIndex,,true) == 0)
			{
				TurretMGAmbient.Play();
			}
			break; */
		
		case `T28_MG_H_L:
			if (LeftMGAmbient != none && !LeftMGAmbient.IsPlaying() && SeatFiringMode(SeatIndex,,true) == 0)
			{
				LeftMGAmbient.Play();
			}
			break;
		
		case `T28_MG_H_R:
			if (RightMGAmbient != none && !RightMGAmbient.IsPlaying() && SeatFiringMode(SeatIndex,,true) == 0)
			{
				RightMGAmbient.Play();
			}
			break;
		
		case `T28_MG_T_R:
			if (RearMGAmbient != none && !RearMGAmbient.IsPlaying() && SeatFiringMode(SeatIndex,,true) == 0)
			{
				RearMGAmbient.Play();
			}
			break;
		
		default:
			`wwdebug("BAD SEAT!",'VH');
	}
}

simulated function VehicleWeaponStoppedFiring(bool bViaReplication, int SeatIndex)
{
	super.VehicleWeaponStoppedFiring(bViaReplication, SeatIndex);
	
	switch (SeatIndex)
	{
		case `T28_GUNNER:
			// Main cannon already handled by super
			
			// But now we need handling for the new coax MG
			if (TurretMGAmbient.bWasPlaying || !TurretMGAmbient.bFinished)
			{
				TurretMGAmbient.Stop();
				PlaySound(TurretMGStopSound, TRUE, FALSE, FALSE, TurretMGAmbient.CurrentLocation, FALSE);
			}
			break;
		
		/* case `T28_MG_T_F:
			TurretMGAmbient.Stop();
			if (TurretMGStopSound != none)
			{
				PlaySound(TurretMGStopSound, TRUE, FALSE, FALSE, TurretMGAmbient.CurrentLocation, FALSE);
			}
			break; */
		
		case `T28_MG_H_L:
			LeftMGAmbient.Stop();
			if (LeftMGStopSound != none)
			{
				PlaySound(LeftMGStopSound, TRUE, FALSE, FALSE, LeftMGAmbient.CurrentLocation, FALSE);
			}
			break;
		
		case `T28_MG_H_R:
			RightMGAmbient.Stop();
			if (RightMGStopSound != none)
			{
				PlaySound(RightMGStopSound, TRUE, FALSE, FALSE, RightMGAmbient.CurrentLocation, FALSE);
			}
			break;
		
		case `T28_MG_T_R:
			RearMGAmbient.Stop();
			if (RearMGStopSound != none)
			{
				PlaySound(RearMGStopSound, TRUE, FALSE, FALSE, RearMGAmbient.CurrentLocation, FALSE);
			}
			break;
		
		default:
			`wwdebug("BAD SEAT!",'VH');
	}
}

DefaultProperties
{
	Team=`ALLIES_TEAM_INDEX
	
	Health=800
	
	ExitRadius=210
	
	Begin Object Name=CollisionCylinder
		CollisionHeight=+70.0
		CollisionRadius=+190.0
		Translation=(X=-40.0,Y=0.0,Z=60.0)
	End Object
	CylinderComponent=CollisionCylinder
	
	HUDBodyTexture=Texture2D'WinterWar_UI.Vehicle.UI_HUD_VH_T28_Body'
	HUDTurretTexture=Texture2D'WinterWar_UI.Vehicle.UI_HUD_VH_T28_Turret'
	
	DriverOverlayTexture=Texture2D'WinterWar_UI.VehicleOptics.UI_HUD_VH_Optics_SquareOverlay'
	
	Seats(`T28_DRIVER)={(
		CameraOffset=-420,
		SeatAnimBlendName=DriverPositionNode,
		SeatPositions=(
			(
				bDriverVisible=false,
				bAllowFocus=false,
				PositionCameraTag=Driver_Camera,
				ViewFOV=70.0,
				bViewFromCameraTag=true,
				bDrawOverlays=true,
				// bDrawOverlays=false,
				PositionUpAnim=Driver_idle,
				PositionIdleAnim=Driver_idle,
				DriverIdleAnim=Driver_idle,
				AlternateIdleAnim=Driver_idle,
				SeatProxyIndex=`T28_DRIVER,
				bIsExterior=false,
				PositionFlinchAnims=(Driver_idle),
				PositionDeathAnims=(Driver_idle)
			)
		),
		bSeatVisible=false,
		SeatBone=Chassis,
		DriverDamageMult=1.0,
		InitialPositionIndex=0
	)}
	
	Seats(`T28_GUNNER)={(
		CameraOffset=-420,
		SeatAnimBlendName=CommanderPositionNode,
		GunClass=class'WWVehicleWeapon_T28_Turret',
		RangeOverlayTexture=Texture2D'WinterWar_UI.VehicleOptics.UI_HUD_VH_Optics_Range_T26',
		PeriscopeRangeTexture=none,
		VignetteOverlayTexture=Texture2D'WinterWar_UI.VehicleOptics.UI_HUD_VH_Optics_CircularOverlay',
		BinocOverlayTexture=Texture2D'WinterWar_UI.VehicleOptics.UI_HUD_VH_Optics_SquareOverlay',
		GunSocket=(Barrel,TurretMG),
		GunPivotPoints=(gun_base,gun_base),
		TurretVarPrefix="Turret",
		TurretControls=(Turret_Gun,Turret_Main,TurretMG_Pitch),
		SeatPositions=(
			(
				bDriverVisible=false,
				bAllowFocus=false,
				PositionCameraTag=Camera_Gunner,
				bViewFromCameraTag=true,
				ViewFOV=20.0,
				bCamRotationFollowSocket=true,
				bDrawOverlays=true,
				// bDrawOverlays=false,
				PositionUpAnim=MG_Hull_Idle,
				PositionDownAnim=MG_Hull_Idle,
				PositionIdleAnim=MG_Hull_Idle,
				DriverIdleAnim=MG_Hull_Idle,
				AlternateIdleAnim=MG_Hull_Idle,
				SeatProxyIndex=`T28_GUNNER,
				bIsExterior=false,
				PositionFlinchAnims=(MG_Hull_Idle),
				PositionDeathAnims=(MG_Hull_Idle)
			)
		),
		bSeatVisible=false,
		SeatBone=Turret,
		DriverDamageMult=1.0,
		InitialPositionIndex=0,
		FiringPositionIndex=0,
		TracerFrequency=5,
		WeaponTracerClass=(none,class'WWProjectile_DT_T28_Tracer'),
		MuzzleFlashLightClass=(none,none)
	)}
	
	/* Seats(`T28_MG_T_F)={(
		GunClass=class'WWVehicleWeapon_T28_MG_Turret',
		VignetteOverlayTexture=Texture2D'WinterWar_UI.VehicleOptics.UI_HUD_VH_Optics_CircularOverlay',
		GunSocket=(TurretMG),
		GunPivotPoints=(TurretMG_Pitch),
		TurretVarPrefix="TurretMG",
		TurretControls=(TurretMG_Yaw,TurretMG_Pitch),
		CameraTag=None,
		CameraOffset=-420,
		SeatAnimBlendName=TurretMGPositionNode,
		SeatPositions=(
			(
				bDriverVisible=true,
				bAllowFocus=false,
				PositionCameraTag=TurretMG_Camera,
				ViewFOV=55.0,
				bCamRotationFollowSocket=true,
				bViewFromCameraTag=true,
				bDrawOverlays=true,
				bUseDOF=true,
				bIsExterior=false,
				PositionIdleAnim=MG_ironIdle,
				bConstrainRotation=false,
				YawContraintIndex=0,
				PitchContraintIndex=1,
				DriverIdleAnim=MG_ironIdle,
				AlternateIdleAnim=MG_ironIdle_AI,
				SeatProxyIndex=`T28_MG_T_F
			)
		),
		bSeatVisible=false,
		SeatBone=Turret,
		DriverDamageMult=1.0,
		InitialPositionIndex=0,
		FiringPositionIndex=0,
		SeatRotation=(Pitch=0,Yaw=0,Roll=0),
		TracerFrequency=5,
		WeaponTracerClass=(class'WWProjectile_DT_T28_Tracer',class'WWProjectile_DT_T28_Tracer'),
		MuzzleFlashLightClass=(none,none)
	)} */
	
	Seats(`T28_MG_H_L)={(
		GunClass=class'WWVehicleWeapon_T28_MG_Left',
		VignetteOverlayTexture=Texture2D'WinterWar_UI.VehicleOptics.UI_HUD_VH_Optics_CircularOverlay',
		GunSocket=(LeftMG),
		GunPivotPoints=(LeftMG_Pitch),
		TurretVarPrefix="LeftMG",
		TurretControls=(LeftMG_Yaw,LeftMG_Pitch),
		CameraTag=None,
		CameraOffset=-420,
		SeatAnimBlendName=LeftMGPositionNode,
		SeatPositions=(
			(
				bDriverVisible=true,
				bAllowFocus=false,
				PositionCameraTag=LeftMG_Camera,
				ViewFOV=55.0,
				bCamRotationFollowSocket=true,
				bViewFromCameraTag=true,
				bDrawOverlays=true,
				bUseDOF=true,
				bIsExterior=false,
				PositionIdleAnim=MG_ironIdle,
				bConstrainRotation=false,
				YawContraintIndex=0,
				PitchContraintIndex=1,
				DriverIdleAnim=MG_ironIdle,
				AlternateIdleAnim=MG_ironIdle_AI,
				SeatProxyIndex=`T28_MG_H_L
			)
		),
		bSeatVisible=false,
		SeatBone=Chassis,
		DriverDamageMult=1.0,
		InitialPositionIndex=0,
		FiringPositionIndex=0,
		SeatRotation=(Pitch=0,Yaw=0,Roll=0),
		TracerFrequency=5,
		WeaponTracerClass=(class'WWProjectile_DT_T28_Tracer',class'WWProjectile_DT_T28_Tracer'),
		MuzzleFlashLightClass=(none,none)
	)}
	
	Seats(`T28_MG_H_R)={(
		GunClass=class'WWVehicleWeapon_T28_MG_Right',
		VignetteOverlayTexture=Texture2D'WinterWar_UI.VehicleOptics.UI_HUD_VH_Optics_CircularOverlay',
		GunSocket=(RightMG),
		GunPivotPoints=(RightMG_Pitch),
		TurretVarPrefix="RightMG",
		TurretControls=(RightMG_Yaw,RightMG_Pitch),
		CameraTag=None,
		CameraOffset=-420,
		SeatAnimBlendName=RightMGPositionNode,
		SeatPositions=(
			(
				bDriverVisible=true,
				bAllowFocus=false,
				PositionCameraTag=RightMG_Camera,
				ViewFOV=55.0,
				bCamRotationFollowSocket=true,
				bViewFromCameraTag=true,
				bDrawOverlays=true,
				bUseDOF=true,
				bIsExterior=false,
				PositionIdleAnim=MG_ironIdle,
				bConstrainRotation=false,
				YawContraintIndex=0,
				PitchContraintIndex=1,
				DriverIdleAnim=MG_ironIdle,
				AlternateIdleAnim=MG_ironIdle_AI,
				SeatProxyIndex=`T28_MG_H_R
			)
		),
		bSeatVisible=false,
		SeatBone=Chassis,
		DriverDamageMult=1.0,
		InitialPositionIndex=0,
		FiringPositionIndex=0,
		SeatRotation=(Pitch=0,Yaw=0,Roll=0),
		TracerFrequency=5,
		WeaponTracerClass=(class'WWProjectile_DT_T28_Tracer',class'WWProjectile_DT_T28_Tracer'),
		MuzzleFlashLightClass=(none,none)
	)}
	
	Seats(`T28_MG_T_R)={(
		GunClass=class'WWVehicleWeapon_T28_MG_Rear',
		VignetteOverlayTexture=Texture2D'WinterWar_UI.VehicleOptics.UI_HUD_VH_Optics_CircularOverlay',
		GunSocket=(RearMG),
		GunPivotPoints=(RearMG_Pitch),
		TurretVarPrefix="RearMG",
		TurretControls=(RearMG_Yaw,RearMG_Pitch),
		CameraTag=None,
		CameraOffset=-420,
		SeatAnimBlendName=RearMGPositionNode,
		SeatPositions=(
			(
				bDriverVisible=true,
				bAllowFocus=false,
				PositionCameraTag=RearMG_Camera,
				ViewFOV=55.0,
				bCamRotationFollowSocket=true,
				bViewFromCameraTag=true,
				bDrawOverlays=true,
				bUseDOF=true,
				bIsExterior=false,
				PositionIdleAnim=MG_ironIdle,
				bConstrainRotation=false,
				YawContraintIndex=0,
				PitchContraintIndex=1,
				DriverIdleAnim=MG_ironIdle,
				AlternateIdleAnim=MG_ironIdle_AI,
				SeatProxyIndex=`T28_MG_T_R
			)
		),
		bSeatVisible=false,
		SeatBone=Turret,
		DriverDamageMult=1.0,
		InitialPositionIndex=0,
		FiringPositionIndex=0,
		SeatRotation=(Pitch=0,Yaw=0,Roll=0),
		TracerFrequency=5,
		WeaponTracerClass=(class'WWProjectile_DT_T28_Tracer',class'WWProjectile_DT_T28_Tracer'),
		MuzzleFlashLightClass=(none,none)
	)}
	
	PassengerAnimTree=AnimTree'CHR_Playeranimtree_Master.CHR_Tanker_animtree'
	CrewAnimSet=AnimSet'VH_VN_ARVN_M113_APC.Anim.CHR_M113_Anim_Master'
	
	LeftWheels( 0)="W01_L"
	LeftWheels( 1)="W02_L"
	LeftWheels( 2)="W03_L"
	LeftWheels( 3)="W04_L"
	LeftWheels( 4)="W05_L"
	LeftWheels( 5)="W06_L"
	LeftWheels( 6)="W07_L"
	LeftWheels( 7)="W08_L"
	LeftWheels( 8)="W09_L"
	LeftWheels( 9)="W10_L"
	LeftWheels(10)="W11_L"
	LeftWheels(11)="W12_L"
	LeftWheels(12)="W13_L"
	LeftWheels(13)="W14_L"
	LeftWheels(14)="W15_L"
	LeftWheels(15)="W16_L"
	LeftWheels(16)="W17_L"
	LeftWheels(17)="W18_L"
	
	RightWheels( 0)="W01_R"
	RightWheels( 1)="W02_R"
	RightWheels( 2)="W03_R"
	RightWheels( 3)="W04_R"
	RightWheels( 4)="W05_R"
	RightWheels( 5)="W06_R"
	RightWheels( 6)="W07_R"
	RightWheels( 7)="W08_R"
	RightWheels( 8)="W09_R"
	RightWheels( 9)="W10_R"
	RightWheels(10)="W11_R"
	RightWheels(11)="W12_R"
	RightWheels(12)="W13_R"
	RightWheels(13)="W14_R"
	RightWheels(14)="W15_R"
	RightWheels(15)="W16_R"
	RightWheels(16)="W17_R"
	RightWheels(17)="W18_R"
	
	Begin Object Name=RRWheel
		BoneName="R_Wheel_06"
		BoneOffset=(X=-10.0,Y=0,Z=0.0)
		WheelRadius=24
	End Object
	
	Begin Object Name=RMWheel
		BoneName="R_Wheel_04"
		BoneOffset=(X=0.0,Y=0,Z=0.0)
		WheelRadius=24
	End Object
	
	Begin Object Name=RFWheel
		BoneName="R_Wheel_02"
		BoneOffset=(X=0.0,Y=0,Z=2.0)
		WheelRadius=24
	End Object
	
	Begin Object Name=LRWheel
		BoneName="L_Wheel_06"
		BoneOffset=(X=-10.0,Y=0,Z=0.0)
		WheelRadius=24
	End Object
	
	Begin Object Name=LMWheel
		BoneName="L_Wheel_04"
		BoneOffset=(X=0.0,Y=0,Z=0.0)
		WheelRadius=24
	End Object
	
	Begin Object Name=LFWheel
		BoneName="L_Wheel_02"
		BoneOffset=(X=0.0,Y=0,Z=2.0)
		WheelRadius=24
	End Object
	
	Begin Object Name=SimObject
		GearArray(0)={(
			GearRatio=-5.64,
			AccelRate=10.25,
			TorqueCurve=(Points={(
				(InVal=0,OutVal=-2100),
				(InVal=300,OutVal=-700),
				(InVal=2800,OutVal=-2100),
				(InVal=3000,OutVal=-700),
				(InVal=3200,OutVal=-0.0)
				)}),
			TurningThrottle=0.83
			)}
		GearArray(1)={()}
		GearArray(2)={(
			GearRatio=6.89,
			AccelRate=12.75,
			TorqueCurve=(Points={(
				(InVal=0,OutVal=2100),
				(InVal=300,OutVal=700),
				(InVal=2800,OutVal=2100),
				(InVal=3000,OutVal=700),
				(InVal=3200,OutVal=0.0)
				)}),
			TurningThrottle=0.84
			)}
		GearArray(3)={(
			GearRatio=3.60,
			AccelRate=13.50,
			TorqueCurve=(Points={(
				(InVal=0,OutVal=2500),
				(InVal=300,OutVal=1750),
				(InVal=2800,OutVal=2500),
				(InVal=3000,OutVal=800),
				(InVal=3200,OutVal=0.0)
				)}),
			TurningThrottle=0.84
			)}
		GearArray(4)={(
			GearRatio=2.14,
			AccelRate=15.00,
			TorqueCurve=(Points={(
				(InVal=0,OutVal=2000),
				(InVal=2800,OutVal=3750),
				(InVal=3000,OutVal=1000),
				(InVal=3200,OutVal=0.0)
				)}),
			TurningThrottle=0.75
			)}
		GearArray(5)={(
			GearRatio=1.42,
			AccelRate=16.50,
			TorqueCurve=(Points={(
				(InVal=0,OutVal=2200),
				(InVal=2800,OutVal=5200),
				(InVal=3000,OutVal=1200),
				(InVal=3200,OutVal=0.0)
				)}),
			TurningThrottle=0.82
			)}
		/* GearArray(6)={(
			GearRatio=1.00,
			AccelRate=16.25,
			TorqueCurve=(Points={(
				(InVal=0,OutVal=3000),
				(InVal=2800,OutVal=7400),
				(InVal=3000,OutVal=1500),
				(InVal=3200,OutVal=0.0)
				)}),
			TurningThrottle=0.78
			)}
		GearArray(7)={(
			GearRatio=0.78,
			AccelRate=16.25,
			TorqueCurve=(Points={(
				(InVal=0,OutVal=3600),
				(InVal=2700,OutVal=9200),
				(InVal=3000,OutVal=2000),
				(InVal=3200,OutVal=0.0)
				)}),
			TurningThrottle=0.78
			)} */
		FirstForwardGear=2
	End Object
	
	TreadSpeedScale=2.5
	
	VehHitZones(0)=(ZoneName=CHASSIS)
	VehHitZones(1)=(ZoneName=TURRET)
	VehHitZones(2)=(ZoneName=ENGINE,VehicleHitZoneType=VHT_Engine)
	VehHitZones(3)=(ZoneName=Root_Track_R,VehicleHitZoneType=VHT_Track)
	VehHitZones(4)=(ZoneName=Root_Track_L,VehicleHitZoneType=VHT_Track)

	VehHitZones(5)=(ZoneName=ENGINEBLOCK,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Engine,ZoneHealth=100,VisibleFrom=14)
	VehHitZones(6)=(ZoneName=ENGINECORE,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Engine,ZoneHealth=300,VisibleFrom=14)
	VehHitZones(7)=(ZoneName=AMMOSTOREMAIN,DamageMultiplier=100.0,VehicleHitZoneType=VHT_Ammo,ZoneHealth=10,KillPercentage=0.4,VisibleFrom=15)
	VehHitZones(8)=(ZoneName=AMMOLEFT,DamageMultiplier=50.0,VehicleHitZoneType=VHT_Ammo,ZoneHealth=10,KillPercentage=0.3)
	VehHitZones(9)=(ZoneName=AMMORIGHT,DamageMultiplier=50.0,VehicleHitZoneType=VHT_Ammo,ZoneHealth=10,KillPercentage=0.3)
	VehHitZones(10)=(ZoneName=GEARBOX,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Mechanicals,ZoneHealth=100)
	VehHitZones(11)=(ZoneName=GEARBOXCORE,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Mechanicals,ZoneHealth=200)
	VehHitZones(12)=(ZoneName=LEFTBRAKES,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Mechanicals,ZoneHealth=25,VisibleFrom=5)
	VehHitZones(13)=(ZoneName=RIGHTBRAKES,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Mechanicals,ZoneHealth=25,VisibleFrom=9)
	VehHitZones(14)=(ZoneName=COAXIALMG,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Mechanicals,ZoneHealth=25)
	VehHitZones(15)=(ZoneName=MAINCANNONREAR,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Mechanicals,ZoneHealth=50,KillPercentage=0.3)
	VehHitZones(16)=(ZoneName=DRIVERBODY,DamageMultiplier=1.0,VehicleHitZoneType=VHT_CrewBody,CrewSeatIndex=0,SeatProxyIndex=0)
	VehHitZones(17)=(ZoneName=DRIVERHEAD,DamageMultiplier=1.0,VehicleHitZoneType=VHT_CrewHead,CrewSeatIndex=0,SeatProxyIndex=0)
	VehHitZones(18)=(ZoneName=LEFTMGGUNNERBODY,DamageMultiplier=1.0,VehicleHitZoneType=VHT_CrewBody,CrewSeatIndex=2,SeatProxyIndex=2)
	VehHitZones(19)=(ZoneName=LEFTMGGUNNERHEAD,DamageMultiplier=1.0,VehicleHitZoneType=VHT_CrewHead,CrewSeatIndex=2,SeatProxyIndex=2)
	VehHitZones(20)=(ZoneName=RIGHTMGGUNNERBODY,DamageMultiplier=1.0,VehicleHitZoneType=VHT_CrewBody,CrewSeatIndex=3,SeatProxyIndex=3)
	VehHitZones(21)=(ZoneName=RIGHTMGGUNNERHEAD,DamageMultiplier=1.0,VehicleHitZoneType=VHT_CrewHead,CrewSeatIndex=3,SeatProxyIndex=3)
	VehHitZones(22)=(ZoneName=MAINGUNNERBODY,DamageMultiplier=1.0,VehicleHitZoneType=VHT_CrewBody,CrewSeatIndex=1,SeatProxyIndex=1)
	VehHitZones(23)=(ZoneName=MAINGUNNERHEAD,DamageMultiplier=1.0,VehicleHitZoneType=VHT_CrewHead,CrewSeatIndex=1,SeatProxyIndex=1)
	VehHitZones(24)=(ZoneName=REARMGGUNNERBODY,DamageMultiplier=1.0,VehicleHitZoneType=VHT_CrewBody,CrewSeatIndex=4,SeatProxyIndex=4)
	VehHitZones(25)=(ZoneName=REARMGGUNNERHEAD,DamageMultiplier=1.0,VehicleHitZoneType=VHT_CrewHead,CrewSeatIndex=4,SeatProxyIndex=4)
	VehHitZones(26)=(ZoneName=LEFTTRACKONE,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Track,ZoneHealth=200,VisibleFrom=5)
	VehHitZones(27)=(ZoneName=LEFTTRACKTWO,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Track,ZoneHealth=200)
	VehHitZones(28)=(ZoneName=LEFTTRACKTHREE,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Track,ZoneHealth=200)
	VehHitZones(29)=(ZoneName=LEFTTRACKFOUR,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Track,ZoneHealth=200)
	VehHitZones(30)=(ZoneName=LEFTTRACKFIVE,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Track,ZoneHealth=200)
	VehHitZones(31)=(ZoneName=LEFTTRACKSIX,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Track,ZoneHealth=200)
	VehHitZones(32)=(ZoneName=LEFTTRACKSEVEN,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Track,ZoneHealth=200)
	VehHitZones(34)=(ZoneName=RIGHTTRACKONE,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Track,ZoneHealth=200,VisibleFrom=9)
	VehHitZones(35)=(ZoneName=RIGHTTRACKTWO,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Track,ZoneHealth=200)
	VehHitZones(36)=(ZoneName=RIGHTTRACKTHREE,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Track,ZoneHealth=200)
	VehHitZones(37)=(ZoneName=RIGHTTRACKFOUR,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Track,ZoneHealth=200)
	VehHitZones(38)=(ZoneName=RIGHTTRACKFIVE,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Track,ZoneHealth=200)
	VehHitZones(39)=(ZoneName=RIGHTTRACKSIX,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Track,ZoneHealth=200)
	VehHitZones(40)=(ZoneName=RIGHTTRACKSEVEN,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Track,ZoneHealth=200)
	VehHitZones(41)=(ZoneName=FUELTANKLEFT,DamageMultiplier=10.0,VehicleHitZoneType=VHT_Fuel,ZoneHealth=200,KillPercentage=0.2,VisibleFrom=15)
	VehHitZones(42)=(ZoneName=FUELTANKRIGHT,DamageMultiplier=5.0,VehicleHitZoneType=VHT_Fuel,ZoneHealth=200,KillPercentage=0.2,VisibleFrom=15)
	
	// VehHitZones(42)=(ZoneName=RIGHTDRIVEWHEEL,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Track,ZoneHealth=200)
	// VehHitZones(43)=(ZoneName=LEFTDRIVEWHEEL,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Track,ZoneHealth=200)
	// VehHitZones(14)=(ZoneName=TURRETRINGONE,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Mechanicals,ZoneHealth=10,VisibleFrom=5)
	// VehHitZones(15)=(ZoneName=TURRETRINGTWO,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Mechanicals,ZoneHealth=10,VisibleFrom=7)
	// VehHitZones(16)=(ZoneName=TURRETRINGTHREE,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Mechanicals,ZoneHealth=10,VisibleFrom=6)
	// VehHitZones(17)=(ZoneName=TURRETRINGFOUR,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Mechanicals,ZoneHealth=10,VisibleFrom=10)
	// VehHitZones(18)=(ZoneName=TURRETRINGFIVE,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Mechanicals,ZoneHealth=10,VisibleFrom=11)
	// VehHitZones(19)=(ZoneName=TURRETRINGSIX,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Mechanicals,ZoneHealth=10,VisibleFrom=9)

	// All the armour hit zones organised into plate classes
	ArmorHitZones(0)=(ZoneName=FRONTPLATELOWER,PhysBodyBoneName=Chassis,ArmorPlateName=FRONTGLACISLOWER)
	ArmorHitZones(1)=(ZoneName=FRONTPLATEUPPER,PhysBodyBoneName=Chassis,ArmorPlateName=FRONTGLACISUPPER)
	ArmorHitZones(2)=(ZoneName=ROOFFRONT,PhysBodyBoneName=Chassis,ArmorPlateName=CHASSISROOF)
	ArmorHitZones(3)=(ZoneName=DRIVERPLATEFRONT,PhysBodyBoneName=Chassis,ArmorPlateName=DRIVERFRONT)
	ArmorHitZones(4)=(ZoneName=DRIVERPLATERIGHT,PhysBodyBoneName=Chassis,ArmorPlateName=DRIVERSIDE)
	ArmorHitZones(5)=(ZoneName=DRIVERPLATELEFT,PhysBodyBoneName=Chassis,ArmorPlateName=DRIVERSIDE)
	ArmorHitZones(6)=(ZoneName=DRIVERPLATEREARRIGHT,PhysBodyBoneName=Chassis,ArmorPlateName=DRIVERSIDE)
	ArmorHitZones(7)=(ZoneName=DRIVERPLATEREARLEFT,PhysBodyBoneName=Chassis,ArmorPlateName=FRONTGLACIS)
	ArmorHitZones(8)=(ZoneName=DRIVERROOFONE,PhysBodyBoneName=Chassis,ArmorPlateName=HULLROOF)
	ArmorHitZones(9)=(ZoneName=DRIVERROOFTWO,PhysBodyBoneName=Chassis,ArmorPlateName=HULLROOF)
	ArmorHitZones(10)=(ZoneName=HULLROOFONE,PhysBodyBoneName=Chassis,ArmorPlateName=HULLROOF)
	ArmorHitZones(11)=(ZoneName=HULLROOFTWO,PhysBodyBoneName=Chassis,ArmorPlateName=HULLROOF)
	ArmorHitZones(12)=(ZoneName=HULLROOFTHREE,PhysBodyBoneName=Chassis,ArmorPlateName=HULLROOF)
	ArmorHitZones(13)=(ZoneName=HULLROOFFOUR,PhysBodyBoneName=Chassis,ArmorPlateName=HULLROOF)
	ArmorHitZones(14)=(ZoneName=REARPLATEUPPER,PhysBodyBoneName=Chassis,ArmorPlateName=REARPLATE)
	ArmorHitZones(15)=(ZoneName=REARPLATELOWER,PhysBodyBoneName=Chassis,ArmorPlateName=REARPLATE)
	ArmorHitZones(16)=(ZoneName=FLOORPLATE,PhysBodyBoneName=Chassis,ArmorPlateName=FLOOR)
	ArmorHitZones(17)=(ZoneName=RIGHTHULLPLATEONE,PhysBodyBoneName=Chassis,ArmorPlateName=RIGHTHULL)
	ArmorHitZones(18)=(ZoneName=RIGHTHULLPLATETWO,PhysBodyBoneName=Chassis,ArmorPlateName=RIGHTHULL)
	ArmorHitZones(19)=(ZoneName=RIGHTCHASSISMAIN,PhysBodyBoneName=Chassis,ArmorPlateName=RIGHTCHASSIS)
	ArmorHitZones(20)=(ZoneName=RIGHTCHASSISFRONT,PhysBodyBoneName=Chassis,ArmorPlateName=RIGHTCHASSIS)
	ArmorHitZones(21)=(ZoneName=RIGHTCHASSISREAR,PhysBodyBoneName=Chassis,ArmorPlateName=RIGHTCHASSIS)
	ArmorHitZones(22)=(ZoneName=LEFTHULLPLATEONE,PhysBodyBoneName=Chassis,ArmorPlateName=LEFTHULL)
	ArmorHitZones(23)=(ZoneName=LEFTHULLPLATETWO,PhysBodyBoneName=Chassis,ArmorPlateName=LEFTHULL)
	ArmorHitZones(24)=(ZoneName=LEFTCHASSISMAIN,PhysBodyBoneName=Chassis,ArmorPlateName=LEFTCHASSIS)
	ArmorHitZones(25)=(ZoneName=LEFTCHASSISFRONT,PhysBodyBoneName=Chassis,ArmorPlateName=LEFTCHASSIS)
	ArmorHitZones(26)=(ZoneName=RIGHTCHASSISREAR,PhysBodyBoneName=Chassis,ArmorPlateName=LEFTCHASSIS)
	ArmorHitZones(27)=(ZoneName=RIGHTTURRETFRONT,PhysBodyBoneName=Chassis,ArmorPlateName=FRONTRIGHTTURRET)
	ArmorHitZones(28)=(ZoneName=RIGHTTURRETONE,PhysBodyBoneName=Chassis,ArmorPlateName=RIGHTTURRET)
	ArmorHitZones(29)=(ZoneName=RIGHTTURRETTWO,PhysBodyBoneName=Turret,ArmorPlateName=RIGHTTURRET)
	ArmorHitZones(30)=(ZoneName=RIGHTTURRETTHREE,PhysBodyBoneName=Turret,ArmorPlateName=RIGHTTURRET)
	ArmorHitZones(31)=(ZoneName=RIGHTTURRETFOUR,PhysBodyBoneName=Turret,ArmorPlateName=RIGHTTURRET)
	ArmorHitZones(32)=(ZoneName=RIGHTTURRETFIVE,PhysBodyBoneName=Turret,ArmorPlateName=RIGHTTURRET)
	ArmorHitZones(33)=(ZoneName=RIGHTTURRETROOFONE,PhysBodyBoneName=Turret,ArmorPlateName=HULLROOF)
	ArmorHitZones(34)=(ZoneName=RIGHTTURRETROOFTWO,PhysBodyBoneName=Turret,ArmorPlateName=HULLROOF)
	ArmorHitZones(35)=(ZoneName=LEFTTURRETFRONT,PhysBodyBoneName=Turret,ArmorPlateName=FRONTLEFTTURRET)
	ArmorHitZones(36)=(ZoneName=LEFTTURRETONE,PhysBodyBoneName=Turret,ArmorPlateName=LEFTTURRET)
	ArmorHitZones(37)=(ZoneName=LEFTTURRETTWO,PhysBodyBoneName=Turret,ArmorPlateName=LEFTTURRET)
	ArmorHitZones(38)=(ZoneName=LEFTTURRETTHREE,PhysBodyBoneName=Turret,ArmorPlateName=LEFTTURRET)
	ArmorHitZones(39)=(ZoneName=LEFTTURRETFOUR,PhysBodyBoneName=Turret,ArmorPlateName=LEFTTURRET)
	ArmorHitZones(40)=(ZoneName=LEFTTURRETFIVE,PhysBodyBoneName=Turret,ArmorPlateName=LEFTTURRET)
	ArmorHitZones(41)=(ZoneName=LEFTTURRETROOFONE,PhysBodyBoneName=Turret,ArmorPlateName=HULLROOF)
	ArmorHitZones(42)=(ZoneName=LEFTTURRETROOFTWO,PhysBodyBoneName=Turret,ArmorPlateName=HULLROOF)
	ArmorHitZones(43)=(ZoneName=RIGHTWHEELGAURD,PhysBodyBoneName=Turret,ArmorPlateName=RIGHTGUARD)
	ArmorHitZones(44)=(ZoneName=RIGHTSTRUTONE,PhysBodyBoneName=Turret,ArmorPlateName=RIGHTSTRUT)
	ArmorHitZones(45)=(ZoneName=RIGHTSTRUTTWO,PhysBodyBoneName=Turret,ArmorPlateName=RIGHTSTRUT)
	ArmorHitZones(46)=(ZoneName=LEFTWHEELGAURD,PhysBodyBoneName=Turret,ArmorPlateName=LEFTGUARD)
	ArmorHitZones(47)=(ZoneName=LEFTSTRUTONE,PhysBodyBoneName=Turret,ArmorPlateName=LEFTSTRUT)
	ArmorHitZones(48)=(ZoneName=LEFTSTRUTTWO,PhysBodyBoneName=Turret,ArmorPlateName=LEFTSTRUT)
	ArmorHitZones(49)=(ZoneName=RIGHTEQUIPMENTBOX,PhysBodyBoneName=Turret,ArmorPlateName=RIGHTBOX)
	ArmorHitZones(50)=(ZoneName=LEFTEQUIPMENTBOX,PhysBodyBoneName=Turret,ArmorPlateName=RIGHTBOX)
	ArmorHitZones(51)=(ZoneName=GUNBASE,PhysBodyBoneName=Turret,ArmorPlateName=MANTLET)
	ArmorHitZones(52)=(ZoneName=TURRETFRONTLEFT,PhysBodyBoneName=Turret,ArmorPlateName=TURRETFRONT)
	ArmorHitZones(53)=(ZoneName=TURRETFRONTRIGHT,PhysBodyBoneName=Turret,ArmorPlateName=TURRETFRONT)
	ArmorHitZones(54)=(ZoneName=TURRETLEFTONE,PhysBodyBoneName=Turret,ArmorPlateName=TURRETFRONT)
	ArmorHitZones(55)=(ZoneName=TURRETLEFTTWO,PhysBodyBoneName=Turret,ArmorPlateName=TURRETFRONT)
	ArmorHitZones(56)=(ZoneName=TURRETRIGHTONE,PhysBodyBoneName=Turret,ArmorPlateName=TURRETFRONT)
	ArmorHitZones(57)=(ZoneName=TURRETRIGHTTWO,PhysBodyBoneName=Turret,ArmorPlateName=TURRETFRONT)
	ArmorHitZones(58)=(ZoneName=TURRETREARONE,PhysBodyBoneName=Chassis,ArmorPlateName=TURRETREAR)
	ArmorHitZones(59)=(ZoneName=TURRETREARTWO,PhysBodyBoneName=Turret_barrel,ArmorPlateName=TURRETREAR)
	ArmorHitZones(60)=(ZoneName=TURRETREARTHREE,PhysBodyBoneName=Turret,ArmorPlateName=TURRETREAR)
	ArmorHitZones(61)=(ZoneName=TURRETREARFOUR,PhysBodyBoneName=Turret,ArmorPlateName=TURRETREAR)
	ArmorHitZones(62)=(ZoneName=TURRETREARFIVE,PhysBodyBoneName=Turret,ArmorPlateName=TURRETREAR)
	ArmorHitZones(63)=(ZoneName=TURRETBUSTLEONE,PhysBodyBoneName=Turret,ArmorPlateName=TURRETBUSTLE)
	ArmorHitZones(64)=(ZoneName=TURRETBUSTLETWO,PhysBodyBoneName=Turret,ArmorPlateName=TURRETBUSTLE)
	ArmorHitZones(65)=(ZoneName=TURRETBUSTLETHREE,PhysBodyBoneName=Turret,ArmorPlateName=TURRETBUSTLE)
	ArmorHitZones(66)=(ZoneName=TURRETBUSTLEFOUR,PhysBodyBoneName=Turret,ArmorPlateName=TURRETBUSTLE)
	ArmorHitZones(67)=(ZoneName=TURRETBUSTLEFIVE,PhysBodyBoneName=Turret,ArmorPlateName=TURRETBUSTLE)
	ArmorHitZones(68)=(ZoneName=TURRETBUSTLESIX,PhysBodyBoneName=Turret,ArmorPlateName=TURRETBUSTLE)
	ArmorHitZones(69)=(ZoneName=TURRETBUSTLESEVEN,PhysBodyBoneName=Turret,ArmorPlateName=TURRETBUSTLE)
	ArmorHitZones(70)=(ZoneName=TURRETROOF,PhysBodyBoneName=Turret,ArmorPlateName=TURRETROOF)
	ArmorHitZones(71)=(ZoneName=BUSTLEROOF,PhysBodyBoneName=Turret,ArmorPlateName=TURRETROOF)
	ArmorHitZones(72)=(ZoneName=BUSTLEFLOOR,PhysBodyBoneName=Turret,ArmorPlateName=TURRETFLOOR)

	// Actual data for the plates
	ArmorPlates(0)=(PlateName=FRONTGLACISLOWER,ArmorZoneType=AZT_Front,PlateThickness=30,OverallHardness=400,bHighHardness=true)
	ArmorPlates(1)=(PlateName=DRIVERFRONT,ArmorZoneType=AZT_Front,PlateThickness=30,OverallHardness=400,bHighHardness=true)
	ArmorPlates(2)=(PlateName=DRIVERSIDE,ArmorZoneType=AZT_Left,PlateThickness=20,OverallHardness=450,bHighHardness=true)
	ArmorPlates(3)=(PlateName=CHASSISROOF,ArmorZoneType=AZT_Roof,PlateThickness=15,OverallHardness=450,bHighHardness=true)
	ArmorPlates(4)=(PlateName=FLOOR,ArmorZoneType=AZT_Floor,PlateThickness=15,OverallHardness=450,bHighHardness=true)
	ArmorPlates(5)=(PlateName=REARPLATE,ArmorZoneType=AZT_Back,PlateThickness=20,OverallHardness=450,bHighHardness=true)
	ArmorPlates(6)=(PlateName=HULLROOF,ArmorZoneType=AZT_Roof,PlateThickness=15,OverallHardness=450,bHighHardness=true)
	ArmorPlates(7)=(PlateName=RIGHTHULL,ArmorZoneType=AZT_Right,PlateThickness=20,OverallHardness=450,bHighHardness=true)
	ArmorPlates(8)=(PlateName=RIGHTCHASSIS,ArmorZoneType=AZT_Right,PlateThickness=20,OverallHardness=450,bHighHardness=true)
	ArmorPlates(9)=(PlateName=LEFTHULL,ArmorZoneType=AZT_Left,PlateThickness=20,OverallHardness=450,bHighHardness=true)
	ArmorPlates(10)=(PlateName=LEFTCHASSIS,ArmorZoneType=AZT_left,PlateThickness=20,OverallHardness=450,bHighHardness=true)
	ArmorPlates(11)=(PlateName=FRONTRIGHTTURRET,ArmorZoneType=AZT_Front,PlateThickness=22,OverallHardness=410,bHighHardness=true)
	ArmorPlates(12)=(PlateName=RIGHTTURRET,ArmorZoneType=AZT_Right,PlateThickness=20,OverallHardness=450,bHighHardness=true)
	ArmorPlates(13)=(PlateName=FRONTLEFTTURRET,ArmorZoneType=AZT_Front,PlateThickness=22,OverallHardness=410,bHighHardness=true)
	ArmorPlates(14)=(PlateName=LEFTTURRET,ArmorZoneType=AZT_Left,PlateThickness=20,OverallHardness=450,bHighHardness=true)
	ArmorPlates(15)=(PlateName=RIGHTGUARD,ArmorZoneType=AZT_Right,PlateThickness=10,OverallHardness=400,bHighHardness=true)
	ArmorPlates(16)=(PlateName=RIGHTSTRUT,ArmorZoneType=AZT_Right,PlateThickness=15,OverallHardness=280,bHighHardness=false)
	ArmorPlates(17)=(PlateName=LEFTGUARD,ArmorZoneType=AZT_Left,PlateThickness=10,OverallHardness=400,bHighHardness=true)
	ArmorPlates(18)=(PlateName=LEFTSTRUT,ArmorZoneType=AZT_Left,PlateThickness=15,OverallHardness=280,bHighHardness=false)
	ArmorPlates(19)=(PlateName=RIGHTBOX,ArmorZoneType=AZT_Right,PlateThickness=15,OverallHardness=310,bHighHardness=false)
	ArmorPlates(20)=(PlateName=LEFTBOX,ArmorZoneType=AZT_Right,PlateThickness=15,OverallHardness=310,bHighHardness=false)
	ArmorPlates(21)=(PlateName=MANTLET,ArmorZoneType=AZT_TurretFront,PlateThickness=20,OverallHardness=450,bHighHardness=true)
	ArmorPlates(22)=(PlateName=TURRETFRONT,ArmorZoneType=AZT_TurretFront,PlateThickness=20,OverallHardness=450,bHighHardness=true)
	ArmorPlates(23)=(PlateName=TURRETREAR,ArmorZoneType=AZT_TurretBack,PlateThickness=20,OverallHardness=450,bHighHardness=true)
	ArmorPlates(24)=(PlateName=TURRETBUSTLE,ArmorZoneType=AZT_TurretBack,PlateThickness=20,OverallHardness=450,bHighHardness=true)
	ArmorPlates(25)=(PlateName=TURRETFLOOR,ArmorZoneType=AZT_TurretBack,PlateThickness=20,OverallHardness=450,bHighHardness=true)
	ArmorPlates(26)=(PlateName=TURRETROOF,ArmorZoneType=AZT_TurretFront,PlateThickness=20,OverallHardness=450,bHighHardness=true)
	ArmorPlates(27)=(PlateName=FRONTGLACISUPPER,ArmorZoneType=AZT_Front,PlateThickness=15,OverallHardness=450,bHighHardness=true)

//	ArmorPlates(28)=(PlateName=GUNNERSVISIONSLIT,ArmorZoneType=AZT_WeakSpots,PlateThickness=5,OverallHardness=100,bHighHardness=false)
//	ArmorPlates(29)=(PlateName=DRIVERSVISIONSLIT,ArmorZoneType=AZT_WeakSpots,PlateThickness=5,OverallHardness=100,bHighHardness=false)
//	ArmorPlates(27)=(PlateName=LOADERSVISIONSLIT,ArmorZoneType=AZT_WeakSpots,PlateThickness=5,OverallHardness=100,bHighHardness=false)


	VehicleEffects(TankVFX_Firing1)=(EffectStartTag=T28_Cannon,EffectTemplate=ParticleSystem'WinterWar_FX.ParticleSystems.FX_WW_MuzzleFlash_T-28',EffectSocket=Barrel)
	
	// We have to sacrifice an MG muzzleflash for the cannon muzzleflash
//	VehicleEffects(TankVFX_Firing1)=(EffectStartTag=T28_TurretMG,	EffectTemplate=ParticleSystem'FX_VN_Weapons.MuzzleFlashes.FX_VN_MuzzleFlash_1stP_Rifles_split',EffectSocket=TurretMG)
	VehicleEffects(TankVFX_Firing2)=(EffectStartTag=T28_LeftMG,		EffectTemplate=ParticleSystem'FX_VN_Weapons.MuzzleFlashes.FX_VN_MuzzleFlash_1stP_Rifles_split',EffectSocket=LeftMG)
	VehicleEffects(TankVFX_Firing3)=(EffectStartTag=T28_RightMG,	EffectTemplate=ParticleSystem'FX_VN_Weapons.MuzzleFlashes.FX_VN_MuzzleFlash_1stP_Rifles_split',EffectSocket=RightMG)
	VehicleEffects(TankVFX_Firing4)=(EffectStartTag=T28_RearMG,		EffectTemplate=ParticleSystem'FX_VN_Weapons.MuzzleFlashes.FX_VN_MuzzleFlash_1stP_Rifles_split',EffectSocket=RearMG)
	
	VehicleEffects(TankVFX_Exhaust)=(EffectStartTag=EngineStart,EffectEndTag=EngineStop,EffectTemplate=ParticleSystem'WinterWar_FX.ParticleSystems.FX_VEH_exhaust_T28',EffectSocket=Exhaust)
	VehicleEffects(TankVFX_TreadWing)=(EffectStartTag=EngineStart,EffectEndTag=EngineStop,bStayActive=true,EffectTemplate=ParticleSystem'WinterWar_FX.ParticleSystems.FX_VEH_LightTank_A_Wing_Snow',EffectSocket=attachments_body_ground)
	
	ExplosionDamageType=class'RODmgType_VehicleExplosion'
	ExplosionDamage=100.0
	ExplosionRadius=300.0
	ExplosionMomentum=60000
	ExplosionInAirAngVel=1.5
	InnerExplosionShakeRadius=400.0
	OuterExplosionShakeRadius=1000.0
	ExplosionLightClass=class'ROGame.ROGrenadeExplosionLight'
	MaxExplosionLightDistance=4000.0
	TimeTilSecondaryVehicleExplosion=2.0f
	SecondaryExplosion=ParticleSystem'WinterWar_FX.ParticleSystems.FX_VEH_Tank_C_Explosion'
	bHasTurretExplosion=true
	TurretExplosiveForce=15000
	
	BigExplosionSocket=FX_Fire
	ExplosionTemplate=ParticleSystem'WinterWar_FX.ParticleSystems.FX_VEH_Tank_C_Explosion'
	
	BurnOutPhase1Timer=25.0
	BurnOutPhase2Timer=30.0
	BurnOutPhase3Timer=40.0
	
	FireEmitterSocketName=attachments_engine
	
	CannonFireImpulseMag=20000
	CannonFireTorqueMag=9375
	
	RPM3DGaugeMaxAngle=54613
	EngineIdleRPM=600
	EngineNormalRPM=3000
	EngineMaxRPM=3750
	Speedo3DGaugeMaxAngle=109226
	EngineTemp3DGaugeNormalAngle=33000
	EngineTemp3DGaugeEngineDamagedAngle=39500
	EngineTemp3DGaugeFireDamagedAngle=47500
	EngineTemp3DGaugeFireDestroyedAngle=54613
	
	EngineTextureOffset=(PositionOffset=(X=58,Y=83,Z=0),MySizeX=24,MYSizeY=24)
	
	TreadTextureOffsets(0)=(PositionOffset=(X=36,Y=34,Z=0),MySizeX=8,MYSizeY=70)
	TreadTextureOffsets(1)=(PositionOffset=(X=95,Y=34,Z=0),MySizeX=8,MYSizeY=70)
	
	SeatTextureOffsets(`T28_DRIVER)=(PositionOffSet=(X=0,Y=-24,Z=0),bTurretPosition=0)
	SeatTextureOffsets(`T28_MG_H_R)=(PositionOffSet=(X=10,Y=-20,Z=0),bTurretPosition=0)
	SeatTextureOffsets(`T28_MG_H_L)=(PositionOffSet=(X=-10,Y=-20,Z=0),bTurretPosition=0)
	SeatTextureOffsets(`T28_GUNNER)=(PositionOffSet=(X=-6,Y=-3,Z=0),bTurretPosition=1)
	SeatTextureOffsets(`T28_MG_T_R)=(PositionOffSet=(X=0,Y=20,Z=0),bTurretPosition=1)
	
	SpeedoMinDegree=5461
	SpeedoMaxDegree=60075
	SpeedoMaxSpeed=1365
	
	RelatedDamageTypes(0)=class'WWDmgType_DT_T28'
	RelatedDamageTypes(1)=class'WWDmgType_T28Shell_AP'
	RelatedDamageTypes(2)=class'WWDmgType_T28Shell_AP_General'
	RelatedDamageTypes(3)=class'WWDmgType_T28Shell_HE'
	RelatedDamageTypes(4)=class'WWDmgType_T28Shell_HEImpact'
	RelatedDamageTypes(5)=class'WWDmgType_T28Shell_HEImpactGeneral'
}
