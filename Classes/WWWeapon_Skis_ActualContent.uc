
class WWWeapon_Skis_ActualContent extends WWWeapon_Skis;

DefaultProperties
{
	ReferenceSkeletalMesh=SkeletalMesh'WinterWar_VH_FIN_SKIS.Mesh.Skis_RIG'
	
	Begin Object Name=FirstPersonMesh
		DepthPriorityGroup=SDPG_Foreground
		SkeletalMesh=SkeletalMesh'WinterWar_VH_FIN_SKIS.Mesh.FIN_Skis'
		PhysicsAsset=PhysicsAsset'WP_VN_VC_PickMattock.Phys.VC_PickMattock_Physics'
		AnimSets(0)=AnimSet'WP_VN_VC_PickMattock.Animation.WP_PickMattockhands'
		AnimTreeTemplate=AnimTree'WP_VN_VC_PickMattock.Animation.VC_PickMattock_Tree'
		Scale=1.0
		FOV=70
	End Object
	
	AttachmentClass=class'WWWeapon_Skis_Attach'
	
	ArmsAnimSet=AnimSet'WP_VN_VC_PickMattock.Animation.WP_PickMattockhands'
}
