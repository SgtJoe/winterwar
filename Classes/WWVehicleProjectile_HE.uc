
class WWVehicleProjectile_HE extends ROTankCannonProjectile;

defaultproperties
{
	LifeSpan=+0018.000000
	
	BallisticCoefficient=2.0
	Speed=16750 //335 M/S
	MaxSpeed=16750
	Damage=300
	DamageRadius=550
	MomentumTransfer=43000
	
	Caliber=45
	ActualRHA=20
	TestPlateHardness=400
	SlopeEffect=0.82472
	ShatterNumber=0.85
	ShatterTd=0.85
	ShatteredPenEffectiveness=0.7
	
	ExplosionSound=AkEvent'WW_CMD_Canberra.Play_CMD_Canberra_Bomb_EXP'
	
	ProjExplosionTemplate=ParticleSystem'FX_VN_Weapons.Explosions.FX_VN_Grenade_explosion'
	
	ShakeScale=2.5
	MaxSuppressBlurDuration=4.5
	SuppressBlurScalar=1.5
	SuppressAnimationScalar=0.6
	ExplodeExposureScale=0.45
	
	Begin Object Name=CollisionCylinder
		CollisionRadius=4
		CollisionHeight=4
		AlwaysLoadOnClient=True
		AlwaysLoadOnServer=True
	End Object
	
	Begin Object Class=DynamicLightEnvironmentComponent Name=MyLightEnvironment
	End Object
	Components.Add(MyLightEnvironment)
	
	Begin Object Class=StaticMeshComponent Name=ProjectileMesh
		StaticMesh=StaticMesh'WinterWar_VH_USSR_COMMON.Mesh.Panzer_IVG_Warhead'
		Materials(0)=MaterialInstanceConstant'WinterWar_VH_USSR_COMMON.MIC.Warhead'
		MaxDrawDistance=500000
		CollideActors=true
		CastShadow=false
		LightEnvironment=MyLightEnvironment
		BlockActors=false
		BlockZeroExtent=true
		BlockNonZeroExtent=true//false
		BlockRigidBody=true
		Scale=0.6
	End Object
	Components.Add(ProjectileMesh)
	
	bExplodeOnDeflect=true
	bExplodeWhenHittingInfantry=true
}
