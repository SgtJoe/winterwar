
class WWFighterPlaneManager extends ROSAMSpawner;

var array<WWFighterPlane> SpawnedPlanes;

function OneSecondLoopingTimer()
{
	local ROTeamInfo ROTI;
	local WWFighterPlane NewPlane;
	local vector SpawnLocation;
	
	if (SpawnedPlanes.length >= 2)
	{
		return;
	}
	
	if (CurrentTarget != none)
	{
		if (ROSupportAircraft(CurrentTarget) != none && !ROSupportAircraft(CurrentTarget).bBegunStrike)
		{
			return;
		}
		
		if (WWAerialReconPlane(CurrentTarget) != none && !WWAerialReconPlane(CurrentTarget).IsInMapBounds())
		{
			return;
		}
		
		if (WorldInfo.TimeSeconds - StartTargetTime >= TargetLockTime)
		{
			SpawnLocation = Location;
			SpawnLocation.Z = CurrentTarget.Location.Z + (200 - rand(400));
			
			NewPlane = Spawn(class'WWFighterPlane',InstigatorController,,SpawnLocation,rotator(CurrentTarget.Location - Location));
			NewPlane.PlaneManager = self;
			NewPlane.InstigatorController = InstigatorController;
			
			NewPlane.SetTarget(CurrentTarget);
			
			SpawnedPlanes.AddItem(NewPlane);
			
			`wwdebug("SpawnedPlanes" @ SpawnedPlanes.length @ "Plane height offset:" @ (SpawnLocation.Z - CurrentTarget.Location.Z),'CMDA');
			
			// Clear the target so we can get another one for the next plane
			CurrentTarget = none;
		}
		
		return;
	}
	
	ROTI = ROTeamInfo(WorldInfo.GRI.Teams[EnemyTeam]);
	
	if (ROTI.ActiveSupportAircraft != none && ROTI.ActiveSupportAircraft.Health > 0 && !WWCarpetBomberAircraft(ROTI.ActiveSupportAircraft).MarkedForDeath)
	{
		CurrentTarget = ROTI.ActiveSupportAircraft;
	}
	else if (ROTI.SecondarySupportAircraft != none && ROTI.SecondarySupportAircraft.Health > 0 && !WWCarpetBomberAircraft(ROTI.SecondarySupportAircraft).MarkedForDeath)
	{
		CurrentTarget = ROTI.SecondarySupportAircraft;
	}
	else if (ROTI.AerialReconPlane != none && ROTI.AerialReconPlane.Health > 0 && !WWAerialReconPlane(ROTI.AerialReconPlane).MarkedForDeath)
	{
		CurrentTarget = ROTI.AerialReconPlane;
	}
	
	if (CurrentTarget != none)
	{
		StartTargetTime = WorldInfo.TimeSeconds;
		
		`wwdebug("CurrentTarget" @ CurrentTarget,'CMDA');
		
		if (WWAerialReconPlane(CurrentTarget) != none)
		{
			WWAerialReconPlane(CurrentTarget).MarkedForDeath = true;
		}
		else if (WWCarpetBomberAircraft(CurrentTarget) != none)
		{
			WWCarpetBomberAircraft(CurrentTarget).MarkedForDeath = true;
		}
	}
}

simulated function RemovePlane(WWFighterPlane P)
{
	local int i;
	
	for (i = 0; i < SpawnedPlanes.length; i++)
	{
		if (SpawnedPlanes[i] == P)
		{
			SpawnedPlanes.Remove(i, 1);
		}
	}
	
	`wwdebug("SpawnedPlanes" @ SpawnedPlanes.length,'CMDA');
}

function Destroyed()
{
	local int i;
	
	super.Destroyed();
	
	`wwdebug("",'CMDA');
	
	for (i = 0; i < SpawnedPlanes.length; i++)
	{
		SpawnedPlanes[i].PlaneManager = none;
	}
	
	SpawnedPlanes.length = 0;
}

DefaultProperties
{
	Lifespan=60
	
	TargetLockTime=1.5
}
