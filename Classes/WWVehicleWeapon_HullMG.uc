
class WWVehicleWeapon_HullMG extends ROVWeap_Transport_HullMG
	abstract;

simulated function TimeReloading()
{
	local float ReloadDuration;
	
	if (WWVehicleTransport(MyVehicle) != none)
	{
		ReloadDuration = WWVehicleTransport(MyVehicle).GetHullMGReloadDuration();
	}
	
	`wwdebug("MG ReloadDuration:" @ ReloadDuration,'VH');
	
	if (Role == ROLE_Authority)
	{
		bMGReloading = true;
	}
	
	if (WorldInfo.NetMode == NM_DedicatedServer || (WorldInfo.NetMode == NM_ListenServer && !Instigator.IsLocallyControlled()))
	{
		ReloadDuration *= 0.9;
	}
	
	if (WorldInfo.NetMode != NM_DedicatedServer)
	{
		PlayReload(ReloadDuration);
	}
	
	SetTimer(ReloadDuration, false, 'ReloadComplete');
}

simulated function PlayReload(float ReloadDuration)
{
	if (WWVehicleTransport(MyVehicle) != none)
	{
		WWVehicleTransport(MyVehicle).VehicleHullMGReload();
	}
}

function ReloadComplete()
{
	if( Role == ROLE_Authority )
	{
		bMGReloading = false;
		PerformReload();
	}
	
	`wwdebug("MG reloaded",'VH');
}