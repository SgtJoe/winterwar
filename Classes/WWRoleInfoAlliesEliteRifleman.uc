
class WWRoleInfoAlliesEliteRifleman extends WWRoleInfoAllies;

DefaultProperties
{
	RoleType=RORIT_Rifleman
	ClassTier=2
	ClassIndex=`RI_ELITE_RIFLEMAN
	
	Items[RORIGM_Default]={(
		PrimaryWeapons=(class'WWWeapon_SVT38',class'WWWeapon_MN38'),
		
		OtherItems=(class'WWWeapon_F1Grenade',class'WWWeapon_RDG1'),
		
		SquadLeaderItems=(class'WWWeapon_Binoculars')
	)}
	
	ClassIcon=Texture2D'WinterWar_UI.RoleIcons.RoleIcon_EliteRifleman'
}
