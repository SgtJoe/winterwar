
class WWVehicleProjectile_AP extends ROTankCannonProjectile;

var SoundCue VehiclePenetrateSoundNew, VehicleDeflectSoundNew;

simulated function PenetrateFX(vector HitLocation, vector HitNormal)
{
	local vector LightLoc, LightHitLocation, LightHitNormal;
	local vector Direction;
	local ParticleSystemComponent ProjExplosion;
	local Actor EffectAttachActor;
	
	bShuttingDown=true;
	SetPhysics(PHYS_None);
	bDoPenetrateEffect = true;
	bForceNetUpdate=true;
	
	if (ProjEffects!=None)
	{
		ProjEffects.DeactivateSystem();
	}
	if (WorldInfo.NetMode != NM_DedicatedServer && !bSuppressExplosionFX)
	{
		if (ProjectileLight != None)
		{
			DetachComponent(ProjectileLight);
			ProjectileLight = None;
		}
		if (ProjPenetrateTemplate != None && EffectIsRelevant(Location, false, MaxEffectDistance))
		{
			EffectAttachActor = (bAttachExplosionToVehicles || (ROVehicleBase(ImpactedActor) == None)) ? ImpactedActor : None;
			Direction = normal(Velocity - 2.0 * HitNormal * (Velocity dot HitNormal)) * Vect(1,1,0);
			ProjExplosion = WorldInfo.MyEmitterPool.SpawnEmitter(ProjPenetrateTemplate, HitLocation, rotator(Direction), EffectAttachActor);
			ProjExplosion.SetVectorParameter('Velocity',Direction);
			ProjExplosion.SetVectorParameter('HitNormal',HitNormal);
			SetExplosionEffectParameters(ProjExplosion);
			if ( !WorldInfo.bDropDetail && ((ExplosionLightClass != None) || (ExplosionDecal != none)) && ShouldSpawnExplosionLight(HitLocation, HitNormal) )
			{
				if ( ExplosionLightClass != None )
				{
					if (Trace(LightHitLocation, LightHitNormal, HitLocation + (0.25 * ExplosionLightClass.default.TimeShift[0].Radius * HitNormal), HitLocation, false) == None)
					{
						LightLoc = HitLocation + (0.25 * ExplosionLightClass.default.TimeShift[0].Radius * HitNormal);
					}
					else
					{
						LightLoc = HitLocation + (0.5 * VSize(HitLocation - LightHitLocation) * HitNormal);
					}
					ROEmitterPool(WorldInfo.MyEmitterPool).SpawnExplosionLight(ExplosionLightClass, LightLoc, EffectAttachActor);
				}
			}
		}
		if (VehiclePenetrateSoundNew != None && !bSuppressSounds)
		{
			PlaySound(VehiclePenetrateSoundNew, true);
		}
		bSuppressExplosionFX=true;
	}
	
	HideProjectile();
	SetCollision(false,false);
	if (bWaitForEffects)
	{
		if (WorldInfo.NetMode == NM_DedicatedServer)
		{
			LifeSpan = 0.15;
		}
		else
		{
			LifeSpan = FMax(LifeSpan, 2.0);
		}
	}
	else
	{
		Destroy();
	}
}

simulated function DeflectFX(vector HitLocation, vector HitNormal)
{
	local vector LightLoc, LightHitLocation, LightHitNormal;
	local vector Direction;
	local ParticleSystemComponent ProjExplosion;
	local Actor EffectAttachActor;
	
	if(Role == ROLE_Authority)
	{
		bDoDeflectEffect = true;
		bForceNetUpdate = true;
	}
	if (ProjEffects!=None)
	{
		if(ProjPostDeflectTemplate != none)
		{
			ProjEffects.SetTemplate(ProjPostDeflectTemplate);
		}
	}
	if (WorldInfo.NetMode != NM_DedicatedServer && !bSuppressDeflectFX)
	{
		if (ProjDefelectTemplate != None && EffectIsRelevant(Location, false, MaxEffectDistance))
		{
			EffectAttachActor = (bAttachExplosionToVehicles || (ROVehicleBase(ImpactedActor) == None)) ? ImpactedActor : None;
			Direction = normal(Velocity - 2.0 * HitNormal * (Velocity dot HitNormal)) * Vect(1,1,0);
			ProjExplosion = WorldInfo.MyEmitterPool.SpawnEmitter(ProjDefelectTemplate, HitLocation, rotator(Direction), EffectAttachActor);
			ProjExplosion.SetVectorParameter('Velocity',Direction);
			ProjExplosion.SetVectorParameter('HitNormal',HitNormal);
			SetExplosionEffectParameters(ProjExplosion);
			if ( !WorldInfo.bDropDetail && ((ExplosionLightClass != None) || (ExplosionDecal != none)) && ShouldSpawnExplosionLight(HitLocation, HitNormal) )
			{
				if ( ExplosionLightClass != None )
				{
					if (Trace(LightHitLocation, LightHitNormal, HitLocation + (0.25 * ExplosionLightClass.default.TimeShift[0].Radius * HitNormal), HitLocation, false) == None)
					{
						LightLoc = HitLocation + (0.25 * ExplosionLightClass.default.TimeShift[0].Radius * HitNormal);
					}
					else
					{
						LightLoc = HitLocation + (0.5 * VSize(HitLocation - LightHitLocation) * HitNormal);
					}
					ROEmitterPool(WorldInfo.MyEmitterPool).SpawnExplosionLight(ExplosionLightClass, LightLoc, EffectAttachActor);
				}
			}
		}
		if (VehicleDeflectSoundNew != None && !bSuppressSounds)
		{
			PlaySound(VehicleDeflectSoundNew, true);
		}
		bSuppressDeflectFX=true;
	}
}

simulated function ShatterFX(vector HitLocation, vector HitNormal)
{
	local vector LightLoc, LightHitLocation, LightHitNormal;
	local vector Direction;
	local ParticleSystemComponent ProjExplosion;
	local Actor EffectAttachActor;
	bDoShatterEffect=true;
	bShuttingDown=true;
	SetPhysics(PHYS_None);
	if (ProjEffects!=None)
	{
		ProjEffects.DeactivateSystem();
	}
	if (WorldInfo.NetMode != NM_DedicatedServer && !bSuppressExplosionFX)
	{
		if (ProjectileLight != None)
		{
			DetachComponent(ProjectileLight);
			ProjectileLight = None;
		}
		if (ProjShatterTemplate != None && EffectIsRelevant(Location, false, MaxEffectDistance))
		{
			EffectAttachActor = (bAttachExplosionToVehicles || (ROVehicleBase(ImpactedActor) == None)) ? ImpactedActor : None;
			Direction = normal(Velocity - 2.0 * HitNormal * (Velocity dot HitNormal)) * Vect(1,1,0);
			ProjExplosion = WorldInfo.MyEmitterPool.SpawnEmitter(ProjShatterTemplate, HitLocation, rotator(Direction), EffectAttachActor);
			ProjExplosion.SetVectorParameter('Velocity',Direction);
			ProjExplosion.SetVectorParameter('HitNormal',HitNormal);
			SetExplosionEffectParameters(ProjExplosion);
			if ( !WorldInfo.bDropDetail && ((ExplosionLightClass != None) || (ExplosionDecal != none)) && ShouldSpawnExplosionLight(HitLocation, HitNormal) )
			{
				if ( ExplosionLightClass != None )
				{
					if (Trace(LightHitLocation, LightHitNormal, HitLocation + (0.25 * ExplosionLightClass.default.TimeShift[0].Radius * HitNormal), HitLocation, false) == None)
					{
						LightLoc = HitLocation + (0.25 * ExplosionLightClass.default.TimeShift[0].Radius * HitNormal);
					}
					else
					{
						LightLoc = HitLocation + (0.5 * VSize(HitLocation - LightHitLocation) * HitNormal);
					}
					ROEmitterPool(WorldInfo.MyEmitterPool).SpawnExplosionLight(ExplosionLightClass, LightLoc, EffectAttachActor);
				}
			}
		}
		if (VehicleDeflectSoundNew != None && !bSuppressSounds)
		{
			PlaySound(VehicleDeflectSoundNew, true);
		}
		bSuppressExplosionFX=true;
	}
	HideProjectile();
	SetCollision(false,false);
	if (bWaitForEffects)
	{
		if (WorldInfo.NetMode == NM_DedicatedServer)
		{
			LifeSpan = 0.15;
		}
		else
		{
			LifeSpan = FMax(LifeSpan, 2.0);
		}
	}
	else
	{
		Destroy();
	}
}

defaultproperties
{
	LifeSpan=+0018.000000
	
	BallisticCoefficient=1.55
	Speed=38000 // 760 M/S
	MaxSpeed=38000
	ImpactDamage=350
	Damage=100
	DamageRadius=50
	MomentumTransfer=43000
	
	Caliber=45
	ActualRHA=58
	TestPlateHardness=400
	SlopeEffect=0.09559
	ShatterNumber=0.85
	ShatterTd=0.85
	ShatteredPenEffectiveness=0.8
	
	PenetrationDepth=95
	MaxPenetrationTests=6
	
	ExplosionSound=AkEvent'WW_CMD_AntiAir.Play_EXP_SAM_Explode'
	
	ProjExplosionTemplate=ParticleSystem'FX_VN_Weapons.Impacts.FX_WEP_20mm'
	ProjDefelectTemplate= ParticleSystem'FX_VN_Weapons.Impacts.FX_WEP_20mm'
	ProjPenetrateTemplate=ParticleSystem'FX_VEH_Tank_Three.FX_VEH_Tank_B_TankShell_Penetrate'
	
	VehiclePenetrateSoundNew=	SoundCue'WinterWar_VH_USSR_COMMON_SFX.Audio.Tank_AP_Armor_Penetrate_Cue'
	VehicleDeflectSoundNew=		SoundCue'WinterWar_VH_USSR_COMMON_SFX.Audio.Tank_AP_Armor_Deflect_Cue'
	
	Begin Object Name=CollisionCylinder
		CollisionRadius=4
		CollisionHeight=4
		AlwaysLoadOnClient=True
		AlwaysLoadOnServer=True
	End Object
	
	Begin Object Class=DynamicLightEnvironmentComponent Name=MyLightEnvironment
	End Object
	Components.Add(MyLightEnvironment)
	
	Begin Object Class=StaticMeshComponent Name=ProjectileMesh
		StaticMesh=StaticMesh'WinterWar_VH_USSR_COMMON.Mesh.Panzer_IVG_Warhead'
		Materials(0)=MaterialInstanceConstant'WinterWar_VH_USSR_COMMON.MIC.Warhead'
		MaxDrawDistance=500000
		CollideActors=true
		CastShadow=false
		LightEnvironment=MyLightEnvironment
		BlockActors=false
		BlockZeroExtent=true
		BlockNonZeroExtent=true//false
		BlockRigidBody=true
		Scale=0.6
	End Object
	Components.Add(ProjectileMesh)
}
