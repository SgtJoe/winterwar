
class WWVehicleWeapon_T20_HullMG extends WWVehicleWeapon_HullMG
	abstract;

defaultproperties
{
	WeaponContentClass(0)="WinterWar.WWVehicleWeapon_T20_HullMG_ActualContent"
	
	SeatIndex=1
	
	PlayerIronSightFOV=55
	
	FiringStatesArray(0)=WeaponFiring
	WeaponFireTypes(0)=EWFT_Projectile
	WeaponProjectiles(0)=class'WWProjectile_DT_T20'
	FireInterval(0)=+0.092
	FireCameraAnim(0)=CameraAnim'1stperson_Cameras.Anim.Camera_MG34_Shoot'
	Spread(0)=0.0007
	
	FiringStatesArray(ALTERNATE_FIREMODE)=none
	WeaponProjectiles(ALTERNATE_FIREMODE)=none
	FireInterval(ALTERNATE_FIREMODE)=+0.092
	FireCameraAnim(ALTERNATE_FIREMODE)=CameraAnim'1stperson_Cameras.Anim.Camera_MG34_Shoot'
	Spread(ALTERNATE_FIREMODE)=0.0007

	FireTriggerTags=(UCHullMG)
	AltFireTriggerTags=(UCHullMG)
	
	VehicleClass=class'WWVehicle_T20'
	
	AmmoClass=class'WWAmmo_DTDrum'
	MaxAmmoCount=63
	bUsesMagazines=true
	InitialNumPrimaryMags=`TANK_AMMO_T20_DT
	
	PenetrationDepth=22.23
	MaxPenetrationTests=3
	MaxNumPenetrations=2
	
	bUseOverlayWhenAiming=true
}
