
class WWLogger extends Object
	config (Game_WinterWar);

struct LogStruct
{
	var string LogType;
	var bool Enabled;
};

var config array<LogStruct> LogOptions;

static function WWLog(string Message, optional name Type)
{
	local int i, index;
	local bool LogExists;
	local LogStruct inData;
	
	Type = (Type == 'None') ? 'WinterWar' : name('WinterWar-'$Type);
	LogExists = false;
	index = 0;
	
	for (i = 0; i < default.LogOptions.length; i++)
	{
		if (string(Type) ~= default.LogOptions[i].LogType)
		{
			LogExists = true;
			index = i;
			break;
		}
	}
	
	if (LogExists && default.LogOptions[index].Enabled)
	{
		`log(Message,,Type);
	}
	else if (!LogExists)
	{
		inData.LogType = string(Type);
		inData.Enabled = true;
		
		default.LogOptions[default.LogOptions.length] = inData;
		
		StaticSaveConfig();
		
		`log("Creating new log" @ Type,,'WinterWar');
		`log(Message,,Type);
	}
}

static function WWDebugLog(string Message, string FuncTrace, optional name Type)
{
`ifndef(RELEASE)
	local WWPlayerController PC;
	PC = WWPlayerController(class'Engine'.static.GetCurrentWorldInfo().GetALocalPlayerController());
	
	Type = (Type == 'None') ? 'DEBUG' : Type;
	
	if (PC != none)
	{
		PC.ClientMessage("("$Type$")" @ FuncTrace @ Message);
		
		if (Message == "")
		{
			`log(FuncTrace,,name("WinterWar-"$Type));
		}
		else
		{
			`log(FuncTrace @ Message,,name("WinterWar-"$Type));
		}
	}
	else
	{
		foreach class'WorldInfo'.static.GetWorldInfo().AllControllers(class'WWPlayerController', PC)
		{
			PC.ClientMessage("("$Type$")" @ FuncTrace @ Message);
		}
		
		`log(FuncTrace @ Message,,name("WinterWar-"$Type));
	}
`endif
}
