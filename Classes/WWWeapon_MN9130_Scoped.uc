
class WWWeapon_MN9130_Scoped extends ROWeap_MN9130Scoped_Rifle
	abstract;

`include(WinterWar\Classes\WWWeaponPickupMessagesOverride.uci)

DefaultProperties
{
	WeaponContentClass(0)="WinterWar.WWWeapon_MN9130_Scoped_ActualContent"
	
	RoleSelectionImage(0)=Texture2D'WinterWar_UI.WP_Render.WP_Render_MN9130_Sniper'
	
	InvIndex=`WI_MN9130_SCOPED
	
	WeaponProjectiles(DEFAULT_FIREMODE)=	class'MN9130ScopedBullet'
	WeaponProjectiles(ALTERNATE_FIREMODE)=	class'MN9130ScopedBullet'
	
	InstantHitDamageTypes(0)=class'RODmgType_MN9130ScopedBullet'
	InstantHitDamageTypes(1)=class'RODmgType_MN9130ScopedBullet'
	
	AmmoClass=class'ROAmmo_762x54R_MNStripper'
	
	AltFireModeType=ROAFT_None
	
	bHasBayonet=false
}
