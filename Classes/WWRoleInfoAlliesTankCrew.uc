
class WWRoleInfoAlliesTankCrew extends WWRoleInfoAllies;

DefaultProperties
{
	RoleType=RORIT_Tank
	ClassTier=3
	ClassIndex=`RI_TANK_CREW
	
	Items[RORIGM_Default]={(
		PrimaryWeapons=(class'WWWeapon_TT33',class'WWWeapon_NagantRevolver')
	)}
	
	// bAllowPistolsInRealism=true
	bIsPilot=true
	bBotSelectable=false
	
	ClassIcon=Texture2D'WinterWar_UI.RoleIcons.RoleIcon_Tank'
}
