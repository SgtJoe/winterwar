
class WWVehicle_Skis_ActualContent extends WWVehicle_Skis
	placeable;

DefaultProperties
{
	Begin Object Name=ROSVehicleMesh
		SkeletalMesh=SkeletalMesh'WinterWar_VH_FIN_SKIS.Mesh.Skis_RIG'
		LightingChannels=(Dynamic=TRUE,Unnamed_1=TRUE,bInitialized=TRUE)
		AnimTreeTemplate=AnimTree'WinterWar_VH_FIN_SKIS.Anim.AT_VH_SKIS'
		PhysicsAsset=PhysicsAsset'WinterWar_VH_FIN_SKIS.Phy.Skis_RIG_Physics'
	End Object
	
	SeatProxies(0)={(
		TunicMeshType=SkeletalMesh'CHR_VN_US_Army.Mesh.US_Tunic_Long_Mesh',
		HeadGearMeshType=SkeletalMesh'CHR_VN_US_Headgear.Mesh.US_headgear_var1',
		HeadAndArmsMeshType=SkeletalMesh'CHR_VN_US_Heads.Mesh.US_Head1_Mesh',
		HeadphonesMeshType=none,
		HeadAndArmsMICTemplate=MaterialInstanceConstant'CHR_VN_US_Heads.Materials.M_US_Head_01_Long_INST',
		BodyMICTemplate=MaterialInstanceConstant'CHR_VN_US_Army.Materials.M_US_Tunic_Long_INST',
		SeatIndex=0,
		PositionIndex=0
	)}
	
	SeatProxyAnimSet=AnimSet'VH_VN_ARVN_M113_APC.Anim.CHR_M113_Anim_Master'
	
	CollisionSound=none
	LargeChunkImpactSound=none
	MediumChunkImpactSound=none
	SmallChunkImpactSound=none
}
