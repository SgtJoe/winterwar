
class WWVehicleWeapon_ATCannon extends WWVehicleWeapon_TankTurret
	abstract;

var float ReloadDelay;
var bool bCannonReloading;

replication
{
	if (bNetDirty && bNetOwner && Role == ROLE_Authority)
		bCannonReloading;
}

simulated function StartFire(byte FireModeNum)
{
	if ((bCannonReloading || IsTimerActive('StartReload')) && !IsTimerActive('SetCannonReloadCamera'))
	{
		return;
	}
	
	if (MyVehicle != none)
	{
		if (FireModeNum == DEFAULT_FIREMODE && IsTimerActive('SetCannonReloadCamera'))
		{
			SetCannonReloadCamera();
			
			if (Role < Role_Authority)
			{
				ServerStartFire(FireModeNum);
			}
			
			return;
		}
	}
	
	Super.StartFire(FireModeNum);
}

reliable server function ServerStartFire(byte FireModeNum)
{
	if (MyVehicle != none)
	{
		if (FireModeNum == DEFAULT_FIREMODE )
		{
			if (IsTimerActive('SetCannonReloadCamera'))
			{
				SetCannonReloadCamera();
			}
		}
	}
	
	super.ServerStartFire(FireModeNum);
}

simulated function MainCannonDelayTimer()
{
	local ROWeaponPawn ROWP;
	
	if (Role == ROLE_Authority)
	{
		bCannonReloading = false;
		MyVehicle.Seats[SeatIndex].bPerformingReload = false;
		EndReloadTime();
		
		if (MyVehicle.SeatProxies[MyVehicle.GetSeatProxyIndexForSeatIndex(SeatIndex)].Health > 0)
		{
			bMainCannonLoaded = true;
			
			MyVehicle.OnMainCannonReloadComplete();
			
			if (MyVehicle.SeatPositionIndex(SeatIndex,,true) != MyVehicle.Seats[SeatIndex].FiringPositionIndex)
			{
				ROWP = ROWeaponPawn(MyVehicle.Seats[SeatIndex].SeatPawn);
				if (ROWP != none)
				{
					`wwdebug("PreviousPosition", 'VH');
					ROWP.PreviousPosition();
				}
			}
		}
	}
}

simulated function FireAmmunition()
{
	super.FireAmmunition();
	
	if (CurrentFireMode == DEFAULT_FIREMODE && WorldInfo.NetMode == NM_Client)
	{
		if (HasMainGunAmmo(DesiredProjectileIndex))
		{
			PrepForReloadTimer(true);
		}
	}
}

function ConsumeMainGunAmmo( byte AmmoIndex )
{
	bMainCannonLoaded = false;
	
	if (AmmoIndex == MAINGUN_AP_INDEX)
	{
		APAmmoCount = Max(0, APAmmoCount - 1);
	}
	
	if (WorldInfo.NetMode == NM_Standalone && HasMainGunAmmo(DesiredProjectileIndex))
	{
		PrepForReloadTimer(true);
	}
}

simulated function PrepForReloadTimer(optional bool bReloadInPlace)
{
	if (!HasMainGunAmmo(MainGunProjectileIndex))
	{
		return;
	}
	
	if (Role == ROLE_Authority)
	{
		PrepForReload(bReloadInPlace);
	}
	else
	{
		ServerPrepForReload(bReloadInPlace);
		bCannonReloading = true;
		
		if (IsTimerActive('EndCannonReloadCamera'))
		{
			EndCannonReloadCamera();
		}
		
		if (bReloadInPlace)
		{
			SetTimer(ReloadDelay, false, 'SetCannonReloadCamera');
			SetTimer(MyVehicle.Mesh.GetAnimLength('Reload') + 0.4, false, 'EndCannonReloadCamera');
		}
		else
		{
			if (MyVehicle.Seats[SeatIndex].SeatPositions[MyVehicle.SeatPositionIndex(SeatIndex,,true)].LinkedPositionIndex >= 0)
			{
				MyVehicle.Seats[SeatIndex].PendingPositionIndex = MyVehicle.Seats[SeatIndex].FiringPositionIndex;
			}
		}
	}
}

reliable server function ServerPrepForReload(bool bReloadInPlace)
{
	if( HasMainGunAmmo(MainGunProjectileIndex) )
		PrepForReload(bReloadInPlace);
	else
		ClientCancelReloadCamera();
}

function PrepForReload(bool bReloadInPlace)
{
	local WWWeaponPawn ROWP;
	local float TransitionTime;
	
	if (IsTimerActive('StartReload') || IsTimerActive('MainCannonDelayTimer') || !HasMainGunAmmo(MainGunProjectileIndex))
	{
		return;
	}
	
	if (!bReloadInPlace)
	{
		ROWP = WWWeaponPawn(MyVehicle.Seats[SeatIndex].SeatPawn);
		
		if (ROWP != none)
		{
			`wwdebug("NextPosition", 'VH');
			ROWP.NextPosition();
		}
		else
		{
			`wwdebug("None WeaponPawn!", 'VH');
		}
	}
	else
	{
		SetTimer(ReloadDelay, false, 'SetCannonReloadCamera');
		SetTimer(MyVehicle.Mesh.GetAnimLength('Reload') + 0.4, false, 'EndCannonReloadCamera');
	}
	
	bCannonReloading = true;
	MyVehicle.Seats[SeatIndex].bPerformingReload = true;
	
	TransitionTime = MyVehicle.GetPositionTransitionTime(SeatIndex, MyVehicle.Seats[SeatIndex].PendingPositionIndex, false);
	
	if (TransitionTime > 0)
	{
		SetTimer(TransitionTime, false, 'StartReload');
	}
	else
	{
		StartReload();
	}
}

function StartReload()
{
	NotifyClientReload();
	TryToLoadCannon(MainGunProjectileIndex);
}

function TryToLoadCannon( byte AmmoIndex )
{
	local WWVehicleStaticATGun ROVT;
	
	if (HasMainGunAmmo(AmmoIndex) && !IsTimerActive('MainCannonDelayTimer'))
	{
		bCannonReloading = true;
		
		SetTimer(MyVehicle.Mesh.GetAnimLength('Reload') + 0.5, false, 'MainCannonDelayTimer' );
		
		ROVT = WWVehicleStaticATGun(MyVehicle);
		if ( ROVT != None )
		{
			ROVT.VehicleLoadMainGun();
			ROVT.MainGunReloadCount++;
		}
	}
	else
	{
		EndReloadTime();
	}
	
	bMainCannonLoaded = false;
}

simulated function SetCannonReloadCamera()
{
	local ROPlayerController ROPC;
	local int SeatPosIndex;
	
	ClearTimer('SetCannonReloadCamera');
	
	ROPC = ROPlayerController(Instigator.Controller);
	SeatPosIndex = MyVehicle.SeatPositionIndex(SeatIndex,,true);
	
	MyVehicle.Seats[SeatIndex].SeatPositions[SeatPosIndex].bDrawOverlays = false;
	MyVehicle.Seats[SeatIndex].CameraTag = '';
	MyVehicle.Seats[SeatIndex].SeatPositions[SeatPosIndex].bViewFromCameraTag = false;
	MyVehicle.Seats[SeatIndex].bUseReloadCam = true;
	
	if ( ROPC != None && LocalPlayer(ROPC.Player) != none )
	{
		ROPC.HandleTransitionFOV(MyVehicle.Seats[0].SeatPositions[MyVehicle.Seats[0].InitialPositionIndex].ViewFOV, 0.0);
		ROPC.IgnoreLookInput(true);
		ROPC.ClientIgnoreLookInput(true);
		
		// For some reason the camera is sometimes turned during the reload animation, so reset it here
		ROPC.SetRotation(rot(0,0,0));
	}
}

simulated function EndCannonReloadCamera()
{
	local ROPlayerController ROPC;
	local int SeatPosIndex;
	
	ClearTimer('SetCannonReloadCamera');
	ClearTimer('EndCannonReloadCamera');
	
	ROPC = ROPlayerController(Instigator.Controller);
	SeatPosIndex = MyVehicle.SeatPositionIndex(SeatIndex,,true);
	
	MyVehicle.Seats[SeatIndex].SeatPositions[SeatPosIndex].bDrawOverlays = MyVehicle.default.Seats[SeatIndex].SeatPositions[SeatPosIndex].bDrawOverlays;
	MyVehicle.Seats[SeatIndex].CameraTag = MyVehicle.Seats[SeatIndex].SeatPositions[SeatPosIndex].PositionCameraTag;
	MyVehicle.Seats[SeatIndex].SeatPositions[SeatPosIndex].bViewFromCameraTag = MyVehicle.default.Seats[SeatIndex].SeatPositions[SeatPosIndex].bViewFromCameraTag;
	MyVehicle.Seats[SeatIndex].bUseReloadCam = false;
	
	if ( ROPC != None && LocalPlayer(ROPC.Player) != none )
	{
		ROPC.HandleTransitionFOV(MyVehicle.Seats[SeatIndex].SeatPositions[SeatPosIndex].ViewFOV, 0.0);
		ROPC.IgnoreLookInput(false);
		ROPC.ClientIgnoreLookInput(false);
	}
}

reliable client function ClientCancelReloadCamera()
{
	bCannonReloading = false;
	EndCannonReloadCamera();
}

simulated exec function SwitchFireMode() {}

simulated function SetMainGunProjectile()
{
	if((Role < ROLE_Authority || !bMainCannonLoaded) && !HasMainGunAmmo(MainGunProjectileIndex) && HasMainGunAmmo(DesiredProjectileIndex))
	{
		bCannonReloading = true;
		SetTimer(ReloadDelay, false, 'SetCannonReloadCamera');
		SetTimer(MyVehicle.Mesh.GetAnimLength('Reload') + 0.4, false, 'EndCannonReloadCamera');
	}
	super.SetMainGunProjectile();
}

reliable server function ServerSetMainGunProjIndex(byte NewProjectileIndex)
{
	super.ServerSetMainGunProjIndex(NewProjectileIndex);
	
	if (!bCannonReloading)
	{
		ClientCancelReloadCamera();
	}
	else
	{
		MyVehicle.Seats[SeatIndex].bPerformingReload = true;
	}
}

defaultproperties
{
	SeatIndex=1
	
	ReloadDelay=0.8
}