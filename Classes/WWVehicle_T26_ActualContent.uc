
class WWVehicle_T26_ActualContent extends WWVehicle_T26
	placeable;

DefaultProperties
{
	Begin Object Name=ROSVehicleMesh
		SkeletalMesh=SkeletalMesh'WinterWar_VH_USSR_T-26.Mesh.T-26_Rig'
		LightingChannels=(Dynamic=TRUE,Unnamed_1=TRUE,bInitialized=TRUE)
		AnimTreeTemplate=AnimTree'WinterWar_VH_USSR_T-26.Anim.T-26_AnimTree'
		PhysicsAsset=PhysicsAsset'WinterWar_VH_USSR_T-26.Phy.T-26_Rig_Physics'
		AnimSets.Add(AnimSet'WinterWar_VH_USSR_T-26.Anim.T-26_Anim')
	End Object
	
	DestroyedSkeletalMesh=SkeletalMesh'WinterWar_VH_USSR_T-26.Mesh.T-26_Wreck'
	DestroyedPhysicsAsset=PhysicsAsset'WinterWar_VH_USSR_T-26.Phy.T-26_Wreck_Physics'
	
	DestroyedSkeletalMeshWithoutTurret=SkeletalMesh'WinterWar_VH_USSR_T-26.Mesh.T-26_Wreck_Body'
	DestroyedTurretClass=class'WWVehicleDeathTurret_T26'
	
	DestroyedMats(0)=MaterialInstanceConstant'WinterWar_VH_USSR_T-26.MIC.T-26_WRECK_M'
	
	DestroyedMatsTurret(0)=MaterialInstanceConstant'WinterWar_VH_USSR_T-26.MIC.T-26_WRECK_M'
	
	SeatProxies(0)={(
		TunicMeshType=SkeletalMesh'CHR_VN_US_Army.Mesh.US_Tunic_Long_Mesh',
		HeadGearMeshType=SkeletalMesh'CHR_VN_US_Headgear.Mesh.US_headgear_var1',
		HeadAndArmsMeshType=SkeletalMesh'CHR_VN_US_Heads.Mesh.US_Head1_Mesh',
		HeadphonesMeshType=none,
		HeadAndArmsMICTemplate=MaterialInstanceConstant'CHR_VN_US_Heads.Materials.M_US_Head_01_Long_INST',
		BodyMICTemplate=MaterialInstanceConstant'CHR_VN_US_Army.Materials.M_US_Tunic_Long_INST',
		SeatIndex=0,
		PositionIndex=0
	)}
	
	SeatProxies(1)={(
		TunicMeshType=SkeletalMesh'CHR_VN_US_Army.Mesh.US_Tunic_Long_Mesh',
		HeadGearMeshType=SkeletalMesh'CHR_VN_US_Headgear.Mesh.US_headgear_var1',
		HeadAndArmsMeshType=SkeletalMesh'CHR_VN_US_Heads.Mesh.US_Head1_Mesh',
		HeadphonesMeshType=none,
		HeadAndArmsMICTemplate=MaterialInstanceConstant'CHR_VN_US_Heads.Materials.M_US_Head_01_Long_INST',
		BodyMICTemplate=MaterialInstanceConstant'CHR_VN_US_Army.Materials.M_US_Tunic_Long_INST',
		SeatIndex=1,
		PositionIndex=0
	)}
	
	SeatProxyAnimSet=AnimSet'VH_VN_ARVN_M113_APC.Anim.CHR_M113_Anim_Master'
	
	EngineStartOffsetSecs=1.8
	
	Begin Object Class=AudioComponent Name=EngineStartupAudioComponent
		SoundCue=SoundCue'WinterWar_VH_USSR_COMMON_SFX.Audio.T70_Movement_Engine_Start_Exhaust_Cue'
	End Object
	EngineStartupAudio=EngineStartupAudioComponent
	Components.Add(EngineStartupAudioComponent)
	
	Begin Object Class=AudioComponent Name=EngineRunAudioComponent
		SoundCue=SoundCue'WinterWar_VH_USSR_COMMON_SFX.Audio.T70_Movement_Engine_Run_Exhaust_Cue'
	End Object
	EngineRunAudio=EngineRunAudioComponent
	Components.Add(EngineRunAudioComponent)
	
	Begin Object Class=AudioComponent Name=EngineShiftUpAudioComponent
		SoundCue=SoundCue'WinterWar_VH_USSR_COMMON_SFX.Audio.T70_Movement_Engine_Exhaust_ShiftUp_Cue'
	End Object
	EngineShiftUpAudio=EngineShiftUpAudioComponent
	Components.Add(EngineShiftUpAudioComponent)
	
	Begin Object Class=AudioComponent Name=EngineShiftDownAudioComponent
		SoundCue=SoundCue'WinterWar_VH_USSR_COMMON_SFX.Audio.Panzer_Movement_Engine_Exhaust_ShiftDown_Cue'
	End Object
	EngineShiftDownAudio=EngineShiftDownAudioComponent
	Components.Add(EngineShiftDownAudioComponent)
	
	Begin Object Class=AudioComponent Name=EngineShutdownAudioComponent
		SoundCue=SoundCue'WinterWar_VH_USSR_COMMON_SFX.Audio.T70_Movement_Engine_Stop_Cue'
	End Object
	EngineShutdownAudio=EngineShutdownAudioComponent
	Components.Add(EngineShutdownAudioComponent)
	
	
	Begin Object Class=AudioComponent Name=TracksDestroyedAudioComponent
		SoundCue=SoundCue'WinterWar_VH_USSR_COMMON_SFX.Audio.Panzer_Destroyed_Treads_Cue'
	End Object
	TracksDestroyedAudio=TracksDestroyedAudioComponent
	Components.Add(TracksDestroyedAudioComponent)
	
	
	Begin Object Class=AudioComponent Name=TurretTraverseComponent
		SoundCue=SoundCue'WinterWar_VH_USSR_COMMON_SFX.Audio.Turret_Traverse_Manual_Cue'
	End Object
	TurretTraverseSound=TurretTraverseComponent
	Components.Add(TurretTraverseComponent)
	
	Begin Object Class=AudioComponent Name=TurretElevationComponent
		SoundCue=SoundCue'WinterWar_VH_USSR_COMMON_SFX.Audio.Turret_Elevate_Cue'
	End Object
	TurretElevationSound=TurretElevationComponent
	Components.Add(TurretElevationComponent)
	
	
	Begin Object Class=AudioComponent Name=MainCannonReloadSoundComponent
		SoundCue=SoundCue'WinterWar_VH_USSR_COMMON_SFX.Audio.Reload_Cannon'
	End Object
	MainCannonReloadSound=MainCannonReloadSoundComponent
	Components.Add(MainCannonReloadSoundComponent)
	
	
	Begin Object Class=AudioComponent Name=CoaxMGReloadSoundComponent
		SoundCue=SoundCue'WinterWar_VH_USSR_COMMON_SFX.Audio.Reload_DT'
	End Object
	CoaxMGReloadSound=CoaxMGReloadSoundComponent
	Components.Add(CoaxMGReloadSoundComponent)
	
	Begin Object Class=AudioComponent name=CoaxMGSoundComponent
		bShouldRemainActiveIfDropped=true
		bStopWhenOwnerDestroyed=true
		SoundCue=SoundCue'WinterWar_VH_USSR_COMMON_SFX.Audio.MG_DP28_Tank_Fire_Loop_M_Cue'
	End Object
	CoaxMGAmbient=CoaxMGSoundComponent
	Components.Add(CoaxMGSoundComponent)
	
	CoaxMGStopSound=SoundCue'WinterWar_VH_USSR_COMMON_SFX.Audio.MG_DP28_Tank_Fire_Loop_Tail_M_Cue'
}
