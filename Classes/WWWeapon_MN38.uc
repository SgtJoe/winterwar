
class WWWeapon_MN38 extends WWWeapon_MosinBase
	abstract;

defaultproperties
{
	WeaponContentClass(0)="WinterWar.WWWeapon_MN38_ActualContent"
	
	RoleSelectionImage(0)=Texture2D'WinterWar_UI.WP_Render.WP_Render_MN38'
	
	TeamIndex=`ALLIES_TEAM_INDEX
	
	InvIndex=`WI_MN38
	
	WeaponProjectiles(DEFAULT_FIREMODE)=	class'WWProjectile_MN38Bullet'
	WeaponProjectiles(ALTERNATE_FIREMODE)=	class'WWProjectile_MN38Bullet'
	
	Spread(DEFAULT_FIREMODE)=	0.00138 // ~5 MOA
	Spread(ALTERNATE_FIREMODE)=	0.00138 // ~5 MOA

   	// Recoil
	maxRecoilPitch=800//850
	minRecoilPitch=800
	maxRecoilYaw=325
	minRecoilYaw=-325
	
	InstantHitDamageTypes(0)=class'WWDmgType_MN38Bullet'
	InstantHitDamageTypes(1)=class'WWDmgType_MN38Bullet'
	
	MuzzleFlashPSCTemplate=ParticleSystem'WinterWar_FX.ParticleSystems.FX_WW_MuzzleFlash_1stP_M38'
	
	AltFireModeType=ROAFT_None
	
	bHasBayonet=false
	
	bShortShotty=true
	
	CollisionCheckLength=51

	SwayScale=0.55
	
	EquipTime=+0.44
	PutDownTime=+0.35
}
