
class WWDmgType_KP31Bullet extends RODmgType_SmallArmsBullet
	abstract;

defaultproperties
{
	WeaponShortName="KP31"
	KDamageImpulse=156.5 // kg * uu/s
	BloodSprayTemplate=ParticleSystem'FX_VN_Impacts.BloodNGore.FX_VN_BloodSpray_Clothes_small'
}
