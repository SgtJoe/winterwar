
class WWVehicle_T28_ActualContent extends WWVehicle_T28
	placeable;

DefaultProperties
{
	Begin Object Name=ROSVehicleMesh
		SkeletalMesh=SkeletalMesh'WinterWar_VH_USSR_T-28.Mesh.T-28_Rig'
		LightingChannels=(Dynamic=TRUE,Unnamed_1=TRUE,bInitialized=TRUE)
		AnimTreeTemplate=AnimTree'WinterWar_VH_USSR_T-28.Anim.T-28_AnimTree'
		PhysicsAsset=PhysicsAsset'WinterWar_VH_USSR_T-28.Phy.T-28_Rig_Physics'
		AnimSets.Add(AnimSet'WinterWar_VH_USSR_T-28.Anim.T-28_Anim')
	End Object
	
	DestroyedSkeletalMesh=SkeletalMesh'WinterWar_VH_USSR_T-28.Mesh.T-28_Wreck'
	DestroyedPhysicsAsset=PhysicsAsset'WinterWar_VH_USSR_T-28.Phy.T-28_Wreck_Physics'
	
	DestroyedSkeletalMeshWithoutTurret=SkeletalMesh'WinterWar_VH_USSR_T-28.Mesh.T-28_Wreck_Body'
	DestroyedTurretClass=class'WWVehicleDeathTurret_T28'
	
	DestroyedMats(0)=MaterialInstanceConstant'WinterWar_VH_USSR_T-28.MIC.T-28_WRECK_M'
	
	DestroyedMatsTurret(0)=MaterialInstanceConstant'WinterWar_VH_USSR_T-28.MIC.T-28_WRECK_M'
	
	SeatProxies(`T28_DRIVER)={(
		TunicMeshType=SkeletalMesh'CHR_VN_US_Army.Mesh.US_Tunic_Long_Mesh',
		HeadGearMeshType=SkeletalMesh'CHR_VN_US_Headgear.Mesh.US_headgear_var1',
		HeadAndArmsMeshType=SkeletalMesh'CHR_VN_US_Heads.Mesh.US_Head1_Mesh',
		HeadphonesMeshType=none,
		HeadAndArmsMICTemplate=MaterialInstanceConstant'CHR_VN_US_Heads.Materials.M_US_Head_01_Long_INST',
		BodyMICTemplate=MaterialInstanceConstant'CHR_VN_US_Army.Materials.M_US_Tunic_Long_INST',
		SeatIndex=`T28_DRIVER,
		PositionIndex=0
	)}
	
	SeatProxies(`T28_GUNNER)={(
		TunicMeshType=SkeletalMesh'CHR_VN_US_Army.Mesh.US_Tunic_Long_Mesh',
		HeadGearMeshType=SkeletalMesh'CHR_VN_US_Headgear.Mesh.US_headgear_var1',
		HeadAndArmsMeshType=SkeletalMesh'CHR_VN_US_Heads.Mesh.US_Head1_Mesh',
		HeadphonesMeshType=none,
		HeadAndArmsMICTemplate=MaterialInstanceConstant'CHR_VN_US_Heads.Materials.M_US_Head_01_Long_INST',
		BodyMICTemplate=MaterialInstanceConstant'CHR_VN_US_Army.Materials.M_US_Tunic_Long_INST',
		SeatIndex=`T28_GUNNER,
		PositionIndex=0
	)}
	
	/* SeatProxies(`T28_MG_T_F)={(
		TunicMeshType=SkeletalMesh'CHR_VN_US_Army.Mesh.US_Tunic_Long_Mesh',
		HeadGearMeshType=SkeletalMesh'CHR_VN_US_Headgear.Mesh.US_headgear_var1',
		HeadAndArmsMeshType=SkeletalMesh'CHR_VN_US_Heads.Mesh.US_Head1_Mesh',
		HeadphonesMeshType=none,
		HeadAndArmsMICTemplate=MaterialInstanceConstant'CHR_VN_US_Heads.Materials.M_US_Head_01_Long_INST',
		BodyMICTemplate=MaterialInstanceConstant'CHR_VN_US_Army.Materials.M_US_Tunic_Long_INST',
		SeatIndex=`T28_MG_T_F,
		PositionIndex=0
	)} */
	
	SeatProxies(`T28_MG_H_L)={(
		TunicMeshType=SkeletalMesh'CHR_VN_US_Army.Mesh.US_Tunic_Long_Mesh',
		HeadGearMeshType=SkeletalMesh'CHR_VN_US_Headgear.Mesh.US_headgear_var1',
		HeadAndArmsMeshType=SkeletalMesh'CHR_VN_US_Heads.Mesh.US_Head1_Mesh',
		HeadphonesMeshType=none,
		HeadAndArmsMICTemplate=MaterialInstanceConstant'CHR_VN_US_Heads.Materials.M_US_Head_01_Long_INST',
		BodyMICTemplate=MaterialInstanceConstant'CHR_VN_US_Army.Materials.M_US_Tunic_Long_INST',
		SeatIndex=`T28_MG_H_L,
		PositionIndex=0
	)}
	
	SeatProxies(`T28_MG_H_R)={(
		TunicMeshType=SkeletalMesh'CHR_VN_US_Army.Mesh.US_Tunic_Long_Mesh',
		HeadGearMeshType=SkeletalMesh'CHR_VN_US_Headgear.Mesh.US_headgear_var1',
		HeadAndArmsMeshType=SkeletalMesh'CHR_VN_US_Heads.Mesh.US_Head1_Mesh',
		HeadphonesMeshType=none,
		HeadAndArmsMICTemplate=MaterialInstanceConstant'CHR_VN_US_Heads.Materials.M_US_Head_01_Long_INST',
		BodyMICTemplate=MaterialInstanceConstant'CHR_VN_US_Army.Materials.M_US_Tunic_Long_INST',
		SeatIndex=`T28_MG_H_R,
		PositionIndex=0
	)}
	
	SeatProxies(`T28_MG_T_R)={(
		TunicMeshType=SkeletalMesh'CHR_VN_US_Army.Mesh.US_Tunic_Long_Mesh',
		HeadGearMeshType=SkeletalMesh'CHR_VN_US_Headgear.Mesh.US_headgear_var1',
		HeadAndArmsMeshType=SkeletalMesh'CHR_VN_US_Heads.Mesh.US_Head1_Mesh',
		HeadphonesMeshType=none,
		HeadAndArmsMICTemplate=MaterialInstanceConstant'CHR_VN_US_Heads.Materials.M_US_Head_01_Long_INST',
		BodyMICTemplate=MaterialInstanceConstant'CHR_VN_US_Army.Materials.M_US_Tunic_Long_INST',
		SeatIndex=`T28_MG_T_R,
		PositionIndex=0
	)}
	
	SeatProxyAnimSet=AnimSet'VH_VN_ARVN_M113_APC.Anim.CHR_M113_Anim_Master'
	
	EngineStartOffsetSecs=2.5
	
	Begin Object Class=AudioComponent Name=EngineStartupAudioComponent
		SoundCue=SoundCue'WinterWar_VH_USSR_COMMON_SFX.Audio.T34_Movement_Engine_Start_Exhaust_Cue'
	End Object
	EngineStartupAudio=EngineStartupAudioComponent
	Components.Add(EngineStartupAudioComponent)
	
	Begin Object Class=AudioComponent Name=EngineRunAudioComponent
		SoundCue=SoundCue'WinterWar_VH_USSR_COMMON_SFX.Audio.T34_Movement_Engine_Run_Exhaust_Cue'
	End Object
	EngineRunAudio=EngineRunAudioComponent
	Components.Add(EngineRunAudioComponent)
	
	Begin Object Class=AudioComponent Name=EngineShiftUpAudioComponent
		SoundCue=SoundCue'WinterWar_VH_USSR_COMMON_SFX.Audio.T34_Movement_Engine_Exhaust_ShiftUp_Cue'
	End Object
	EngineShiftUpAudio=EngineShiftUpAudioComponent
	Components.Add(EngineShiftUpAudioComponent)
	
	Begin Object Class=AudioComponent Name=EngineShiftDownAudioComponent
		SoundCue=SoundCue'WinterWar_VH_USSR_COMMON_SFX.Audio.Panzer_Movement_Engine_Exhaust_ShiftDown_Cue'
	End Object
	EngineShiftDownAudio=EngineShiftDownAudioComponent
	Components.Add(EngineShiftDownAudioComponent)
	
	Begin Object Class=AudioComponent Name=EngineShutdownAudioComponent
		SoundCue=SoundCue'WinterWar_VH_USSR_COMMON_SFX.Audio.T70_Movement_Engine_Stop_Cue'
	End Object
	EngineShutdownAudio=EngineShutdownAudioComponent
	Components.Add(EngineShutdownAudioComponent)
	
	
	Begin Object Class=AudioComponent Name=TracksDestroyedAudioComponent
		SoundCue=SoundCue'WinterWar_VH_USSR_COMMON_SFX.Audio.Panzer_Destroyed_Treads_Cue'
	End Object
	TracksDestroyedAudio=TracksDestroyedAudioComponent
	Components.Add(TracksDestroyedAudioComponent)
	
	
	Begin Object Class=AudioComponent Name=TurretTraverseComponent
		SoundCue=SoundCue'WinterWar_VH_USSR_COMMON_SFX.Audio.Turret_Traverse_Manual_Cue'
	End Object
	TurretTraverseSound=TurretTraverseComponent
	Components.Add(TurretTraverseComponent)
	
	Begin Object Class=AudioComponent Name=TurretElevationComponent
		SoundCue=SoundCue'WinterWar_VH_USSR_COMMON_SFX.Audio.Turret_Elevate_Cue'
	End Object
	TurretElevationSound=TurretElevationComponent
	Components.Add(TurretElevationComponent)
	
	
	Begin Object Class=AudioComponent Name=MainCannonReloadSoundComponent
		SoundCue=SoundCue'WinterWar_VH_USSR_COMMON_SFX.Audio.Reload_Cannon'
	End Object
	MainCannonReloadSound=MainCannonReloadSoundComponent
	Components.Add(MainCannonReloadSoundComponent)
	
	
	
	Begin Object Class=AudioComponent name=TurretMGSoundComponent
		bShouldRemainActiveIfDropped=true
		bStopWhenOwnerDestroyed=true
		SoundCue=SoundCue'WinterWar_VH_USSR_COMMON_SFX.Audio.MG_DP28_Tank_Fire_Loop_M_Cue'
	End Object
	TurretMGAmbient=TurretMGSoundComponent
	Components.Add(TurretMGSoundComponent)
	TurretMGStopSound=SoundCue'WinterWar_VH_USSR_COMMON_SFX.Audio.MG_DP28_Tank_Fire_Loop_Tail_M_Cue'
	
	Begin Object Class=AudioComponent name=LeftMGSoundComponent
		bShouldRemainActiveIfDropped=true
		bStopWhenOwnerDestroyed=true
		SoundCue=SoundCue'WinterWar_VH_USSR_COMMON_SFX.Audio.MG_DP28_Tank_Fire_Loop_M_Cue'
	End Object
	LeftMGAmbient=LeftMGSoundComponent
	Components.Add(LeftMGSoundComponent)
	LeftMGStopSound=SoundCue'WinterWar_VH_USSR_COMMON_SFX.Audio.MG_DP28_Tank_Fire_Loop_Tail_M_Cue'
	
	Begin Object Class=AudioComponent name=RightMGSoundComponent
		bShouldRemainActiveIfDropped=true
		bStopWhenOwnerDestroyed=true
		SoundCue=SoundCue'WinterWar_VH_USSR_COMMON_SFX.Audio.MG_DP28_Tank_Fire_Loop_M_Cue'
	End Object
	RightMGAmbient=RightMGSoundComponent
	Components.Add(RightMGSoundComponent)
	RightMGStopSound=SoundCue'WinterWar_VH_USSR_COMMON_SFX.Audio.MG_DP28_Tank_Fire_Loop_Tail_M_Cue'
	
	Begin Object Class=AudioComponent name=RearMGSoundComponent
		bShouldRemainActiveIfDropped=true
		bStopWhenOwnerDestroyed=true
		SoundCue=SoundCue'WinterWar_VH_USSR_COMMON_SFX.Audio.MG_DP28_Tank_Fire_Loop_M_Cue'
	End Object
	RearMGAmbient=RearMGSoundComponent
	Components.Add(RearMGSoundComponent)
	RearMGStopSound=SoundCue'WinterWar_VH_USSR_COMMON_SFX.Audio.MG_DP28_Tank_Fire_Loop_Tail_M_Cue'
	
	
	
	Begin Object Class=AudioComponent Name=TurretMGReloadSoundComponent
		SoundCue=SoundCue'WinterWar_VH_USSR_COMMON_SFX.Audio.Reload_DT'
	End Object
	TurretMGReloadSound=TurretMGReloadSoundComponent
	Components.Add(TurretMGReloadSoundComponent)
	
	Begin Object Class=AudioComponent Name=LeftMGReloadSoundComponent
		SoundCue=SoundCue'WinterWar_VH_USSR_COMMON_SFX.Audio.Reload_DT'
	End Object
	LeftMGReloadSound=LeftMGReloadSoundComponent
	Components.Add(LeftMGReloadSoundComponent)
	
	Begin Object Class=AudioComponent Name=RightMGReloadSoundComponent
		SoundCue=SoundCue'WinterWar_VH_USSR_COMMON_SFX.Audio.Reload_DT'
	End Object
	RightMGReloadSound=RightMGReloadSoundComponent
	Components.Add(RightMGReloadSoundComponent)
	
	Begin Object Class=AudioComponent Name=RearMGReloadSoundComponent
		SoundCue=SoundCue'WinterWar_VH_USSR_COMMON_SFX.Audio.Reload_DT'
	End Object
	RearMGReloadSound=RearMGReloadSoundComponent
	Components.Add(RearMGReloadSoundComponent)
}
