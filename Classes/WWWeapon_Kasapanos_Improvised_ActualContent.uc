
class WWWeapon_Kasapanos_Improvised_ActualContent extends WWWeapon_Kasapanos;

DefaultProperties
{
	ArmsAnimSet=AnimSet'WP_VN_VC_Type67_Grenade.Animation.WP_Type67grenadeHands'
	
	Begin Object Name=FirstPersonMesh
		DepthPriorityGroup=SDPG_Foreground
		SkeletalMesh=SkeletalMesh'WinterWar_WP_FIN_KASAPANOS.Mesh.FIN_Kasapanos_Improvised'
		PhysicsAsset=PhysicsAsset'WP_VN_VC_Type67_Grenade.Phys.VC_Type67_Grenade_Physics'
		AnimSets(0)=AnimSet'WP_VN_VC_Type67_Grenade.Animation.WP_Type67grenadeHands'
		AnimTreeTemplate=AnimTree'WP_VN_VC_Type67_Grenade.Animation.VC_Type67grenade_Tree'
		Scale=1.0
		FOV=70
	End Object
	
	Begin Object Name=PickupMesh
		SkeletalMesh=SkeletalMesh'WinterWar_WP_FIN_KASAPANOS.Mesh.FIN_Kasapanos_Improvised_3rd'
		PhysicsAsset=PhysicsAsset'WP_VN_3rd_Master.Phy.Type67_grenade_3rd_Master_Physics'
		CollideActors=true
		BlockActors=true
		BlockZeroExtent=true
		BlockNonZeroExtent=true//false
		BlockRigidBody=true
		bHasPhysicsAssetInstance=false
		bUpdateKinematicBonesFromAnimation=false
		PhysicsWeight=1.0
		RBChannel=RBCC_GameplayPhysics
		RBCollideWithChannels=(Default=TRUE,GameplayPhysics=TRUE,EffectPhysics=TRUE)
		bSkipAllUpdateWhenPhysicsAsleep=TRUE
		bSyncActorLocationToRootRigidBody=true
	End Object
	
	AttachmentClass=class'WWWeapon_Kasapanos_Improvised_Attach'
}
