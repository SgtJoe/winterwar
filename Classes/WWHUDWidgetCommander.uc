
class WWHUDWidgetCommander extends ROHUDWidgetCommander;

var	Texture2D CancelButtonTex;

var	MaterialInstanceConstant ForceDeployButtonTexTemplate;
var	MaterialInstanceConstant ReconButtonTexTemplate;

var	MaterialInstanceConstant FinAbility1ButtonTexTemplate;
var	MaterialInstanceConstant FinAbility2ButtonTexTemplate;
var	MaterialInstanceConstant FinAbility3ButtonTexTemplate;

var	MaterialInstanceConstant SovAbility1ButtonTexTemplate;
var	MaterialInstanceConstant SovAbility2ButtonTexTemplate;
var	MaterialInstanceConstant SovAbility3ButtonTexTemplate;

var	localized string FinAbility1TipTitleString;
var	localized string FinAbility1TipString;
var	localized string FinAbility2TipTitleString;
var	localized string FinAbility2TipString;
var	localized string FinAbility3TipTitleString;
var	localized string FinAbility3TipString;

var	localized string SovAbility1TipTitleString;
var	localized string SovAbility1TipString;
var	localized string SovAbility2TipTitleString;
var	localized string SovAbility2TipString;
var	localized string SovAbility3TipTitleString;
var	localized string SovAbility3TipString;

function Initialize(PlayerController HUDPlayerOwner)
{
	local Sequence GameSeq;
	local array<SequenceObject> AerialReconSeqEvents;
	local WWMapInfo WWMI;
	local ROGameReplicationInfo ROGRI;
	local int i, HUDComponentIndex;
	local ROHUDWidgetComponent TempComponent;
	local int TeamIndex;
	
	HUDComponents[ROCT_ForceRespawnTime].Text = ZeroTimeString;
	HUDComponents[ROCT_AerialReconTime].Text = ZeroTimeString;
	HUDComponents[ROCT_MortarsTime].Text = ZeroTimeString;
	HUDComponents[ROCT_ArtilleryTime].Text = ZeroTimeString;
	HUDComponents[ROCT_RocketsTime].Text = ZeroTimeString;
	HUDComponents[ROCT_ReqUpdatedCoordsButtonText].Text = RequestCoordsString;
	HUDComponents[ROCT_ClearCoordsButtonText].Text = ClearCoordsString;
	HUDComponents[ROCT_ArtySummaryText].Text = CoordsLogString;
	HUDComponents[ROCT_SquadText].Text = GetCurrentSquadText();
	HUDComponents[ROCT_ScrollSquadInstructionText].Text = ScrollThroughSquadString;
	
	AerialReconProgressMat = new class'MaterialInstanceConstant';
	AerialReconProgressMat.SetParent(ProgressMaterialTemplate);
	AerialReconProgressMat.SetTextureParameterValue(ProgressBarTypeTexParamName, ProgressTexture);
	Ability1ProgressMat = new class'MaterialInstanceConstant';
	Ability1ProgressMat.SetParent(ProgressMaterialTemplate);
	Ability1ProgressMat.SetTextureParameterValue(ProgressBarTypeTexParamName, ProgressTexture);
	Ability2ProgressMat = new class'MaterialInstanceConstant';
	Ability2ProgressMat.SetParent(ProgressMaterialTemplate);
	Ability2ProgressMat.SetTextureParameterValue(ProgressBarTypeTexParamName, ProgressTexture);
	Ability3ProgressMat = new class'MaterialInstanceConstant';
	Ability3ProgressMat.SetParent(ProgressMaterialTemplate);
	Ability3ProgressMat.SetTextureParameterValue(ProgressBarTypeTexParamName, ProgressTexture);
	
	HUDComponents[ROCT_AerialReconProgress].Mat = AerialReconProgressMat;
	HUDComponents[ROCT_MortarsProgress].Mat = Ability1ProgressMat;
	HUDComponents[ROCT_ArtilleryProgress].Mat = Ability2ProgressMat;
	HUDComponents[ROCT_RocketsProgress].Mat = Ability3ProgressMat;
	
	ForceRespawnButtonTex = new class'MaterialInstanceConstant';
	AerialReconButtonTex = new class'MaterialInstanceConstant';
	NorthAbility1ButtonTex = new class'MaterialInstanceConstant';
	NorthAbility2ButtonTex = new class'MaterialInstanceConstant';
	NorthAbility3ButtonTex = new class'MaterialInstanceConstant';
	SouthAbility1ButtonTex = new class'MaterialInstanceConstant';
	SouthAbility2ButtonTex = new class'MaterialInstanceConstant';
	SouthAbility3ButtonTex = new class'MaterialInstanceConstant';
	
	ForceRespawnButtonTex.SetParent(ForceDeployButtonTexTemplate);
	
	AerialReconButtonTex.SetParent(ReconButtonTexTemplate);
	
	NorthAbility1ButtonTex.SetParent(FinAbility1ButtonTexTemplate);
	NorthAbility2ButtonTex.SetParent(FinAbility2ButtonTexTemplate);
	NorthAbility3ButtonTex.SetParent(FinAbility3ButtonTexTemplate);
	
	SouthAbility1ButtonTex.SetParent(SovAbility1ButtonTexTemplate);
	SouthAbility2ButtonTex.SetParent(SovAbility2ButtonTexTemplate);
	SouthAbility3ButtonTex.SetParent(SovAbility3ButtonTexTemplate);
	
	WWMI = WWMapInfo(WorldInfo.GetMapInfo());
	ROGRI = ROGameReplicationInfo(WorldInfo.GRI);
	
	super(ROHUDWidget).Initialize(HUDPlayerOwner);
	
	GameSeq = WorldInfo.GetGameSequence();
	if ( GameSeq != none )
	{
		GameSeq.FindSeqObjectsByClass(class'ROSeqEvent_AerialRecon', true, AerialReconSeqEvents);
		
		if ( AerialReconSeqEvents.Length > 0 )
		{
			bAerialReconAvailable = true;
		}
	}
	
	if ( WWMI != none && ROGRI != none )
	{
		TeamIndex = PlayerOwner.GetTeamNum();
		MaxArtyAvailable = WWMI.GetMaxArtyType(TeamIndex, false);
		bArtilleryAvailable = ROGRI.ArtilleryStrikeLimit[TeamIndex] > 0;
		bMortarArtyAvailable = WWMI.GetFirstAbilityLimit( ROGRI.MaxPlayers, TeamIndex ) > 0;
		bMediumArtyAvailable = WWMI.GetSecondAbilityLimit( ROGRI.MaxPlayers, TeamIndex ) > 0;
		
		bRocketArtyAvailable = WWMI.GetThirdAbilityLimit( ROGRI.MaxPlayers, TeamIndex ) > 0;
		
		if (MaxArtyAvailable < 2)
		{
			bRocketArtyAvailable = false;
		}
		
		bAllowStrikeDir = bRocketArtyAvailable;
		
		if (bAerialReconAvailable && !WWMI.MapHasRecon(TeamIndex))
		{
			bAerialReconAvailable = false;
			HUDComponents[ROCT_AerialReconTime].Text = "";
		}
	}
	
	if( OverheadMap != none )
	{
		LocalPlayer(HUDPlayerOwner.Player).ViewportClient.GetViewportSize(OverheadMap.ScreenCenter);
	}
	
	for ( i = 1; i < RowCount; i++ )
	{
		HUDComponentIndex = ROCT_TipDescription1 + i;
		HUDComponents[HUDComponentIndex] = new class'ROHUDWidgetComponent';
		HUDComponents[HUDComponentIndex].X = 0;
		HUDComponents[HUDComponentIndex].Y = 0;
		HUDComponents[HUDComponentIndex].TextFont = HUDComponents[ROCT_TipDescription1].TextFont;
		HUDComponents[HUDComponentIndex].TextAlignment = HUDComponents[ROCT_TipDescription1].TextAlignment;
		HUDComponents[HUDComponentIndex].TextScale = HUDComponents[ROCT_TipDescription1].TextScale;
		HUDComponents[HUDComponentIndex].DrawColor = HUDComponents[ROCT_TipDescription1].DrawColor;
		HUDComponents[HUDComponentIndex].FullAlpha = HUDComponents[ROCT_TipDescription1].FullAlpha;
		HUDComponents[HUDComponentIndex].bVisible = HUDComponents[ROCT_TipDescription1].bVisible;
		HUDComponents[HUDComponentIndex].FadeOutTime = HUDComponents[ROCT_TipDescription1].FadeOutTime;
		HUDComponents[HUDComponentIndex].FadeInTime = HUDComponents[ROCT_TipDescription1].FadeInTime;
		HUDComponents[HUDComponentIndex].bDropShadow = HUDComponents[ROCT_TipDescription1].bDropShadow;
		HUDComponents[HUDComponentIndex].DropShadowOffset = HUDComponents[ROCT_TipDescription1].DropShadowOffset;
		HUDComponents[HUDComponentIndex].DropShadowColor = HUDComponents[ROCT_TipDescription1].DropShadowColor;
		HUDComponents[HUDComponentIndex].bWrapText = HUDComponents[ROCT_TipDescription1].bWrapText;
		HUDComponents[HUDComponentIndex].SortPriority = HUDComponents[ROCT_TipDescription1].SortPriority;
		HUDComponents[HUDComponentIndex].Initialize();
	}
	
	ROCT_CoordsStartIndex = HUDComponents.length;
	
	for ( i = 0; i < `MAX_SQUADS; i++ )
	{
		TempComponent = new class'ROHUDWidgetComponent';
		TempComponent.X = HUDComponents[ROCT_ArtySummaryText].X - CoordLogColumnOffset ;
		TempComponent.Y = HUDComponents[ROCT_ArtySummaryText].Y + CoordLogTitleOffset  + (i*CoordLogTextSize);
		TempComponent.TextFont = Font'VN_UI_Fonts.Font_VN_Condensed';
		TempComponent.TextAlignment = ROHTA_Center;
		TempComponent.TextScale = 0.35;
		TempComponent.DrawColor = class'HUD'.default.WhiteColor;
		TempComponent.bVisible = true;
		TempComponent.FadeOutTime = 0.5;
		TempComponent.bDropShadow = true;
		TempComponent.DropShadowOffset = vect2d(2,2);
		TempComponent.SortPriority = DSP_High;
		TempComponent.Initialize();
		HUDComponents.AddItem(TempComponent);
		TempComponent = new class'ROHUDWidgetComponent';
		TempComponent.X = HUDComponents[ROCT_ArtySummaryText].X;
		TempComponent.Y = HUDComponents[ROCT_ArtySummaryText].Y + CoordLogTitleOffset  + (i*CoordLogTextSize);
		TempComponent.TextFont = Font'VN_UI_Fonts.Font_VN_Condensed';
		TempComponent.TextAlignment = ROHTA_Center;
		TempComponent.TextScale = 0.35;
		TempComponent.DrawColor = class'HUD'.default.WhiteColor;
		TempComponent.bVisible = true;
		TempComponent.FadeOutTime = 0.5;
		TempComponent.bDropShadow = true;
		TempComponent.DropShadowOffset = vect2d(2,2);
		TempComponent.SortPriority = DSP_High;
		TempComponent.Initialize();
		HUDComponents.AddItem(TempComponent);
		TempComponent = new class'ROHUDWidgetComponent';
		TempComponent.X = HUDComponents[ROCT_ArtySummaryText].X + CoordLogColumnOffset ;
		TempComponent.Y = HUDComponents[ROCT_ArtySummaryText].Y + CoordLogTitleOffset  + (i*CoordLogTextSize);
		TempComponent.TextFont = Font'VN_UI_Fonts.Font_VN_Condensed';
		TempComponent.TextAlignment = ROHTA_Center;
		TempComponent.TextScale = 0.35;
		TempComponent.DrawColor = class'HUD'.default.WhiteColor;
		TempComponent.bVisible = true;
		TempComponent.FadeOutTime = 0.5;
		TempComponent.bDropShadow = true;
		TempComponent.DropShadowOffset = vect2d(2,2);
		TempComponent.SortPriority = DSP_High;
		TempComponent.Initialize();
		HUDComponents.AddItem(TempComponent);
	}
	
	for ( i = 0; i < `MAX_SQUADS; i++ )
	{
		TempComponent = new class'ROHUDWidgetComponent';
		TempComponent.X = HUDComponents[ROCT_ArtySummaryText].X - 70; // Middle - (Width/2), to get centered
		TempComponent.Y = HUDComponents[ROCT_ArtySummaryText].Y + CoordLogTitleOffset  + (i*CoordLogTextSize) + 3; // plus 3 for a small gap inbetween them
		TempComponent.Width = 140;
		TempComponent.Height = 18;
		TempComponent.TexWidth = 32;
		TempComponent.TexHeight = 32;
		TempComponent.Tex=Texture2D'EngineResources.WhiteSquareTexture';
		TempComponent.DrawColor = MakeColor(255,255,0,100);
		TempComponent.FullAlpha = 100;
		TempComponent.bVisible = false;
		TempComponent.FadeInTime = 0.25;
		TempComponent.FadeOutTime = 0.25;
		TempComponent.SortPriority = DSP_High;
		TempComponent.Initialize();
		HUDComponents.AddItem(TempComponent);
		ROCT_YellowFlashes[i] = TempComponent;
	}
}

function UpdateWidget()
{
	local WWTeamInfo WWTI;
	local ROGameReplicationInfo ROGRI;
	local int TeamIndex, i, InitialCooldown;
	local ROPlayerReplicationInfo ROPRI;
	local WWMapInfo WWMI;
	local bool bShouldDoRadioCheck;
	local bool bShouldLocalChangeTexParam;
	
	if ( !bVisible || !UpdateVisibility() )
	{
		return;
	}
	
	UpdateMouseForCommander();
	TeamIndex = PlayerOwner.GetTeamNum();
	
	ROPRI = ROPlayerReplicationInfo(PlayerOwner.PlayerReplicationInfo);
	WWTI = WWTeamInfo(PlayerOwner.PlayerReplicationInfo.Team);
	WWMI = WWMapInfo(WorldInfo.GetMapInfo());
	ROGRI = ROGameReplicationInfo(WorldInfo.GRI);
	
	if (AbilityRequested >= 0)
	{
		i = GetProgressComponentFromAbility(AbilityRequested);
		if (i>=0)
		{
			if ( AbilityRequestedTimeToStart >= WorldInfo.TimeSeconds )
			{
				MaterialInstanceConstant(HUDComponents[i].Mat).SetScalarParameterValue(ProgressParamName, (AbilityRequestedTimeToStart - WorldInfo.TimeSeconds) / AbilityRequestedTimerTime);
			}
			else
			{
				HideProgressBar(i);
			}
		}
		else
		{
			AbilityRequested = -1;
		}
	}
	
	/* if (WWTI.GetRemainingReconPlanes() <= 0)
	{
		bAerialReconAvailable = false;
		HUDComponents[ROCT_AerialReconTime].Text = "";
	} */
	
	HUDComponents[ROCT_SquadText].Text = GetCurrentSquadText();
	
	if ( ROGRI != none )
	{
		InitialCooldown = ROGRI.ElapsedTime < WWMI.GetInitialCommanderCooldown(WorldInfo.GetGameClass()) ? WWMI.GetInitialCommanderCooldown(WorldInfo.GetGameClass()) : 0;
		
		if ( bAerialReconAvailable )
		{
			if ( WWTI != none )
			{
				if ( WWTI.NextAerialReconTime < WorldInfo.GRI.RemainingTime )
				{
					bShouldLocalChangeTexParam = float(WWMI.GetReconInterval(TeamIndex)) != 0.0f;
					if ( bShouldLocalChangeTexParam )
						MaterialInstanceConstant(HUDComponents[ROCT_AerialReconButton].Mat).SetScalarParameterValue(ProgressParamName, 1 - (WorldInfo.GRI.RemainingTime - WWTI.NextAerialReconTime) / float((InitialCooldown > 0 ? InitialCooldown : WWMI.GetReconInterval(TeamIndex))));
					HUDComponents[ROCT_AerialReconTime].Text = BuildTimeString(WorldInfo.GRI.RemainingTime - WWTI.NextAerialReconTime);
					SetButtonState( AerialReconButtonState, ROHBS_Disabled, ROCT_AerialReconButton, !bShouldLocalChangeTexParam );
					HideProgressBar(ROCT_AerialReconProgress);
				}
				else
				{
					HUDComponents[ROCT_AerialReconTime].Text = "";
					
					if ( !ROPRI.RoleInfo.bCanBeTankCrew && (RadioUsed == none || PlayerOwner.Pawn == none || VSizeSq(RadioUsed.Location - PlayerOwner.Pawn.Location) > 10000) )
					{
						SetButtonState( AerialReconButtonState, ROHBS_Disabled, ROCT_AerialReconButton, true);
					}
					else if ( AerialReconButtonState == ROHBS_Disabled )
					{
						SetButtonState( AerialReconButtonState, ROHBS_None, ROCT_AerialReconButton, true );
					}
					else
					{
						MaterialInstanceConstant(HUDComponents[ROCT_AerialReconButton].Mat).SetScalarParameterValue(ProgressParamName, 1.f);
					}
				}
			}
		}
		else
		{
			SetButtonState( AerialReconButtonState, ROHBS_Disabled, ROCT_AerialReconButton, true );
			HUDComponents[ROCT_AerialReconTime].Text = "";
		}
		
		if (bArtilleryAvailable)
		{
			if ( ROTeamInfo(ROGRI.Teams[TeamIndex]).ArtyStrikeLocation != vect(-999999.0,-999999.0,-999999.0) && (ROTeamInfo(ROGRI.Teams[TeamIndex]).ActiveArtillerySpawner != none || ROTeamInfo(ROGRI.Teams[TeamIndex]).ActiveSupportAircraft != none) )
			{
				bArtilleryStrikeActive = true;
				
				if ( CancelButtonState == ROHBS_Disabled )
				{
					SetButtonState( CancelButtonState, ROHBS_None, ROCT_CancelButton );
				}
			}
			else
			{
				bArtilleryStrikeActive = false;
				
				if ( CancelButtonState != ROHBS_Disabled )
				{
					SetButtonState( CancelButtonState, ROHBS_Disabled, ROCT_CancelButton );
				}
			}
			
			if ( ROGRI.NextAbilityOneTime[TeamIndex] < WorldInfo.GRI.RemainingTime)
			{
				bShouldLocalChangeTexParam = float(ROGRI.AbilityOneDelay[TeamIndex]) != 0.0f;
				if ( bShouldLocalChangeTexParam )
					MaterialInstanceConstant(HUDComponents[ROCT_MortarsButton].Mat).SetScalarParameterValue(ProgressParamName, 1 - (WorldInfo.GRI.RemainingTime - ROGRI.NextAbilityOneTime[TeamIndex]) / float(InitialCooldown > 0 ? InitialCooldown : Max(ROGRI.AbilityOneDelay[TeamIndex], 1)));
				HUDComponents[ROCT_MortarsTime].Text = BuildTimeString(WorldInfo.GRI.RemainingTime - ROGRI.NextAbilityOneTime[TeamIndex]);
				SetButtonState( MortarsButtonState, ROHBS_Disabled, ROCT_MortarsButton, !bShouldLocalChangeTexParam );
				HideProgressBar(ROCT_MortarsProgress);
			}
			else
			{
				MaterialInstanceConstant(HUDComponents[ROCT_MortarsButton].Mat).SetScalarParameterValue(ProgressParamName, 1.f );
				HUDComponents[ROCT_MortarsTime].Text = "";
				bShouldDoRadioCheck = true;
			}
			
			if ( ROGRI.NextAbilityTwoTime[TeamIndex] < WorldInfo.GRI.RemainingTime )
			{
				bShouldLocalChangeTexParam = float(ROGRI.AbilityTwoDelay[TeamIndex]) != 0.0f;
				if ( bShouldLocalChangeTexParam )
					MaterialInstanceConstant(HUDComponents[ROCT_ArtilleryButton].Mat).SetScalarParameterValue(ProgressParamName, 1 - (WorldInfo.GRI.RemainingTime - ROGRI.NextAbilityTwoTime[TeamIndex]) / float(InitialCooldown > 0 ? InitialCooldown : Max(ROGRI.AbilityTwoDelay[TeamIndex], 1)));
				HUDComponents[ROCT_ArtilleryTime].Text = BuildTimeString(WorldInfo.GRI.RemainingTime - ROGRI.NextAbilityTwoTime[TeamIndex]);
				SetButtonState( ArtilleryButtonState, ROHBS_Disabled, ROCT_ArtilleryButton, !bShouldLocalChangeTexParam );
				HideProgressBar(ROCT_ArtilleryProgress);
			}
			else
			{
				MaterialInstanceConstant(HUDComponents[ROCT_ArtilleryButton].Mat).SetScalarParameterValue(ProgressParamName, 1.f);
				HUDComponents[ROCT_ArtilleryTime].Text = "";
				bShouldDoRadioCheck = true;
			}
			
			if ( ROGRI.NextAbilityThreeTime[TeamIndex] < WorldInfo.GRI.RemainingTime )
			{
				bShouldLocalChangeTexParam = float(ROGRI.AbilityThreeDelay[TeamIndex]) != 0.0f;
				if ( bShouldLocalChangeTexParam )
					MaterialInstanceConstant(HUDComponents[ROCT_RocketsButton].Mat).SetScalarParameterValue(ProgressParamName, 1 - (WorldInfo.GRI.RemainingTime - ROGRI.NextAbilityThreeTime[TeamIndex]) / float(InitialCooldown > 0 ? InitialCooldown : Max(ROGRI.AbilityThreeDelay[TeamIndex], 1)));
				HUDComponents[ROCT_RocketsTime].Text = BuildTimeString(WorldInfo.GRI.RemainingTime - ROGRI.NextAbilityThreeTime[TeamIndex]);
				SetButtonState( RocketsButtonState, ROHBS_Disabled, ROCT_RocketsButton, !bShouldLocalChangeTexParam );
				HideProgressBar(ROCT_RocketsProgress);
			}
			else
			{
				MaterialInstanceConstant(HUDComponents[ROCT_RocketsButton].Mat).SetScalarParameterValue(ProgressParamName, 1.f);
				HUDComponents[ROCT_RocketsTime].Text = "";
				bShouldDoRadioCheck = true;
			}
			
			if( bShouldDoRadioCheck || bArtilleryStrikeActive )
			{
				if ( !ROPRI.RoleInfo.bCanBeTankCrew && (RadioUsed == none || PlayerOwner.Pawn == none || VSizeSq(RadioUsed.Location - PlayerOwner.Pawn.Location) > 10000) )
				{
					SetButtonState( MortarsButtonState, ROHBS_Disabled, ROCT_MortarsButton, true );
					SetButtonState( ArtilleryButtonState, ROHBS_Disabled, ROCT_ArtilleryButton, true );
					SetButtonState( RocketsButtonState, ROHBS_Disabled, ROCT_RocketsButton, true );
				}
				else if( ROTeamInfo(ROGRI.Teams[TeamIndex]).SavedArtilleryCoords ==  vect(-999999.0,-999999.0,-999999.0) )
				{
					if( TeamIndex == `ALLIES_TEAM_INDEX )
					{
						SetButtonState( MortarsButtonState, ROHBS_Disabled, ROCT_MortarsButton, true );
						SetButtonState( ArtilleryButtonState, ROHBS_Disabled, ROCT_ArtilleryButton, true );
						SetButtonState( RocketsButtonState, ROHBS_Disabled, ROCT_RocketsButton, true );
					}
					else
					{
						SetButtonState( MortarsButtonState, ROHBS_Disabled, ROCT_MortarsButton, true );
						SetButtonState( ArtilleryButtonState, ROHBS_Disabled, ROCT_ArtilleryButton, true );
						
						// Air Defence, does not require an arty target
						if ( RocketsButtonState == ROHBS_Disabled && HUDComponents[ROCT_RocketsTime].Text == "" )
							SetButtonState( RocketsButtonState, ROHBS_None, ROCT_RocketsButton, true );
					}
				}
				else
				{
					if ( MortarsButtonState == ROHBS_Disabled && HUDComponents[ROCT_MortarsTime].Text == "" )
						SetButtonState( MortarsButtonState, ROHBS_None, ROCT_MortarsButton, true );
					if ( ArtilleryButtonState == ROHBS_Disabled && HUDComponents[ROCT_ArtilleryTime].Text == "" )
						SetButtonState( ArtilleryButtonState, ROHBS_None, ROCT_ArtilleryButton, true );
					if ( RocketsButtonState == ROHBS_Disabled && HUDComponents[ROCT_RocketsTime].Text == "" )
						SetButtonState( RocketsButtonState, ROHBS_None, ROCT_RocketsButton, true );
				}
				
				if ( bArtilleryStrikeActive && CancelButtonState == ROHBS_Disabled )
				{
					SetButtonState( CancelButtonState, ROHBS_None, ROCT_CancelButton );
				}
			}
			
			if( !bMortarArtyAvailable )
			{
				SetButtonState( MortarsButtonState, ROHBS_Disabled, ROCT_MortarsButton, true );
			}
			if( !bMediumArtyAvailable )
			{
				SetButtonState( ArtilleryButtonState, ROHBS_Disabled, ROCT_ArtilleryButton, true );
			}
			if( !bRocketArtyAvailable )
			{
				SetButtonState( RocketsButtonState, ROHBS_Disabled, ROCT_RocketsButton, true );
			}
		}
		else
		{
			SetButtonState( MortarsButtonState, ROHBS_Disabled, ROCT_MortarsButton, true );
			SetButtonState( ArtilleryButtonState, ROHBS_Disabled, ROCT_ArtilleryButton, true );
			SetButtonState( RocketsButtonState, ROHBS_Disabled, ROCT_RocketsButton, true );
			SetButtonState( CancelButtonState, ROHBS_Disabled, ROCT_CancelButton );
		}
		
		if ( PlayerOwner.bRadioCallinProgress )
		{
			SetButtonState( AerialReconButtonState, ROHBS_Disabled, ROCT_AerialReconButton, true );
			SetButtonState( MortarsButtonState, ROHBS_Disabled, ROCT_MortarsButton, true );
			SetButtonState( ArtilleryButtonState, ROHBS_Disabled, ROCT_ArtilleryButton, true );
			SetButtonState( RocketsButtonState, ROHBS_Disabled, ROCT_RocketsButton, true );
		}
		
		for ( i = 0; i < `MAX_ARTY_TARGETS; i++ )
		{
			if ( OverheadMap.HUDComponents[OverheadMap.ROOMT_ArtyRequestStart + (2 * i)].bVisible )
			{
				if ( !OverheadMap.HUDComponents[OverheadMap.ROOMT_ArtyRequestStart + (2 * i)].ContainsPoint(AdjustedMouseLocation) )
				{
					if ( i == WWTI.SelectedArtyIndex )
					{
						OverheadMap.HUDComponents[OverheadMap.ROOMT_ArtyRequestStart + (2 * i)].DrawColor = SelectedArtyColor;
					}
					else
					{
						OverheadMap.HUDComponents[OverheadMap.ROOMT_ArtyRequestStart + (2 * i)].DrawColor = UnSelectedArtyColor;
					}
				}
			}
		}
		
		if(TeamIndex == `AXIS_TEAM_INDEX)
		{
			if ( ROGRI.NextInstantRespawn[TeamIndex] < WorldInfo.GRI.RemainingTime )
			{
				bShouldLocalChangeTexParam = float(WWMI.InstantRespawnInterval) != 0.0f;
				if ( bShouldLocalChangeTexParam )
					MaterialInstanceConstant(HUDComponents[ROCT_ForceRespawnButton].Mat).SetScalarParameterValue(ProgressParamName, 1 - (WorldInfo.GRI.RemainingTime - ROGRI.NextInstantRespawn[TeamIndex]) / float(WWMI.InstantRespawnInterval));
				HUDComponents[ROCT_ForceRespawnTime].Text = BuildTimeString(WorldInfo.GRI.RemainingTime - ROGRI.NextInstantRespawn[TeamIndex]);
				SetButtonState( ForceRespawnButtonState, ROHBS_Disabled, ROCT_ForceRespawnButton, !bShouldLocalChangeTexParam );
			}
			else
			{
				HUDComponents[ROCT_ForceRespawnTime].Text = "";
				if ( ForceRespawnButtonState == ROHBS_Disabled )
					SetButtonState( ForceRespawnButtonState, ROHBS_None, ROCT_ForceRespawnButton, true );
				else
					MaterialInstanceConstant(HUDComponents[ROCT_ForceRespawnButton].Mat).SetScalarParameterValue(ProgressParamName, 1.f);
			}
		}
		else
		{
			if ( ROGRI.NextInstantRespawn[TeamIndex] < WorldInfo.GRI.RemainingTime )
			{
				bShouldLocalChangeTexParam = float(WWMI.InstantRespawnInterval) != 0.0f;
				if ( bShouldLocalChangeTexParam )
					MaterialInstanceConstant(HUDComponents[ROCT_ForceRespawnButton].Mat).SetScalarParameterValue(ProgressParamName, 1 - (WorldInfo.GRI.RemainingTime - ROGRI.NextInstantRespawn[TeamIndex]) / float(WWMI.InstantRespawnInterval));
				HUDComponents[ROCT_ForceRespawnTime].Text = BuildTimeString(WorldInfo.GRI.RemainingTime - ROGRI.NextInstantRespawn[TeamIndex]);
				SetButtonState( ForceRespawnButtonState, ROHBS_Disabled, ROCT_ForceRespawnButton, !bShouldLocalChangeTexParam );
			}
			else
			{
				HUDComponents[ROCT_ForceRespawnTime].Text = "";
				if ( ForceRespawnButtonState == ROHBS_Disabled )
					SetButtonState( ForceRespawnButtonState, ROHBS_None, ROCT_ForceRespawnButton, true );
				else
					MaterialInstanceConstant(HUDComponents[ROCT_ForceRespawnButton].Mat).SetScalarParameterValue(ProgressParamName, 1.f);
			}
		}
	}
	
	if( OverheadMap != none && OverheadMap.bHideMousePointer )
	{
		DisableAllButtons();
	}
	
	UpdateCoordsLog(WWTI);
	
	super(ROHUDWidget).UpdateWidget();
}

event bool HandleInputKey(name Key, EInputEvent EventType)
{
	local int i, ComponentIndex;
	local ROPlayerReplicationInfo ROPRI;
	local ROTeamInfo ROTI;
	local ROGameReplicationInfo ROGRI;
	local bool bHasClickedComponent;
	local EROOrder Order;
	local vector2d TempNorm;
	
	ROPRI = ROPlayerReplicationInfo(PlayerOwner.PlayerReplicationInfo);
	ROGRI = ROGameReplicationInfo(WorldInfo.GRI);
	
	if ( bVisible && UpdateVisibility() && OverheadMap != none && !OverheadMap.bHideMousePointer )
	{
		if ( Key == 'LeftMouseButton' || Key == 'MiddleMouseButton' || Key == 'LeftShift' || Key == 'MouseScrollUp' || Key == 'MouseScrollDown' )
		{
			UpdateMouseForCommander();
			
			if ( OverheadMap.HUDComponents[OverheadMap.ROOMT_Map].ContainsPoint(AdjustedMouseLocation) && !(OverheadMap.bShowOverheadKey && OverheadMap.HUDComponents[OverheadMap.ROOMT_KeyBackground].ContainsPoint(AdjustedMouseLocation)) )
			{
				ROTI = ROTeamInfo(PlayerOwner.PlayerReplicationInfo.Team);
				
				if ( EventType == IE_Pressed )
				{
					if( Key == 'LeftMouseButton' )
					{
						for ( i = 0; i < `MAX_ARTY_TARGETS; i++ )
						{
							if ( OverheadMap.HUDComponents[OverheadMap.ROOMT_ArtyRequestStart + (2 * i)].bVisible )
							{
								if ( OverheadMap.HUDComponents[OverheadMap.ROOMT_ArtyRequestStart + (2 * i)].ContainsPoint(AdjustedMouseLocation) )
								{
									if ( ROTI != none )
									{
										PlayerOwner.AttemptServerSaveArtilleryPosition(ROTI.GetArtyRequestVector(i), ROTI.GetArtyRequestValid(i));
										OverheadMap.HUDComponents[OverheadMap.ROOMT_ArtyRequestStart + (2 * i)].DrawColor = SelectedArtyColor;
										PlayerOwner.ServerSetSelectedArtyIndex(i);
										bHasClickedComponent = true;
										
										if( AllowDisplayStrikeVector() )
										{
											InitialMouseLocation = AdjustedMouseLocation;
											bDraggingVector = true;
											OverheadMap.UpdateStrikeDirectionIndicator(OverheadMap.ROOMT_ArtyRequestStart + (2 * ROTI.SelectedArtyIndex), vect2D(TempNorm.Y,-TempNorm.X));
										}
									}
								}
								else
								{
									OverheadMap.HUDComponents[OverheadMap.ROOMT_ArtyRequestStart + (2 * i)].DrawColor = UnSelectedArtyColor;
								}
							}
						}
						if( OverheadMap.HasDoubleLeftClicked() )
						{
							if( ROGRI != none )
							{
								for ( i = 0; i < ROGRI.ObjectiveLocations.Length; i++ )
								{
									ComponentIndex = OverheadMap.ROOMT_ObjectiveStart + (i * 7);
									if ( OverheadMap.HUDComponents[ComponentIndex].bVisible )
									{
										if ( OverheadMap.HUDComponents[ComponentIndex].ContainsPoint(AdjustedMouseLocation) )
										{
											Order = PlayerOwner.GetTeamNum() == ROGRI.GetObjectiveOwner(i) ? ROORDER_Defend : ROORDER_Attack;
											OverheadMap.SendOrderToCom(Order, , ROGRI.GetObjective(i), i);
											bHasClickedComponent = true;
										}
									}
								}
								
								if( !bHasClickedComponent )
								{
									OverheadMap.SendOrderToCom(ROORDER_Move, OverheadMap.ScreenToWorldLocation(OverheadMap.ROOMT_MousePointer));
								}
							}
						}
					}
					
					if( Key == 'MiddleMouseButton' )
					{
						OverheadMap.SendOrderToCom(ROORDER_Move /* ROORDER_MarkLZ */, OverheadMap.ScreenToWorldLocation(OverheadMap.ROOMT_MousePointer)); //, 0.5f));
					}
				}
				else if ( EventType == IE_Released )
				{
					if( Key == 'LeftMouseButton' )
					{
						if ( ROTI != none && bDraggingVector )
						{
							TempNorm = Normal2D(InitialMouseLocation - AdjustedMouseLocation);
							PlayerOwner.AttemptServerSetStrikeDirection(vect2D(TempNorm.Y,-TempNorm.X));
							bDraggingVector = false;
							
							if ( OverheadMap.HUDComponents[OverheadMap.ROOMT_ArtyRequestStart + (2 * ROTI.SelectedArtyIndex)].ContainsPoint(AdjustedMouseLocation) )
								OverheadMap.UpdateStrikeDirectionIndicator(OverheadMap.ROOMT_ArtyRequestStart + (2 * ROTI.SelectedArtyIndex), vect2D(TempNorm.Y,-TempNorm.X));
							else
								OverheadMap.UpdateStrikeDirectionIndicator(-1);
						}
					}
				}
			}
			
			if ( EventType == IE_Pressed )
			{
				if ( HUDComponents[ROCT_AerialReconButton].ContainsPoint(ActualMouseLocation) )
				{
					if ( AerialReconButtonState != ROHBS_Disabled )
					{
						SetButtonState( AerialReconButtonState, ROHBS_Down, ROCT_AerialReconButton );
					}
				}
				else if ( HUDComponents[ROCT_ForceRespawnButton].ContainsPoint(ActualMouseLocation) )
				{
					if ( ForceRespawnButtonState != ROHBS_Disabled )
					{
						SetButtonState( ForceRespawnButtonState, ROHBS_Down, ROCT_ForceRespawnButton );
					}
				}
				else if ( HUDComponents[ROCT_MortarsButton].ContainsPoint(ActualMouseLocation) )
				{
					if ( MortarsButtonState != ROHBS_Disabled )
					{
						SetButtonState( MortarsButtonState, ROHBS_Down, ROCT_MortarsButton );
					}
				}
				else if ( HUDComponents[ROCT_ArtilleryButton].ContainsPoint(ActualMouseLocation) )
				{
					if ( ArtilleryButtonState != ROHBS_Disabled )
					{
						SetButtonState( ArtilleryButtonState, ROHBS_Down, ROCT_ArtilleryButton );
					}
				}
				else if ( HUDComponents[ROCT_RocketsButton].ContainsPoint(ActualMouseLocation) )
				{
					if ( RocketsButtonState != ROHBS_Disabled )
					{
						SetButtonState( RocketsButtonState, ROHBS_Down, ROCT_RocketsButton );
					}
				}
				else if ( HUDComponents[ROCT_CancelButton].ContainsPoint(ActualMouseLocation) )
				{
					if ( CancelButtonState != ROHBS_Disabled )
					{
						SetButtonState( CancelButtonState, ROHBS_Down, ROCT_CancelButton );
					}
				}
				else if ( HUDComponents[ROCT_ReqUpdatedCoordsButton].ContainsPoint(ActualMouseLocation) )
				{
					if ( ReqUpdatedCoordsButtonState != ROHBS_Disabled )
					{
						SetButtonWithTextState( ReqUpdatedCoordsButtonState, ROHBS_Down, ROCT_ReqUpdatedCoordsButton );
					}
				}
				else if ( HUDComponents[ROCT_ClearCoordsButton].ContainsPoint(ActualMouseLocation) )
				{
					if ( ClearCoordsButtonState != ROHBS_Disabled )
					{
						SetButtonWithTextState( ClearCoordsButtonState, ROHBS_Down, ROCT_ClearCoordsButton );
					}
				}
			}
			else if ( EventType == IE_Released )
			{
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				if ( HUDComponents[ROCT_AerialReconButton].ContainsPoint(ActualMouseLocation) )
				{
					if ( AerialReconButtonState == ROHBS_Down )
					{
						SetButtonState( AerialReconButtonState, ROHBS_Hovered, ROCT_AerialReconButton );
						
						PlayerOwner.AttemptRequestAerialRecon(RadioUsed);
					}
				}
				else if ( AerialReconButtonState != ROHBS_Disabled )
				{
					SetButtonState( AerialReconButtonState, ROHBS_None, ROCT_AerialReconButton );
				}
				if ( HUDComponents[ROCT_MortarsButton].ContainsPoint(ActualMouseLocation) )
				{
					if ( MortarsButtonState == ROHBS_Down )
					{
						SetButtonState( MortarsButtonState, ROHBS_Hovered, ROCT_MortarsButton );
						PlayerOwner.AttemptRequestArtillery(RadioUsed, 0);
					}
				}
				else if ( MortarsButtonState != ROHBS_Disabled )
				{
					SetButtonState( MortarsButtonState, ROHBS_None, ROCT_MortarsButton );
				}
				
				if ( HUDComponents[ROCT_ArtilleryButton].ContainsPoint(ActualMouseLocation) )
				{
					if ( ArtilleryButtonState == ROHBS_Down )
					{
						SetButtonState( ArtilleryButtonState, ROHBS_Hovered, ROCT_ArtilleryButton );
						PlayerOwner.AttemptRequestArtillery(RadioUsed, 1);
					}
				}
				else if ( ArtilleryButtonState != ROHBS_Disabled )
				{
					SetButtonState( ArtilleryButtonState, ROHBS_None, ROCT_ArtilleryButton );
				}
				
				if ( HUDComponents[ROCT_RocketsButton].ContainsPoint(ActualMouseLocation) )
				{
					if ( RocketsButtonState == ROHBS_Down )
					{
						SetButtonState( RocketsButtonState, ROHBS_Hovered, ROCT_RocketsButton );
						if( PlayerOwner.GetTeamNum() == `AXIS_TEAM_INDEX )
						{
							PlayerOwner.AttemptRequestAntiAir(RadioUsed);
						}
						else
							PlayerOwner.AttemptRequestArtillery(RadioUsed, 2);
					}
				}
				else if ( RocketsButtonState != ROHBS_Disabled )
				{
					SetButtonState( RocketsButtonState, ROHBS_None, ROCT_RocketsButton );
				}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				
				if ( HUDComponents[ROCT_ForceRespawnButton].ContainsPoint(ActualMouseLocation) )
				{
					if ( ForceRespawnButtonState == ROHBS_Down )
					{
						SetButtonState( ForceRespawnButtonState, ROHBS_Hovered, ROCT_ForceRespawnButton );
						PlayerOwner.ServerReceiveTeamLeaderCommand(255, ROORDER_ForcedRespawn);
					}
				}
				else if ( ForceRespawnButtonState != ROHBS_Disabled )
				{
					SetButtonState( ForceRespawnButtonState, ROHBS_None, ROCT_ForceRespawnButton );
				}
				
				if ( HUDComponents[ROCT_CancelButton].ContainsPoint(ActualMouseLocation) )
				{
					if ( CancelButtonState == ROHBS_Down )
					{
						SetButtonState( CancelButtonState, ROHBS_Hovered, ROCT_CancelButton );
						PlayerOwner.ServerReceiveTeamLeaderCommand(255, ROORDER_CancelArtilleryStrike);
					}
				}
				else if ( CancelButtonState != ROHBS_Disabled )
				{
					SetButtonState( CancelButtonState, ROHBS_None, ROCT_CancelButton );
				}
				
				if ( HUDComponents[ROCT_ReqUpdatedCoordsButton].ContainsPoint(ActualMouseLocation) )
				{
					if ( ReqUpdatedCoordsButtonState == ROHBS_Down )
					{
						SetButtonWithTextState( ReqUpdatedCoordsButtonState, ROHBS_Hovered, ROCT_ReqUpdatedCoordsButton );
						
						if ( (WorldInfo.TimeSeconds - LastCoordsRequest) > 2 )
						{
							PlayerOwner.ReceiveLocalizedVoiceCom(PlayerOwner.Pawn, 0, ROPRI, PlayerOwner.Pawn.Location, `VOICECOM_TLRequestArtyCoords, ROPRI, ROPRI);
							PlayerOwner.ServerReceiveTeamLeaderCommand(255, ROORDER_None);
							LastCoordsRequest = WorldInfo.TimeSeconds;
						}
					}
				}
				else if ( ReqUpdatedCoordsButtonState != ROHBS_Disabled )
				{
					SetButtonWithTextState( ReqUpdatedCoordsButtonState, ROHBS_None, ROCT_ReqUpdatedCoordsButton );
				}
				
				if ( HUDComponents[ROCT_ClearCoordsButton].ContainsPoint(ActualMouseLocation) )
				{
					if ( ClearCoordsButtonState == ROHBS_Down )
					{
						SetButtonWithTextState( ClearCoordsButtonState, ROHBS_Hovered, ROCT_ClearCoordsButton );
						PlayerOwner.ServerClearArtyInfo();
					}
				}
				else if ( ClearCoordsButtonState != ROHBS_Disabled )
				{
					SetButtonWithTextState( ClearCoordsButtonState, ROHBS_None, ROCT_ClearCoordsButton );
				}
			}
			
			if( OverheadMap.IsVisible() )
			{
				if( Key == 'LeftShift' )
				{
					if ( EventType == IE_Pressed )
					{
						OverheadMap.bAllowZoom = false;
					}
					else if ( EventType == IE_Released )
					{
						OverheadMap.bAllowZoom = true;
					}
				}
				
				if ( Key == 'MouseScrollUp' )
				{
					if ( EventType == IE_Released )
					{
						if(PlayerOwner.myROHUD.VoiceCommsWidgetMode == 0) // Classic voice comms.
						{
							if( PlayerOwner.myROHUD.OrdersWidget != none )
							{
								PlayerOwner.myROHUD.OrdersWidget.SelectPreviousUnit();
							}
						}
						else if(PlayerOwner.myROHUD.VoiceCommsWidgetMode == 1) // Compact voice comms.
						{
							if( PlayerOwner.myROHUD.CompactVoiceCommsWidget != none )
							{
								PlayerOwner.myROHUD.CompactVoiceCommsWidget.SelectPreviousUnit();
							}
						}
					}
					return true;
				}
				else if ( Key == 'MouseScrollDown' )
				{
					if ( EventType == IE_Released )
					{
						if(PlayerOwner.myROHUD.VoiceCommsWidgetMode == 0)
						{
							if( PlayerOwner.myROHUD.OrdersWidget != none )
							{
								PlayerOwner.myROHUD.OrdersWidget.SelectNextUnit();
							}
						}
						else if(PlayerOwner.myROHUD.VoiceCommsWidgetMode == 1)
						{
							if( PlayerOwner.myROHUD.CompactVoiceCommsWidget != none )
							{
								PlayerOwner.myROHUD.CompactVoiceCommsWidget.SelectNextUnit();
							}
						}
					}
					return true;
				}
			}
			return true;
		}
	}
	
	return false;
}

function UpdateForTeam(byte MyTeam)
{
	local ROGameReplicationInfo ROGRI;
	ROGRI = ROGameReplicationInfo(WorldInfo.GRI);
	
	if(WorldInfo.GetMapInfo() == none)
		return;
	
	if( MyTeam == `AXIS_TEAM_INDEX )
	{
		HUDComponents[ROCT_ForceRespawnTime].Text = ZeroTimeString;
		HUDComponents[ROCT_AerialReconTime].Text = ZeroTimeString;
		HUDComponents[ROCT_MortarsTime].Text = ZeroTimeString;
		HUDComponents[ROCT_ArtilleryTime].Text = ZeroTimeString;
		HUDComponents[ROCT_RocketsTime].Text = ZeroTimeString;
		
		HUDComponents[ROCT_ForceRespawnButton].Mat=	ForceRespawnButtonTex;
		ButtonOneTipTitleString =					ForceRespawnTipTitleString;
		ButtonOneTipDescriptionString =				ForceRespawnTipString;
		
		HUDComponents[ROCT_AerialReconButton].Mat =	AerialReconButtonTex;
		ButtonTwoTipTitleString =					AerialReconTipTitleString;
		ButtonTwoTipDescriptionString =				AerialReconTipString;
		
		HUDComponents[ROCT_MortarsButton].Mat =		NorthAbility1ButtonTex;
		ButtonThreeTipTitleString =					FinAbility1TipTitleString;
		ButtonThreeTipDescriptionString =			FinAbility1TipString;
		
		HUDComponents[ROCT_ArtilleryButton].Mat =	NorthAbility2ButtonTex;
		ButtonFourTipTitleString =					FinAbility2TipTitleString;
		ButtonFourTipDescriptionString =			FinAbility2TipString;
		
		HUDComponents[ROCT_RocketsButton].Mat =		NorthAbility3ButtonTex;
		ButtonFiveTipTitleString =					FinAbility3TipTitleString;
		ButtonFiveTipDescriptionString =			FinAbility3TipString;
		
		HUDComponents[ROCT_CancelButton].Tex = CancelButtonTex;
	}
	else
	{
		HUDComponents[ROCT_ForceRespawnTime].Text = ZeroTimeString;
		HUDComponents[ROCT_AerialReconTime].Text = ZeroTimeString;
		HUDComponents[ROCT_MortarsTime].Text = ZeroTimeString;
		HUDComponents[ROCT_ArtilleryTime].Text = ZeroTimeString;
		HUDComponents[ROCT_RocketsTime].Text = ZeroTimeString;
		
		HUDComponents[ROCT_ForceRespawnButton].Mat=	ForceRespawnButtonTex;
		ButtonOneTipTitleString =					ForceRespawnTipTitleString;
		ButtonOneTipDescriptionString =				ForceRespawnTipString;
		
		HUDComponents[ROCT_AerialReconButton].Mat =	AerialReconButtonTex;
		ButtonTwoTipTitleString =					AerialReconTipTitleString;
		ButtonTwoTipDescriptionString =				AerialReconTipString;
		
		HUDComponents[ROCT_MortarsButton].Mat =		SouthAbility1ButtonTex;
		ButtonThreeTipTitleString =					SovAbility1TipTitleString;
		ButtonThreeTipDescriptionString =			SovAbility1TipString;
		
		HUDComponents[ROCT_ArtilleryButton].Mat =	SouthAbility2ButtonTex;
		ButtonFourTipTitleString =					SovAbility2TipTitleString;
		ButtonFourTipDescriptionString =			SovAbility2TipString;
		
		HUDComponents[ROCT_RocketsButton].Mat =		SouthAbility3ButtonTex;
		ButtonFiveTipTitleString =					SovAbility3TipTitleString;
		ButtonFiveTipDescriptionString =			SovAbility3TipString;
		
		HUDComponents[ROCT_CancelButton].Tex = CancelButtonTex;
	}
	
	ButtonOneTipCooldownString = CooldownString @ WWMapInfo(WorldInfo.GetMapInfo()).InstantRespawnInterval;
	ButtonTwoTipCooldownString = CooldownString @ WWMapInfo(WorldInfo.GetMapInfo()).GetReconInterval(MyTeam);
	ButtonThreeTipCooldownString = CooldownString @ ROGRI.AbilityOneDelay[MyTeam];
	ButtonFourTipCooldownString = CooldownString @ ROGRI.AbilityTwoDelay[MyTeam];
	ButtonFiveTipCooldownString = CooldownString @ ROGRI.AbilityThreeDelay[MyTeam];
	
	HUDComponents[ROCT_AerialReconProgress].bVisible = false;
	MaterialInstanceConstant(HUDComponents[ROCT_AerialReconProgress].Mat).SetScalarParameterValue(ProgressParamName, 0);
	HUDComponents[ROCT_MortarsProgress].bVisible = false;
	MaterialInstanceConstant(HUDComponents[ROCT_MortarsProgress].Mat).SetScalarParameterValue(ProgressParamName, 0);
	HUDComponents[ROCT_ArtilleryProgress].bVisible = false;
	MaterialInstanceConstant(HUDComponents[ROCT_ArtilleryProgress].Mat).SetScalarParameterValue(ProgressParamName, 0);
	HUDComponents[ROCT_RocketsProgress].bVisible = false;
	MaterialInstanceConstant(HUDComponents[ROCT_RocketsProgress].Mat).SetScalarParameterValue(ProgressParamName, 0);
}

defaultproperties
{
	CancelButtonTex=Texture2D'WinterWar_UI.CommanderAbility.CommanderAbility_Cancel'
	ForceDeployButtonTexTemplate=MaterialInstanceConstant'WinterWar_UI.CommanderAbility.CommanderAbility_ForceSpawn_MIC'
	ReconButtonTexTemplate=MaterialInstanceConstant'WinterWar_UI.CommanderAbility.CommanderAbility_Recon_MIC'
	FinAbility1ButtonTexTemplate=MaterialInstanceConstant'WinterWar_UI.CommanderAbility.CommanderAbility_FIN_Mortars_MIC'
	FinAbility2ButtonTexTemplate=MaterialInstanceConstant'WinterWar_UI.CommanderAbility.CommanderAbility_FIN_Artillery_MIC'
	FinAbility3ButtonTexTemplate=MaterialInstanceConstant'WinterWar_UI.CommanderAbility.CommanderAbility_FIN_Fighters_MIC'
	SovAbility1ButtonTexTemplate=MaterialInstanceConstant'WinterWar_UI.CommanderAbility.CommanderAbility_SOV_Artillery_MIC'
	SovAbility2ButtonTexTemplate=MaterialInstanceConstant'WinterWar_UI.CommanderAbility.CommanderAbility_SOV_HeavyArtillery_MIC'
	SovAbility3ButtonTexTemplate=MaterialInstanceConstant'WinterWar_UI.CommanderAbility.CommanderAbility_SOV_Bombers_MIC'
}
