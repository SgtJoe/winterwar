
class WWWeapon_MN9130 extends WWWeapon_MosinBase
	abstract;

defaultproperties
{
	WeaponContentClass(0)="WinterWar.WWWeapon_MN9130_ActualContent"
	
	RoleSelectionImage(0)=Texture2D'WinterWar_UI.WP_Render.WP_Render_MN9130'
	
	TeamIndex=`ALLIES_TEAM_INDEX
	
	InvIndex=`WI_MN9130
	
	WeaponProjectiles(DEFAULT_FIREMODE)=	class'MN9130Bullet'
	WeaponProjectiles(ALTERNATE_FIREMODE)=	class'MN9130Bullet'
	
	Spread(DEFAULT_FIREMODE)=	0.0007// ~2.5 MOA
	Spread(ALTERNATE_FIREMODE)=	0.0007// ~2.5 MOA
	
	InstantHitDamageTypes(0)=class'RODmgType_MN9130Bullet'
	InstantHitDamageTypes(1)=class'RODmgType_MN9130Bullet'
}
