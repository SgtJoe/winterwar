
class WWWeapon_LahtiSaloranta_ActualContent extends WWWeapon_LahtiSaloranta;

simulated function SetupArmsAnim()
{
	super.SetupArmsAnim();
	
	// ArmsMesh.AnimSets has slots 0-2-3 filled, so we need to back fill slot 1 and then move to slot 4
	ROPawn(Instigator).ArmsMesh.AnimSets[1] = SkeletalMeshComponent(Mesh).AnimSets[0];
	ROPawn(Instigator).ArmsMesh.AnimSets[4] = SkeletalMeshComponent(Mesh).AnimSets[1];
}

DefaultProperties
{
	ArmsAnimSet=AnimSet'WP_VN_AUS_L2A1.Animation.WP_L2A1hands'
	
	Begin Object Name=FirstPersonMesh
		DepthPriorityGroup=SDPG_Foreground
		SkeletalMesh=SkeletalMesh'WinterWar_WP_FIN_LS_M26.Mesh.FIN_LahtiSaloranta'
		PhysicsAsset=PhysicsAsset'WP_VN_AUS_L2A1.Phys.AUS_L2A1_UPDG1_Physics'
		AnimSets(0)=AnimSet'WP_VN_AUS_L2A1.Animation.WP_L2A1hands'
		AnimSets(1)=AnimSet'WinterWar_WP_FIN_LS_M26.Anim.LShands'
		AnimTreeTemplate=AnimTree'WP_VN_AUS_L2A1.Animation.WP_L2A1hands_Tree'
		Scale=1.0
		FOV=70
	End Object
	
	Begin Object Name=PickupMesh
		SkeletalMesh=SkeletalMesh'WinterWar_WP_FIN_LS_M26.Mesh.FIN_LahtiSaloranta_3rd'
		PhysicsAsset=PhysicsAsset'WP_VN_AUS_3rd_Master.Phy.L2A1_3rd_Physics'
		AnimTreeTemplate=AnimTree'WP_VN_AUS_3rd_Master.AnimTree.L2A1_3rd_Tree'
		CollideActors=true
		BlockActors=true
		BlockZeroExtent=true
		BlockNonZeroExtent=true
		BlockRigidBody=true
		bHasPhysicsAssetInstance=false
		bUpdateKinematicBonesFromAnimation=false
		PhysicsWeight=1.0
		RBChannel=RBCC_GameplayPhysics
		RBCollideWithChannels=(Default=TRUE,GameplayPhysics=TRUE,EffectPhysics=TRUE)
		bSkipAllUpdateWhenPhysicsAsleep=TRUE
		bSyncActorLocationToRootRigidBody=true
	End Object
	
	AttachmentClass=class'WWWeapon_LahtiSaloranta_Attach'
	
	WeaponFireSnd(DEFAULT_FIREMODE)=(	DefaultCue=AkEvent'WW_WEP_M1918.Play_WEP_M1918_Single_3P', FirstPersonCue=AkEvent'WW_WEP_M1918.Play_WEP_M1918_Fire_Single')
	WeaponFireSnd(ALTERNATE_FIREMODE)=(	DefaultCue=AkEvent'WW_WEP_M1918.Play_WEP_M1918_Single_3P', FirstPersonCue=AkEvent'WW_WEP_M1918.Play_WEP_M1918_Fire_Single')
	bLoopingFireSnd(DEFAULT_FIREMODE)=true
	bLoopHighROFSounds(DEFAULT_FIREMODE)=false
}
