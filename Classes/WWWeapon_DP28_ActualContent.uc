
class WWWeapon_DP28_ActualContent extends WWWeapon_DP28;

DefaultProperties
{
	ArmsAnimSet=AnimSet'WP_VN_VC_DP_28_LMG.animation.WP_DP28bipodhands'
	
	Begin Object Name=FirstPersonMesh
		DepthPriorityGroup=SDPG_Foreground
		SkeletalMesh=SkeletalMesh'WP_VN_VC_DP_28_LMG.Mesh.Sov_DP28'
		PhysicsAsset=PhysicsAsset'WP_VN_VC_DP_28_LMG.Phys.Sov_DP28_Physics'
		AnimSets(0)=AnimSet'WP_VN_VC_DP_28_LMG.animation.WP_DP28bipodhands'
		AnimTreeTemplate=AnimTree'WP_VN_VC_DP_28_LMG.animation.Sov_DP28Bipod_Tree'
		Scale=1.0
		FOV=70
	End Object
	
	Begin Object Name=PickupMesh
		SkeletalMesh=SkeletalMesh'WP_VN_3rd_Master.Mesh.DP28_3rd_Master'
		PhysicsAsset=PhysicsAsset'WP_VN_3rd_Master.Phy.DP28_3rd_Master_Physics'
		AnimTreeTemplate=AnimTree'WP_VN_3rd_Master.Animation.DP28_3rd_Tree'
		CollideActors=true
		BlockActors=true
		BlockZeroExtent=true
		BlockNonZeroExtent=true//false
		BlockRigidBody=true
		bHasPhysicsAssetInstance=false
		bUpdateKinematicBonesFromAnimation=false
		PhysicsWeight=1.0
		RBChannel=RBCC_GameplayPhysics
		RBCollideWithChannels=(Default=TRUE,GameplayPhysics=TRUE,EffectPhysics=TRUE)
		bSkipAllUpdateWhenPhysicsAsleep=TRUE
		bSyncActorLocationToRootRigidBody=true
	End Object
	
	AttachmentClass=class'WWWeapon_DP28_Attach'
}
