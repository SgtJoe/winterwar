
class WWHUDWidgetStatus extends ROHUDWidgetStatus;

enum EROStatusTexturesNew
{
	ROST_HealthBackground,
	ROST_HealthDamageHead,
	ROST_HealthDamageNeck,
	ROST_HealthDamageChest,
	ROST_HealthDamageHeart,
	ROST_HealthDamageStomach,
	ROST_HealthDamageAbdomen,
	ROST_HealthDamageRightUpperArm,
	ROST_HealthDamageRightUpperArmCrit,
	ROST_HealthDamageRightForearm,
	ROST_HealthDamageRightHand,
	ROST_HealthDamageLeftUpperArm,
	ROST_HealthDamageLeftUpperArmCrit,
	ROST_HealthDamageLeftForearm,
	ROST_HealthDamageLeftHand,
	ROST_HealthDamageRightThigh,
	ROST_HealthDamageRightThighCrit,
	ROST_HealthDamageRightCalf,
	ROST_HealthDamageRightFoot,
	ROST_HealthDamageLeftThigh,
	ROST_HealthDamageLeftThighCrit,
	ROST_HealthDamageLeftCalf,
	ROST_HealthDamageLeftFoot,
	ROST_HealthDamageUpperSpine,
	ROST_HealthDamageLowerSpine,
	ROST_LegsInjuredOutline,
	ROST_StaminaOutline,
	ROST_Stamina
};

var Texture2D BGTextureGerHiMorale;
var Texture2D BGTextureGerMeMorale;
var Texture2D BGTextureGerLoMorale;

var Texture2D BGTextureSovHiMorale;
var Texture2D BGTextureSovMeMorale;
var Texture2D BGTextureSovLoMorale;

var bool bForceShowMorale;
var int DisplayedMorale;

var float	DisplayedStamina;
var float	LastStaminaChange;
var color	StaminaStartColor;
var color	StaminaMediumColor;
var color	StaminaLowColor;

function UpdateWidget()
{
	local ROPawn ROP;
	local float CurrentStamina;
	
	if (ROVehicle(PlayerOwner.Pawn) != none || ROWeaponPawn(PlayerOwner.Pawn) != none)
	{
		bVisible = false;
		return;
	}
	
	super.UpdateWidget();
	
	if(PlayerOwner.Pawn != none)
	{
		ROP = ROPawn(PlayerOwner.Pawn);
	}
	
	if (PlayerOwner != none && PlayerOwner.Pawn != none && ROP != none && ROTeamInfo(PlayerOwner.PlayerReplicationInfo.Team) != none)
	{
		// Morale
		if (bForceShow || DisplayedHealth != PlayerOwner.Pawn.Health || ROP.BleedingStatus > 0 || DisplayedMorale != ROTeamInfo(PlayerOwner.PlayerReplicationInfo.Team).Morale || bForceShowMorale)
		{
			if (ROTeamInfo(PlayerOwner.PlayerReplicationInfo.Team).Morale > 50 /* && (DisplayedHealth != PlayerOwner.Pawn.Health || ROP.BleedingStatus > 0) */)
			{
				// `wwdebug("Morale High");
				
				if (PlayerOwner.GetTeamNum() == `AXIS_TEAM_INDEX)
				{
					HUDComponents[ROST_HealthBackground].Tex = BGTextureGerHiMorale;
				}
				else
				{
					HUDComponents[ROST_HealthBackground].Tex = BGTextureSovHiMorale;
				}
			}
			else if (ROTeamInfo(PlayerOwner.PlayerReplicationInfo.Team).Morale > 0)
			{
				// `wwdebug("Morale Med");
				
				if ( PlayerOwner.GetTeamNum() == `AXIS_TEAM_INDEX )
				{
					HUDComponents[ROST_HealthBackground].Tex = BGTextureGerMeMorale;
				}
				else
				{
					HUDComponents[ROST_HealthBackground].Tex = BGTextureSovMeMorale;
				}
			}
			else
			{
				// `wwdebug("Morale Low");
				
				if (PlayerOwner.GetTeamNum() == `AXIS_TEAM_INDEX)
				{
					HUDComponents[ROST_HealthBackground].Tex = BGTextureGerLoMorale;
				}
				else
				{
					HUDComponents[ROST_HealthBackground].Tex = BGTextureSovLoMorale;
				}
			}
			
			DisplayedMorale = ROTeamInfo(PlayerOwner.PlayerReplicationInfo.Team).Morale;
		}
		
		// Stamina
		if (bForceShow || (ROP.StaminaPercentage >= ROP.BreathStaminaStandardLimit && ROP.StaminaPercentage < ROP.BreathStaminaStandardLimit + 0.1) ||
			(ROP.StaminaPercentage < ROP.BreathStaminaExhaustedLimit + 0.05) || (ROP.StaminaPercentage >= 0.90 && ROP.StaminaPercentage < 1.00))
		{
			FadeOutDelay = default.FadeOutDelay;
			
			CurrentStamina = FMax(0.05, ROP.StaminaPercentage);
			
			HUDComponents[ROST_Stamina].ScaleTextureWidthToPercentage(CurrentStamina);
			
			if (ROP.StaminaPercentage > ROP.BreathStaminaStandardLimit)
			{
				HUDComponents[ROST_Stamina].DrawColor = InterpolateColor(StaminaStartColor, StaminaMediumColor, ROP.StaminaPercentage, 1.0, ROP.BreathStaminaStandardLimit);
				HUDComponents[ROST_Stamina].ClearFlash();
				HUDComponents[ROST_StaminaOutline].bColorFlashing = false;
			}
			else if (ROP.StaminaPercentage > ROP.BreathStaminaExhaustedLimit)
			{
				HUDComponents[ROST_Stamina].DrawColor = InterpolateColor(StaminaMediumColor, StaminaLowColor, ROP.StaminaPercentage, ROP.BreathStaminaStandardLimit, ROP.BreathStaminaExhaustedLimit);
				HUDComponents[ROST_Stamina].ClearFlash();
				HUDComponents[ROST_StaminaOutline].bColorFlashing = false;
			}
			else
			{
				HUDComponents[ROST_Stamina].DrawColor = StaminaLowColor;
				HUDComponents[ROST_Stamina].bFlashing = true;
				HUDComponents[ROST_StaminaOutline].FlashColor = MakeColor(255,0,0,255);
				HUDComponents[ROST_StaminaOutline].bColorFlashing = true;
			}
			
			DisplayedStamina = ROP.Stamina;
			
			HandleHUDOnDemandFadeIn(ROST_StaminaOutline, LastStaminaChange, ROST_Stamina);
		}
		else
		{
			HUDComponents[ROST_Stamina].bFlashing = false;
			HandleHUDOnDemandFadeOut(ROST_StaminaOutline, LastStaminaChange, ROST_Stamina);
		}
	}
	else
	{
		DisplayedStamina = -1;
		HUDComponents[ROST_Stamina].bFlashing = false;
		HandleHUDOnDemandFadeOut(ROST_StaminaOutline, LastStaminaChange, ROST_Stamina);
	}
}

function EndForcedShowMorale()
{
	bForceShowMorale = false;
}

defaultproperties
{
	BGTextureGerHiMorale=Texture2D'WinterWar_UI.Status.Status_FIN_High'
	BGTextureGerMeMorale=Texture2D'WinterWar_UI.Status.Status_FIN_Norm'
	BGTextureGerLoMorale=Texture2D'WinterWar_UI.Status.Status_FIN_Low'
	
	BGTextureSovHiMorale=Texture2D'WinterWar_UI.Status.Status_SOV_High'
	BGTextureSovMeMorale=Texture2D'WinterWar_UI.Status.Status_SOV_Norm'
	BGTextureSovLoMorale=Texture2D'WinterWar_UI.Status.Status_SOV_Low'
	
	StaminaStartColor=(R=58,G=114,B=46,A=255)
	StaminaMediumColor=(R=205,G=131,B=64,A=255)
	StaminaLowColor=(R=200,G=0,B=0,A=255)
	
	`define XVALUE -130
	`define YVALUE 112
	
	Begin Object Class=ROHUDWidgetComponent Name=StaminaOutlineTexture
		X=`XVALUE
		Y=`YVALUE
		Width=118
		Height=28
		TexWidth=256
		TexHeight=64
		Tex=Texture2D'WinterWar_UI.Status.Status_StaminaBar_BG'
		FadeOutTime=0.5
		DrawColor=(R=255,G=255,B=255,A=255)
		bFadedOut=true
		SortPriority=DSP_AboveNormal
	End Object
	HUDComponents(ROST_StaminaOutline)=StaminaOutlineTexture
	
	Begin Object Class=ROHUDWidgetComponent Name=StaminaTexture
		X=`XVALUE
		Y=`YVALUE
		Width=118
		Height=28
		TexWidth=256
		TexHeight=64
		Tex=Texture2D'WinterWar_UI.Status.Status_StaminaBar'
		FadeOutTime=0.5
		DrawColor=(R=255,G=255,B=255,A=255)
		bFadedOut=true
		SortPriority=DSP_AboveNormal
	End Object
	HUDComponents(ROST_Stamina)=StaminaTexture
	
	Begin Object Name=HeadTexture
		Tex=Texture2D'WinterWar_UI.Status.Status_Damage_Head'
	End Object
	
	Begin Object Name=NeckTexture
		Tex=Texture2D'WinterWar_UI.Status.Status_Damage_neck'
	End Object
	
	Begin Object Name=ChestTexture
		Tex=Texture2D'WinterWar_UI.Status.Status_Damage_torso'
	End Object
	
	Begin Object Name=HeartTexture
		Tex=Texture2D'WinterWar_UI.Status.Status_Damage_heart'
	End Object
	
	Begin Object Name=StomachTexture
		Tex=Texture2D'WinterWar_UI.Status.Status_Damage_stomach'
	End Object
	
	Begin Object Name=AbdomenTexture
		Tex=Texture2D'WinterWar_UI.Status.Status_Damage_abdomen'
	End Object
	
	Begin Object Name=R_UpperArmTexture
		Tex=Texture2D'WinterWar_UI.Status.Status_Damage_RarmUpper'
	End Object
	
	Begin Object Name=R_UpperArmCriticalTexture
		Tex=Texture2D'WinterWar_UI.Status.Status_Damage_RarmUpper'
	End Object
	
	Begin Object Name=R_ForeArmTexture
		Tex=Texture2D'WinterWar_UI.Status.Status_Damage_RarmLower'
	End Object
	
	Begin Object Name=R_HandTexture
		Tex=Texture2D'WinterWar_UI.Status.Status_Damage_Rhand'
	End Object
	
	Begin Object Name=L_UpperArmTexture
		Tex=Texture2D'WinterWar_UI.Status.Status_Damage_LarmUpper'
	End Object
	
	Begin Object Name=L_UpperArmCriticalTexture
		Tex=Texture2D'WinterWar_UI.Status.Status_Damage_LarmUpper'
	End Object
	
	Begin Object Name=L_ForeArmTexture
		Tex=Texture2D'WinterWar_UI.Status.Status_Damage_LarmLower'
	End Object
	
	Begin Object Name=L_HandTexture
		Tex=Texture2D'WinterWar_UI.Status.Status_Damage_Lhand'
	End Object
	
	Begin Object Name=R_ThighTexture
		Tex=Texture2D'WinterWar_UI.Status.Status_Damage_RlegUpper'
	End Object
	
	Begin Object Name=R_ThighCriticalTexture
		Tex=Texture2D'WinterWar_UI.Status.Status_Damage_RlegUpper'
	End Object
	
	Begin Object Name=R_CalfTexture
		Tex=Texture2D'WinterWar_UI.Status.Status_Damage_RlegLower'
	End Object
	
	Begin Object Name=R_FootTexture
		Tex=Texture2D'WinterWar_UI.Status.Status_Damage_Rfoot'
	End Object
	
	Begin Object Name=L_ThighTexture
		Tex=Texture2D'WinterWar_UI.Status.Status_Damage_LlegUpper'
	End Object
	
	Begin Object Name=L_ThighCriticalTexture
		Tex=Texture2D'WinterWar_UI.Status.Status_Damage_LlegUpper'
	End Object
	
	Begin Object Name=L_CalfTexture
		Tex=Texture2D'WinterWar_UI.Status.Status_Damage_LlegLower'
	End Object
	
	Begin Object Name=L_FootTexture
		Tex=Texture2D'WinterWar_UI.Status.Status_Damage_Lfoot'
	End Object
	
	Begin Object Name=Spine_UpperTexture
		Tex=Texture2D'VN_UI_Textures_Two.HUD.Paperdoll.ui_hud_damage_player_UpperSpine'
	End Object
	
	Begin Object Name=Spine_LowerTexture
		Tex=Texture2D'VN_UI_Textures_Two.HUD.Paperdoll.ui_hud_damage_player_LowerSpine'
	End Object
}

