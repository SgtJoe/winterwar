
class WWRoleInfoAxisCommander extends WWRoleInfoAxis;

DefaultProperties
{
	RoleType=RORIT_Commander
	ClassTier=4
	ClassIndex=`RI_COMMANDER
	
	Items[RORIGM_Default]={(
		PrimaryWeapons=(class'WWWeapon_KP31',class'WWWeapon_MN27'),
		
		OtherItems=(class'WWWeapon_Binoculars',class'WWWeapon_RDG1')
	)}
	
	bAllowPistolsInRealism=true
	bIsTeamLeader=true
	
	ClassIcon=Texture2D'VN_UI_Textures.menu.class_icon_commander'
}
