
class WWVehicleProjectile_T28_AP extends WWVehicleProjectile_AP;

defaultproperties
{
	ImpactDamageType=	class'WWDmgType_T28Shell_AP'
	GeneralDamageType=	class'WWDmgType_T28Shell_AP_General'
	MyDamageType=		class'WWDmgType_T28Shell_AP'

	BallisticCoefficient=1.55
	Speed=18500 // 370 M/S
	MaxSpeed=18500
	ImpactDamage=500
	Damage=100
	DamageRadius=75
	MomentumTransfer=43000
	
	Caliber=76
	ActualRHA=37
	TestPlateHardness=400
	SlopeEffect=0.09559
	ShatterNumber=1.00
	ShatterTd=0.65
	ShatteredPenEffectiveness=0.8
	
	PenetrationDepth=75
	MaxPenetrationTests=6
}
