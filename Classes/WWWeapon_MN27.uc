
class WWWeapon_MN27 extends WWWeapon_MosinBase
	abstract;

defaultproperties
{
	WeaponContentClass(0)="WinterWar.WWWeapon_MN27_ActualContent"
	
	RoleSelectionImage(0)=Texture2D'WinterWar_UI.WP_Render.WP_Render_MN27'
	
	TeamIndex=`AXIS_TEAM_INDEX
	
	InvIndex=`WI_MN27
	
	WeaponProjectiles(DEFAULT_FIREMODE)=	class'WWProjectile_MN27Bullet'
	WeaponProjectiles(ALTERNATE_FIREMODE)=	class'WWProjectile_MN27Bullet'
	
	Spread(DEFAULT_FIREMODE)=	0.000414 // 1.5 MOA
	Spread(ALTERNATE_FIREMODE)=	0.000414 // 1.5 MOA
	
	InstantHitDamageTypes(0)=class'WWDmgType_MN27Bullet'
	InstantHitDamageTypes(1)=class'WWDmgType_MN27Bullet'
	
	AltFireModeType=ROAFT_None
	
	bHasBayonet=false

	CollisionCheckLength=59.35
}
