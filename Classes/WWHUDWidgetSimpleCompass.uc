
class WWHUDWidgetSimpleCompass extends ROHUDWidgetSimpleCompass;

enum EROCompassTexturesNew
{
	ROSCC_BG,
	ROSCC_Rose,
//	ROSCC_GridLocation
};
/* 
var float	WorldMinX,				// Unreal Units for Map Boundaries
			WorldMinY,				// Unreal Units for Map Boundaries
			WorldMaxX,				// Unreal Units for Map Boundaries
			WorldMaxY;				// Unreal Units for Map Boundaries
var	float	LevelToMapScaleX;		// Unreal Units to Map Pixels Scale on the X Axis
var	float	LevelToMapScaleY;		// Unreal Units to Map Pixels Scale on the Y Axis
var	float	MapOffsetX;				// X Offset fudge factor for all markers in pixels
var	float	MapOffsetY;				// Y Offset fudge factor for all markers in pixels
var float	GridDivisionSizeWorld;	// Grid Size(1/10th of the width or height of the world)
 */
var rotator NorthOffset;

function Initialize(PlayerController HUDPlayerOwner)
{
	// local array<Actor> AllMapBounds;
	local MapInfo CurMapInfo;
	local int i;
	// local Texture2d TempTexture;
	
	CurMapInfo = WorldInfo.GetMapInfo();
	
	if ( CurMapInfo == none || ROMapInfo(CurMapInfo) == none || ROMapInfo(CurMapInfo).OverheadMapTexture == none )
	{
		return;
	}
	
	// TempTexture = ROMapInfo(CurMapInfo).OverheadMapTexture;
	/* 
	if ( FindActorsOfClass(class'ROVolumeMapBounds', AllMapBounds) )
	{
		if ( ROVolumeMapBounds(AllMapBounds[0]).GetBounds(WorldMinX, WorldMinY, WorldMaxX, WorldMaxY) )
		{
			LevelToMapScaleX = TempTexture.SizeX / (WorldMaxY - WorldMinY);
			LevelToMapScaleY = TempTexture.SizeY / (WorldMaxX - WorldMinX);
			MapOffsetX = (WorldMinY + (WorldMaxY - WorldMinY) / 2.0) * LevelToMapScaleX;
			MapOffsetY = (WorldMinX + (WorldMaxX - WorldMinX) / 2.0) * LevelToMapScaleY;
			GridDivisionSizeWorld = (WorldMaxY - WorldMinY) / `NUM_GRID_DIVISIONS;
		}
	}
	 */
	PlayerOwner = ROPlayerController(HUDPlayerOwner);
	
	for ( i = 0; i < HUDComponents.Length; i++ )
	{
		if ( HUDComponents[i] != none )
		{
			HUDComponents[i].Initialize();
		}
	}
	
	NorthOffset.Yaw = ROMapInfo(CurMapInfo).NorthOffset * (65535 / 360);
	
	super(ROHUDWidget).Initialize(HUDPlayerOwner);
}

function UpdateWidget()
{
	local rotator NewPlayerRotation;
	local vector TempVector/* , NewPlayerPosition */;
	// local int VMSIndex;
	
	if (PlayerOwner == none)
	{
		bVisible = false;
		return;
	}
	
	if (HudComponents.Length <= 0)
	{
		bVisible = false;
		return;
	}
	/* 
	if (PlayerOwner.Pawn != none)
	{
		NewPlayerPosition = PlayerOwner.Pawn.Location;
	}
	else
	{
		NewPlayerPosition = PlayerOwner.Location;
	}
	 */
	PlayerOwner.GetPlayerViewPoint(TempVector, NewPlayerRotation);
	
	HudComponents[ROSCC_ROSE].Rotation.Yaw = -(NewPlayerRotation.Yaw + NorthOffset.Yaw);
	/* 
	VMSIndex = FFloor((WorldMaxX - NewPlayerPosition.X) / GridDivisionSizeWorld);
	if( VMSIndex < VerticalMarkersString.Length )
	{
		HudComponents[ROSCC_GridLocation].Text = VerticalMarkersString[VMSIndex] $ (FFloor((NewPlayerPosition.Y - WorldMinY) / GridDivisionSizeWorld) + 1);
	}
	 */
	super(ROHUDWidget).UpdateWidget();
}

defaultproperties
{
	// GridDivisionSizeWorld=1.0
	// LevelToMapScaleX=0.1
	// LevelToMapScaleY=0.1
	// MapOffsetX=0.0
	// MapOffsetY=0.0
	
	X=-128
	Y=-128
	
	bVerticalCenter=false
	bHorizontalCenter=false
	
	HudComponents.Empty()
	
	Begin Object Class=ROHUDWidgetComponent Name=CompassTexture
		X=0
		Y=0
		Width=128
		Height=128
		TexWidth=256
		TexHeight=256
		Tex=Texture2D'WinterWar_UI.Compass.Compass_Ring'
		bFadedOut=false
		FadeOutTime=0.5
		bVisible=true
		SortPriority=DSP_AboveNormal
	End Object
	HudComponents(ROSCC_BG)=CompassTexture
	
	Begin Object Class=ROHUDWidgetComponent Name=Rose
		X=0
		Y=0
		Width=128
		Height=128
		TexWidth=256
		TexHeight=256
		Tex=Texture2D'WinterWar_UI.Compass.Compass_Interior'
		bFadedOut=false
		FadeOutTime=0.5
		bVisible=true
		SortPriority=DSP_AboveNormal
	End Object
	HudComponents(ROSCC_ROSE)=Rose
	
	/* Begin Object Class=ROHUDWidgetComponent Name=GridLocation
		X=114
		Y=110
		TextFont=Font'ui_fonts.Font_Clean'
		TextScale=0.55
		TextAlignment=ROHTA_Center
		DrawColor=(R=255,G=255,B=255,A=255)
		bFadedOut=false
		FadeOutTime=0.5
		bDropShadow=true
		DropShadowOffset=(X=2,Y=2)
		DropShadowColor=(R=0,G=0,B=0,A=255)
		bVisible=true
		SortPriority=DSP_High
	End Object
	HudComponents(ROSCC_GridLocation)=GridLocation */
}
