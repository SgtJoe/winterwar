
class WWWeapon_Luger extends WWWeapon_L35
	abstract;

var name RoundBoneNames[8];

var name MagFollowerSkelControlName;

var SkelControlBase MagFollowerSkelControl;

var int UpcomingMagAmmoCount;

simulated event PostInitAnimTree(SkeletalMeshComponent SkelComp)
{
	super.PostInitAnimTree(SkelComp);
	
	if(MagFollowerSkelControlName != 'none')
	{
		MagFollowerSkelControl = SkelComp.FindSkelControl(MagFollowerSkelControlName);
	}
}

simulated function FireAmmunition()
{
	super.FireAmmunition();
	UpdateRounds(false);
}

simulated function TimeAmmoCheck()
{
	super.TimeAmmoCheck();
	UpdateRounds(false);
}

private reliable client function SendNextMagAmmoCount(int NewMagCount)
{
	UpcomingMagAmmoCount = NewMagCount;
}

simulated function TimeReloading()
{
	if( Role == ROLE_Authority )
	{
		UpcomingMagAmmoCount = AmmoArray.Length > 0 ? AmmoArray[GetNextMagIndex(AmmoCount <= 0)] : 0;
		SendNextMagAmmoCount(UpcomingMagAmmoCount);
	}
	
	UpdateRounds(false);
	
	super.TimeReloading();
}

simulated function UpdateRoundsTimeReloading()
{
	UpdateRounds(true);
}

simulated function UpdateRounds(bool bReloadingAnimNotify)
{
	local SkeletalMeshComponent SkelMesh;
	local int i, NumRoundsInMag;
	
	SkelMesh = SkeletalMeshComponent(Mesh);
	
	NumRoundsInMag = bReloadingAnimNotify ? UpcomingMagAmmoCount : AmmoCount - 1;
	
	if(SkelMesh != none)
	{
		for(i = 0; i < MaxAmmoCount /*- 1*/; ++i)
		{
			if(i >= NumRoundsInMag)
			{
				SkelMesh.HideBoneByName(RoundBoneNames[i], PBO_Disable);
			}
			else
			{
				SkelMesh.UnHideBoneByName(RoundBoneNames[i]);
			}
		}
	}
	
	if(MagFollowerSkelControl != none)
	{
		MagFollowerSkelControl.SetSkelControlStrength( 1 - FPctByRange(NumRoundsInMag, 0, MaxAmmoCount /*- 1*/), 0.f );
	}
}

`include(WinterWar\Classes\WWWeaponPickupMessagesOverride.uci)

defaultproperties
{
	WeaponContentClass(0)="WinterWar.WWWeapon_Luger_ActualContent"
	
	RoleSelectionImage(0)=Texture2D'WinterWar_UI.WP_Render.WP_Render_Luger'
	
	InvIndex=`WI_LUGER
	
	WeaponProjectiles(0)=class'WWProjectile_Luger'
	
	InstantHitDamageTypes(0)=class'WWDmgType_Luger'
	InstantHitDamageTypes(1)=class'WWDmgType_Luger'
	
	WeaponReloadNonEmptyMagAnim=Luger_reloadhalf
	WeaponReloadEmptyMagAnim=Luger_reloadempty
	
	WeaponAmmoCheckAnim=Luger_ammocheck
	
	BoltControllerNames[0]=SlideControl_Barrel
	BoltControllerNames[1]=SlideControl_MainSlide
	BoltControllerNames[2]=SlideControl_Toggle
	BoltControllerNames[3]=SlideControl_Link
	
	MagFollowerSkelControlName=FeedControl
	
	RoundBoneNames(0)=B1
	RoundBoneNames(1)=B2
	RoundBoneNames(2)=B3
	RoundBoneNames(3)=B4
	RoundBoneNames(4)=B5
	RoundBoneNames(5)=B6
	RoundBoneNames(6)=B7
	RoundBoneNames(7)=B8
}
