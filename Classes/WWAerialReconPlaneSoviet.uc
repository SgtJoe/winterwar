
class WWAerialReconPlaneSoviet extends WWAerialReconPlane;

defaultproperties
{
	TeamIndex=`ALLIES_TEAM_INDEX
	
	Begin Object Name=PlaneMesh
		SkeletalMesh=SkeletalMesh'WinterWar_VH_USSR_RECON.Mesh.SOV_ReconPlane'
	End Object
	
	DefaultPhysicsAsset=PhysicsAsset'WinterWar_VH_USSR_RECON.Phy.SOV_ReconPlane_Phy'
	CrashingPhysicsAsset=PhysicsAsset'WinterWar_VH_USSR_RECON.Phy.SOV_ReconPlane_CrashingPhy'
}
