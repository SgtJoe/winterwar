
class WWVehicleWeapon_53K_Cannon extends WWVehicleWeapon_ATCannon
	abstract;

defaultproperties
{
	WeaponContentClass(0)="WinterWar.WWVehicleWeapon_53K_Cannon_ActualContent"
	
	PlayerIronSightFOV=13.0
	
	FiringStatesArray(0)=WeaponSingleFiring
	WeaponFireTypes(0)=EWFT_Projectile
	WeaponProjectiles(0)=class'WWVehicleProjectile_53K_AP'
	FireInterval(0)=+5.0
	Spread(0)=0.0001
	
	FireTriggerTags=(Cannon)
	
	VehicleClass=class'WWVehicle_53K'
	
	MainGunProjectiles(MAINGUN_AP_INDEX)=class'WWVehicleProjectile_53K_AP'
	APAmmoCount=`CANNON_AMMO_53K
	
	PenetrationDepth=22.23
	MaxPenetrationTests=3
	MaxNumPenetrations=2
}
