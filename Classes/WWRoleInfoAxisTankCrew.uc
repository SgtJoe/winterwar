
class WWRoleInfoAxisTankCrew extends WWRoleInfoAxis;

DefaultProperties
{
	RoleType=RORIT_Tank
	ClassTier=3
	ClassIndex=`RI_TANK_CREW
	
	Items[RORIGM_Default]={(
		PrimaryWeapons=(class'WWWeapon_L35',class'WWWeapon_Luger')
	)}
	
	// bAllowPistolsInRealism=true
	bIsPilot=true
	bBotSelectable=false
	
	ClassIcon=Texture2D'WinterWar_UI.RoleIcons.RoleIcon_Tank'
}
