
class WWWeapon_MN91 extends WWWeapon_MosinBase
	abstract;

defaultproperties
{
	WeaponContentClass(0)="WinterWar.WWWeapon_MN91_ActualContent"
	
	RoleSelectionImage(0)=Texture2D'WinterWar_UI.WP_Render.WP_Render_MN91'
	
	TeamIndex=`AXIS_TEAM_INDEX
	
	InvIndex=`WI_MN91
	
	WeaponProjectiles(DEFAULT_FIREMODE)=	class'WWProjectile_MN91Bullet'
	WeaponProjectiles(ALTERNATE_FIREMODE)=	class'WWProjectile_MN91Bullet'
	
	Spread(DEFAULT_FIREMODE)=	0.001458 // 5.2 MOA
	Spread(ALTERNATE_FIREMODE)=	0.001458 // 5.2 MOA

   	// Recoil
	maxRecoilPitch=650//850
	minRecoilPitch=650
	maxRecoilYaw=275
	minRecoilYaw=-275
	
	InstantHitDamageTypes(0)=class'WWDmgType_MN91Bullet'
	InstantHitDamageTypes(1)=class'WWDmgType_MN91Bullet'

	CollisionCheckLength=65.5

	BayonetAttackRange=113.0

	SwayScale=0.67
}
