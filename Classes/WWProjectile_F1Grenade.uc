
class WWProjectile_F1Grenade extends M61GrenadeProjectile;

defaultproperties
{
	MyDamageType=class'WWDmgType_F1Grenade'
	
	Begin Object Name=ThowableMeshComponent
		SkeletalMesh=SkeletalMesh'WinterWar_WP_SOV_F1_GRENADE.Mesh.SOV_F1_Grenade_3rd'
		PhysicsAsset=PhysicsAsset'WinterWar_WP_SOV_F1_GRENADE.Phy.F1_Grenade_3rd_Master_Physics'
	End Object
}
