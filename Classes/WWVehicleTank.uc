
class WWVehicleTank extends WWVehicleTransport
	abstract;

var repnotify vector TurretFlashLocation;
var	repnotify byte TurretFlashCount;
var repnotify rotator TurretWeaponRotation;
var repnotify byte TurretFiringMode;
var repnotify byte TurretCurrentPositionIndex;
var repnotify bool bDrivingTurret;

struct WeaponAnimationInfo
{
	var AnimNodeSlot AnimNode;
	var() name AnimNodeName;
	var() name FiringAnimName;
};

var WeaponAnimationInfo MainGunAnimInfo;

var AudioComponent TurretTraverseSound, TurretMotorTraverseSound, TurretElevationSound;
var AudioComponent MainCannonReloadSound, CoaxMGReloadSound;

var() float CannonFireImpulseMag;
var() float CannonFireTorqueMag;

var repnotify byte CoaxialMGReloadCount;
var repnotify byte MainGunReloadCount;

var int SeatIndexGunner, CaughtFireTimer;

replication
{
	if (bNetDirty)
		TurretFlashLocation, TurretCurrentPositionIndex,
		bDrivingTurret;
	
	if (bNetDirty)
		CoaxialMGReloadCount, MainGunReloadCount;
	
	if (SeatIndexGunner >= 0 && !IsSeatControllerReplicationViewer(SeatIndexGunner))
		TurretFlashCount, TurretWeaponRotation, TurretFiringMode;
}

simulated function PostBeginPlay()
{
	super.PostBeginPlay();
	
	SeatIndexGunner = GetGunnerSeatIndex();
}

simulated event PostInitAnimTree(SkeletalMeshComponent SkelComp)
{
	if (SkelComp == Mesh)
	{
		MainGunAnimInfo.AnimNode = AnimNodeSlot(Mesh.Animations.FindAnimNode(MainGunAnimInfo.AnimNodeName));
	}
}

simulated function InitializeTurrets()
{
	local int CannonSeatIndex;
	local ROSkelControl_TurretConstrained TraverseControl;
	local ROSkelControl_TurretConstrained ElevationControl;
	
	Super.InitializeTurrets();
	
	CannonSeatIndex = Seats.Find('TurretVarPrefix',"Turret");
	
	ElevationControl = Seats[CannonSeatIndex].TurretControllers[0];
	TraverseControl = Seats[CannonSeatIndex].TurretControllers[1];
	
	if ( ElevationControl != None )
	{
		ElevationControl.OnTurretStatusChange = OnTurretElevationStatusChange;
	}
	
	if ( TraverseControl != None )
	{
		TraverseControl.OnTurretStatusChange = OnTurretTraverseStatusChange;
	}
}

simulated event ReplicatedEvent(name VarName)
{
	local int SeatIndex;
	
	if ( VarName == 'CoaxialMGReloadCount' )
	{
		SeatIndex = GetGunnerSeatIndex();
		
		if( SeatIndex >= 0 )
		{
			if( Seats[SeatIndex].Gun != none )
			{
				Seats[SeatIndex].Gun.ClientTimeReloading();
			}
			// else
			// {
				// This function is already called from the TurretWeapon, which handles it for both client and server
				// VehicleCoaxMGReload();
			// }
		}
	}
	else if ( VarName == 'MainGunReloadCount' )
	{
		SeatIndex = GetGunnerSeatIndex();
		
		if( SeatIndex >= 0 )
		{
			VehicleLoadMainGun();
		}
	}
	else
	{
		super.ReplicatedEvent(VarName);
	}
}

function OnMainCannonReloadComplete()
{
	local ROPlayerReplicationInfo ROPRI;
	ROPRI = ROPlayerReplicationInfo(GetValidPRI());
	
	`wwdebug("main cannon reloaded", 'VH');
	
	if ( ROPRI != none )
	{
		ROGameInfo(WorldInfo.Game).BroadcastLocalizedVoiceCom(`VOICECOM_TankCannonReloaded, self, GetLoaderSeatIndex(), , ROPRI, true, GetTeamNum(), ROPRI.SquadIndex);
	}
}

static function bool IsTank()
{
	return true;
}

simulated function PositionIndexUpdated(int SeatIndex, byte NewPositionIndex)
{
	super(ROVehicle).PositionIndexUpdated(SeatIndex, NewPositionIndex);
}

function float GetMainCannonReloadDuration()
{
	if (MainCannonReloadSound.SoundCue != none)
	{
		return MainCannonReloadSound.SoundCue.GetCueDuration();
	}
	
	return 5.0;
}

function float GetCoaxMGReloadDuration()
{
	if (CoaxMGReloadSound.SoundCue != none)
	{
		return CoaxMGReloadSound.SoundCue.GetCueDuration();
	}
	
	return 7.0;
}

simulated function VehicleCoaxMGReload()
{
	`wwdebug("playing MG reload sound", 'VH');
	
	if (CoaxMGReloadSound != None && !CoaxMGReloadSound.IsPlaying())
	{
		CoaxMGReloadSound.Play();
	}
}

simulated function VehicleLoadMainGun()
{
	`wwdebug("playing main cannon reload sound", 'VH');
	
	if (MainCannonReloadSound != None && !MainCannonReloadSound.IsPlaying())
	{
		MainCannonReloadSound.Play();
	}
}

simulated function StopVehicleSounds()
{
	super.StopVehicleSounds();
	
	if (TurretTraverseSound != None)
	{
		TurretTraverseSound.Stop();
	}
	if (TurretMotorTraverseSound != None)
	{
		TurretMotorTraverseSound.Stop();
	}
	if (TurretElevationSound != None)
	{
		TurretElevationSound.Stop();
	}
	if (MainCannonReloadSound != None)
	{
		MainCannonReloadSound.Stop();
	}
	if (CoaxMGReloadSound != None)
	{
		CoaxMGReloadSound.Stop();
	}
}

simulated function OnTurretTraverseStatusChange(bool bTurretMoving, bool bHighSpeed)
{
	if ( TurretTraverseSound != None )
	{
		if ( bTurretMoving && !bHighSpeed )
		{
			TurretTraverseSound.FadeIn(0.1, 1.0);
		}
		else if ( TurretTraverseSound.IsPlaying() )
		{
			if ( TurretTraverseSound.FadeOutStartTime == 0.0 )
			{
				TurretTraverseSound.FadeOut(0.2, 0.0);
			}
		}
	}
	
	if (TurretMotorTraverseSound != None)
	{
		if ( bTurretMoving && bHighSpeed )
		{
			TurretMotorTraverseSound.FadeIn(0.1, 1.0);
		}
		else if ( TurretMotorTraverseSound.IsPlaying() )
		{
			if ( TurretMotorTraverseSound.FadeOutStartTime == 0.0 )
			{
				TurretMotorTraverseSound.FadeOut(0.2, 0.0);
			}
		}
	}
}

simulated function OnTurretElevationStatusChange(bool bTurretMoving, bool bHighSpeed)
{
	if ( TurretElevationSound != None )
	{
		if ( bTurretMoving )
		{
			TurretElevationSound.FadeIn(0.1, 1.0);
		}
		else if ( TurretElevationSound.IsPlaying() )
		{
			if ( TurretElevationSound.FadeOutStartTime == 0.0 )
			{
				TurretElevationSound.FadeOut(0.2, 0.0);
			}
		}
	}
}

simulated function bool CanEnterVehicle(Pawn P)
{
	return (ROPlayerReplicationInfo(P.PlayerReplicationInfo).RoleInfo.ClassIndex == `RI_TANK_CREW) ? super.CanEnterVehicle(P) : false;
}

simulated function VehicleWeaponFired( bool bViaReplication, vector HitLocation, int SeatIndex )
{
	local rotator SocketRotation;
	local vector SocketLocation, ImpulseDir;
	
	super.VehicleWeaponFired( bViaReplication, HitLocation, SeatIndex );
	
	if( SeatIndex == GetGunnerSeatIndex() && SeatFiringMode(SeatIndex,,true) == 0 )
	{
		if ( SeatFiringMode(SeatIndex,,true) == 0 )
		{
			if ( MainGunAnimInfo.AnimNode != None )
			{
				MainGunAnimInfo.AnimNode.PlayCustomAnim(MainGunAnimInfo.FiringAnimName, 1.0, , , , true);
			}
			
			if ( Role == ROLE_Authority )
			{
				GetBarrelLocationAndRotation(SeatIndex, SocketLocation, SocketRotation);
				ImpulseDir = -vector(SocketRotation);
				
				if ( CannonFireImpulseMag > 0 )
				{
					AddImpulse(CannonFireImpulseMag * ImpulseDir);
				}
				if ( CannonFireTorqueMag > 0 )
				{
					AddTorque(CannonFireTorqueMag * (ImpulseDir >> rot(0, 16384, 0)));
				}
			}
		}
	}
}

simulated function SpawnTurretGib()
{
	local vector SpawnLoc;
	local rotator SpawnRot;
	local ROGib DestroyedTurret;
	local int i; 
	
	if ( WorldInfo.NetMode == NM_DedicatedServer || !bHasTurretExplosion )
	{
		return;
	}
	
	Mesh.GetSocketWorldLocationAndRotation(TurretSocketName, SpawnLoc, SpawnRot);
	
	DestroyedTurret = Spawn(DestroyedTurretClass, self, , SpawnLoc, SpawnRot, , true);
	
	if (DestroyedTurret != none)
	{
		for (i = 0; i < DestroyedMatsTurret.length; i++)
		{
			DestroyedTurret.GibMeshComp.SetMaterial(i, DestroyedMatsTurret[i]);
		}
	}
}

function OneSecondLoopingTimer()
{
	local int i;
	
	super.OneSecondLoopingTimer();
	
	if (Health <= 0)
	{
		return;
	}
	
	// If we're on fire, nag everyone to GTFO
	if (CaughtFire)
	{
		if (CaughtFireTimer >= 5.0)
		{
			for (i = 0; i < Seats.Length; i++)
			{
				if (ROPlayerController(Seats[i].SeatPawn.Controller) != none && (Seats[i].SeatPawn) != none)
				{
					ROGameInfo(WorldInfo.Game).BroadcastLocalizedVoiceCom(`VOICECOM_TankBailOut, Seats[i].SeatPawn, , , ROPlayerReplicationInfo(Seats[i].SeatPawn.Controller.PlayerReplicationInfo), true, self.Team);
					break;
				}
			}
			LastCombatEventTime = WorldInfo.TimeSeconds;
			CaughtFireTimer = 0;
		}
		else
		{
			CaughtFireTimer++;
		}
	}
	// Tank is crippled, crew is ok but getting really nervous
	else if (DetrackedRight || DetrackedBoth /*HasDestroyedPart()*/)
	{
		if (WorldInfo.TimeSeconds - LastCombatEventTime > 20.0)
		{
			HandleBattleChatterEvent(`BATTLECHATTER_TankIdleVehicleHorrible);
			LastCombatEventTime = WorldInfo.TimeSeconds;
		}
	}
	// There's a bug where even if we get hit with some random bullets that don't actually do anything,
	// we're still considered "damaged". So we'll use this for "we're in battle now" random chatter
	else if (HasDamagedPart())
	{
		if (WorldInfo.TimeSeconds - LastCombatEventTime > 15.0)
		{
			HandleBattleChatterEvent(`BATTLECHATTER_TankIdleVehicleBad);
			LastCombatEventTime = WorldInfo.TimeSeconds + rand(10);
		}
	}
	// Otherwise we haven't seen any enemies yet, random idle chatter
	else
	{
		if (WorldInfo.TimeSeconds - LastCombatEventTime > 15.0)
		{
			HandleBattleChatterEvent(`BATTLECHATTER_TankIdleVehicleGood);
			LastCombatEventTime = WorldInfo.TimeSeconds + rand(10);
		}
	}
}

simulated function EngineCaughtFire()
{
	local int i;
	
	super.EngineCaughtFire();
	
	CaughtFireTimer = 0;
	
	for (i = 0; i < Seats.Length; i++)
	{
		if (ROPlayerController(Seats[i].SeatPawn.Controller) != none && (Seats[i].SeatPawn) != none)
		{
			ROGameInfo(WorldInfo.Game).BroadcastLocalizedVoiceCom(`VOICECOM_TankBailOut, Seats[i].SeatPawn, , , ROPlayerReplicationInfo(Seats[i].SeatPawn.Controller.PlayerReplicationInfo), true, self.Team);
			break;
		}
	}
}

defaultproperties
{
	MainGunAnimInfo=(AnimNodeName=MainGunAnimSlot, FiringAnimName=Shoot)
	
	bHasTurret=true
	
	CannonFireImpulseMag=25000
	CannonFireTorqueMag=12500
}
