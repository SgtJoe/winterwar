
class WWProjectile_SatchelCharge extends M61GrenadeProjectile;

defaultproperties
{
	FuseLength=7.0
	LifeSpan=7.0
	Damage=500
	DamageRadius=1000
	RadialDamageFalloffExponent=3.0
	
	Speed=700
	MinSpeed=400
	MaxSpeed=800
	MinTossSpeed=400
	MaxTossSpeed=600
	MomentumTransfer=9000
	
	ExplosionSound=AkEvent'WW_EXP_C4.Play_EXP_C4_Explosion'
	bWaitForEffects=false
	bRotationFollowsVelocity=true
	
	ProjExplosionTemplate=ParticleSystem'FX_VN_Weapons.Explosions.FX_VN_C4'
	WaterExplosionTemplate=ParticleSystem'FX_VN_Weapons.Explosions.FX_VN_C4'
	
	ShakeScale=2.5
	MaxSuppressBlurDuration=5.0
	SuppressBlurScalar=1.75
	SuppressAnimationScalar=0.65
	ExplodeExposureScale=0.40
	
	ExplosionOffsetDist=10
	
	SpottedBattleChatterIndex=`BATTLECHATTER_Satchel
	
	MyDamageType=class'RODmgType_Satchel'
	
	Begin Object Name=ThowableMeshComponent
		SkeletalMesh=SkeletalMesh'WinterWar_WP_SOV_SATCHEL.Mesh.SOV_3kg_Satchel_3rd'
		PhysicsAsset=PhysicsAsset'WinterWar_WP_SOV_SATCHEL.Phy.SOV_3kg_Satchel_3rd_Physics'
	End Object
}
