
class WWRoleInfoAxisMachineGunner extends WWRoleInfoAxis;

DefaultProperties
{
	RoleType=RORIT_MachineGunner
	ClassTier=2
	ClassIndex=`RI_MACHINE_GUNNER
	
	Items[RORIGM_Default]={(
		PrimaryWeapons=(class'WWWeapon_LahtiSaloranta'),
		
		SecondaryWeapons=(class'WWWeapon_Luger'),
		
		OtherItems=(class'WWWeapon_M32Grenade')
	)}
	
	bAllowPistolsInRealism=true
	
	ClassIcon=Texture2D'VN_UI_Textures.menu.class_icon_mg'
}
