
class WWVehicleProjectile_Smoke extends ROTankCannonSmokeRound;

defaultproperties
{
	BallisticCoefficient=2.0
	Speed=16750 //335 M/S
	MaxSpeed=16750
	
	Damage=100
	DamageRadius=0.01
	MomentumTransfer=1000
	MyDamageType=class'RODamageType_CannonShell_Smoke'
	
	ExplosionSound=AkEvent'WW_WEP_M8_Smoke.Play_EXP_M8_Ignite'
	SmokeSound=AkEvent'WW_WEP_M8_Smoke.Play_EXP_M8_Smoke_Release'
	
	ProjExplosionTemplate=ParticleSystem'FX_VN_Smoke.FX_Wep_A_Smoke_Grenade'
	ExplosionDecal=None
	
	Begin Object Name=ProjectileMesh
		StaticMesh=StaticMesh'WinterWar_VH_USSR_COMMON.Mesh.Panzer_IVG_Warhead'
		Materials(0)=MaterialInstanceConstant'WinterWar_VH_USSR_COMMON.MIC.Warhead'
		Scale=0.6
	End Object
	Components.Add(ProjectileMesh)
}
