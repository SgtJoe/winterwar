
class WWVehicle_53K_ActualContent extends WWVehicle_53K
	placeable;

DefaultProperties
{
	Begin Object Name=ROSVehicleMesh
		SkeletalMesh=SkeletalMesh'WinterWar_VH_USSR_53-K.Mesh.53-K_Rig'
		LightingChannels=(Dynamic=TRUE,Unnamed_1=TRUE,bInitialized=TRUE)
		AnimTreeTemplate=AnimTree'WinterWar_VH_USSR_53-K.Anim.53-K_AnimTree'
		PhysicsAsset=PhysicsAsset'WinterWar_VH_USSR_53-K.Phy.53-K_Rig_Physics'
		AnimSets.Add(AnimSet'WinterWar_VH_USSR_53-K.Anim.VH_53-K')
		AnimSets.Add(AnimSet'WinterWar_VH_USSR_53-K.Anim.VH_53-K_Shoot')
	End Object
	
	DestroyedSkeletalMesh=SkeletalMesh'WinterWar_VH_USSR_53-K.Mesh.53-K_Wreck'
	DestroyedPhysicsAsset=PhysicsAsset'WinterWar_VH_USSR_53-K.Phy.53-K_Wreck_Physics'
	DestroyedMats(0)=MaterialInstanceConstant'WinterWar_VH_USSR_53-K.MIC.53-K_WRECK_M'
	
	SeatProxies(0)={(
		TunicMeshType=SkeletalMesh'CHR_VN_US_Army.Mesh.US_Tunic_Long_Mesh',
		HeadGearMeshType=SkeletalMesh'CHR_VN_US_Headgear.Mesh.US_headgear_var1',
		HeadAndArmsMeshType=SkeletalMesh'CHR_VN_US_Heads.Mesh.US_Head1_Mesh',
		HeadphonesMeshType=none,
		HeadAndArmsMICTemplate=MaterialInstanceConstant'CHR_VN_US_Heads.Materials.M_US_Head_01_Long_INST',
		BodyMICTemplate=MaterialInstanceConstant'CHR_VN_US_Army.Materials.M_US_Tunic_Long_INST',
		SeatIndex=0,
		PositionIndex=0
	)}
	
	SeatProxies(1)={(
		TunicMeshType=SkeletalMesh'CHR_VN_US_Army.Mesh.US_Tunic_Long_Mesh',
		HeadGearMeshType=SkeletalMesh'CHR_VN_US_Headgear.Mesh.US_headgear_var1',
		HeadAndArmsMeshType=SkeletalMesh'CHR_VN_US_Heads.Mesh.US_Head1_Mesh',
		HeadphonesMeshType=none,
		HeadAndArmsMICTemplate=MaterialInstanceConstant'CHR_VN_US_Heads.Materials.M_US_Head_01_Long_INST',
		BodyMICTemplate=MaterialInstanceConstant'CHR_VN_US_Army.Materials.M_US_Tunic_Long_INST',
		SeatIndex=1,
		PositionIndex=0
	)}
	
	Begin Object Class=AudioComponent Name=TurretTraverseComponent
		SoundCue=SoundCue'WinterWar_VH_USSR_COMMON_SFX.Audio.Turret_Traverse_Manual_Cue'
	End Object
	TurretTraverseSound=TurretTraverseComponent
	Components.Add(TurretTraverseComponent)
	
	Begin Object Class=AudioComponent Name=TurretElevationComponent
		SoundCue=SoundCue'WinterWar_VH_USSR_COMMON_SFX.Audio.Turret_Elevate_Cue'
	End Object
	TurretElevationSound=TurretElevationComponent
	Components.Add(TurretElevationComponent)
	
	
	Begin Object Class=AudioComponent Name=MainCannonReloadSoundComponent
		SoundCue=SoundCue'WinterWar_VH_USSR_COMMON_SFX.Audio.Reload_Cannon'
	End Object
	MainCannonReloadSound=MainCannonReloadSoundComponent
	Components.Add(MainCannonReloadSoundComponent)
}
