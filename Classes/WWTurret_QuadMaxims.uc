
class WWTurret_QuadMaxims extends ROTurret_DShK_HMG
	placeable;

simulated function UnHideBulletsNotify() {}

defaultproperties
{
	OwningTeam=`ALLIES_TEAM_INDEX
	
	Begin Object Name=WeaponSkeletalMeshComponent
		SkeletalMesh=SkeletalMesh'WinterWar_VH_USSR_ZIS.Mesh.ZIS_QuadMaxims_3rd'
		AnimTreeTemplate=AnimTree'WP_VN_3rd_Master.AnimTree.DShK_HMG_3rd_Tree'
		AnimSets(0)=AnimSet'WP_VN_3rd_Master.Anim.DShK_HMG_3rd_Anims'
		PhysicsAsset=PhysicsAsset'WP_VN_3rd_Master.Phy.DShK_HMG_3rd_Master_Physics'
	End Object
	
	Begin Object Name=CollisionCylinder
		CollisionHeight=35.000000
		CollisionRadius=5.000000
		Translation=(X=32,Z=-26)
	End Object
	
	DefaultInventoryContentClass(0)="WinterWar.WWWeapon_QuadMaxims_ActualContent"
	
	TurretOffsetFromPlayerLoc=40
	
	PitchLimit=(X=-1000,Y=9999)
}
