
class WWWeapon_Molotov extends ROWeap_Molotov
	abstract;

`include(WinterWar\Classes\WWWeaponPickupMessagesOverride.uci)

defaultproperties
{
	WeaponContentClass(0)="WinterWar.WWWeapon_Molotov_ActualContent"
	
	RoleSelectionImage(0)=Texture2D'WinterWar_UI.WP_Render.WP_Render_Molotov'
	
	InvIndex=`WI_MOLOTOV_IM
	
	ThrowingBattleChatterIndex=`BATTLECHATTER_ThrowingSatchel
}
