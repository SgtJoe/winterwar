
class WWPawnHandler extends ROPawnHandler
	config(Game_WinterWar_Client);

struct RankTabs
{
	var array<StaticMesh> RankTabsByTunicType;
};

var config array<CharacterConfig> CFGA_FIN;
var config array<CharacterConfig> CFGA_SOV;

var array<TunicInfo>		FIN_Tunics;
var array<HeadgearInfo>		FIN_Headgear;
var array<HeadgearMICInfo>	FIN_HeadgearMICs;
var array<FieldgearMeshes>	FIN_FieldgearByRole;
var TunicSVInfo				FIN_TunicSV;
var	array<PlayerHeadInfo>	FIN_Heads;
var array<FaceItemInfo>		FIN_FaceItems;
var array<FacialHairInfo>	FIN_FacialHair;

var array<TunicInfo>		SOV_Tunics, SOV_TankCrew_Tunic, FIN_TankCrew_Tunic;
var array<HeadgearInfo>		SOV_Headgear, SOV_TankCrew_Headgear, FIN_TankCrew_Headgear;
var array<HeadgearMICInfo>	SOV_HeadgearMICs;
var array<FieldgearMeshes>	SOV_FieldgearByRole;
var array<RankTabs>			SOV_RankBadges;
var SkeletalMesh			SOV_SLFieldgear;
var TunicSVInfo				SOV_TunicSV, SOV_TankCrewSV;
var	array<PlayerHeadInfo>	SOV_Heads;
var array<FaceItemInfo>		SOV_FaceItems;
var array<FacialHairInfo>	SOV_FacialHair;

static function SkeletalMesh GetTunicMeshSV(int Team, int ArmyIndex, int ClassIndex, byte bPilot, bool bFlamer, out MaterialInstanceConstant TunicMatSV)
{
	if (Team == `AXIS_TEAM_INDEX)
	{
		TunicMatSV = default.FIN_TunicSV.BodyMICTemplate;
		return default.FIN_TunicSV.TunicMesh_SV;
	}
	else
	{
		if (bPilot > 0)
		{
			TunicMatSV = default.SOV_TankCrewSV.BodyMICTemplate;
			return default.SOV_TankCrewSV.TunicMesh_SV;
		}
		
		TunicMatSV = default.SOV_TunicSV.BodyMICTemplate;
		return default.SOV_TunicSV.TunicMesh_SV;
	}
}

static function SkeletalMesh GetFieldgearMesh(int Team, int ArmyIndex, int TunicMeshID, int ClassIndex, byte BodyMIC)
{
	if( Team == `AXIS_TEAM_INDEX )
	{
		/* 1.2.2 New Customization
		if (TunicMeshID == 4)
		{
			return default.FIN_FieldgearByRole[ClassIndex].FieldgearByTunicType[1];
		}*/
		if (TunicMeshID >= 2)
		{
			return default.FIN_FieldgearByRole[ClassIndex].FieldgearByTunicType[2];
		}
		else if (TunicMeshID == 1)
		{
			return default.FIN_FieldgearByRole[`RI_NOGEAR].FieldgearByTunicType[0];
		}
		
		return default.FIN_FieldgearByRole[ClassIndex].FieldgearByTunicType[0];
	}
	else
	{
		if (ArmyIndex > 0)
		{
			return default.SOV_SLFieldgear;
		}
		
		return default.SOV_FieldgearByRole[ClassIndex].FieldgearByTunicType[0];
	}
}

static function array<TunicInfo> GetTunicArray(byte TeamIndex, byte ArmyIndex, optional byte bPilot)
{
	if (TeamIndex == `AXIS_TEAM_INDEX)
	{
		if (bPilot > 0)
		{
			return default.FIN_TankCrew_Tunic;
		}
		
		return default.FIN_Tunics;
	}
	else
	{
		if (bPilot > 0)
		{
			return default.SOV_TankCrew_Tunic;
		}
		
		return default.SOV_Tunics;
	}
}

static function array<ShirtInfo> GetShirtArray(byte TeamIndex, byte ArmyIndex, optional byte bPilot)
{
	return default.USPilotShirts;
}

static function array<PlayerHeadInfo> GetHeadArray(byte TeamIndex, byte ArmyIndex, optional byte bPilot)
{
	if (TeamIndex == `AXIS_TEAM_INDEX)
	{
		return default.FIN_Heads;
	}
	else
	{
		return default.SOV_Heads;
	}
}

static function array<HeadgearInfo> GetHeadgearArray(byte TeamIndex, byte ArmyIndex, optional byte bPilot)
{
	if (TeamIndex == `AXIS_TEAM_INDEX)
	{
		if (bPilot > 0)
		{
			return default.FIN_TankCrew_Headgear;
		}
		
		return default.FIN_Headgear;
	}
	else
	{
		if (bPilot > 0)
		{
			return default.SOV_TankCrew_Headgear;
		}
		
		return default.SOV_Headgear;
	}
}

static function array<HeadgearMICInfo> GetHeadgearMICArray(byte TeamIndex, byte ArmyIndex, optional byte bPilot)
{
	if (TeamIndex == `AXIS_TEAM_INDEX)
	{
		return default.FIN_HeadgearMICs;
	}
	else
	{
		return default.SOV_HeadgearMICs;
	}
}

static function array<FaceItemInfo> GetFaceItemArray(byte TeamIndex, byte ArmyIndex, optional byte bPilot)
{
	local array<FaceItemInfo> TankerItems;
	
	if (TeamIndex == `AXIS_TEAM_INDEX)
	{
		return default.FIN_FaceItems;
	}
	else
	{
		if (bPilot > 0)
		{
			TankerItems.AddItem(default.SOV_FaceItems[0]);
			return TankerItems;
		}
		
		return default.SOV_FaceItems;
	}
}

static function array<FacialHairInfo> GetFacialHairArray(byte TeamIndex, byte ArmyIndex)
{
	if (TeamIndex == `AXIS_TEAM_INDEX)
	{
		return default.FIN_FacialHair;
	}
	else
	{
		return default.SOV_FacialHair;
	}
}

static function array<TattooInfo> GetTattooArray(byte TeamIndex, byte ArmyIndex, optional byte bPilot)
{
	return default.USPilotTattoos;
}

static function SkeletalMesh GetFaceItemMesh(int Team, int ArmyIndex, byte bPilot, byte HeadgearID, out byte FaceItemID, out name SocketName, out byte bHideFacialHair)
{
	local array<FaceItemInfo> FaceItems;
	
	FaceItems = GetFaceItemArray(Team, ArmyIndex, bPilot);
	SocketName = FaceItems[FaceItemID].FaceItemSocket;
	bHideFacialHair = 0;
	
	return FaceItems[FaceItemID].FaceItemMesh;
}

static function SkeletalMesh GetFPArmsMesh(int Team, int ArmyIndex, byte bPilot, byte TunicMeshID, byte TunicMaterialID, byte SkinToneID, out MaterialInstanceConstant SkinMIC, out MaterialInstanceConstant SleeveMIC)
{
	if (Team == `ALLIES_TEAM_INDEX && bPilot > 0)
	{
		SkinMIC = default.FPSkinToneMICs[0];
		
		SleeveMIC = default.SOV_TankCrew_Tunic[0].BodyMICS[TunicMaterialID].SleeveMICFP;
		
		return default.SOV_TankCrew_Tunic[0].ArmsMeshFP;
	}
	else
	{
		return super.GetFPArmsMesh(Team, ArmyIndex, bPilot, TunicMeshID, TunicMaterialID, SkinToneID, SkinMIC, SleeveMIC);
	}
}

static function SkeletalMesh GetHeadgearMesh(int Team, int ArmyIndex, byte bPilot, byte HeadID, byte HairID, byte HeadgearID, byte HeadgearMatID, out MaterialInstanceConstant HeadgearMIC, out MaterialInstanceConstant HairMIC, out name SocketName, out byte bIsHelmet)
{
	local array<HeadgearInfo> Headgear;
	local array<HeadgearMICInfo> HeadgearMICs;
	
	Headgear = GetHeadgearArray(Team, ArmyIndex, bPilot);
	HeadgearMICs = GetHeadgearMICArray(Team, ArmyIndex, bPilot);
	
	SocketName = Headgear[HeadgearID].HeadgearSocket;
	bIsHelmet = Headgear[HeadgearID].bIsHelmet;
	
	HeadgearMIC = HeadgearMICs[Headgear[HeadgearID].HeadgearMICs[HeadgearMatID]].HeadgearMICTemplate;
	
	if (Team == `AXIS_TEAM_INDEX)
	{
		HairMIC = default.HairMICs[default.FIN_Heads[HeadID].HairColours].HairMIC;
	}
	else
	{
		HairMIC = default.HairMICs[default.SOV_Heads[HeadID].HairColours].HairMIC;
	}
	return Headgear[HeadgearID].HeadgearMeshes[0];
}

static function SkeletalMesh GetGoreMeshes(int TeamNum, int ArmyIndex, byte TunicID, byte SkinToneID, out MaterialInstanceConstant GoreMIC, out class<ROGib> LeftHandGibClass, out class<ROGib> RightHandGibClass, out class<ROGib> LeftLegGibClass, out class<ROGib> RightLegGibClass)
{
	LeftHandGibClass=class'ROGib_HumanArm_Gore_BareArm';
	RightHandGibClass=class'ROGib_HumanArm_Gore_BareArm';
	LeftLegGibClass=class'ROGib_HumanLeg_Gore_BareLeg';
	RightLegGibClass=class'ROGib_HumanLeg_Gore_BareLeg';
	GoreMIC = default.GoreSkinToneMICs[SkinToneID];
	
	return SkeletalMesh'CHR_VN_Gore.Mesh.Gore_Main_Mesh';
}

static function StaticMesh GetBadgeMesh(int TeamNum, int TunicID, int ClassIndex, int isSL)
{
	/* 1.2.2 New Customization
	if (TeamNum == `ALLIES_TEAM_INDEX && ClassIndex != `RI_TANK_CREW)
	{
		if (ClassIndex == `RI_COMMANDER)
		{
			return default.SOV_RankBadges[2].RankTabsByTunicType[TunicID];
		}
		else if (isSL > 0)
		{
			return default.SOV_RankBadges[1].RankTabsByTunicType[TunicID];
		}
		else
		{
			return default.SOV_RankBadges[0].RankTabsByTunicType[TunicID];
		}
	}
	*/
	return none;
}

static function GetCharConfig(int Team, int ArmyIndex, byte bPilot, int ClassIndex, int HonorLevel, out byte TunicID, out byte TunicMaterialID, out byte ShirtID, out byte HeadID, out byte HairID, out byte HeadgearID, out byte HeadgearMatID, out byte FaceItemID, out byte FacialHairID, out byte TattooID, ROPlayerReplicationInfo ROPRI, optional bool bRandomiseAll, optional bool bInitByMenu, optional out byte bUseBase)
{
	if (bRandomiseAll)
	{
		TunicID			= default.RandomConfig.TunicMesh;
		TunicMaterialID	= default.RandomConfig.TunicMaterial;
		ShirtID			= default.RandomConfig.ShirtTexture;
		HeadID			= default.RandomConfig.HeadMesh;
		HairID			= default.RandomConfig.HairMaterial;
		HeadgearID		= default.RandomConfig.HeadgearMesh;
		HeadgearMatID	= default.RandomConfig.HeadgearMaterial;
		FaceItemID		= default.RandomConfig.FaceItemMesh;
		FacialHairID	= default.RandomConfig.FacialHairMesh;
		TattooID		= default.RandomConfig.TattooTex;
	}
	else if (Team == `AXIS_TEAM_INDEX)
	{
		TunicID			= default.CFGA_FIN[ClassIndex].TunicMesh;
		TunicMaterialID	= default.CFGA_FIN[ClassIndex].TunicMaterial;
		ShirtID			= default.CFGA_FIN[ClassIndex].ShirtTexture;
		HeadID			= default.CFGA_FIN[ClassIndex].HeadMesh;
		HairID			= default.CFGA_FIN[ClassIndex].HairMaterial;
		HeadgearID		= default.CFGA_FIN[ClassIndex].HeadgearMesh;
		HeadgearMatID	= default.CFGA_FIN[ClassIndex].HeadgearMaterial;
		FaceItemID		= default.CFGA_FIN[ClassIndex].FaceItemMesh;
		FacialHairID	= default.CFGA_FIN[ClassIndex].FacialHairMesh;
		TattooID		= default.CFGA_FIN[ClassIndex].TattooTex;
	}
	else
	{
		TunicID			= default.CFGA_SOV[ClassIndex].TunicMesh;
		TunicMaterialID	= default.CFGA_SOV[ClassIndex].TunicMaterial;
		ShirtID			= default.CFGA_SOV[ClassIndex].ShirtTexture;
		HeadID			= default.CFGA_SOV[ClassIndex].HeadMesh;
		HairID			= default.CFGA_SOV[ClassIndex].HairMaterial;
		HeadgearID		= default.CFGA_SOV[ClassIndex].HeadgearMesh;
		HeadgearMatID	= default.CFGA_SOV[ClassIndex].HeadgearMaterial;
		FaceItemID		= default.CFGA_SOV[ClassIndex].FaceItemMesh;
		FacialHairID	= default.CFGA_SOV[ClassIndex].FacialHairMesh;
		TattooID		= default.CFGA_SOV[ClassIndex].TattooTex;
	}
	
	if( !bInitByMenu )
		ValidateCharConfig(Team, ArmyIndex, bPilot, HonorLevel, TunicID, TunicMaterialID, ShirtID, HeadID, HairID, HeadgearID, HeadgearMatID, FaceItemID, FacialHairID, TattooID, ROPRI);
}

static function SaveCharConfig(int Team, int ArmyIndex, byte bPilot, int ClassIndex, int HonorLevel, out byte TunicID, out byte TunicMaterialID, out byte ShirtID, out byte HeadID, out byte HairID, out byte HeadgearID, out byte HeadgearMatID, out byte FaceItemID, out byte FacialHairID, out byte TattooID, byte bUseBase)
{
	if (default.CFGA_FIN.length < `RI_TANK_CREW)
	{
		default.CFGA_FIN.length = `RI_TANK_CREW + 1;
	}
	
	if (default.CFGA_SOV.length < `RI_TANK_CREW)
	{
		default.CFGA_SOV.length = `RI_TANK_CREW + 1;
	}
	
	StaticSaveConfig();
	
	if (Team == `AXIS_TEAM_INDEX)
	{
		default.CFGA_FIN[ClassIndex].TunicMesh			= TunicID;
		default.CFGA_FIN[ClassIndex].TunicMaterial		= TunicMaterialID;
		default.CFGA_FIN[ClassIndex].ShirtTexture		= ShirtID;
		default.CFGA_FIN[ClassIndex].HeadMesh			= HeadID;
		default.CFGA_FIN[ClassIndex].HairMaterial		= HairID;
		default.CFGA_FIN[ClassIndex].HeadgearMesh		= HeadgearID;
		default.CFGA_FIN[ClassIndex].HeadgearMaterial	= HeadgearMatID;
		default.CFGA_FIN[ClassIndex].FaceItemMesh		= FaceItemID;
		default.CFGA_FIN[ClassIndex].FacialHairMesh		= FacialHairID;
		default.CFGA_FIN[ClassIndex].TattooTex			= TattooID;
	}
	else
	{
		default.CFGA_SOV[ClassIndex].TunicMesh			= TunicID;
		default.CFGA_SOV[ClassIndex].TunicMaterial		= TunicMaterialID;
		default.CFGA_SOV[ClassIndex].ShirtTexture		= ShirtID;
		default.CFGA_SOV[ClassIndex].HeadMesh			= HeadID;
		default.CFGA_SOV[ClassIndex].HairMaterial		= HairID;
		default.CFGA_SOV[ClassIndex].HeadgearMesh		= HeadgearID;
		default.CFGA_SOV[ClassIndex].HeadgearMaterial	= HeadgearMatID;
		default.CFGA_SOV[ClassIndex].FaceItemMesh		= FaceItemID;
		default.CFGA_SOV[ClassIndex].FacialHairMesh		= FacialHairID;
		default.CFGA_SOV[ClassIndex].TattooTex			= TattooID;
	}
	
	StaticSaveConfig();
}

static function bool CopyConfigToClass(int Team, int ArmyIndex, int SourceClassIndex, int TargetClassIndex)
{
	local int i;
	
	if (Team == `AXIS_TEAM_INDEX)
	{
		for (i = 0; i < default.CFGA_FIN.length; i++)
		{
			if (i != `RI_TANK_CREW)
			{
				default.CFGA_FIN[i].TunicMesh			= default.CFGA_FIN[SourceClassIndex].TunicMesh;
				default.CFGA_FIN[i].TunicMaterial		= default.CFGA_FIN[SourceClassIndex].TunicMaterial;
				default.CFGA_FIN[i].ShirtTexture		= default.CFGA_FIN[SourceClassIndex].ShirtTexture;
				default.CFGA_FIN[i].HeadMesh			= default.CFGA_FIN[SourceClassIndex].HeadMesh;
				default.CFGA_FIN[i].HairMaterial		= default.CFGA_FIN[SourceClassIndex].HairMaterial;
				default.CFGA_FIN[i].HeadgearMesh		= default.CFGA_FIN[SourceClassIndex].HeadgearMesh;
				default.CFGA_FIN[i].HeadgearMaterial	= default.CFGA_FIN[SourceClassIndex].HeadgearMaterial;
				default.CFGA_FIN[i].FaceItemMesh		= default.CFGA_FIN[SourceClassIndex].FaceItemMesh;
				default.CFGA_FIN[i].FacialHairMesh		= default.CFGA_FIN[SourceClassIndex].FacialHairMesh;
				default.CFGA_FIN[i].TattooTex			= default.CFGA_FIN[SourceClassIndex].TattooTex;
			}
		}
	}
	else
	{
		for (i = 0; i < default.CFGA_SOV.length; i++)
		{
			if (i != `RI_TANK_CREW)
			{
				default.CFGA_SOV[i].TunicMesh			= default.CFGA_SOV[SourceClassIndex].TunicMesh;
				default.CFGA_SOV[i].TunicMaterial		= default.CFGA_SOV[SourceClassIndex].TunicMaterial;
				default.CFGA_SOV[i].ShirtTexture		= default.CFGA_SOV[SourceClassIndex].ShirtTexture;
				default.CFGA_SOV[i].HeadMesh			= default.CFGA_SOV[SourceClassIndex].HeadMesh;
				default.CFGA_SOV[i].HairMaterial		= default.CFGA_SOV[SourceClassIndex].HairMaterial;
				default.CFGA_SOV[i].HeadgearMesh		= default.CFGA_SOV[SourceClassIndex].HeadgearMesh;
				default.CFGA_SOV[i].HeadgearMaterial	= default.CFGA_SOV[SourceClassIndex].HeadgearMaterial;
				default.CFGA_SOV[i].FaceItemMesh		= default.CFGA_SOV[SourceClassIndex].FaceItemMesh;
				default.CFGA_SOV[i].FacialHairMesh		= default.CFGA_SOV[SourceClassIndex].FacialHairMesh;
				default.CFGA_SOV[i].TattooTex			= default.CFGA_SOV[SourceClassIndex].TattooTex;
			}
		}
	}
	
	StaticSaveConfig();
	
	return true;
}

defaultproperties
{
	`include(WinterWar\Classes\WWPawnHandler_Axis.uci)
	`include(WinterWar\Classes\WWPawnHandler_Allies.uci)
}
