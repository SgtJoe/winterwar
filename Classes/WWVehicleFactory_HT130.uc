
class WWVehicleFactory_HT130 extends WWVehicleFactory;

defaultproperties
{
	Begin Object Name=SVehicleMesh
		SkeletalMesh=SkeletalMesh'WinterWar_VH_USSR_T-26.Mesh.T-26_PHAT'
	End Object
	
	Begin Object Name=CollisionCylinder
		CollisionHeight=60.0
		CollisionRadius=120.0
		Translation=(X=0.0,Y=0.0,Z=55.0)
		bAlwaysRenderIfSelected=true
	End Object
	
	Begin Object Name=Sprite
		Sprite=Texture2D'VN_UI_Textures.HUD.DeathMessage.UI_Kill_Icon_Fire'
		Translation=(X=0,Y=0,Z=150.0)
		Scale=0.5
	End Object
	
	VehicleClass=class'WWVehicle_HT130_ActualContent'
}
