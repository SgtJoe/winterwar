
class WWVehicleWeapon_T28_MG_Left extends WWVehicleWeapon_T28_MG
	abstract;

function BeginReload()
{
	local WWVehicle_T28 T;
	T = WWVehicle_T28(MyVehicle);
	
	TimeReloading();
	
	if (T != none)
	{
		if (T.LeftMGReloadCount < 255)
		{
			T.LeftMGReloadCount++;
		}
		else
		{
			T.LeftMGReloadCount = 0;
		}
	}
}

defaultproperties
{
	WeaponContentClass(0)="WinterWar.WWVehicleWeapon_T28_MG_Left_ActualContent"
	
	SeatIndex=`T28_MG_H_L
	
	FireTriggerTags=(T28_LeftMG)
}
