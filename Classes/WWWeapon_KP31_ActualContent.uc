
class WWWeapon_KP31_ActualContent extends WWWeapon_KP31;

simulated function SetupArmsAnim()
{
	super.SetupArmsAnim();
	
	// ArmsMesh.AnimSets has slots 0-2-3 filled, so we need to back fill slot 1 and then move to slot 4
	ROPawn(Instigator).ArmsMesh.AnimSets[1] = SkeletalMeshComponent(Mesh).AnimSets[0];
	ROPawn(Instigator).ArmsMesh.AnimSets[4] = SkeletalMeshComponent(Mesh).AnimSets[1];
}

DefaultProperties
{
	ArmsAnimSet=AnimSet'WP_VN_VC_ppsh.animation.WP_PpshHands'
	
	Begin Object Name=FirstPersonMesh
		DepthPriorityGroup=SDPG_Foreground
		SkeletalMesh=SkeletalMesh'WinterWar_WP_FIN_KP31.Mesh.FIN_KP-31'
		PhysicsAsset=PhysicsAsset'WP_VN_VC_ppsh.Phys.Sov_ppsh_Physics'
		AnimSets(0)=AnimSet'WP_VN_VC_ppsh.animation.WP_PpshHands'
		AnimSets(1)=AnimSet'WinterWar_WP_FIN_KP31.Anim.KPHands'
		AnimTreeTemplate=AnimTree'WP_VN_VC_ppsh.animation.Sov_PPSH41_Tree'
		Scale=1.0
		FOV=70
	End Object
	
	Begin Object Name=PickupMesh
		SkeletalMesh=SkeletalMesh'WinterWar_WP_FIN_KP31.Mesh.FIN_KP-31_3rd'
		PhysicsAsset=PhysicsAsset'WP_VN_3rd_Master.Phy.PPSH_3rd_Master_Physics'
		CollideActors=true
		BlockActors=true
		BlockZeroExtent=true
		BlockNonZeroExtent=true//false
		BlockRigidBody=true
		bHasPhysicsAssetInstance=false
		bUpdateKinematicBonesFromAnimation=false
		PhysicsWeight=1.0
		RBChannel=RBCC_GameplayPhysics
		RBCollideWithChannels=(Default=TRUE,GameplayPhysics=TRUE,EffectPhysics=TRUE)
		bSkipAllUpdateWhenPhysicsAsleep=TRUE
		bSyncActorLocationToRootRigidBody=true
	End Object
	
	AttachmentClass=class'WWWeapon_KP31_Attach'
}
