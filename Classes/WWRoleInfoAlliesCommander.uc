
class WWRoleInfoAlliesCommander extends WWRoleInfoAllies;

DefaultProperties
{
	RoleType=RORIT_Commander
	ClassTier=4
	ClassIndex=`RI_COMMANDER
	
	Items[RORIGM_Default]={(
		PrimaryWeapons=(class'WWWeapon_SVT38',class'WWWeapon_PPD34'),
		
		SecondaryWeapons=(class'WWWeapon_TT33',class'WWWeapon_NagantRevolver'),
		
		OtherItems=(class'WWWeapon_Binoculars',class'WWWeapon_RDG1')
	)}
	
	bAllowPistolsInRealism=true
	bIsTeamLeader=true
	
	ClassIcon=Texture2D'VN_UI_Textures.menu.class_icon_commander'
}
