
class WWVehicle_Skis extends ROVehicleTransport
	abstract;

var SkelControlSingleBone LeftPoleSkelControl, RightPoleSkelControl, BodySkelControl;

var repnotify bool Poles1, Poles2;

// Constant replication is possibly performance-intensive, but can't find any other way to trigger pole effects non-locally
replication
{
	if (bNetDirty)
		Poles1, Poles2;
}

simulated event ReplicatedEvent(name VarName)
{
	if (VarName == 'Poles1' && Poles1 == true)
	{
		// `wwdebug("Poles1",'S');
		LeftPoleSkelControl.SetSkelControlActive(true);
		RightPoleSkelControl.SetSkelControlActive(true);
		BodySkelControl.SetSkelControlActive(true);
	}
	else if (VarName == 'Poles2' && Poles2 == true)
	{
		// `wwdebug("Poles2",'S');
		LeftPoleSkelControl.SetSkelControlActive(false);
		RightPoleSkelControl.SetSkelControlActive(false);
		BodySkelControl.SetSkelControlActive(false);
	}
	else
	{
		super.ReplicatedEvent(VarName);
	}
}

simulated event PostInitAnimTree(SkeletalMeshComponent SkelComp)
{
	super.PostInitAnimTree(SkelComp);
	
	LeftPoleSkelControl =	SkelControlSingleBone(Mesh.FindSkelControl('LeftPole'));
	RightPoleSkelControl =	SkelControlSingleBone(Mesh.FindSkelControl('RightPole'));
	BodySkelControl =		SkelControlSingleBone(Mesh.FindSkelControl('Body'));
	
	if (!IsTimerActive('DestroyIfEmpty'))
	{
		SetTimer(0.2, true, 'DestroyIfEmpty');
	}
}

simulated event Tick(float DeltaTime)
{
	// Don't attempt to set a bunch of effects that don't exist
	super(ROVehicle).Tick(DeltaTime);
}

simulated event TakeDamage(int Damage, Controller EventInstigator, vector HitLocation, vector Momentum, class<DamageType> DamageType, optional TraceHitInfo HitInfo, optional Actor DamageCauser)
{
	local int HitIndex;
	local WWPawn Victim;
	
	super.TakeDamage(Damage, EventInstigator, HitLocation, Momentum, DamageType, HitInfo, DamageCauser);
	
	if (HitInfo.BoneName != '')
	{
		HitIndex = VehHitZones.Find('ZoneName', HitInfo.BoneName);
		
		Victim = WWPawn(Seats[0].StoragePawn);
		
		`wwdebug(Victim @ "taking" @ Damage @ DamageType @ "in" @ HitInfo.BoneName @ "Zone" @ HitIndex, 'S');
		
		Seats[0].SeatPawn.DriverLeave(true);
		
		Victim.ShotOffSkisInstigator = EventInstigator;
		Victim.ShotOffSkisMomentum = Momentum + Velocity;
		Victim.ShotOffSkisDamageType = DamageType;
		Victim.ShotOffSkisDamageCauser = DamageCauser;
		
		Victim.SetTimer(0.1, false, 'ShotOffSkis');
		
		if (!IsTimerActive('DestroyIfEmpty'))
		{
			SetTimer(0.2, true, 'DestroyIfEmpty');
		}
		
		// if (HitIndex >= 0)
		// TODO: Is it worth setting up more complex head/arms/legs modeling?
	}
}

function DriverLeft()
{
	super.DriverLeft();
	
	if (!IsTimerActive('DestroyIfEmpty'))
	{
		SetTimer(0.2, true, 'DestroyIfEmpty');
	}
}

simulated function DetachDriver(Pawn P)
{
	if (ROPawn(P) != none)
	{
		ROSkeletalMeshComponent(P.Mesh).ReplaceSkeletalMesh(ROPawn(P).CompositedBodyMesh);
		`wwdebug("Pawn Mesh:" @ P.Mesh.SkeletalMesh, 'P');
	}
	
	super.DetachDriver(P);
}

simulated function SitDriver(ROPawn ROP, int SeatIndex)
{
	local ROPlayerController ROPC;
	local Pawn LocalPawn;
	local SkeletalMesh TempMesh;
	
	if( Seats[SeatIndex].SeatPawn != none && Seats[SeatIndex].SeatPawn.Driver != none )
	{
		ROPC = ROPlayerController(Seats[SeatIndex].SeatPawn.Driver.Controller);
	}
	
	if( ROPC == none && Seats[SeatIndex].SeatPawn != none )
	{
		ROPC = ROPlayerController(Seats[SeatIndex].SeatPawn.Controller);
	}
	
	if( ROPC == none )
	{
		if( ROP.DrivenVehicle.Controller != none && ROP.DrivenVehicle.Controller == GetALocalPlayerController() )
		{
			ROPC = ROPlayerController(ROP.DrivenVehicle.Controller);
		}
	}
	
	if( ROPC == none && SeatIndex == 0 )
	{
		if( GetALocalPlayerController() != none && GetALocalPlayerController().Pawn == self )
		{
			ROPC = ROPlayerController(GetALocalPlayerController());
		}
	}
	
	if( ROPC == none  )
	{
		LocalPawn = GetALocalPlayerController().Pawn;
		
		if( GetALocalPlayerController() != none && LocalPawn == Seats[SeatIndex].SeatPawn )
		{
			ROPC = ROPlayerController(GetALocalPlayerController());
		}
	}
	
	if( WorldInfo.NetMode != NM_DedicatedServer )
	{
		if ( ROPC != None && LocalPlayer(ROPC.Player) != none && (WorldInfo.NetMode == NM_Standalone || IsLocalPlayerInThisVehicle()) )
		{
			if (ROP != none && WWPawn(ROP).TunicID >= 3)
			{
				`wwdebug("taking off hood while on skis", 'P');
				
				TempMesh = SkeletalMesh'WinterWar_CHR_FIN.Mesh.FIN_SV2';
				
				TempMesh.Characterization = ROP.PlayerHIKCharacterization;
				
				ROSkeletalMeshComponent(ROP.Mesh).ReplaceSkeletalMesh(TempMesh);
				
				ROP.Mesh.SetAnimTreeTemplate(PassengerAnimTree);
				ROP.UpdateVehicleIK(self, 0, 0);
			}
		}
	}
	
	super.SitDriver(ROP, SeatIndex);
	
	if (!IsTimerActive('DestroyIfEmpty'))
	{
		SetTimer(0.2, true, 'DestroyIfEmpty');
	}
}

function DestroyIfEmpty()
{
	// if (bPlayerTransitioning)
	// {
		// return;
	// }
	
	if(Seats[0].SeatPawn != None && Seats[0].SeatPawn.Controller != None && Seats[0].SeatPawn.IsHumanControlled())
	{
		// `wwdebug("Keeping",'S');
		return;
	}
	
	// `wwdebug("DESTROYED",'S');
	Destroy();
}

simulated function SetInputs(float InForward, float InStrafe, float InUp)
{
	if (InForward < 0)
	{
		InForward = 0;
	}
	
	super.SetInputs(InForward, InStrafe, InUp);
}

simulated function EngineStatusUpdated()
{
	if (!IsTimerActive('PushPoles1'))
	{
		// `wwdebug("",'S');
		SetTimer(2.5, true, 'PushPoles1');
	}
	
	super.EngineStatusUpdated();
	
	if (!IsTimerActive('DestroyIfEmpty'))
	{
		SetTimer(0.2, true, 'DestroyIfEmpty');
	}
}

simulated function PushPoles1()
{
	if (Throttle > 0)
	{
		// `wwdebug("Push",'S');
		
		if (WorldInfo.NetMode == NM_Standalone)
		{
			LeftPoleSkelControl.SetSkelControlActive(true);
			RightPoleSkelControl.SetSkelControlActive(true);
			BodySkelControl.SetSkelControlActive(true);
		}
		else
		{
			Poles1 = true;
			Poles2 = false;
		}
		
		// If we're going max speed then push less often
		// if (OutputGear == ROVehicleSimTreaded(SimObj).GearArray.length - 1)
		// {
			// if (!IsTimerActive('PushPoles2'))
			// {
				// SetTimer(4.6, false, 'PushPoles2');
			// }
		// }
		// else
		// {
			SetTimer(1.9, false, 'PushPoles2');
		// }
	}
}

simulated function PushPoles2()
{
	// `wwdebug("Reset",'S');
	
	if (WorldInfo.NetMode == NM_Standalone)
	{
		LeftPoleSkelControl.SetSkelControlActive(false);
		RightPoleSkelControl.SetSkelControlActive(false);
		BodySkelControl.SetSkelControlActive(false);
	}
	else
	{
		Poles1 = false;
		Poles2 = true;
	}
}

simulated function LeaveBloodSplats(int InSeatIndex) {}

simulated exec function SwitchFireMode() {}

DefaultProperties
{
	Health=100
	
	Team=`AXIS_TEAM_INDEX
	
	bOpenVehicle=true
	bInfantryCanUse=true
	
	ExitRadius=80
	
	Begin Object Name=CollisionCylinder
		CollisionHeight=60.0
		CollisionRadius=60.0
		Translation=(X=0.0,Y=0.0,Z=0.0)
	End Object
	CylinderComponent=CollisionCylinder
	
	bDontUseCollCylinderForRelevancyCheck=true
	RelevancyHeight=70.0
	RelevancyRadius=130.0
	
	DrivingPhysicalMaterial=PhysicalMaterial'VH_VN_ARVN_M113_APC.Phys.M113_PhysMat_Moving'
	DefaultPhysicalMaterial=PhysicalMaterial'VH_VN_ARVN_M113_APC.Phys.M113_PhysMat'
	
	Seats(0)={(
		CameraOffset=-420,
		SeatAnimBlendName=DriverPositionNode,
		SeatPositions=(
			(
				bDriverVisible=true,
				bAllowFocus=true,
				ViewFOV=70.0,
				PositionUpAnim=Driver_idle,
				PositionIdleAnim=Driver_idle,
				DriverIdleAnim=Driver_idle,
				AlternateIdleAnim=Driver_idle,
				PositionFlinchAnims=(Driver_idle),
				PositionDeathAnims=(Driver_idle),
				LeftHandIKInfo=	(IKEnabled=true,DefaultEffectorLocationTargetName=IK_LeftHand,	DefaultEffectorRotationTargetName=IK_LeftHand),
				RightHandIKInfo=(IKEnabled=true,DefaultEffectorLocationTargetName=IK_RightHand,	DefaultEffectorRotationTargetName=IK_RightHand),
				LeftFootIKInfo=	(IKEnabled=true,DefaultEffectorLocationTargetName=IK_LeftFoot,	DefaultEffectorRotationTargetName=IK_LeftFoot),
				RightFootIKInfo=(IKEnabled=true,DefaultEffectorLocationTargetName=IK_RightFoot,	DefaultEffectorRotationTargetName=IK_RightFoot),
				HipsIKInfo=		(PinEnabled=false),
				LookAtInfo=		(LookAtEnabled=true,DefaultLookAtTargetName=IK_LookAt,HeadInfluence=1.0,BodyInfluence=0.0),
				SeatProxyIndex=0,
				bIsExterior=true
			)
		),
		bSeatVisible=true,
		SeatBone=Root_Driver,
		DriverDamageMult=1.0,
		InitialPositionIndex=0
	)}
	
	PassengerAnimTree=AnimTree'CHR_Playeranimtree_Master.CHR_Tanker_animtree'
	CrewAnimSet=AnimSet'VH_VN_ARVN_M113_APC.Anim.CHR_M113_Anim_Master'
	
	LeftWheels(0)="L3_Wheel"
	LeftWheels(1)="L5_Wheel"
	
	RightWheels(0)="R3_Wheel"
	RightWheels(1)="R5_Wheel"
	
	Begin Object Name=RRWheel
		BoneName="R_Wheel_05"
		BoneOffset=(X=-10.0,Y=0,Z=0.0)
		WheelRadius=20
	End Object
	
	Begin Object Name=RMWheel
		BoneName="R_Wheel_04"
		BoneOffset=(X=0.0,Y=0,Z=0.0)
		WheelRadius=20
	End Object
	
	Begin Object Name=RFWheel
		BoneName="R_Wheel_03"
		BoneOffset=(X=0.0,Y=0,Z=0.0)
		WheelRadius=20
	End Object
	
	Begin Object Name=LRWheel
		BoneName="L_Wheel_05"
		BoneOffset=(X=0.0,Y=0,Z=0.0)
		WheelRadius=20
	End Object
	
	Begin Object Name=LMWheel
		BoneName="L_Wheel_04"
		BoneOffset=(X=0.0,Y=0,Z=0.0)
		WheelRadius=20
	End Object
	
	Begin Object Name=LFWheel
		BoneName="L_Wheel_03"
		BoneOffset=(X=0.0,Y=0,Z=0.0)
		WheelRadius=20
	End Object
	
	Begin Object Name=SimObject
		WheelSuspensionStiffness=325
		WheelSuspensionDamping=25.0
		GearArray(0)={()}
		GearArray(1)={(
			// Real world - [4.22] ~10.0 kph
			GearRatio=2.1,
			AccelRate=15,
			TorqueCurve=(Points={(
				(InVal=0,OutVal=2500),
				(InVal=300,OutVal=1800),
				(InVal=2800,OutVal=4000),
				(InVal=3000,OutVal=1000),
				(InVal=3200,OutVal=0.0)
				)}),
			TurningThrottle=0.3
			)}
		GearArray(2)={(
			// Real world - [2.1] ~20.0 kph
			GearRatio=2.1,
			AccelRate=12,
			TorqueCurve=(Points={(
				(InVal=0,OutVal=3000),
				(InVal=2800,OutVal=6200),
				(InVal=3000,OutVal=2000),
				(InVal=3200,OutVal=0.0)
				)}),
			TurningThrottle=0.3
			)}
		GearArray(3)={(
			// Real world - [1.4] ~30.0 kph
			GearRatio=1.4,
			AccelRate=12,
			TorqueCurve=(Points={(
				(InVal=0,OutVal=3500),
				(InVal=2800,OutVal=9000),
				(InVal=3000,OutVal=3500),
				(InVal=3200,OutVal=0.0)
				)}),
			TurningThrottle=0.3
			)}
		GearArray(4)={(
			// Real world - [1.05] ~40.0 kph
			GearRatio=1.2,
			AccelRate=12,
			TorqueCurve=(Points={(
				(InVal=0,OutVal=5000),
				(InVal=2800,OutVal=11700),
				(InVal=3000,OutVal=6000),
				(InVal=3200,OutVal=0.0)
				)}),
			TurningThrottle=0.25
			)}
		FirstForwardGear=1
		AppliedTurnBrakingRate=3.0
		AppliedTurnBrakingFactor=0.0
		TurningLongSlipFactor=150
		bTurnInPlaceOnSteer=false
		SteepHillTopGear=1
		GearShiftSlopeThreshold=0.15
		GearShiftDownSlopeThreshold=0.2
	End Object
	
	GroundSpeed=723
	MaxSpeed=723
	
	VehHitZones(0)=(ZoneName=TORSO,DamageMultiplier=1.0,VehicleHitZoneType=VHT_CrewBody)
	VehHitZones(1)=(ZoneName=LEGS,DamageMultiplier=1.0,VehicleHitZoneType=VHT_CrewBody)
	VehHitZones(2)=(ZoneName=HED,DamageMultiplier=1.0,VehicleHitZoneType=VHT_CrewHead)
	
	SeatTextureOffsets(0)=(PositionOffSet=(X=0,Y=0,Z=0),bTurretPosition=0)
	
	RanOverDamageType=WWDmgType_RoadRage
}
