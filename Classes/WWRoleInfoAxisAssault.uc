
class WWRoleInfoAxisAssault extends WWRoleInfoAxis;

DefaultProperties
{
	RoleType=RORIT_Scout
	ClassTier=2
	ClassIndex=`RI_ASSAULT
	
	Items[RORIGM_Default]={(
		PrimaryWeapons=(class'WWWeapon_KP31',class'WWWeapon_M20'),
		
		OtherItems=(class'WWWeapon_M32Grenade',class'WWWeapon_RDG1'),
		
		SquadLeaderItems=(class'WWWeapon_Binoculars')
	)}
	
	ClassIcon=Texture2D'WinterWar_UI.RoleIcons.RoleIcon_Assault'
}
