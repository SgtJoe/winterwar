
class WWWeapon_Satchel extends ROSatchelChargeWeapon
	abstract;

`include(WinterWar\Classes\WWWeaponPickupMessagesOverride.uci)

defaultproperties
{
	WeaponContentClass(0)="WinterWar.WWWeapon_Satchel_ActualContent"
	
	RoleSelectionImage(0)=Texture2D'WinterWar_UI.WP_Render.WP_Render_Satchel'
	
	InvIndex=`WI_SATCHEL
	
	FuzeLength=7.0
	
	AmmoClass=class'ROAmmo_SatchelCharge'
	
	WeaponProjectiles(0)=class'WWProjectile_SatchelCharge'
	
	ThrowingBattleChatterIndex=`BATTLECHATTER_ThrowingGrenade
}
