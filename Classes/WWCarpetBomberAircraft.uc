
class WWCarpetBomberAircraft extends ROCarpetBomberAircraft;

`define BOMBDISTANCE (PayloadClass.default.DamageRadius * 0.9)
`define BOMBTIMEMODIFIER (TimeTweenDrops * 0.8)

var vector XOffset;

var PointLightComponent FlyingLight;
var name PropellerAnim;

var bool MarkedForDeath;

simulated function PostBeginPlay()
{
	super.PostBeginPlay();
	
	// We need to spawn a light otherwise the mesh will be pitch black
	Mesh.AttachComponentToSocket(FlyingLight, 'LightSocket');
	
	Mesh.PlayAnim(PropellerAnim,,true);
	
	if (Role == ROLE_Authority)
	{
		SetTimer(0.1, true, 'ShotDown');
	}
}

simulated event StartStrike()
{
	super(ROSupportAircraft).StartStrike();
}

function CheckForSAMs() {}

function TakeDamage(int DamageAmount, Controller EventInstigator, vector HitLocation, vector Momentum, class<DamageType> DamageType, optional TraceHitInfo HitInfo, optional Actor DamageCauser)
{
	local bool bTeamKill;
	
	bTeamKill = false;
	
	`wwdebug("taking" @ DamageType.name @ "in" @ HitInfo.BoneName,'CMDA');
	
	if (HitInfo.BoneName == 'ENGINE')
	{
		DamageAmount *= EngineHitDamageScale;
	}
	
	if (class<WWDmgType_FighterPlane>(DamageType) != none)
	{
		Health = 0; // Health - (default.Health * 0.4); // -40% of base health per fighter run
	}
	else if (class<RODamageType>(DamageType) != none && class<RODamageType>(DamageType).default.AirVehicleDamageScaling >= 1.0)
	{
		Health -= DamageAmount * class<RODamageType>(DamageType).default.AirVehicleDamageScaling;
	}
	
	if (Health >= 0 && !bShotDown)
	{
		`wwdebug("health:" @ Max(0, Health),'CMDA');
	}
	
	if( Health <= 0 && !bShotDown )
	{
		bShotDown = true;
		KilledBy = Controller(DamageCauser);
		CallOffStrike();
		
		SetBase(none);
		
		LifeSpan = 25.0;
		
		if ( EventInstigator.GetTeamNum() == TeamIndex )
		{
			bTeamKill = true;
		}
		
		if( EventInstigator != none )
		{
			ROGameInfo(WorldInfo.Game).ScoreReconKill(EventInstigator, bTeamKill, 2);
		}
	}
	
	if ( WorldInfo.NetMode != NM_DedicatedServer )
	{
		UpdatedHealth();
	}
}

simulated function Tick(float DeltaTime)
{
	super(Actor).Tick(DeltaTime);
	
	if( bCheckMapBounds && WorldInfo.TimeSeconds - LastKillCheckTime > 1.0 )
	{
		LastKillCheckTime = WorldInfo.TimeSeconds;
		
		if ( Location.X < WorldMinX || Location.X > WorldMaxX || Location.Y < WorldMinY || Location.Y > WorldMaxY )
		{
			ShutDown();
		}
	}
	
	/* if( Velocity == vect(0,0,0) && bBegunStrike && Physics == PHYS_RigidBody )
	{
		Explode();
	} */
}

simulated function ShotDown()
{
	local vector X, Y, Z, VelNorm;
	local rotator R;
	
	// Often times we start to drop payload even if we got a fighter coming to take us down
	// That's not very fair, so tack this on here
	if (MarkedForDeath && !bAbortStrike)
	{
		bAbortStrike = true;
	}
	
	if (bShotDown && !bExploded)
	{
		GetAxes(Rotation, X, Y, Z);
		VelNorm = Normal(Velocity);
		
		Velocity = (vector(Rotation) * Speed) + (Z * (VelNorm dot X) * -100);
		
		R.Roll = Rotation.Roll - int(DeathTorque / 2);
		R.Pitch = Rotation.Pitch - int(DeathTorque);
		R.Yaw = Rotation.Yaw;
		SetRotation(R);
	}
}

simulated event Landed(vector HitNormal, Actor FloorActor) {}

simulated event HitWall( vector HitNormal, actor Wall, PrimitiveComponent WallComp, optional PhysicalMaterial WallPhysMaterial) {}

simulated function Explode() {}

simulated function Destroyed()
{
	if (AmbientComponent != none)
	{
		AmbientComponent.StopEvents();
	}
	
	super.Destroyed();
}

function SetOffset(int AircraftNum)
{
	local vector X, Y, Z;
	
	if( AircraftNum == 0 )
		return;
	
	GetAxes(Rotation, X, Y, Z);
	
	if( AircraftNum == 1 )
	{
		YOffset = Y * 50;
		SetLocation(Location + YOffset);
	}
	else
	{
		bLeadAircraft = false;
		
		XOffset = X * -5000;
		YOffset = Y * -50;
		
		SetLocation(Location - XOffset + YOffset + Z * 1000);
		ClearTimer('StartStrike');
		SetTimer(InboundDelay, false, 'StartStrike');
	}
}

function SetDropPoint()
{
	local int i;
	local float FallDist, PreDropDist, TimeTillDrop, ImpactVelPct;
	local vector DropLocationTwo, BaseDropLocation;
	BombsDropped = 0;
	FallDist = Location.Z - TargetLocation.Z;
	
	if( FallDist < 0 )
	{
		return;
	}
	
	if( FallDist > 5000 )
		ImpactVelPct = default.MinImpactVelPct + (default.MaxImpactVelPct - default.MinImpactVelPct) * (1 - FMin(1.0, ((FallDist - 5000) / 5000)));
	else
		ImpactVelPct = default.MaxImpactVelPct;
	
	FallTime = Sqrt((FallDist * 2) / Abs(PhysicsVolume.GetGravityZ()));
	
	DropLocationOne = TargetLocation;
	DropLocationOne.Z = Location.Z;
	BaseDropLocation = DropLocationOne;
	PreDropDist = (Speed + Speed * ImpactVelPct) / 2 * FallTime;
	
	DropLocationOne += Normal(Location - DropLocationOne) * (PreDropDist + `BOMBDISTANCE * (BombsToDrop / 2));
	
	for( i=0; i<BombsToDrop / 2; i++ )
	{
		if( PreDropDist + `BOMBDISTANCE * ((BombsToDrop / 2) - i) <= VSize(Location - BaseDropLocation) )
		{
			DropLocationTwo = DropLocationOne - Normal(Location - DropLocationOne) * `BOMBDISTANCE;
			ImpactLocationOne = TargetLocation + Normal(Location - DropLocationOne) * (`BOMBDISTANCE * (BombsToDrop / 2));
			TimeTillDrop = VSize(DropLocationOne - Location) / Speed;
			TimeTweenDrops = (VSize(DropLocationTwo - Location) / Speed) - TimeTillDrop;
			TimeTillDrop +=	InboundDelay;
			SetTimer(TimeTillDrop, false, 'DropPayload');
			break;
		}
		
		BombsDropped++;
		DropLocationOne -= Normal(Location - DropLocationOne) * `BOMBDISTANCE;
	}
}

function DropPayload()
{
	local ROBombProjectile SpawnedPayload;
	local vector X, Y, Z, Accel, ImpactLoc, HitNorm, TraceStart;
	local float FallDist, ImpactVelPct;
	local ROVolumeTest RVT;
	local bool bTargetInvalid;
	
	bCheckMapBounds = true;
	
	if (bAbortStrike)
	{
		return;
	}
	
	TraceStart = ImpactLocationOne + vector(Rotation) * `BOMBDISTANCE * BombsDropped + YOffset;
	Trace(ImpactLoc, HitNorm, TraceStart - vect(0,0,1) * 15000, TraceStart + vect(0,0,1) * 15000, false);
	RVT = Spawn(class'ROVolumeTest',self,,ImpactLoc);
	if ( RVT != none && RVT.IsInNoArtyVolume() )
	{
		bTargetInvalid = true;
	}
	
	RVT.Destroy();
	
	TimeTweenDrops = `BOMBTIMEMODIFIER;
	
	if( bTargetInvalid )
	{
		BombsDropped++;
		
		if( BombsDropped < BombsToDrop )
			SetTimer(TimeTweenDrops, false, 'DropPayload');
		
		return;
	}
	
	GetAxes(Rotation,X,Y,Z);
	FallDist = DropLocationOne.Z - TargetLocation.Z;
	
	SpawnedPayload = Spawn(PayloadClass, InstigatorController,, Mesh.GetBoneLocation(BombBayName), rotator(velocity));
	
	if( SpawnedPayload != none )
	{
		if( FallDist > 5000 )
			ImpactVelPct = default.MinImpactVelPct + (default.MaxImpactVelPct - default.MinImpactVelPct) * (1 - FMin(1.0, ((FallDist - 5000) / 5000)));
		else
			ImpactVelPct = default.MaxImpactVelPct;
		
		Accel = (Velocity * ImpactVelPct - Velocity) / FallTime;
		Accel.Z = PhysicsVolume.GetGravityZ();
		SpawnedPayload.AccelToApply = Accel;
		SpawnedPayload.Acceleration = Accel;
		SpawnedPayload.Velocity = Vector(AddSpreadY(rotator(Velocity))) * Speed;
		SpawnedPayload.FallDist = FallDist;
		SpawnedPayload.Lifespan = FallTime + 0.5;
		SpawnedPayload.SetExplosionTime(FallTime);
	}
	
	BombsDropped++;
	
	if( BombsDropped < BombsToDrop )
	{
		SetTimer(TimeTweenDrops, false, 'DropPayload');
	}
}

defaultproperties
{
	TeamIndex=`ALLIES_TEAM_INDEX
	
	Health=9000
	
	Speed=1500
	
	DeathTorque=6
	
	BombsToDrop=10
	
	PayloadClass=class'ROCarpetBomb'
	
	BombBayName=Canberra_Root
	
	Begin Object Name=PlaneMesh
		SkeletalMesh=SkeletalMesh'WinterWar_VH_USSR_TB-3.Mesh.TB-3'
		AnimSets[0]=AnimSet'WinterWar_VH_USSR_TB-3.Anim.TB-3_Anim'
		PhysicsAsset=PhysicsAsset'WinterWar_VH_USSR_TB-3.Phy.TB-3_Physics'
		Materials[0]=MaterialInstanceConstant'WinterWar_VH_USSR_TB-3.MIC.SOV_TB-3'
	End Object
	
	PropellerAnim=propeller_loop
	
	AmbientSound=AkEvent'WW_CMD_Spooky.Play_CMD_Spooky_Plane'
	AmbientStopSound=AkEvent'WW_CMD_Spooky.Stop_CMD_Spooky_Plane'
	ShotDownAmbientSound=AkEvent'WW_CMD_Spooky.Play_CMD_Spooky_Crashing_Engine_Loop'
	bPlaysShotDownAmbient=true
	
	Begin Object class=PointLightComponent name=PlaneLight
		bCastCompositeShadow=False
		LightingChannels=(Dynamic=TRUE,CompositeDynamic=TRUE)
		UseDirectLightMap=FALSE
		Radius=1500.0
	End Object
	Components.Add(PlaneLight)
	FlyingLight=PlaneLight
	
	MarkedForDeath=false
}
