
class WWWeapon_QuadMaxims_Attach extends ROWeapAttach_DShK_HMG_Tripod;

var ROParticleSystemComponent MuzzleFlashPSC1, MuzzleFlashPSC2, MuzzleFlashPSC3, MuzzleFlashPSC4;

simulated function DetachFrom( SkeletalMeshComponent MeshCpnt )
{
	if (Mesh != None)
	{
		if (MuzzleFlashPSC1 != None)
		{
			Mesh.DetachComponent(MuzzleFlashPSC1);
		}
		
		if (MuzzleFlashPSC2 != None)
		{
			Mesh.DetachComponent(MuzzleFlashPSC2);
		}
		
		if (MuzzleFlashPSC3 != None)
		{
			Mesh.DetachComponent(MuzzleFlashPSC3);
		}
		
		if (MuzzleFlashPSC4 != None)
		{
			Mesh.DetachComponent(MuzzleFlashPSC4);
		}
		
		if (MuzzleFlashLight != None)
		{
			Mesh.DetachComponent(MuzzleFlashLight);
		}
		
		if (ShellEjectPSC != None)
		{
			Mesh.DetachComponent(ShellEjectPSC);
		}
		
		Mesh.SetShadowParent(None);
		Mesh.SetLightEnvironment(None);
		Mesh.SetHidden(true);
		
		if (MeshCpnt != None)
		{
			MeshCpnt.DetachComponent( mesh );
		}
	}
	
	GotoState('');
}

simulated function AttachMuzzleFlash()
{
	if ( MuzzleFlashPSCTemplate == None && MuzzleFlashAltPSCTemplate == None )
	{
		return;
	}
	
	MuzzleFlashPSC1 = new(self) class'ROParticleSystemComponent';
	MuzzleFlashPSC1.bAutoActivate = false;
	MuzzleFlashPSC1.SetOwnerNoSee(true);
	
	MuzzleFlashPSC2 = new(self) class'ROParticleSystemComponent';
	MuzzleFlashPSC2.bAutoActivate = false;
	MuzzleFlashPSC2.SetOwnerNoSee(true);
	
	MuzzleFlashPSC3 = new(self) class'ROParticleSystemComponent';
	MuzzleFlashPSC3.bAutoActivate = false;
	MuzzleFlashPSC3.SetOwnerNoSee(true);
	
	MuzzleFlashPSC4 = new(self) class'ROParticleSystemComponent';
	MuzzleFlashPSC4.bAutoActivate = false;
	MuzzleFlashPSC4.SetOwnerNoSee(true);
	
	Mesh.AttachComponentToSocket(MuzzleFlashPSC1, class'WWWeapon_QuadMaxims'.default.MaximMuzzleFlashSockets[0]);
	Mesh.AttachComponentToSocket(MuzzleFlashPSC2, class'WWWeapon_QuadMaxims'.default.MaximMuzzleFlashSockets[1]);
	Mesh.AttachComponentToSocket(MuzzleFlashPSC3, class'WWWeapon_QuadMaxims'.default.MaximMuzzleFlashSockets[2]);
	Mesh.AttachComponentToSocket(MuzzleFlashPSC4, class'WWWeapon_QuadMaxims'.default.MaximMuzzleFlashSockets[3]);
	
	MuzzleFlashPSC1.SetTickGroup(TG_PostUpdateWork);
	MuzzleFlashPSC2.SetTickGroup(TG_PostUpdateWork);
	MuzzleFlashPSC3.SetTickGroup(TG_PostUpdateWork);
	MuzzleFlashPSC4.SetTickGroup(TG_PostUpdateWork);
}

simulated function MuzzleFlashTimer()
{
	if ( MuzzleFlashLight != None )
	{
		MuzzleFlashLight.SetEnabled(FALSE);
	}
	
	if (MuzzleFlashPSC1 != none)
	{
		MuzzleFlashPSC1.DeactivateSystem();
	}
	
	if (MuzzleFlashPSC2 != none)
	{
		MuzzleFlashPSC2.DeactivateSystem();
	}
	
	if (MuzzleFlashPSC3 != none)
	{
		MuzzleFlashPSC3.DeactivateSystem();
	}
	
	if (MuzzleFlashPSC4 != none)
	{
		MuzzleFlashPSC4.DeactivateSystem();
	}
}

simulated function CauseMuzzleFlash()
{
	if ( !WorldInfo.bDropDetail && !class'Engine'.static.IsSplitScreen() )
	{
		if ( MuzzleFlashLight == None )
		{
			if ( MuzzleFlashLightClass != None )
			{
				MuzzleFlashLight = new(Outer) MuzzleFlashLightClass;
				if ( Mesh != None && Mesh.GetSocketByName(MuzzleFlashSocket) != None )
				{
					Mesh.AttachComponentToSocket(MuzzleFlashLight, MuzzleFlashSocket);
				}
				else if ( OwnerMesh != None )
				{
					OwnerMesh.AttachComponentToSocket(MuzzleFlashLight, AttachmentSocket);
				}
			}
		}
		else
		{
			MuzzleFlashLight.ResetLight();
		}
	}
	
	if (MuzzleFlashPSC1 != none)
	{
		if (!bMuzzleFlashPSCLoops || !MuzzleFlashPSC1.bIsActive)
		{
			MuzzleFlashPSC1.SetTemplate(MuzzleFlashPSCTemplate);
			SetMuzzleFlashParams(MuzzleFlashPSC1);
			MuzzleFlashPSC1.ActivateSystem();
		}
	}
	
	if (MuzzleFlashPSC2 != none)
	{
		if (!bMuzzleFlashPSCLoops || !MuzzleFlashPSC2.bIsActive)
		{
			MuzzleFlashPSC2.SetTemplate(MuzzleFlashPSCTemplate);
			SetMuzzleFlashParams(MuzzleFlashPSC2);
			MuzzleFlashPSC2.ActivateSystem();
		}
	}
	
	if (MuzzleFlashPSC3 != none)
	{
		if (!bMuzzleFlashPSCLoops || !MuzzleFlashPSC3.bIsActive)
		{
			MuzzleFlashPSC3.SetTemplate(MuzzleFlashPSCTemplate);
			SetMuzzleFlashParams(MuzzleFlashPSC3);
			MuzzleFlashPSC3.ActivateSystem();
		}
	}
	
	if (MuzzleFlashPSC4 != none)
	{
		if (!bMuzzleFlashPSCLoops || !MuzzleFlashPSC4.bIsActive)
		{
			MuzzleFlashPSC4.SetTemplate(MuzzleFlashPSCTemplate);
			SetMuzzleFlashParams(MuzzleFlashPSC4);
			MuzzleFlashPSC4.ActivateSystem();
		}
	}
	
	SetTimer(MuzzleFlashDuration,false,'MuzzleFlashTimer');
}

simulated function StopMuzzleFlash()
{
	ClearTimer('MuzzleFlashTimer');
	MuzzleFlashTimer();
	
	if (MuzzleFlashPSC1 != none)
	{
		MuzzleFlashPSC1.DeactivateSystem();
	}
	
	if (MuzzleFlashPSC2 != none)
	{
		MuzzleFlashPSC2.DeactivateSystem();
	}
	
	if (MuzzleFlashPSC3 != none)
	{
		MuzzleFlashPSC3.DeactivateSystem();
	}
	
	if (MuzzleFlashPSC4 != none)
	{
		MuzzleFlashPSC4.DeactivateSystem();
	}
}

defaultproperties
{
	WeaponClass=class'WWWeapon_QuadMaxims'
	
	TracerFrequency=0
	
	// MuzzleFlashPSCTemplate=ParticleSystem'FX_VN_Weapons.MuzzleFlashes.FX_VN_MuzzleFlash_3rdP_DShK'
	MuzzleFlashPSCTemplate=ParticleSystem'FX_VN_Weapons.MuzzleFlashes.FX_VN_MuzzleFlash_3rdP_Rifles_split_alwaysFlash'
	
	ReloadAnims(0)=DshK_ReloadEmpty
	ReloadAnims(1)=DshK_ReloadEmpty
	
	CHR_AnimSet=AnimSet'WinterWar_VH_USSR_ZIS.Anim.CHR_QuadMaxim'
	
	FireAnim=Shoot
	FireLastAnim=Shoot_Last
	IdleAnim=Idle
	IdleEmptyAnim=Idle_Empty
}
