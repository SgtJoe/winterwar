
class WWMGBarrelLS26 extends ROMGBarrel;

DefaultProperties
{
	FiringHeatIncrement=1.3

	ROMGSteamTemp=120
	ROMGCriticalTemp=280
	ROMGFailTemp=400
	ROMGWarnTemp=280

	BarrelCoolingRate=1.7
}
