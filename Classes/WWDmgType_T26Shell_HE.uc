
class WWDmgType_T26Shell_HE extends RODamageType_CannonShell_HE
	abstract;

defaultproperties
{
	WeaponShortName="T26"
	VehicleDamageScaling=0.15
	RadialDamageImpulse=2000
}
