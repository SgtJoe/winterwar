
class WWVehicle_T26 extends WWVehicleTank
	abstract;

var AudioComponent CoaxMGAmbient;

var SoundCue CoaxMGStopSound;

var repnotify TakeHitInfo DeathHitInfo_ProxyDriver, DeathHitInfo_ProxyCommander;

replication
{
	if (bNetDirty)
		DeathHitInfo_ProxyDriver, DeathHitInfo_ProxyCommander;
}

simulated event ReplicatedEvent(name VarName)
{
	if (VarName == 'DeathHitInfo_ProxyDriver')
	{
		if( IsLocalPlayerInThisVehicle() )
		{
			PlaySeatProxyDeathHitEffects(0, DeathHitInfo_ProxyDriver);
		}
	}
	else if (VarName == 'DeathHitInfo_ProxyCommander')
	{
		if( IsLocalPlayerInThisVehicle() )
		{
			PlaySeatProxyDeathHitEffects(1, DeathHitInfo_ProxyCommander);
		}
	}
	else
	{
		super.ReplicatedEvent(VarName);
	}
}

function DamageSeatProxy(int SeatProxyIndex, int Damage, Controller InstigatedBy, vector HitLocation, vector Momentum, class<DamageType> DamageType, optional Actor DamageCauser)
{
	switch( SeatProxyIndex )
	{
	case 0:
		DeathHitInfo_ProxyDriver.Damage = Damage;
		DeathHitInfo_ProxyDriver.HitLocation = HitLocation;
		DeathHitInfo_ProxyDriver.Momentum = Momentum;
		DeathHitInfo_ProxyDriver.DamageType = DamageType;
		break;
	case 1:
		DeathHitInfo_ProxyCommander.Damage = Damage;
		DeathHitInfo_ProxyCommander.HitLocation = HitLocation;
		DeathHitInfo_ProxyCommander.Momentum = Momentum;
		DeathHitInfo_ProxyCommander.DamageType = DamageType;
		break;
	}
	
	Super.DamageSeatProxy(SeatProxyIndex, Damage, InstigatedBy, HitLocation, Momentum, DamageType, DamageCauser);
}

simulated function int GetCommanderSeatIndex()
{
	return GetSeatIndexFromPrefix("Turret");
}

simulated function int GetGunnerSeatIndex()
{
	return GetSeatIndexFromPrefix("Turret");
}

simulated function int GetLoaderSeatIndex()
{
	return GetSeatIndexFromPrefix("Turret");
}

simulated function int GetHullMGSeatIndex()
{
	return -1;
}

simulated event PostBeginPlay()
{
	super.PostBeginPlay();
	
	if( WorldInfo.NetMode != NM_DedicatedServer )
	{
		Mesh.AttachComponentToSocket(CoaxMGAmbient, 'CoaxMG');
	}
}

simulated event TornOff()
{
	CoaxMGAmbient.Stop();
	
	Super.TornOff();
}

simulated function StopVehicleSounds()
{
	Super.StopVehicleSounds();
	
	CoaxMGAmbient.Stop();
}

simulated function VehicleWeaponFireEffects(vector HitLocation, int SeatIndex)
{
	Super.VehicleWeaponFireEffects(HitLocation, SeatIndex);
	
	if (SeatIndex == GetGunnerSeatIndex() && SeatFiringMode(SeatIndex,,true) == 1 && !CoaxMGAmbient.bWasPlaying)
	{
		CoaxMGAmbient.Play();
	}
}

simulated function VehicleWeaponStoppedFiring(bool bViaReplication, int SeatIndex)
{
	Super.VehicleWeaponStoppedFiring(bViaReplication, SeatIndex);
	
	if ( SeatIndex == GetGunnerSeatIndex() )
	{
		if ( CoaxMGAmbient.bWasPlaying || !CoaxMGAmbient.bFinished )
		{
			CoaxMGAmbient.Stop();
			PlaySound(CoaxMGStopSound, TRUE, FALSE, FALSE, CoaxMGAmbient.CurrentLocation, FALSE);
		}
	}
}

simulated function bool IsInPeriscope(byte SeatIndex, byte PositionIndex)
{
	return false;
}

DefaultProperties
{
	Team=`ALLIES_TEAM_INDEX
	
	Health=600
	
	ExitRadius=140
	
	Begin Object Name=CollisionCylinder
		CollisionHeight=60.0
		CollisionRadius=120.0
		Translation=(X=0.0,Y=0.0,Z=55.0)
	End Object
	CylinderComponent=CollisionCylinder
	
	MaxSpeed=723
	
	HUDBodyTexture=Texture2D'WinterWar_UI.Vehicle.UI_HUD_VH_T26_Body'
	HUDTurretTexture=Texture2D'WinterWar_UI.Vehicle.UI_HUD_VH_T26_Turret'
	
	DriverOverlayTexture=Texture2D'WinterWar_UI.VehicleOptics.UI_HUD_VH_Optics_SquareOverlay'
	
	Seats(0)={(
		CameraOffset=-420,
		SeatAnimBlendName=DriverPositionNode,
		SeatPositions=(
			(
				bDriverVisible=false,
				bAllowFocus=false,
				PositionCameraTag=Driver_Camera,
				ViewFOV=70.0,
				bViewFromCameraTag=true,
				bDrawOverlays=true,
				// bDrawOverlays=false,
				PositionUpAnim=Driver_idle,
				PositionIdleAnim=Driver_idle,
				DriverIdleAnim=Driver_idle,
				AlternateIdleAnim=Driver_idle,
				SeatProxyIndex=0,
				bIsExterior=false,
				PositionFlinchAnims=(Driver_idle),
				PositionDeathAnims=(Driver_idle)
			)
		),
		bSeatVisible=false,
		SeatBone=Root_Driver,
		DriverDamageMult=1.0,
		InitialPositionIndex=0
	)}
	
	Seats(1)={(
		CameraOffset=-420,
		SeatAnimBlendName=CommanderPositionNode,
		GunClass=class'WWVehicleWeapon_T26_Turret',
		RangeOverlayTexture=Texture2D'WinterWar_UI.VehicleOptics.UI_HUD_VH_Optics_Range_T26',
		PeriscopeRangeTexture=none,
		VignetteOverlayTexture=Texture2D'WinterWar_UI.VehicleOptics.UI_HUD_VH_Optics_CircularOverlay',
		BinocOverlayTexture=Texture2D'WinterWar_UI.VehicleOptics.UI_HUD_VH_Optics_SquareOverlay',
		GunSocket=(Barrel,CoaxMG),
		GunPivotPoints=(gun_base,gun_base),
		TurretVarPrefix="Turret",
		TurretControls=(Turret_Gun,Turret_Main),
		SeatPositions=(
			(
				bDriverVisible=false,
				bAllowFocus=false,
				PositionCameraTag=Camera_Gunner,
				bViewFromCameraTag=true,
				ViewFOV=20,
				bCamRotationFollowSocket=true,
				bDrawOverlays=true,
				// bDrawOverlays=false,
				PositionUpAnim=MG_Hull_Idle,
				PositionDownAnim=MG_Hull_Idle,
				PositionIdleAnim=MG_Hull_Idle,
				DriverIdleAnim=MG_Hull_Idle,
				AlternateIdleAnim=MG_Hull_Idle,
				SeatProxyIndex=1,
				bIsExterior=false,
				PositionFlinchAnims=(MG_Hull_Idle),
				PositionDeathAnims=(MG_Hull_Idle)
			)
		),
		bSeatVisible=false,
		SeatBone=Turret,
		DriverDamageMult=1.0,
		InitialPositionIndex=0,
		FiringPositionIndex=0,
		TracerFrequency=5,
		WeaponTracerClass=(none,class'WWProjectile_DT_T26_Tracer'),
		MuzzleFlashLightClass=(none,none),
	)}
	
	PassengerAnimTree=AnimTree'CHR_Playeranimtree_Master.CHR_Tanker_animtree'
	CrewAnimSet=AnimSet'VH_VN_ARVN_M113_APC.Anim.CHR_M113_Anim_Master'
	
	LeftWheels( 0)="W01_L"
	LeftWheels( 1)="W02_L"
	LeftWheels( 2)="W03_L"
	LeftWheels( 3)="W04_L"
	LeftWheels( 4)="W05_L"
	LeftWheels( 5)="W06_L"
	LeftWheels( 6)="W07_L"
	LeftWheels( 7)="W08_L"
	LeftWheels( 8)="W09_L"
	LeftWheels( 9)="W10_L"
	LeftWheels(10)="W11_L"
	LeftWheels(11)="W12_L"
	LeftWheels(12)="W13_L"
	LeftWheels(13)="W14_L"
	
	RightWheels( 0)="W01_R"
	RightWheels( 1)="W02_R"
	RightWheels( 2)="W03_R"
	RightWheels( 3)="W04_R"
	RightWheels( 4)="W05_R"
	RightWheels( 5)="W06_R"
	RightWheels( 6)="W07_R"
	RightWheels( 7)="W08_R"
	RightWheels( 8)="W09_R"
	RightWheels( 9)="W10_R"
	RightWheels(10)="W11_R"
	RightWheels(11)="W12_R"
	RightWheels(12)="W13_R"
	RightWheels(13)="W14_R"
	
	Begin Object Name=RRWheel
		BoneName="R_Wheel_06"
		BoneOffset=(X=-10.0,Y=0,Z=0.0)
		WheelRadius=24
	End Object
	
	Begin Object Name=RMWheel
		BoneName="R_Wheel_04"
		BoneOffset=(X=0.0,Y=0,Z=0.0)
		WheelRadius=24
	End Object
	
	Begin Object Name=RFWheel
		BoneName="R_Wheel_02"
		BoneOffset=(X=0.0,Y=0,Z=2.0)
		WheelRadius=24
	End Object
	
	Begin Object Name=LRWheel
		BoneName="L_Wheel_06"
		BoneOffset=(X=-10.0,Y=0,Z=0.0)
		WheelRadius=24
	End Object
	
	Begin Object Name=LMWheel
		BoneName="L_Wheel_04"
		BoneOffset=(X=0.0,Y=0,Z=0.0)
		WheelRadius=24
	End Object
	
	Begin Object Name=LFWheel
		BoneName="L_Wheel_02"
		BoneOffset=(X=0.0,Y=0,Z=2.0)
		WheelRadius=24
	End Object
	
	Begin Object Name=SimObject
		GearArray(0)={(
			GearRatio=-5.64,
			AccelRate=10.25,
			TorqueCurve=(Points={(
				(InVal=0,OutVal=-2100),
				(InVal=300,OutVal=-700),
				(InVal=2800,OutVal=-2100),
				(InVal=3000,OutVal=-700),
				(InVal=3200,OutVal=-0.0)
				)}),
			TurningThrottle=0.93
			)}
		GearArray(1)={()}
		GearArray(2)={(
			GearRatio=3.60,
			AccelRate=13.50,
			TorqueCurve=(Points={(
				(InVal=0,OutVal=2500),
				(InVal=300,OutVal=1750),
				(InVal=2800,OutVal=2500),
				(InVal=3000,OutVal=800),
				(InVal=3200,OutVal=0.0)
				)}),
			TurningThrottle=0.94
			)}
		GearArray(3)={(
			GearRatio=2.14,
			AccelRate=15.00,
			TorqueCurve=(Points={(
				(InVal=0,OutVal=2000),
				(InVal=2800,OutVal=3750),
				(InVal=3000,OutVal=1000),
				(InVal=3200,OutVal=0.0)
				)}),
			TurningThrottle=0.93
			)}
		GearArray(4)={(
			GearRatio=1.42,
			AccelRate=16.50,
			TorqueCurve=(Points={(
				(InVal=0,OutVal=2200),
				(InVal=2800,OutVal=5200),
				(InVal=3000,OutVal=1200),
				(InVal=3200,OutVal=0.0)
				)}),
			TurningThrottle=0.93
			)}
		GearArray(5)={(
			GearRatio=1.00,
			AccelRate=16.25,
			TorqueCurve=(Points={(
				(InVal=0,OutVal=3000),
				(InVal=2800,OutVal=7400),
				(InVal=3000,OutVal=1500),
				(InVal=3200,OutVal=0.0)
				)}),
			TurningThrottle=0.93
			)}
		FirstForwardGear=2
	End Object
	
	TreadSpeedScale=2.5
	
	// Non armour hit zones such as the crew, componants and tracks.
	VehHitZones(0)=(ZoneName=ENGINEBLOCK,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Engine,ZoneHealth=100,VisibleFrom=14)
	VehHitZones(1)=(ZoneName=ENGINECORE,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Engine,ZoneHealth=300,VisibleFrom=14)
	VehHitZones(2)=(ZoneName=AMMOSTOREMAIN,DamageMultiplier=100.0,VehicleHitZoneType=VHT_Ammo,ZoneHealth=10,KillPercentage=0.4,VisibleFrom=15)
	VehHitZones(3)=(ZoneName=AMMOTURRETLEFT,DamageMultiplier=50.0,VehicleHitZoneType=VHT_Ammo,ZoneHealth=10,KillPercentage=0.3)
	VehHitZones(4)=(ZoneName=AMMOTURRETRIGHT,DamageMultiplier=50.0,VehicleHitZoneType=VHT_Ammo,ZoneHealth=10,KillPercentage=0.3)
	VehHitZones(5)=(ZoneName=GEARBOX,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Mechanicals,ZoneHealth=100,VisibleFrom=1)
	VehHitZones(6)=(ZoneName=GEARBOXCORE,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Mechanicals,ZoneHealth=200,VisibleFrom=1)
	VehHitZones(7)=(ZoneName=LEFTBRAKES,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Mechanicals,ZoneHealth=25,VisibleFrom=5)
	VehHitZones(8)=(ZoneName=RIGHTBRAKES,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Mechanicals,ZoneHealth=25,VisibleFrom=9)
	VehHitZones(9)=(ZoneName=TURRETRINGONE,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Mechanicals,ZoneHealth=10,VisibleFrom=5)
	VehHitZones(10)=(ZoneName=TURRETRINGTWO,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Mechanicals,ZoneHealth=10,VisibleFrom=7)
	VehHitZones(11)=(ZoneName=TURRETRINGTHREE,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Mechanicals,ZoneHealth=10,VisibleFrom=6)
	VehHitZones(12)=(ZoneName=TURRETRINGFOUR,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Mechanicals,ZoneHealth=10,VisibleFrom=10)
	VehHitZones(13)=(ZoneName=TURRETRINGFIVE,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Mechanicals,ZoneHealth=10,VisibleFrom=11)
	VehHitZones(14)=(ZoneName=TURRETRINGSIX,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Mechanicals,ZoneHealth=10,VisibleFrom=9)
	VehHitZones(15)=(ZoneName=COAXIALMG,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Mechanicals,ZoneHealth=25)
	VehHitZones(16)=(ZoneName=MAINCANNONREAR,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Mechanicals,ZoneHealth=50,KillPercentage=0.3)
	VehHitZones(17)=(ZoneName=DRIVERBODY,DamageMultiplier=1.0,VehicleHitZoneType=VHT_CrewBody,CrewSeatIndex=0,SeatProxyIndex=0)
	VehHitZones(18)=(ZoneName=DRIVERHEAD,DamageMultiplier=1.0,VehicleHitZoneType=VHT_CrewHead,CrewSeatIndex=0,SeatProxyIndex=0)
	VehHitZones(19)=(ZoneName=GUNNERBODY,DamageMultiplier=1.0,VehicleHitZoneType=VHT_CrewBody,CrewSeatIndex=1,SeatProxyIndex=1)
	VehHitZones(20)=(ZoneName=GUNNERHEAD,DamageMultiplier=1.0,VehicleHitZoneType=VHT_CrewHead,CrewSeatIndex=1,SeatProxyIndex=1)
	VehHitZones(21)=(ZoneName=LEFTTRACKONE,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Track,ZoneHealth=200,VisibleFrom=5)
	VehHitZones(22)=(ZoneName=LEFTTRACKTWO,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Track,ZoneHealth=200)
	VehHitZones(23)=(ZoneName=LEFTTRACKTHREE,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Track,ZoneHealth=200)
	VehHitZones(24)=(ZoneName=LEFTTRACKFOUR,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Track,ZoneHealth=200)
	VehHitZones(25)=(ZoneName=LEFTTRACKFIVE,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Track,ZoneHealth=200)
	VehHitZones(26)=(ZoneName=LEFTTRACKSIX,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Track,ZoneHealth=200)
	VehHitZones(27)=(ZoneName=LEFTTRACKSEVEN,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Track,ZoneHealth=200)
	VehHitZones(28)=(ZoneName=LEFTTRACKEIGHT,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Track,ZoneHealth=200)
	VehHitZones(29)=(ZoneName=RIGHTTRACKONE,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Track,ZoneHealth=200,VisibleFrom=9)
	VehHitZones(30)=(ZoneName=RIGHTTRACKTWO,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Track,ZoneHealth=200)
	VehHitZones(31)=(ZoneName=RIGHTTRACKTHREE,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Track,ZoneHealth=200)
	VehHitZones(32)=(ZoneName=RIGHTTRACKFOUR,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Track,ZoneHealth=200)
	VehHitZones(33)=(ZoneName=RIGHTTRACKFIVE,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Track,ZoneHealth=200)
	VehHitZones(34)=(ZoneName=RIGHTTRACKSIX,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Track,ZoneHealth=200)
	VehHitZones(35)=(ZoneName=RIGHTTRACKSEVEN,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Track,ZoneHealth=200)
	VehHitZones(36)=(ZoneName=RIGHTTRACKEIGHT,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Track,ZoneHealth=200)
	VehHitZones(37)=(ZoneName=RIGHTDRIVEWHEEL,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Track,ZoneHealth=200)
	VehHitZones(38)=(ZoneName=LEFTDRIVEWHEEL,DamageMultiplier=1.0,VehicleHitZoneType=VHT_Track,ZoneHealth=200)
	VehHitZones(39)=(ZoneName=FUELTANKONE,DamageMultiplier=10.0,VehicleHitZoneType=VHT_Fuel,ZoneHealth=200,KillPercentage=0.3,VisibleFrom=15)
	VehHitZones(40)=(ZoneName=FUELTANKTWO,DamageMultiplier=5.0,VehicleHitZoneType=VHT_Fuel,ZoneHealth=200,KillPercentage=0.2,VisibleFrom=15)
	
	VehHitZones(41)=(ZoneName=CHASSIS)
	VehHitZones(42)=(ZoneName=TURRET)
	VehHitZones(43)=(ZoneName=ENGINE,VehicleHitZoneType=VHT_Engine)
	VehHitZones(44)=(ZoneName=Root_Track_R,VehicleHitZoneType=VHT_Track)
	VehHitZones(45)=(ZoneName=Root_Track_L,VehicleHitZoneType=VHT_Track)
	
	// All the armour hit zones organised into plate classes
	ArmorHitZones(0)=(ZoneName=GLACISMAINFRONT,PhysBodyBoneName=Chassis,ArmorPlateName=FRONTGLACIS)
	ArmorHitZones(1)=(ZoneName=GLACISUPPERFRONT,PhysBodyBoneName=Chassis,ArmorPlateName=FRONTGLACIS)
	ArmorHitZones(2)=(ZoneName=DRIVERPLATELOWER,PhysBodyBoneName=Chassis,ArmorPlateName=DRIVERFRONT)
	ArmorHitZones(3)=(ZoneName=DRIVERPLATEUPPER,PhysBodyBoneName=Chassis,ArmorPlateName=DRIVERFRONT)
	ArmorHitZones(4)=(ZoneName=DRIVERSIDEPLATE,PhysBodyBoneName=Chassis,ArmorPlateName=DRIVERSIDE)
	ArmorHitZones(5)=(ZoneName=DRIVERSIDEPLATETWO,PhysBodyBoneName=Chassis,ArmorPlateName=DRIVERSIDE)
	ArmorHitZones(6)=(ZoneName=HULLPLATEFRONT,PhysBodyBoneName=Chassis,ArmorPlateName=HULLFRONT)
	ArmorHitZones(7)=(ZoneName=GLACISPLATELOWER,PhysBodyBoneName=Chassis,ArmorPlateName=FRONTGLACIS)
	ArmorHitZones(8)=(ZoneName=GLACISMAINLOWER,PhysBodyBoneName=Chassis,ArmorPlateName=FRONTGLACIS)
	ArmorHitZones(9)=(ZoneName=FLOORPLATE,PhysBodyBoneName=Chassis,ArmorPlateName=FLOOR)
	ArmorHitZones(10)=(ZoneName=LOWERGLACISREAR,PhysBodyBoneName=Chassis,ArmorPlateName=REARGLACIS)
	ArmorHitZones(11)=(ZoneName=MAINGLACISREAR,PhysBodyBoneName=Chassis,ArmorPlateName=REARGLACIS)
	ArmorHitZones(12)=(ZoneName=TOPPLATEREAR,PhysBodyBoneName=Chassis,ArmorPlateName=ENGINEROOF)
	ArmorHitZones(13)=(ZoneName=HULLPLATEREAR,PhysBodyBoneName=Chassis,ArmorPlateName=HULLREAR)
	ArmorHitZones(14)=(ZoneName=HULLPLATETOP,PhysBodyBoneName=Chassis,ArmorPlateName=HULLROOF)
	ArmorHitZones(15)=(ZoneName=DRIVERPLATETOP,PhysBodyBoneName=Chassis,ArmorPlateName=HULLROOF)
	ArmorHitZones(16)=(ZoneName=LEFTPLATEMAIN,PhysBodyBoneName=Chassis,ArmorPlateName=LEFTSIDE)
	ArmorHitZones(17)=(ZoneName=LEFTPLATEREARONE,PhysBodyBoneName=Chassis,ArmorPlateName=LEFTSIDE)
	ArmorHitZones(18)=(ZoneName=LEFTPLATEREARTWO,PhysBodyBoneName=Chassis,ArmorPlateName=LEFTSIDE)
	ArmorHitZones(19)=(ZoneName=LEFTPLATEFRONTONE,PhysBodyBoneName=Chassis,ArmorPlateName=LEFTSIDE)
	ArmorHitZones(20)=(ZoneName=LEFTPLATEFRONTTWO,PhysBodyBoneName=Chassis,ArmorPlateName=LEFTSIDE)
	ArmorHitZones(21)=(ZoneName=RIGHTPLATEMAIN,PhysBodyBoneName=Chassis,ArmorPlateName=RIGHTSIDE)
	ArmorHitZones(22)=(ZoneName=RIGHTPLATEREARONE,PhysBodyBoneName=Chassis,ArmorPlateName=RIGHTSIDE)
	ArmorHitZones(23)=(ZoneName=RIGHTPLATEREARTWO,PhysBodyBoneName=Chassis,ArmorPlateName=RIGHTSIDE)
	ArmorHitZones(24)=(ZoneName=RIGHTPLATEFRONTONE,PhysBodyBoneName=Chassis,ArmorPlateName=RIGHTSIDE)
	ArmorHitZones(25)=(ZoneName=RIGHTPLATEFRONTTWO,PhysBodyBoneName=Chassis,ArmorPlateName=RIGHTSIDE)
	ArmorHitZones(26)=(ZoneName=RADIATOREXHAUST,PhysBodyBoneName=Chassis,ArmorPlateName=RADIATORPLATE)
	ArmorHitZones(27)=(ZoneName=LEFTBOX,PhysBodyBoneName=Chassis,ArmorPlateName=EQUIPMENTBOXLEFT)
	ArmorHitZones(28)=(ZoneName=RIGHTBOX,PhysBodyBoneName=Chassis,ArmorPlateName=EQUIPMENTBOXRIGHT)
	ArmorHitZones(29)=(ZoneName=MANTLETUPPER,PhysBodyBoneName=Turret,ArmorPlateName=MANTLET)
	ArmorHitZones(30)=(ZoneName=MANTLETLOWER,PhysBodyBoneName=Turret,ArmorPlateName=MANTLET)
	ArmorHitZones(31)=(ZoneName=MANTLETCENTER,PhysBodyBoneName=Turret,ArmorPlateName=MANTLET)
	ArmorHitZones(32)=(ZoneName=LEFTTURRETSIDEONE,PhysBodyBoneName=Turret,ArmorPlateName=LEFTTURRETSIDE)
	ArmorHitZones(33)=(ZoneName=LEFTTURRETSIDETWO,PhysBodyBoneName=Turret,ArmorPlateName=LEFTTURRETSIDE)
	ArmorHitZones(34)=(ZoneName=TURRETBUSTLELEFT,PhysBodyBoneName=Turret,ArmorPlateName=LEFTTURRETBUSTLE)
	ArmorHitZones(35)=(ZoneName=TURRETBUSTLEREAR,PhysBodyBoneName=Turret,ArmorPlateName=REARTURRETBUSTLE)
	ArmorHitZones(36)=(ZoneName=TURRETBUSTLERIGHT,PhysBodyBoneName=Turret,ArmorPlateName=RIGHTTURRETBUSTLE)
	ArmorHitZones(37)=(ZoneName=RIGHTTURRETSIDEONE,PhysBodyBoneName=Turret,ArmorPlateName=RIGHTTURRETSIDE)
	ArmorHitZones(38)=(ZoneName=RIGHTTURRETSIDETWO,PhysBodyBoneName=Turret,ArmorPlateName=RIGHTTURRETSIDE)
	ArmorHitZones(39)=(ZoneName=REARTURRETONE,PhysBodyBoneName=Turret,ArmorPlateName=REARTURRET)
	ArmorHitZones(40)=(ZoneName=REARTURRETTWO,PhysBodyBoneName=Turret,ArmorPlateName=REARTURRET)
	ArmorHitZones(41)=(ZoneName=REARTURRETTHREE,PhysBodyBoneName=Turret,ArmorPlateName=REARTURRET)
	ArmorHitZones(42)=(ZoneName=REARTURRETFOUR,PhysBodyBoneName=Turret,ArmorPlateName=REARTURRET)
	ArmorHitZones(43)=(ZoneName=REARTURRETFIVE,PhysBodyBoneName=Turret,ArmorPlateName=REARTURRET)
	ArmorHitZones(44)=(ZoneName=LEFTTURRETFRONT,PhysBodyBoneName=Turret,ArmorPlateName=FRONTURRETSIDES)
	ArmorHitZones(45)=(ZoneName=RIGHTTURRETFRONT,PhysBodyBoneName=Turret,ArmorPlateName=FRONTTURRETSIDES)
	ArmorHitZones(46)=(ZoneName=TURRETFRONTONE,PhysBodyBoneName=Turret,ArmorPlateName=TURRETFRONT)
	ArmorHitZones(47)=(ZoneName=TURRETFRONTTWO,PhysBodyBoneName=Turret,ArmorPlateName=TURRETFRONT)
	ArmorHitZones(48)=(ZoneName=TURRETFRONTTHREE,PhysBodyBoneName=Turret,ArmorPlateName=TURRETFRONT)
	ArmorHitZones(49)=(ZoneName=TURRETFRONTFOUR,PhysBodyBoneName=Turret,ArmorPlateName=TURRETFRONT)
	ArmorHitZones(50)=(ZoneName=TURRETFRONTFIVE,PhysBodyBoneName=Turret,ArmorPlateName=TURRETFRONT)
	ArmorHitZones(51)=(ZoneName=LEFTSIDEMANTLET,PhysBodyBoneName=Turret,ArmorPlateName=LEFTMANTLET)
	ArmorHitZones(52)=(ZoneName=RIGHTSIDEMANTLET,PhysBodyBoneName=Turret,ArmorPlateName=RIGHTMANTLET)
	ArmorHitZones(53)=(ZoneName=MAINTURRETROOF,PhysBodyBoneName=Turret,ArmorPlateName=TURRETROOF)
	ArmorHitZones(54)=(ZoneName=REARTURRETROOF,PhysBodyBoneName=Turret,ArmorPlateName=TURRETROOF)
	ArmorHitZones(55)=(ZoneName=TURRETBUSTLEUNDER,PhysBodyBoneName=Turret,ArmorPlateName=TURRETFLOOR)
	ArmorHitZones(56)=(ZoneName=TURRETVISIONSLITONE,PhysBodyBoneName=Turret,ArmorPlateName=LOADERSVISIONSLIT)
	ArmorHitZones(57)=(ZoneName=TURRETVISIONSLITTWO,PhysBodyBoneName=Turret,ArmorPlateName=GUNNERSVISIONSLIT)
	ArmorHitZones(58)=(ZoneName=HULLVISIONSLIT,PhysBodyBoneName=Chassis,ArmorPlateName=DRIVERSVISIONSLIT)
	ArmorHitZones(59)=(ZoneName=BARRELPLATEUNDER,PhysBodyBoneName=Turret_barrel,ArmorPlateName=BARRELPLATE)
	
	// Actual data for the plates
	ArmorPlates(0)=(PlateName=FRONTGLACIS,ArmorZoneType=AZT_Front,PlateThickness=15,OverallHardness=450,bHighHardness=true)
	ArmorPlates(1)=(PlateName=DRIVERFRONT,ArmorZoneType=AZT_Front,PlateThickness=15,OverallHardness=450,bHighHardness=true)
	ArmorPlates(2)=(PlateName=DRIVERSIDE,ArmorZoneType=AZT_Left,PlateThickness=15,OverallHardness=450,bHighHardness=true)
	ArmorPlates(3)=(PlateName=HULLFRONT,ArmorZoneType=AZT_Front,PlateThickness=15,OverallHardness=450,bHighHardness=true)
	ArmorPlates(4)=(PlateName=FLOOR,ArmorZoneType=AZT_Floor,PlateThickness=7,OverallHardness=450,bHighHardness=true)
	ArmorPlates(5)=(PlateName=REARGLACIS,ArmorZoneType=AZT_Back,PlateThickness=15,OverallHardness=450,bHighHardness=true)
	ArmorPlates(6)=(PlateName=HULLROOF,ArmorZoneType=AZT_Roof,PlateThickness=7,OverallHardness=450,bHighHardness=true)
	ArmorPlates(7)=(PlateName=HULLREAR,ArmorZoneType=AZT_Roof,PlateThickness=15,OverallHardness=450,bHighHardness=true)
	ArmorPlates(8)=(PlateName=ENGINEROOF,ArmorZoneType=AZT_Roof,PlateThickness=7,OverallHardness=450,bHighHardness=true)
	ArmorPlates(9)=(PlateName=LEFTSIDE,ArmorZoneType=AZT_Left,PlateThickness=15,OverallHardness=450,bHighHardness=true)
	ArmorPlates(10)=(PlateName=RIGHTSIDE,ArmorZoneType=AZT_Right,PlateThickness=15,OverallHardness=450,bHighHardness=true)
	ArmorPlates(11)=(PlateName=RADIATORPLATE,ArmorZoneType=AZT_Back,PlateThickness=10,OverallHardness=330,bHighHardness=false)
	ArmorPlates(12)=(PlateName=EQUIPMENTBOXLEFT,ArmorZoneType=AZT_Left,PlateThickness=10,OverallHardness=280,bHighHardness=false)
	ArmorPlates(13)=(PlateName=EQUIPMENTBOXRIGHT,ArmorZoneType=AZT_Right,PlateThickness=10,OverallHardness=280,bHighHardness=false)
	ArmorPlates(14)=(PlateName=MANTLET,ArmorZoneType=AZT_Front,PlateThickness=15,OverallHardness=450,bHighHardness=true)
	ArmorPlates(15)=(PlateName=LEFTTURRETSIDE,ArmorZoneType=AZT_TurretLeft,PlateThickness=15,OverallHardness=450,bHighHardness=true)
	ArmorPlates(16)=(PlateName=LEFTTURRETBUSTLE,ArmorZoneType=AZT_TurretLeft,PlateThickness=15,OverallHardness=450,bHighHardness=true)
	ArmorPlates(17)=(PlateName=RIGHTTURRETBUSTLE,ArmorZoneType=AZT_TurretRight,PlateThickness=15,OverallHardness=450,bHighHardness=true)
	ArmorPlates(18)=(PlateName=RIGHTTURRETSIDE,ArmorZoneType=AZT_TurretRight,PlateThickness=15,OverallHardness=450,bHighHardness=true)
	ArmorPlates(19)=(PlateName=REARTURRET,ArmorZoneType=AZT_TurretBack,PlateThickness=15,OverallHardness=450,bHighHardness=true)
	ArmorPlates(20)=(PlateName=FRONTURRETSIDES,ArmorZoneType=AZT_TurretFront,PlateThickness=15,OverallHardness=450,bHighHardness=true)
	ArmorPlates(21)=(PlateName=TURRETFRONT,ArmorZoneType=AZT_TurretFront,PlateThickness=15,OverallHardness=450,bHighHardness=true)
	ArmorPlates(22)=(PlateName=LEFTMANTLET,ArmorZoneType=AZT_TurretLeft,PlateThickness=15,OverallHardness=450,bHighHardness=true)
	ArmorPlates(23)=(PlateName=RIGHTMANTLET,ArmorZoneType=AZT_TurretRight,PlateThickness=15,OverallHardness=450,bHighHardness=true)
	ArmorPlates(24)=(PlateName=TURRETROOF,ArmorZoneType=AZT_TurretRoof,PlateThickness=7,OverallHardness=450,bHighHardness=true)
	ArmorPlates(25)=(PlateName=TURRETFLOOR,ArmorZoneType=AZT_TurretBack,PlateThickness=7,OverallHardness=450,bHighHardness=true)
	ArmorPlates(26)=(PlateName=BARRELPLATE,ArmorZoneType=AZT_TurretFront,PlateThickness=15,OverallHardness=450,bHighHardness=true)
	ArmorPlates(27)=(PlateName=LOADERSVISIONSLIT,ArmorZoneType=AZT_WeakSpots,PlateThickness=5,OverallHardness=100,bHighHardness=false)
	ArmorPlates(28)=(PlateName=GUNNERSVISIONSLIT,ArmorZoneType=AZT_WeakSpots,PlateThickness=5,OverallHardness=100,bHighHardness=false)
	ArmorPlates(29)=(PlateName=DRIVERSVISIONSLIT,ArmorZoneType=AZT_WeakSpots,PlateThickness=5,OverallHardness=100,bHighHardness=false)
	
	VehicleEffects(TankVFX_Firing1)=(EffectStartTag=T26_Cannon,EffectTemplate=ParticleSystem'WinterWar_FX.ParticleSystems.FX_WW_MuzzleFlash_T-26',EffectSocket=Barrel)
	VehicleEffects(TankVFX_Firing3)=(EffectStartTag=T26_CoaxMG,EffectTemplate=ParticleSystem'FX_VN_Weapons.MuzzleFlashes.FX_VN_MuzzleFlash_1stP_Rifles_split',EffectSocket=CoaxMG)
	VehicleEffects(TankVFX_Exhaust)=(EffectStartTag=EngineStart,EffectEndTag=EngineStop,EffectTemplate=ParticleSystem'WinterWar_FX.ParticleSystems.FX_VEH_exhaust',EffectSocket=Exhaust)
	VehicleEffects(TankVFX_TreadWing)=(EffectStartTag=EngineStart,EffectEndTag=EngineStop,bStayActive=true,EffectTemplate=ParticleSystem'WinterWar_FX.ParticleSystems.FX_VEH_LightTank_A_Wing_Snow',EffectSocket=attachments_body_ground)
	
	ExplosionDamageType=class'RODmgType_VehicleExplosion'
	ExplosionDamage=100.0
	ExplosionRadius=300.0
	ExplosionMomentum=60000
	ExplosionInAirAngVel=1.5
	InnerExplosionShakeRadius=400.0
	OuterExplosionShakeRadius=1000.0
	ExplosionLightClass=class'ROGame.ROGrenadeExplosionLight'
	MaxExplosionLightDistance=4000.0
	TimeTilSecondaryVehicleExplosion=2.0f
	SecondaryExplosion=ParticleSystem'WinterWar_FX.ParticleSystems.FX_VEH_Tank_C_Explosion'
	bHasTurretExplosion=true
	TurretExplosiveForce=15000
	
	BigExplosionSocket=FX_Fire
	ExplosionTemplate=ParticleSystem'WinterWar_FX.ParticleSystems.FX_VEH_Tank_C_Explosion'
	
	BurnOutPhase1Timer=15.0
	BurnOutPhase2Timer=20.0
	BurnOutPhase3Timer=25.0
	
	FireEmitterSocketName=attachments_engine
	
	CannonFireImpulseMag=20000
	CannonFireTorqueMag=9375
	
	RPM3DGaugeMaxAngle=54613
	EngineIdleRPM=600
	EngineNormalRPM=3000
	EngineMaxRPM=3750
	Speedo3DGaugeMaxAngle=109226
	EngineTemp3DGaugeNormalAngle=33000
	EngineTemp3DGaugeEngineDamagedAngle=39500
	EngineTemp3DGaugeFireDamagedAngle=47500
	EngineTemp3DGaugeFireDestroyedAngle=54613
	
	EngineTextureOffset=(PositionOffset=(X=58,Y=83,Z=0),MySizeX=24,MYSizeY=24)
	
	TreadTextureOffsets(0)=(PositionOffset=(X=36,Y=34,Z=0),MySizeX=8,MYSizeY=70)
	TreadTextureOffsets(1)=(PositionOffset=(X=95,Y=34,Z=0),MySizeX=8,MYSizeY=70)
	
	SeatTextureOffsets(0)=(PositionOffSet=(X=+10,Y=-10,Z=0),bTurretPosition=0)
	SeatTextureOffsets(1)=(PositionOffSet=(X=-7,Y=+2,Z=0),bTurretPosition=1)
	
	SpeedoMinDegree=5461
	SpeedoMaxDegree=60075
	SpeedoMaxSpeed=1365
	
	RelatedDamageTypes(0)=class'WWDmgType_DT_T26'
	RelatedDamageTypes(1)=class'WWDmgType_T26Shell_AP'
	RelatedDamageTypes(2)=class'WWDmgType_T26Shell_AP_General'
	RelatedDamageTypes(3)=class'WWDmgType_T26Shell_HE'
	RelatedDamageTypes(4)=class'WWDmgType_T26Shell_HEImpact'
	RelatedDamageTypes(5)=class'WWDmgType_T26Shell_HEImpactGeneral'
}
