
class WWWeapon_M20_Attach extends ROWeaponAttachment;

defaultproperties
{
	TriggerHoldDuration=0.2
	
	CarrySocketName=WeaponSling
	
	ThirdPersonHandsAnim=Type100_Handpose
	
	IKProfileName=mp40
	
	Begin Object Name=SkeletalMeshComponent0
		SkeletalMesh=SkeletalMesh'WinterWar_WP_FIN_M20.Mesh.FIN_M20_3rd'
		AnimSets(0)=AnimSet'WinterWar_WP_FIN_M20.Anim.Type100_3rd_Anims'
		PhysicsAsset=PhysicsAsset'WinterWar_WP_FIN_M20.Phy.FIN_M20_3rd_Bounds'
		CullDistance=5000
	End Object
	
	MuzzleFlashSocket=MuzzleFlashSocket
	MuzzleFlashPSCTemplate=ParticleSystem'FX_VN_Weapons.MuzzleFlashes.FX_VN_MuzzleFlash_3rdP_SMG'
	MuzzleFlashDuration=0.33
	MuzzleFlashLightClass=class'ROGame.RORifleMuzzleFlashLight'
	
	WeaponClass=class'WWWeapon_M20'
	
	ShellEjectSocket=ShellEjectSocket
	ShellEjectPSCTemplate=ParticleSystem'FX_VN_Weapons.ShellEjects.FX_Wep_ShellEject_VC_PPSH'
	
	CHR_AnimSet=AnimSet'WinterWar_WP_FIN_M20.Anim.CHR_Type100SMG'
	
	FireAnim=Shoot
	FireLastAnim=Shoot_Last
	IdleAnim=Idle
	IdleEmptyAnim=Idle_Empty
}
