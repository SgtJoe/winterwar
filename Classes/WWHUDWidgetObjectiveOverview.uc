
class WWHUDWidgetObjectiveOverview extends ROHUDWidgetObjectiveOverview;

defaultproperties
{
	ObjectiveNeutralIcon=						Texture2D'WinterWar_UI.Objective.64x64.Objective_64_Neutral'
	ObjectiveNeutralUncontestedBorder=			Texture2D'WinterWar_UI.Objective.64x64.Objective_64_Neutral'
	ObjectiveStatusNorthAttackingNeutral=		Texture2D'WinterWar_UI.Objective.64x64.Objective_64_Neutral_FIN_Taking_Progress'
	ObjectiveStatusSouthAttackingNeutral=		Texture2D'WinterWar_UI.Objective.64x64.Objective_64_Neutral_SOV_Taking_Progress'
	ObjectiveStatusNorthAttackingNeutralBorder=	Texture2D'WinterWar_UI.Objective.64x64.Objective_64_Neutral_Taking'
	ObjectiveStatusSouthAttackingNeutralBorder=	Texture2D'WinterWar_UI.Objective.64x64.Objective_64_Neutral_Taking'
	
	NeutralUpperEdgeMargin=0.22, // 28px
	NeutralLowerEdgeMargin=0.22, // 28px
	
	NorthIcons(0)={(
		UpperEdgeMargin=0.22, // 28px
		LowerEdgeMargin=0.22, // 28px
		ObjIconRed=					Texture2D'WinterWar_UI.Objective.64x64.Objective_64_FIN',
		ObjIconBlue=				Texture2D'WinterWar_UI.Objective.64x64.Objective_64_FIN_Losing_Progress',
		ObjBorder=					Texture2D'WinterWar_UI.Objective.64x64.Objective_64_Neutral',
		CappingObjBorderRed=		Texture2D'WinterWar_UI.Objective.64x64.Objective_64_Neutral_Taking',
		CappingObjBorderBlue=		Texture2D'WinterWar_UI.Objective.64x64.Objective_64_Neutral_Taking',
		
		HomeObjIconRed=				Texture2D'WinterWar_UI.Objective.64x64.Objective_64_FINHQ',
		HomeObjIconBlue=			Texture2D'WinterWar_UI.Objective.64x64.Objective_64_FINHQ_Losing_Progress',
		HomeObjEnemyHeldIconRed=	Texture2D'WinterWar_UI.Objective.64x64.Objective_64_SOVHQ_Losing_Progress',
		HomeObjEnemyHeldIconBlue=	Texture2D'WinterWar_UI.Objective.64x64.Objective_64_SOVHQ',
		HomeObjNeutralRed=			Texture2D'WinterWar_UI.Objective.64x64.Objective_64_FINHQ_Neutral',
		HomeObjNeutralBlue=			Texture2D'WinterWar_UI.Objective.64x64.Objective_64_FINHQ_Neutral'
	)}
	
	SouthIcons(0)={(
		UpperEdgeMargin=0.22, // 28px
		LowerEdgeMargin=0.22, // 28px
		ObjIconRed=					Texture2D'WinterWar_UI.Objective.64x64.Objective_64_SOV_Losing_Progress',
		ObjIconBlue=				Texture2D'WinterWar_UI.Objective.64x64.Objective_64_SOV',
		ObjBorder=					Texture2D'WinterWar_UI.Objective.64x64.Objective_64_Neutral',
		CappingObjBorderRed=		Texture2D'WinterWar_UI.Objective.64x64.Objective_64_Neutral_Taking',
		CappingObjBorderBlue=		Texture2D'WinterWar_UI.Objective.64x64.Objective_64_Neutral_Taking',
		
		HomeObjIconRed=				Texture2D'WinterWar_UI.Objective.64x64.Objective_64_SOVHQ_Losing',
		HomeObjIconBlue=			Texture2D'WinterWar_UI.Objective.64x64.Objective_64_SOVHQ',
		HomeObjEnemyHeldIconRed=	Texture2D'WinterWar_UI.Objective.64x64.Objective_64_FINHQ',
		HomeObjEnemyHeldIconBlue=	Texture2D'WinterWar_UI.Objective.64x64.Objective_64_FINHQ_Losing_Progress',
		HomeObjNeutralRed=			Texture2D'WinterWar_UI.Objective.64x64.Objective_64_SOVHQ_Neutral',
		HomeObjNeutralBlue=			Texture2D'WinterWar_UI.Objective.64x64.Objective_64_SOVHQ_Neutral'
	)}
}
