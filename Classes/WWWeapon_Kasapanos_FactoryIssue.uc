
class WWWeapon_Kasapanos_FactoryIssue extends WWWeapon_Kasapanos
	abstract;

`include(WinterWar\Classes\WWWeaponPickupMessagesOverride.uci)

defaultproperties
{
	WeaponContentClass(0)="WinterWar.WWWeapon_Kasapanos_FactoryIssue_ActualContent"
	
	RoleSelectionImage(0)=Texture2D'WinterWar_UI.WP_Render.WP_Render_Kasapanos_FI'
	
	InvIndex=`WI_KASAPANOS_FI
	
	AmmoClass=class'ROAmmo_Type67_Grenade'
	
	WeaponProjectiles(DEFAULT_FIREMODE)=class'WWProjectile_Kasapanos_FI'
	WeaponProjectiles(ALTERNATE_FIREMODE)=class'WWProjectile_Kasapanos_FI'
}
