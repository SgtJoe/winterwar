
class WWVehicleProjectile_T26_HE extends WWVehicleProjectile_HE;

defaultproperties
{
	ImpactDamageType=	class'WWDmgType_T26Shell_HEImpact'
	GeneralDamageType=	class'WWDmgType_T26Shell_HEImpactGeneral'
	MyDamageType=		class'WWDmgType_T26Shell_HE'
}
