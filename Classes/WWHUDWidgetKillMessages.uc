
class WWHUDWidgetKillMessages extends ROHUDWidgetKillMessages
	config(Game_WinterWar_Client);

var Texture2D T20KillTexture, T26KillTexture, T28KillTexture, HT130KillTexture;

var Texture2D ATGunKillTexture;

var Texture2D MaximKillTexture, QuadMaximKillTexture;

var Texture2D DyakonovKillTexture;

function Texture2D GetNonWeaponKillTextureFromDamageType(class<DamageType> InDamageType, byte InKillType, out string WeaponShortName, out int KillIconX, out int KillIconY, optional out byte WasSpecificHitZoneKill)
{
	KillIconX = 64;
	KillIconY = 64;
	
	WeaponShortName = GetWeaponShortNameFromDamageClass(InDamageType, InKillType);
	
	WasSpecificHitZoneKill = 0;
	
	if ( ClassIsChildOf(InDamageType, class'RODmgTypeArtillery') )
	{
		return ArtilleryKillTexture;
	}
	else if ( InDamageType == class'RODmgType_MeleeBlunt' )
	{
		return MeleeKillTexture;
	}
	else if ( InDamageType == class'RODmgType_MeleePierce' )
	{
		return BayonetteKillTexture;
	}
	// TB-3 Bombs
	else if ( InDamageType == class'RODmgType_DroppedBomb' )
	{
		return DroppedBombKillTexture;
	}
	else if ( InDamageType == class'RODmgType_AirCrash' )
	{
		return AirCrashKillTexture;
	}
	else if ( InDamageType == class'RODmgType_Fire' )
	{
		return FireKillTexture;
	}
	
	// Generic Vehicle-Related
	else if ( InDamageType == class'WWDmgType_VehicleFire' || InDamageType == class'RODmgType_VehicleExplosion' )
	{
		return EngineKillTexture;
	}
	else if( InDamageType == class'WWDmgType_RoadRage' )
	{
		KillIconX = 128;
		return HeloCrushedKillTexture;
	}
	
	// T-20 Komsomolets
	else if (class'WWVehicle_T20'.static.IsDamageTypeRelated(InDamageType))
	{
		return T20KillTexture;
	}
	// T-26 Light Tank
	else if (class'WWVehicle_T26'.static.IsDamageTypeRelated(InDamageType))
	{
		return T26KillTexture;
	}
	// T-28 Heavy Tank
	else if (class'WWVehicle_T28'.static.IsDamageTypeRelated(InDamageType))
	{
		return T28KillTexture;
	}
	// HT-130 Flamethrower Tank
	else if (class'WWVehicle_HT130'.static.IsDamageTypeRelated(InDamageType))
	{
		return HT130KillTexture;
	}
	
	// 53-K Static Anti-Tank Cannon
	else if (class'WWVehicle_53K'.static.IsDamageTypeRelated(InDamageType))
	{
		return ATGunKillTexture;
	}
	
	// Static HMGs
	else if( InDamageType == class'WWDmgType_Maxim' )
	{
		KillIconX = 128;
		return MaximKillTexture;
	}
	else if( InDamageType == class'WWDmgType_QuadMaxim' )
	{
		KillIconX = 128;
		return QuadMaximKillTexture;
	}
	
	// Dyakonov Grenade
	else if( InDamageType == class'WWDmgType_DyakonovGrenade' || InDamageType == class'WWDmgType_DyakonovGrenadeImpact' )
	{
		return DyakonovKillTexture;
	}
	
	else if ( InKillType == ROKT_Suicide )
	{
		return SuicideKillTexture;
	}
	else if ( InDamageType == class'DmgType_Fell' )
	{
		return FellKillTexture;
	}
	else if ( InKillType == ROKT_HeadShot )
	{
		WasSpecificHitZoneKill = 1;
		return HeadShotKillTexture;
	}
	else if ( InKillType == ROKT_Heartshot )
	{
		WasSpecificHitZoneKill = 1;
		return HeartShotKillTexture;
	}
	else if(InKillType == ROKT_UpperSpine)
	{
		WasSpecificHitZoneKill = 1;
		return UpperSpineKillTexture;
	}
	else if(InKillType == ROKT_LowerSpine)
	{
		WasSpecificHitZoneKill = 1;
		return LowerSpineKillTexture;
	}
	
	return none;
}

static function class<ROWeapon> GetWeaponClassByIndex(int InvIndex)
{
	switch (InvIndex)
	{
		case `WI_BINOCS:
			return class'WWWeapon_Binoculars';
		
		case `WI_DP28:
			return class'WWWeapon_DP28';
		
		case `WI_F1GRENADE:
			return class'WWWeapon_F1Grenade';
		
		case `WI_KASAPANOS_IM:
			return class'WWWeapon_Kasapanos_Improvised_ActualContent';
		
		case `WI_KASAPANOS_FI:
			return class'WWWeapon_Kasapanos_FactoryIssue_ActualContent';
		
		case `WI_KP31:
			return class'WWWeapon_KP31';
		
		case `WI_L35:
			return class'WWWeapon_L35';
		
		case `WI_LS26:
			return class'WWWeapon_LahtiSaloranta';
		
		case `WI_LUGER:
			return class'WWWeapon_Luger';
		
		case `WI_M32GRENADE:
			return class'WWWeapon_M32Grenade';
		
		case `WI_MN27:
			return class'WWWeapon_MN27';
		
		case `WI_MN38:
			return class'WWWeapon_MN38';
		
		case `WI_MN91:
			return class'WWWeapon_MN91';
		
		case `WI_MN9130:
			return class'WWWeapon_MN9130';
		
		case `WI_MN9130_SCOPED:
			return class'WWWeapon_MN9130_Scoped';
		
		case `WI_MOLOTOV_IM:
		case `WI_MOLOTOV_FI:
			return class'WWWeapon_Molotov';
		
		case `WI_NAGANT:
			return class'WWWeapon_NagantRevolver';
		
		case `WI_SATCHEL:
			return class'WWWeapon_Satchel';
		
		case `WI_SVT38:
			return class'WWWeapon_SVT38';
		
		case `WI_TT33:
			return class'WWWeapon_TT33';
		
		case `WI_RGD33:
			return class'WWWeapon_RGD33';
		
		case `WI_AVS36:
			return class'WWWeapon_AVS36';
		
		case `WI_PPD34:
			return class'WWWeapon_PPD34';
		
		case `WI_M20:
			return class'WWWeapon_M20';
		
		case `ARTILLERY_STAT_INVINDEX:
			return class'ROWeaponArtillery';
		
		case `TANKSHELL_STAT_INVINDEX:
			return class'ROVWeap_TankTurret';
	}
	
	return none;
}

static function int GetInvIndexFromDamageType(class<DamageType> DamageType)
{
	switch ( DamageType )
	{
		case class'WWWeapon_DP28'.default.InstantHitDamageTypes[0]:
		case class'WWWeapon_DP28'.default.InstantHitDamageTypes[1]:
			return `WI_DP28;
		
		case class'WWWeapon_F1Grenade'.default.WeaponProjectiles[0].default.MyDamageType:
		case class'WWWeapon_F1Grenade'.default.WeaponProjectiles[1].default.MyDamageType:
			return `WI_F1GRENADE;
		
		case class'WWWeapon_Kasapanos_Improvised_ActualContent'.default.WeaponProjectiles[0].default.MyDamageType:
		case class'WWWeapon_Kasapanos_Improvised_ActualContent'.default.WeaponProjectiles[1].default.MyDamageType:
			return `WI_KASAPANOS_IM;
		
		case class'WWWeapon_Kasapanos_FactoryIssue_ActualContent'.default.WeaponProjectiles[0].default.MyDamageType:
		case class'WWWeapon_Kasapanos_FactoryIssue_ActualContent'.default.WeaponProjectiles[1].default.MyDamageType:
			return `WI_KASAPANOS_FI;
		
		case class'WWWeapon_KP31'.default.InstantHitDamageTypes[0]:
		case class'WWWeapon_KP31'.default.InstantHitDamageTypes[1]:
			return `WI_KP31;
		
		case class'WWWeapon_L35'.default.InstantHitDamageTypes[0]:
		case class'WWWeapon_L35'.default.InstantHitDamageTypes[1]:
			return `WI_L35;
		
		case class'WWWeapon_LahtiSaloranta'.default.InstantHitDamageTypes[0]:
		case class'WWWeapon_LahtiSaloranta'.default.InstantHitDamageTypes[1]:
			return `WI_LS26;
		
		case class'WWWeapon_Luger'.default.InstantHitDamageTypes[0]:
		case class'WWWeapon_Luger'.default.InstantHitDamageTypes[1]:
			return `WI_LUGER;
		
		case class'WWWeapon_M32Grenade'.default.WeaponProjectiles[0].default.MyDamageType:
		case class'WWWeapon_M32Grenade'.default.WeaponProjectiles[1].default.MyDamageType:
			return `WI_M32GRENADE;
		
		case class'WWWeapon_MN27'.default.InstantHitDamageTypes[0]:
		case class'WWWeapon_MN27'.default.InstantHitDamageTypes[1]:
			return `WI_MN27;
		
		case class'WWWeapon_MN38'.default.InstantHitDamageTypes[0]:
		case class'WWWeapon_MN38'.default.InstantHitDamageTypes[1]:
			return `WI_MN38;
		
		case class'WWWeapon_MN91'.default.InstantHitDamageTypes[0]:
		case class'WWWeapon_MN91'.default.InstantHitDamageTypes[1]:
			return `WI_MN91;
		
		case class'WWWeapon_MN9130'.default.InstantHitDamageTypes[0]:
		case class'WWWeapon_MN9130'.default.InstantHitDamageTypes[1]:
			return `WI_MN9130;
		
		case class'WWWeapon_MN9130_Scoped'.default.InstantHitDamageTypes[0]:
		case class'WWWeapon_MN9130_Scoped'.default.InstantHitDamageTypes[1]:
			return `WI_MN9130_SCOPED;
		
		case class'WWWeapon_Molotov'.default.WeaponProjectiles[0].default.MyDamageType:
		case class'WWWeapon_Molotov'.default.WeaponProjectiles[1].default.MyDamageType:
			return `WI_MOLOTOV_IM;
		/* 
		case class'WWWeapon_Molotov'.default.WeaponProjectiles[0].default.MyDamageType:
		case class'WWWeapon_Molotov'.default.WeaponProjectiles[1].default.MyDamageType:
			return `WI_MOLOTOV_FI;
		 */
		case class'WWWeapon_NagantRevolver'.default.InstantHitDamageTypes[0]:
		case class'WWWeapon_NagantRevolver'.default.InstantHitDamageTypes[1]:
			return `WI_NAGANT;
		
		case class'WWWeapon_Satchel'.default.WeaponProjectiles[0].default.MyDamageType:
		case class'WWWeapon_Satchel'.default.WeaponProjectiles[1].default.MyDamageType:
			return `WI_SATCHEL;
		
		case class'WWWeapon_SVT38'.default.InstantHitDamageTypes[0]:
		case class'WWWeapon_SVT38'.default.InstantHitDamageTypes[1]:
			return `WI_SVT38;
		
		case class'WWWeapon_TT33'.default.InstantHitDamageTypes[0]:
		case class'WWWeapon_TT33'.default.InstantHitDamageTypes[1]:
			return `WI_TT33;
		
		case class'WWWeapon_RGD33'.default.WeaponProjectiles[0].default.MyDamageType:
		case class'WWWeapon_RGD33'.default.WeaponProjectiles[1].default.MyDamageType:
			return `WI_RGD33;
		
		case class'WWWeapon_AVS36'.default.InstantHitDamageTypes[0]:
		case class'WWWeapon_AVS36'.default.InstantHitDamageTypes[1]:
			return `WI_AVS36;
		
		case class'WWWeapon_PPD34'.default.InstantHitDamageTypes[0]:
		case class'WWWeapon_PPD34'.default.InstantHitDamageTypes[1]:
			return `WI_PPD34;
		
		case class'WWWeapon_M20'.default.InstantHitDamageTypes[0]:
		case class'WWWeapon_M20'.default.InstantHitDamageTypes[1]:
			return `WI_M20;
		
		case class'RODmgTypeArtillery':
			return `ARTILLERY_STAT_INVINDEX;
	}
	
	return -1;
}

defaultproperties
{
	MaximKillTexture=Texture2D'WinterWar_UI.WP_KillIcon.WP_KillIcon_Maxim'
	QuadMaximKillTexture=Texture2D'WinterWar_UI.WP_KillIcon.WP_KillIcon_QuadMaxim'
	
	DyakonovKillTexture=Texture2D'WinterWar_UI.WP_KillIcon.WP_KillIcon_Dyakonov'
	
	EngineKillTexture=Texture2D'WinterWar_UI.WP_KillIcon.WP_KillIcon_TankFire'
	
	DroppedBombKillTexture=Texture2D'VN_UI_Textures_Three.HUD.DeathMessage.UI_Kill_Icon_Canberra'
	
	HeloCrushedKillTexture=Texture2D'WinterWar_UI.Vehicle.UI_Kill_Icon_RoadRage'
	
	T20KillTexture=Texture2D'WinterWar_UI.WP_KillIcon.WP_KillIcon_T20'
	T26KillTexture=Texture2D'WinterWar_UI.WP_KillIcon.WP_KillIcon_T26'
	T28KillTexture=Texture2D'WinterWar_UI.WP_KillIcon.WP_KillIcon_T28'
	HT130KillTexture=Texture2D'WinterWar_UI.WP_KillIcon.WP_KillIcon_HT130'
	
	ATGunKillTexture=Texture2D'WinterWar_UI.WP_KillIcon.WP_KillIcon_53-K'
}
