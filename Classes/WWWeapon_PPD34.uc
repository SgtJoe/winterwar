
class WWWeapon_PPD34 extends ROWeap_PPSH41_SMG
	abstract;

`include(WinterWar\Classes\WWWeaponPickupMessagesOverride.uci)

defaultproperties
{
	WeaponContentClass.Empty
	WeaponContentClass(0)="WinterWar.WWWeapon_PPD34_ActualContent"
	
	AltAmmoLoadouts.Empty
	AmmoContentClassStart=0
	
	RoleSelectionImage.Empty
	RoleSelectionImage(0)=Texture2D'WinterWar_UI.WP_Render.WP_Render_PPD'
	
	TeamIndex=`ALLIES_TEAM_INDEX
	
	InvIndex=`WI_PPD34
	
	WeaponProjectiles(DEFAULT_FIREMODE)=	class'WWProjectile_PPD'
	WeaponProjectiles(ALTERNATE_FIREMODE)=	class'WWProjectile_PPD'
	
	FireInterval(0)=+0.075 // 800rpm
	
	AmmoClass=class'ROAmmo_762x25_PPSHStick'
	MaxAmmoCount=25
	bUsesMagazines=true
	InitialNumPrimaryMags=5
	bPlusOneLoading=false
	bCanReloadNonEmptyMag=true
	PenetrationDepth=12
	MaxPenetrationTests=3
	MaxNumPenetrations=2
	PerformReloadPct=0.85f
	
	maxRecoilPitch=200//150
	minRecoilPitch=200//150//115
	maxRecoilYaw=80//95
	minRecoilYaw=-80//-35
	
	
	PreFireTraceLength=1250 //25 Meters
	
	InstantHitDamage(0)=63
	InstantHitDamage(1)=63
	
	InstantHitDamageTypes(0)=class'WWDmgType_PPD'
	InstantHitDamageTypes(1)=class'WWDmgType_PPD'
	
	MuzzleFlashPSCTemplate=ParticleSystem'WinterWar_FX.ParticleSystems.FX_WW_MuzzleFlash_1stP_SMG'
	
	PlayerViewOffset=(X=5.5,Y=2.0,Z=-0.8)
	ShoulderedPosition=(X=5.5,Y=1.5,Z=-0.5)
	IronSightPosition=(X=4.0,Y=0,Z=0.0)
	
	WeaponEquipAnim=PPD_pullout
	WeaponPutDownAnim=PPD_putaway
	
	WeaponUpAnim=PPD_pullout//PPD_Up
	WeaponDownAnim=PPD_putaway//PPD_Down
	
	WeaponFireAnim(0)=PPD_shoot
	WeaponFireAnim(1)=PPD_shoot
	WeaponFireLastAnim=PPD_shoot_last
	
	WeaponFireShoulderedAnim(0)=PPD_shoot
	WeaponFireShoulderedAnim(1)=PPD_shoot
	WeaponFireLastShoulderedAnim=PPD_shoot_last
	
	WeaponFireSightedAnim(0)=PPD_shoot
	WeaponFireSightedAnim(1)=PPD_shoot
	WeaponFireLastSightedAnim=PPD_shoot_last
	
	WeaponIdleAnims(0)=PPD_idle
	WeaponIdleAnims(1)=PPD_idle
	WeaponIdleShoulderedAnims(0)=PPD_idle
	WeaponIdleShoulderedAnims(1)=PPD_idle
	
	WeaponIdleSightedAnims(0)=PPD_iron_idle
	WeaponIdleSightedAnims(1)=PPD_iron_idle
	
	WeaponCrawlingAnims(0)=PPD_Crawl
	WeaponCrawlStartAnim=PPD_Crawl_into
	WeaponCrawlEndAnim=PPD_Crawl_out
	
	WeaponReloadNonEmptyMagAnim=PPD_reloadhalf
	WeaponReloadEmptyMagAnim=PPD_reloadempty
	
	WeaponAmmoCheckAnim=PPD_ammocheck
	
	WeaponSprintStartAnim=PPD_sprint_into
	WeaponSprintLoopAnim=PPD_sprint
	WeaponSprintEndAnim=PPD_sprint_out
	
	Weapon1HSprintStartAnim=PPD_ger_sprint_into
	Weapon1HSprintLoopAnim=PPD_ger_sprint
	Weapon1HSprintEndAnim=PPD_ger_sprint_out
	
	WeaponMantleOverAnim=PPD_Mantle
	
	WeaponSwitchToAltFireModeAnim=PPD_autoTOsemi
	WeaponSwitchToDefaultFireModeAnim=PPD_semiTOauto
	
	WeaponSpotEnemyAnim=PPD_idle//PPD_SpotEnemy
	WeaponSpotEnemySightedAnim=PPD_iron_idle//PPD_SpotEnemy_ironsight
	
	WeaponMeleeAnims(0)=PPD_Bash
	WeaponMeleeHardAnim=PPD_BashHard
	MeleePullbackAnim=PPD_Pullback
	MeleeHoldAnim=PPD_Pullback_Hold
	
	BoltControllerNames[0]=BoltSlide_PPD
	
	FireModeRotControlName=FireSelect_PPD
	
	SightRanges.Empty
	// SightRanges[0]=(SightRange=100,SightPitch=16384,SightPositionOffset=-0.03)
	
	bLoopingFireSnd(DEFAULT_FIREMODE)=false
	bLoopHighROFSounds(DEFAULT_FIREMODE)=false
	
	WeaponFireSnd(DEFAULT_FIREMODE)=(DefaultCue=AkEvent'WW_WEP_Makarov.Play_WEP_Makarov_Fire_Single_3P', FirstPersonCue=AkEvent'WW_WEP_Makarov.Play_WEP_Makarov_Fire_Single')
	WeaponFireSnd(ALTERNATE_FIREMODE)=(DefaultCue=AkEvent'WW_WEP_Makarov.Play_WEP_Makarov_Fire_Single_3P', FirstPersonCue=AkEvent'WW_WEP_Makarov.Play_WEP_Makarov_Fire_Single')
}
