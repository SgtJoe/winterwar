
class WWWeapon_L35 extends ROProjectileWeapon
	abstract;

`include(WinterWar\Classes\WWWeaponPickupMessagesOverride.uci)

defaultproperties
{
	WeaponContentClass(0)="WinterWar.WWWeapon_L35_ActualContent"
	
	RoleSelectionImage(0)=Texture2D'WinterWar_UI.WP_Render.WP_Render_L35'
	
	WeaponClassType=ROWCT_HandGun
	
	TeamIndex=`AXIS_TEAM_INDEX
	
	InvIndex=`WI_L35
	
	Category=ROIC_Secondary
	Weight=0.90
	InventoryWeight=0
	RoleEncumbranceModifier=0.0
	
	PlayerIronSightFOV=60.0
	
	// MAIN FIREMODE
	FiringStatesArray(0)=WeaponSingleFiring
	WeaponFireTypes(0)=EWFT_Custom
	WeaponProjectiles(0)=class'WWProjectile_L35'
	FireInterval(0)=+0.15
	Spread(0)=0.013 //0.011
	
	// ALT FIREMODE
	FiringStatesArray(ALTERNATE_FIREMODE)=none
	//WeaponFireTypes(ALTERNATE_FIREMODE)=EWFT_Custom
	WeaponProjectiles(ALTERNATE_FIREMODE)=none
	FireInterval(ALTERNATE_FIREMODE)=+0.15
	Spread(ALTERNATE_FIREMODE)=0
	
	PreFireTraceLength=1250 //25 Meters
	
	ShoulderedSpreadMod=6.0
	HippedSpreadMod=10.0
	
	MinBurstAmount=1
	MaxBurstAmount=3
	BurstWaitTime=1.5
	AISpreadScale=20.0
	
	maxRecoilPitch=500
	minRecoilPitch=350
	maxRecoilYaw=130
	minRecoilYaw=-85
	RecoilRate=0.125
	RecoilMaxYawLimit=500
	RecoilMinYawLimit=65035
	RecoilMaxPitchLimit=750
	RecoilMinPitchLimit=64785
	RecoilISMaxYawLimit=500
	RecoilISMinYawLimit=65035
	RecoilISMaxPitchLimit=500
	RecoilISMinPitchLimit=65035
	RecoilBlendOutRatio=0.65
	
	InstantHitDamage(0)=69
	InstantHitDamage(1)=69
	
	InstantHitDamageTypes(0)=class'WWDmgType_L35'
	InstantHitDamageTypes(1)=class'WWDmgType_L35'
	
	MuzzleFlashSocket=MuzzleFlashSocket
	MuzzleFlashPSCTemplate=ParticleSystem'WinterWar_FX.ParticleSystems.FX_WW_MuzzleFlash_1stP_Pistol'
	MuzzleFlashDuration=0.33
	MuzzleFlashLightClass=class'ROGame.RORifleMuzzleFlashLight'
	
	ShellEjectSocket=ShellEjectSocket
	ShellEjectPSCTemplate=ParticleSystem'FX_VN_Weapons.ShellEjects.FX_Wep_ShellEject_USA_M1911A1Pistol'
	
	bHasIronSights=true
	
	PlayerViewOffset=(X=7.0,Y=8.0,Z=-5)
	ShoulderedPosition=(X=8.5,Y=4.0,Z=-2.0)
	IronSightPosition=(X=5.0,Y=0,Z=0.0)
	
	WeaponEquipAnim=Lahti_pullout
	WeaponPutDownAnim=Lahti_putaway
	
	WeaponUpAnim=Lahti_pullout//Lahti_Up
	WeaponDownAnim=Lahti_putaway//Lahti_Down
	
	WeaponFireAnim(0)=Lahti_shoot
	WeaponFireAnim(1)=Lahti_shoot
	WeaponFireLastAnim=Lahti_shoot_last
	
	WeaponFireShoulderedAnim(0)=Lahti_shoot
	WeaponFireShoulderedAnim(1)=Lahti_shoot
	WeaponFireLastShoulderedAnim=Lahti_shoot_last
	
	WeaponFireSightedAnim(0)=Lahti_shoot
	WeaponFireSightedAnim(1)=Lahti_shoot
	WeaponFireLastSightedAnim=Lahti_shoot_last
	
	WeaponIdleAnims(0)=Lahti_idle
	WeaponIdleAnims(1)=Lahti_idle
	WeaponIdleShoulderedAnims(0)=Lahti_idle
	WeaponIdleShoulderedAnims(1)=Lahti_idle
	
	WeaponIdleSightedAnims(0)=Lahti_iron_idle
	WeaponIdleSightedAnims(1)=Lahti_iron_idle
	
	WeaponCrawlingAnims(0)=Lahti_Crawl
	WeaponCrawlStartAnim=Lahti_Crawl_into
	WeaponCrawlEndAnim=Lahti_Crawl_out
	
	WeaponReloadNonEmptyMagAnim=Lahti_reloadhalf
	WeaponReloadEmptyMagAnim=Lahti_reloadempty
	
	WeaponAmmoCheckAnim=Lahti_ammocheck
	
	WeaponSprintStartAnim=Lahti_sprint_into
	WeaponSprintLoopAnim=Lahti_sprint
	WeaponSprintEndAnim=Lahti_sprint_out
	
	Weapon1HSprintStartAnim=Lahti_sprint_into
	Weapon1HSprintLoopAnim=Lahti_sprint
	Weapon1HSprintEndAnim=Lahti_sprint_out
	
	WeaponMantleOverAnim=Lahti_Mantle
	
	WeaponSpotEnemyAnim=Lahti_SpotEnemy
	WeaponSpotEnemySightedAnim=Lahti_SpotEnemy_ironsight
	
	WeaponMeleeAnims(0)=Lahti_Bash
	WeaponMeleeHardAnim=Lahti_BashHard
	MeleePullbackAnim=Lahti_Pullback
	MeleeHoldAnim=Lahti_Pullback_Hold
	
	BoltControllerNames[0]=SlideControl_Barrel
	BoltControllerNames[1]=SlideControl_MainSlide
	
	EquipTime=+0.50
	PutDownTime=+0.33
	
	bDebugWeapon=false
	
	ISFocusDepth=20
	ISFocusBlendRadius=8

	// Ammo
	MaxAmmoCount=8
	AmmoClass=class'ROAmmo_9x19_BHPMag'
	bUsesMagazines=true
	InitialNumPrimaryMags=4
	bPlusOneLoading=true
	bCanReloadNonEmptyMag=true
	bCanLoadStripperClip=false
	bCanLoadSingleBullet=false
	//StripperClipBulletCount=10
	PenetrationDepth=10
	MaxPenetrationTests=3
	MaxNumPenetrations=2
	PerformReloadPct=0.81f
	
	bUsesFreeAim=true
	
	// Free Aim variables
	FreeAimMaxYawLimit=2000
	FreeAimMinYawLimit=63535
	FreeAimMaxPitchLimit=1500
	FreeAimMinPitchLimit=64035
	FreeAimISMaxYawLimit=500
	FreeAimISMinYawLimit=65035
	FreeAimISMaxPitchLimit=350
	FreeAimISMinPitchLimit=65185
	FullFreeAimISMaxYaw=250
	FullFreeAimISMinYaw=65285
	FullFreeAimISMaxPitch=175
	FullFreeAimISMinPitch=65360
	FreeAimSpeedScale=0.35
	FreeAimISSpeedScale=0.4
	FreeAimHipfireOffsetX=25
	
	Begin Object Class=ForceFeedbackWaveform Name=ForceFeedbackWaveformShooting1
		Samples(0)=(LeftAmplitude=30,RightAmplitude=30,LeftFunction=WF_Constant,RightFunction=WF_Constant,Duration=0.120)
	End Object
	WeaponFireWaveForm=ForceFeedbackWaveformShooting1
	
	CollisionCheckLength=22.0
	
	FireCameraAnim[0]=CameraAnim'1stperson_Cameras.Anim.Camera_C96_Shoot'
	FireCameraAnim[1]=CameraAnim'1stperson_Cameras.Anim.Camera_C96_Shoot'
	
	SuppressionPower=5
}
