
class WWWeapon_MN27_ActualContent extends WWWeapon_MN27;

defaultproperties
{
	ArmsAnimSet=AnimSet'WinterWar_WP_COMMON.Anim.MNHands_Normal'
	
	Begin Object Name=FirstPersonMesh
		DepthPriorityGroup=SDPG_Foreground
		SkeletalMesh=SkeletalMesh'WinterWar_WP_FIN_MN27.Mesh.FIN_MN27'
		PhysicsAsset=PhysicsAsset'WP_VN_VC_MN9130_rifle.Phys.Sov_MN9130_Physics'
		AnimSets(0)=AnimSet'WinterWar_WP_COMMON.Anim.MNHands_Normal'
		AnimTreeTemplate=AnimTree'WP_VN_VC_MN9130_rifle.animation.Sov_MN9130_Tree'
		Scale=1.0
		FOV=70
	End Object
	
	Begin Object Name=PickupMesh
		SkeletalMesh=SkeletalMesh'WinterWar_WP_FIN_MN27.Mesh.FIN_MN27_3rd'
		PhysicsAsset=PhysicsAsset'WP_VN_3rd_Master.Phy.MN9130_3rd_Master_Physics'
		AnimTreeTemplate=AnimTree'WP_VN_3rd_Master.AnimTree.MN9130_3rd_Tree'
		CollideActors=true
		BlockActors=true
		BlockZeroExtent=true
		BlockNonZeroExtent=true//false
		BlockRigidBody=true
		bHasPhysicsAssetInstance=false
		bUpdateKinematicBonesFromAnimation=false
		PhysicsWeight=1.0
		RBChannel=RBCC_GameplayPhysics
		RBCollideWithChannels=(Default=TRUE,GameplayPhysics=TRUE,EffectPhysics=TRUE)
		bSkipAllUpdateWhenPhysicsAsleep=TRUE
		bSyncActorLocationToRootRigidBody=true
	End Object
	
	AttachmentClass=class'WWWeapon_MN27_Attach'
}
