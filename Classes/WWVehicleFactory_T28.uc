
class WWVehicleFactory_T28 extends WWVehicleFactory;

defaultproperties
{
	Begin Object Name=SVehicleMesh
		SkeletalMesh=SkeletalMesh'WinterWar_VH_USSR_T-28.Mesh.T-28_PHAT'
	End Object
	
	Begin Object Name=CollisionCylinder
		CollisionHeight=+70.0
		CollisionRadius=+190.0
		Translation=(X=-40.0,Y=0.0,Z=60.0)
		bAlwaysRenderIfSelected=true
	End Object
	
	VehicleClass=class'WWVehicle_T28_ActualContent'
}
