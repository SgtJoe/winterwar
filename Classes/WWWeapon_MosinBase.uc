
class WWWeapon_MosinBase extends ROWeap_MN9130_Rifle
	abstract;

`include(WinterWar\Classes\WWWeaponPickupMessagesOverride.uci)

defaultproperties
{
	AmmoClass=class'ROAmmo_762x54R_MNStripper'
	
	MuzzleFlashPSCTemplate=ParticleSystem'WinterWar_FX.ParticleSystems.FX_WW_MuzzleFlash_1stP_Rifles_round'
	
	SightRanges.Empty
	SightRanges[0]=(SightRange=100,SightPitch=-175,SightSlideOffset=0.05,SightPositionOffset=-0.1)
	SightRanges[1]=(SightRange=200,SightPitch=-140,SightSlideOffset=0.2,SightPositionOffset=-0.12)
	SightRanges[2]=(SightRange=300,SightPitch=-40,SightSlideOffset=0.35,SightPositionOffset=-0.18)
	SightRanges[3]=(SightRange=400,SightPitch=10,SightSlideOffset=0.5,SightPositionOffset=-0.22)
	SightRanges[4]=(SightRange=500,SightPitch=150,SightSlideOffset=0.65,SightPositionOffset=-0.295)
}
