
class WWVehicleWeapon_T28_Turret extends WWVehicleWeapon_TankTurret
	abstract;

defaultproperties
{
	WeaponContentClass(0)="WinterWar.WWVehicleWeapon_T28_Turret_ActualContent"
	
	SeatIndex=`T28_GUNNER
	
	PlayerIronSightFOV=20.0
	
	FiringStatesArray(0)=WeaponSingleFiring
	WeaponFireTypes(0)=EWFT_Projectile
	WeaponProjectiles(0)=class'WWVehicleProjectile_T28_HE'
	FireInterval(0)=+6.0
	Spread(0)=0.0001
	
	FiringStatesArray(ALTERNATE_FIREMODE)=WeaponFiring
	WeaponFireTypes(ALTERNATE_FIREMODE)=EWFT_Projectile
	WeaponProjectiles(ALTERNATE_FIREMODE)=class'WWProjectile_DT_T28'
	FireInterval(ALTERNATE_FIREMODE)=+0.092
	Spread(ALTERNATE_FIREMODE)=0.0007
	
	FireTriggerTags=(T28_Cannon)
	AltFireTriggerTags=(T28_TurretMG)
	
	VehicleClass=class'WWVehicle_T28'
	
	MainGunProjectiles(MAINGUN_AP_INDEX)=class'WWVehicleProjectile_T28_HE'
	MainGunProjectiles(MAINGUN_HE_INDEX)=class'WWVehicleProjectile_T28_AP'
	MainGunProjectiles(MAINGUN_SMOKE_INDEX)=class'WWVehicleProjectile_Smoke'
	
	HEAmmoCount=`TANK_AMMO_T28_AP
	APAmmoCount=`TANK_AMMO_T28_HE
	SmokeAmmoCount=`TANK_AMMO_SMOKE
	
	AmmoClass=class'WWAmmo_DTDrum'
	MaxAmmoCount=63
	bUsesMagazines=true
	InitialNumPrimaryMags=`TANK_AMMO_T28_DT
	
	PenetrationDepth=22.23
	MaxPenetrationTests=3
	MaxNumPenetrations=2
}
