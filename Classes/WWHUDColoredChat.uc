
class WWHUDColoredChat extends ROHUDColoredChat
	config(Game_WinterWar_Client);

function SetupCommanderColor(PlayerReplicationInfo PRI, int type, name MessageType, out TextAttr attr)
{
	super.SetupCommanderColor(PRI, type, MessageType, attr);
	
	SetTeamColors(MessageType, attr, PRI.Team.TeamIndex);
}

function SetupLeaderColor(PlayerReplicationInfo PRI, int type, name MessageType, out TextAttr attr)
{
	super.SetupLeaderColor(PRI, type, MessageType, attr);
	
	SetTeamColors(MessageType, attr, PRI.Team.TeamIndex);
}

function SetNorthernColor(name MessageType, out TextAttr attr)
{
	super.SetNorthernColor(MessageType, attr);
	
	SetTeamColors(MessageType, attr, `AXIS_TEAM_INDEX);
}

function SetSouthernColor(name MessageType, out TextAttr attr)
{
	super.SetSouthernColor(MessageType, attr);
	
	SetTeamColors(MessageType, attr, `ALLIES_TEAM_INDEX);
}

function SetTeamColors(name MessageType, out TextAttr attr, int Team)
{
	if (MessageType == 'Say')
	{
		attr.drawColor = class'WWHUD'.default.SideColors[Team];
	}
	else if (MessageType == 'TeamSay')
	{
		attr.drawColor = class'WWHUD'.default.SideColors[Team];
	}
	else if (MessageType == 'SquadSay')
	{
		attr.drawColor = SquadColors[Team];
	}
	else if (MessageType == 'ASquadSay')
	{
		attr.drawColor = AttachedSquadColors[Team];
	}
}
