
class WWDmgType_LS26Bullet extends RODmgType_SmallArmsBullet
	abstract;

defaultproperties
{
	WeaponShortName="LS26"
	KDamageImpulse=384 // kg * uu/s
	BloodSprayTemplate=ParticleSystem'FX_VN_Impacts.BloodNGore.FX_VN_BloodSpray_Clothes_large'
}