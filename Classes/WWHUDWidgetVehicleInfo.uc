
class WWHUDWidgetVehicleInfo extends ROHUDWidgetVehicleInfo;

function UpdateWidget()
{
	if (WWVehicle_Skis(PlayerOwner.Pawn) != none)
	{
		bVisible = false;
		return;
	}
	
	super.UpdateWidget();
}

function string GetMainGunAmmoText(ROVWeap_TankTurret Turret, int ProjectileIndex)
{
	if (WWVehicleWeapon_HT130_Turret(Turret) != none)
	{
		HUDComponents[ROVT_AmmoTexture].Tex = class'WWVehicle_HT130'.default.FlamethrowerAmmoTex;
		HUDComponents[ROVT_AmmoOverlayTexture].bVisible = false;
		
		HUDComponents[ROVT_AmmoTypeLabel_1].bVisible = false;
		HUDComponents[ROVT_AmmoTypeLabel_2].bVisible = false;
		HUDComponents[ROVT_AmmoTypeLabel_3].bVisible = true;
		HUDComponents[ROVT_AmmoTypeLabel_3].DrawColor = MakeColor(255, 255, 255, 255);
		
		return class'WWVehicle_HT130'.default.FlamethrowerAmmoString;
	}
	else
	{
		HUDComponents[ROVT_AmmoTexture].Tex = HUDComponents[ROVT_AmmoTexture].default.Tex;
	}
	
	return super.GetMainGunAmmoText(Turret, ProjectileIndex);
}

function UpdateDamageIndicators()
{
	if (WWVehicleTransport(MyVehicle) == none)
	{
		return;
	}
	
	if (WWVehicleTransport(MyVehicle).CaughtFire)
	{
		HUDComponents[ROVT_Engine].bVisible = true;
		
		HUDComponents[ROVT_Engine].DrawColor = DestroyedColor;
		HUDComponents[ROVT_Vehicle].DrawColor = DestroyedColor;
		HUDComponents[ROVT_Turret].DrawColor = DestroyedColor;
		
		if( !HUDComponents[ROVT_Engine].bColorFlashing )
		{
			HUDComponents[ROVT_Engine].FlashColor = DestroyedColor;
			HUDComponents[ROVT_Engine].bColorFlashing = true;
		}
		
		if( !HUDComponents[ROVT_Vehicle].bColorFlashing )
		{
			HUDComponents[ROVT_Vehicle].FlashColor = DestroyedColor;
			HUDComponents[ROVT_Vehicle].bColorFlashing = true;
		}
		
		if( !HUDComponents[ROVT_Turret].bColorFlashing )
		{
			HUDComponents[ROVT_Turret].FlashColor = DestroyedColor;
			HUDComponents[ROVT_Turret].bColorFlashing = true;
		}
	}
	else
	{
		if ( HUDComponents[ROVT_Engine].bVisible )
		{
			HUDComponents[ROVT_Engine].bVisible = false;
			HUDComponents[ROVT_Engine].DrawColor = NeutralColor;
			HUDComponents[ROVT_Engine].bColorFlashing = false;
		}
		
		HUDComponents[ROVT_Vehicle].DrawColor = NeutralColor;
		HUDComponents[ROVT_Vehicle].bColorFlashing = false;
		HUDComponents[ROVT_Turret].DrawColor = NeutralColor;
		HUDComponents[ROVT_Turret].bColorFlashing = false;
	}
}

DefaultProperties
{
	Begin Object Name=Engine
		Tex=Texture2D'WinterWar_UI.Vehicle.UI_HUD_VH_Engine'
	End Object
	HUDComponents(ROVT_Engine)=Engine
}
