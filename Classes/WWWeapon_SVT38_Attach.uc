
class WWWeapon_SVT38_Attach extends ROWeaponAttachment;

defaultproperties
{
	CarrySocketName=WeaponSling
	ThirdPersonHandsAnim=SVT40_Handpose
	IKProfileName=svt40
	
	Begin Object Name=SkeletalMeshComponent0
		SkeletalMesh=SkeletalMesh'WinterWar_WP_SOV_SVT38.Mesh.SOV_SVT38_3rd'
		AnimSets(0)=AnimSet'WinterWar_WP_SOV_SVT38.Anim.SVT40_3rd_anim'
		AnimTreeTemplate=AnimTree'WinterWar_WP_SOV_SVT38.Anim.SOV_SVT38_3rd_Tree'
		Animations=NONE
		PhysicsAsset=PhysicsAsset'WinterWar_WP_SOV_SVT38.Phy.SVT40_3rd_Master_Bounds'
		CullDistance=5000
	End Object
	
	WeaponClass=class'WWWeapon_SVT38'
	
	MuzzleFlashSocket=MuzzleFlashSocket
	MuzzleFlashPSCTemplate=ParticleSystem'FX_VN_Weapons.MuzzleFlashes.FX_VN_MuzzleFlash_3rdP_Rifles_round'
	MuzzleFlashDuration=0.33
	MuzzleFlashLightClass=class'ROGame.RORifleMuzzleFlashLight'
	
	ShellEjectSocket=ShellEjectSocket
	ShellEjectPSCTemplate=ParticleSystem'FX_VN_Weapons.ShellEjects.FX_Wep_ShellEject_VC_MN9130'
	
	CHR_AnimSet=AnimSet'WinterWar_WP_SOV_SVT38.Anim.CHR_SVT40'
	
	FireAnim=Shoot
	FireLastAnim=Shoot_Last
	IdleAnim=Idle
	IdleEmptyAnim=Idle_Empty
}
