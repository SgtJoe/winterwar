
class WWHUD extends ROHUD
	config(Game_WinterWar_Client);

static function color GetFriendlyColor(byte Team)
{
	// Turn this off, it breaks all our custom icons
	default.bUseFriendAndFoeColours = false;
	StaticSaveConfig();
	
	return GetTeamColor(Team);
}

function SetTeamColoursForWidgets(byte MyTeamIndex)
{
	// Just to make sure, since before it wasn't setting correctly
	default.bUseFriendAndFoeColours = false;
	bUseFriendAndFoeColours = false;
	SaveConfig();
	
	super.SetTeamColoursForWidgets(MyTeamIndex);
}

event PostBeginPlay()
{
	super.PostBeginPlay();
	
	ColoredChat = new(self) class'WWHUDColoredChat';
	
	// Sometimes the compass widget's Initialize randomly fails to set PlayerOwner, so let's try to set it again
	if (CompassWidget != none && CompassWidget.PlayerOwner == none)
	{
		`wwdebug(CompassWidget @ "no PlayerOwner, attempting reset", 'HUD');
		SetUpCompassWidget();
	}
}

function ReceivedLocalizedMessage(class<ROLocalMessage> InMessageClass, string MessageString, float LifeTime, float TextScaleBoost = 1.0)
{
	if (InMessageClass == class'ROLocalMessageGamePickup')
	{
		// `wwdebug("Not parsing ROLocalMessageGamePickup",'HUD');
		return;
	}
	else if (InMessageClass == class'WWLocalMessageGamePickup')
	{
		MessagesPickupsWidget.AddMessage(MessageString, LifeTime);
	}
	else
	{
		super.ReceivedLocalizedMessage(InMessageClass, MessageString, LifeTime, TextScaleBoost);
	}
}

defaultproperties
{
	SideColors(0)=(R=119,G=215,B=240,A=255)
	SideColors(1)=(R=229,G=73,B=39,A=255)
	SideDeadColors(0)=(R=50,G=70,B=120,A=255)
	SideDeadColors(1)=(R=100,G=46,B=35,A=255)
	
	DefaultStatusWidget=					class'WWHUDWidgetStatus'
	DefaultWeaponWidget=					class'WWHUDWidgetWeapon'
	DefaultObjectiveWidget=					class'WWHUDWidgetObjective'
	DefaultScoreboardWidgetTE=				class'WWHUDWidgetScoreboardTE'
	DefaultScoreboardWidgetSU=				class'WWHUDWidgetScoreboardSU'
	DefaultOverheadMapWidget=				class'WWHUDWidgetOverheadMap'
	DefaultKillMessageWidget=				class'WWHUDWidgetKillMessages'
	DefaultCompassWidget=					class'WWHUDWidgetSimpleCompass'
	DefaultObjectiveOverviewWidget=			class'WWHUDWidgetObjectiveOverview'
	DefaultWorldWidget=						class'WWHUDWidgetWorld'
	DefaultCommanderWidget=					class'WWHUDWidgetCommander'
	DefaultVehicleListWidget=				class'WWHUDWidgetVehicleList'
	DefaultVehicleInfoWidget=				class'WWHUDWidgetVehicleInfo'
	DefaultCamoIndicatorWidget=				class'WWHUDWidgetCamoIndicator'
	DefaultHelicopterInfoWidget=			class'WWHUDWidgetHelicopterInfo'
	DefaultGameStatusWidgetSU=				class'WWHUDWidgetGameStatusSU'
	
	WeaponUVs.Empty
	
	// Standard Weapons
	WeaponUVs(`WI_DP28)=			(WeaponClass=WWWeapon_DP28,				WeaponTexture=Texture2D'VN_UI_Textures.HUD.WeaponSelect.ui_hud_weaponselect_DP28',			KillMessageTexture=Texture2D'VN_UI_Textures.HUD.DeathMessage.ui_kill_icon_DP28',			KillMessageWidth=128,KillMessageHeight=64)
	WeaponUVs(`WI_F1GRENADE)=		(WeaponClass=WWWeapon_F1Grenade,		WeaponTexture=Texture2D'WinterWar_UI.WP_Select.WP_Select_F1',								KillMessageTexture=Texture2D'WinterWar_UI.WP_KillIcon.WP_KillIcon_F1',						KillMessageWidth=64,KillMessageHeight=64)
	WeaponUVs(`WI_KP31)=			(WeaponClass=WWWeapon_KP31,				WeaponTexture=Texture2D'WinterWar_UI.WP_Select.WP_Select_KP31',								KillMessageTexture=Texture2D'WinterWar_UI.WP_KillIcon.WP_KillIcon_KP31',					KillMessageWidth=128,KillMessageHeight=64)
	WeaponUVs(`WI_LS26)=			(WeaponClass=WWWeapon_LahtiSaloranta,	WeaponTexture=Texture2D'WinterWar_UI.WP_Select.WP_Select_LS',								KillMessageTexture=Texture2D'WinterWar_UI.WP_KillIcon.WP_KillIcon_LS26',					KillMessageWidth=128,KillMessageHeight=64)
	WeaponUVs(`WI_LUGER)=			(WeaponClass=WWWeapon_Luger,			WeaponTexture=Texture2D'WinterWar_UI.WP_Select.WP_Select_Luger',							KillMessageTexture=Texture2D'WinterWar_UI.WP_KillIcon.WP_KillIcon_Luger',					KillMessageWidth=64,KillMessageHeight=64)
	WeaponUVs(`WI_L35)=				(WeaponClass=WWWeapon_L35,				WeaponTexture=Texture2D'WinterWar_UI.WP_Select.WP_Select_L35',								KillMessageTexture=Texture2D'WinterWar_UI.WP_KillIcon.WP_KillIcon_L35',						KillMessageWidth=64,KillMessageHeight=64)
	WeaponUVs(`WI_M32GRENADE)=		(WeaponClass=WWWeapon_M32Grenade,		WeaponTexture=Texture2D'VN_UI_Textures.HUD.WeaponSelect.UI_HUD_WeaponSelect_Type67_Grenade',KillMessageTexture=Texture2D'VN_UI_Textures.HUD.DeathMessage.UI_Kill_Icon_Type67_Grenade',	KillMessageWidth=64,KillMessageHeight=64)
	WeaponUVs(`WI_MN27)=			(WeaponClass=WWWeapon_MN27,				WeaponTexture=Texture2D'VN_UI_Textures.HUD.WeaponSelect.UI_HUD_WeaponSelect_MN1891-30',		KillMessageTexture=Texture2D'VN_UI_Textures.HUD.DeathMessage.UI_Kill_Icon_MN1891-30',		KillMessageWidth=128,KillMessageHeight=64)
	WeaponUVs(`WI_MN38)=			(WeaponClass=WWWeapon_MN38,				WeaponTexture=Texture2D'VN_UI_Textures.HUD.WeaponSelect.UI_HUD_WeaponSelect_MN1891-30',		KillMessageTexture=Texture2D'VN_UI_Textures.HUD.DeathMessage.UI_Kill_Icon_MN1891-30',		KillMessageWidth=128,KillMessageHeight=64)
	WeaponUVs(`WI_MN91)=			(WeaponClass=WWWeapon_MN91,				WeaponTexture=Texture2D'VN_UI_Textures.HUD.WeaponSelect.UI_HUD_WeaponSelect_MN1891-30',		KillMessageTexture=Texture2D'VN_UI_Textures.HUD.DeathMessage.UI_Kill_Icon_MN1891-30',		KillMessageWidth=128,KillMessageHeight=64)
	WeaponUVs(`WI_MN9130)=			(WeaponClass=WWWeapon_MN9130,			WeaponTexture=Texture2D'VN_UI_Textures.HUD.WeaponSelect.UI_HUD_WeaponSelect_MN1891-30',		KillMessageTexture=Texture2D'VN_UI_Textures.HUD.DeathMessage.UI_Kill_Icon_MN1891-30',		KillMessageWidth=128,KillMessageHeight=64)
	WeaponUVs(`WI_MN9130_SCOPED)=	(WeaponClass=WWWeapon_MN9130_Scoped,	WeaponTexture=Texture2D'VN_UI_Textures.HUD.WeaponSelect.UI_HUD_WeaponSelect_MN9130Scoped',	KillMessageTexture=Texture2D'VN_UI_Textures.HUD.DeathMessage.UI_Kill_Icon_ScopedMN9130',	KillMessageWidth=128,KillMessageHeight=64)
	WeaponUVs(`WI_NAGANT)=			(WeaponClass=WWWeapon_NagantRevolver,	WeaponTexture=Texture2D'WinterWar_UI.WP_Select.WP_Select_Revolver',							KillMessageTexture=Texture2D'WinterWar_UI.WP_KillIcon.WP_KillIcon_Revolver',				KillMessageWidth=64,KillMessageHeight=64)
	WeaponUVs(`WI_SATCHEL)=			(WeaponClass=WWWeapon_Satchel,			WeaponTexture=Texture2D'WinterWar_UI.WP_Select.WP_Select_Satchel',							KillMessageTexture=Texture2D'WinterWar_UI.WP_KillIcon.WP_KillIcon_Satchel',					KillMessageWidth=64,KillMessageHeight=64)
	WeaponUVs(`WI_SVT38)=			(WeaponClass=WWWeapon_SVT38,			WeaponTexture=Texture2D'WinterWar_UI.WP_Select.WP_Select_SVT',								KillMessageTexture=Texture2D'WinterWar_UI.WP_KillIcon.WP_KillIcon_SVT',						KillMessageWidth=128,KillMessageHeight=64)
	WeaponUVs(`WI_TT33)=			(WeaponClass=WWWeapon_TT33,				WeaponTexture=Texture2D'VN_UI_Textures.HUD.WeaponSelect.ui_hud_weaponselect_TT33',			KillMessageTexture=Texture2D'VN_UI_Textures.HUD.DeathMessage.ui_kill_icon_TT33',			KillMessageWidth=64,KillMessageHeight=64)
	WeaponUVs(`WI_RGD33)=			(WeaponClass=WWWeapon_RGD33,			WeaponTexture=Texture2D'WinterWar_UI.WP_Select.WP_Select_RGD33',							KillMessageTexture=Texture2D'WinterWar_UI.WP_KillIcon.WP_KillIcon_RGD33',					KillMessageWidth=64,KillMessageHeight=64)
	WeaponUVs(`WI_AVS36)=			(WeaponClass=WWWeapon_AVS36,			WeaponTexture=Texture2D'WinterWar_UI.WP_Select.WP_Select_AVS',								KillMessageTexture=Texture2D'WinterWar_UI.WP_KillIcon.WP_KillIcon_AVS',						KillMessageWidth=128,KillMessageHeight=64)
	WeaponUVs(`WI_PPD34)=			(WeaponClass=WWWeapon_PPD34,			WeaponTexture=Texture2D'WinterWar_UI.WP_Select.WP_Select_PPD',								KillMessageTexture=Texture2D'WinterWar_UI.WP_KillIcon.WP_KillIcon_PPD',						KillMessageWidth=128,KillMessageHeight=64)
	WeaponUVs(`WI_M20)=				(WeaponClass=WWWeapon_M20,				WeaponTexture=Texture2D'WinterWar_UI.WP_Select.WP_Select_M20',								KillMessageTexture=Texture2D'WinterWar_UI.WP_KillIcon.WP_KillIcon_M20',						KillMessageWidth=128,KillMessageHeight=64)
	
	// Special Weapons
	WeaponUVs(`WI_BINOCS)=			(WeaponClass=WWWeapon_Binoculars,							WeaponTexture=Texture2D'ui_textures.HUD.WeaponSelect.ui_hud_weaponselect_binoculars',		KillMessageTexture=Texture2D'VN_UI_Textures.HUD.DeathMessage.UI_Kill_Icon_Bash',		KillMessageWidth=64,KillMessageHeight=64,MagTexture=Texture2D'ui_textures.HUD.ui_hud_ammo_binocs')
	WeaponUVs(`WI_SKIS)=			(WeaponClass=WWWeapon_Skis,									WeaponTexture=Texture2D'WinterWar_UI.WP_Select.WP_Select_Skis',								KillMessageTexture=none,																KillMessageWidth=64,KillMessageHeight=64,MagTexture=Texture2D'WinterWar_UI.WP_Select.WP_Select_Skis_MagTexture')
	WeaponUVs(`WI_RDG1)=			(WeaponClass=WWWeapon_RDG1,									WeaponTexture=Texture2D'VN_UI_Textures.HUD.WeaponSelect.ui_hud_weaponselect_RDG1',			KillMessageTexture=none,																KillMessageWidth=64,KillMessageHeight=64)
	WeaponUVs(`WI_KASAPANOS_IM)=	(WeaponClass=WWWeapon_Kasapanos_Improvised_ActualContent,	WeaponTexture=Texture2D'WinterWar_UI.WP_Select.WP_Select_Kasapanos',						KillMessageTexture=Texture2D'WinterWar_UI.WP_KillIcon.WP_KillIcon_Kasapanos',			KillMessageWidth=64,KillMessageHeight=64)
	WeaponUVs(`WI_KASAPANOS_FI)=	(WeaponClass=WWWeapon_Kasapanos_FactoryIssue_ActualContent,	WeaponTexture=Texture2D'WinterWar_UI.WP_Select.WP_Select_Kasapanos',						KillMessageTexture=Texture2D'WinterWar_UI.WP_KillIcon.WP_KillIcon_Kasapanos',			KillMessageWidth=64,KillMessageHeight=64)
	WeaponUVs(`WI_MOLOTOV_IM)=		(WeaponClass=WWWeapon_Molotov,								WeaponTexture=Texture2D'VN_UI_Textures.HUD.WeaponSelect.UI_HUD_WeaponSelect_Molotov',		KillMessageTexture=Texture2D'VN_UI_Textures.HUD.DeathMessage.UI_Kill_Icon_Molotov',		KillMessageWidth=64,KillMessageHeight=64)
	WeaponUVs(`WI_DYAKONOV)=		(WeaponClass=WWWeapon_MN9130_Dyakonov,						WeaponTexture=Texture2D'WinterWar_UI.WP_Select.WP_Select_Dyakonov',							KillMessageTexture=Texture2D'VN_UI_Textures.HUD.DeathMessage.UI_Kill_Icon_MN1891-30',	KillMessageWidth=128,KillMessageHeight=64)
	WeaponUVs(`WI_ATMINE)=			(WeaponClass=WWWeapon_AntiTankMine,							WeaponTexture=Texture2D'VN_UI_Textures.HUD.WeaponSelect.UI_HUD_WeaponSelect_MD82_Mine',		KillMessageTexture=Texture2D'VN_UI_Textures.HUD.DeathMessage.UI_Kill_Icon_MD82Mine',	KillMessageWidth=64,KillMessageHeight=64)
	
	AmmoUVs.Empty
	
	// REMEMBER: AmmoUVs has a corresponding localization array!
	
	// Tank Ammo
	AmmoUVs(0)=(AmmoClass=ROAmmo_PG7V_Rocket,	MagTexture=Texture2D'VN_UI_Textures.HUD.Ammo.UI_HUD_Ammo_M79_HE')
	AmmoUVs(1)=(AmmoClass=ROAmmo_C4_Explosive,	MagTexture=Texture2D'VN_UI_Textures.HUD.Ammo.UI_HUD_Ammo_M79_HE')
	AmmoUVs(2)=(AmmoClass=ROAmmo_M18_Smoke,		MagTexture=Texture2D'VN_UI_Textures.HUD.Ammo.UI_HUD_Ammo_M79_HE')
	AmmoUVs(3)=(AmmoClass=WWAmmo_DTDrum,		MagTexture=Texture2D'VN_UI_Textures.HUD.Ammo.UI_HUD_Ammo_DP28_Drum')
	AmmoUVs(4)=(AmmoClass=ROAmmo_15L_M9A1Tank,	MagTexture=Texture2D'VN_UI_Textures.HUD.DeathMessage.UI_Kill_Icon_Fire')
	
	// Weapon Ammo
	AmmoUVs(5)= (AmmoClass=ROAmmo_Type67_Grenade,		MagTexture=Texture2D'VN_UI_Textures.HUD.Ammo.UI_HUD_Ammo_Type67_Grenade')
	AmmoUVs(6)= (AmmoClass=ROAmmo_762x54R_MNStripper,	MagTexture=Texture2D'VN_UI_Textures.HUD.Ammo.UI_HUD_Ammo_M40')
	AmmoUVs(7)= (AmmoClass=ROAmmo_Molotov,				MagTexture=Texture2D'VN_UI_Textures.HUD.Ammo.UI_HUD_Ammo_Molotov')
	AmmoUVs(8)= (AmmoClass=WWAmmo_NagantRevolver,		MagTexture=Texture2D'VN_UI_Textures.HUD.Ammo.UI_HUD_Ammo_M1917')
	AmmoUVs(9)= (AmmoClass=ROAmmo_RDG1_Smoke,			MagTexture=Texture2D'VN_UI_Textures.HUD.Ammo.UI_HUD_Ammo_RGD1_Smoke')
	AmmoUVs(10)=(AmmoClass=ROAmmo_SatchelCharge,		MagTexture=Texture2D'VN_UI_Textures.HUD.Ammo.UI_HUD_Ammo_C4')
	AmmoUVs(11)=(AmmoClass=ROAmmo_762x39_SKSStripper,	MagTexture=Texture2D'VN_UI_Textures.HUD.Ammo.UI_HUD_Ammo_SVD')
	AmmoUVs(12)=(AmmoClass=ROAmmo_762x25_TT33Mag,		MagTexture=Texture2D'VN_UI_Textures.HUD.Ammo.ui_hud_ammo_tt33')
	AmmoUVs(13)=(AmmoClass=ROAmmo_762x54R_DP28Drum,		MagTexture=Texture2D'VN_UI_Textures.HUD.Ammo.UI_HUD_Ammo_DP28_Drum')
	AmmoUVs(14)=(AmmoClass=ROAmmo_M61_Grenade,			MagTexture=Texture2D'VN_UI_Textures.HUD.Ammo.UI_HUD_Ammo_M61_Grenade')
	AmmoUVs(15)=(AmmoClass=ROAmmo_Kasapanos,			MagTexture=Texture2D'VN_UI_Textures.HUD.Ammo.UI_HUD_Ammo_Type67_Grenade')
	AmmoUVs(16)=(AmmoClass=ROAmmo_762x25_PPSHDrum,		MagTexture=Texture2D'VN_UI_Textures.HUD.Ammo.UI_HUD_Ammo_PPSH_Drum')
	AmmoUVs(17)=(AmmoClass=WWAmmo_LS26Mag,				MagTexture=Texture2D'VN_UI_Textures.HUD.Ammo.UI_HUD_Ammo_M16_20Rnd')
	AmmoUVs(18)=(AmmoClass=ROAmmo_9x19_BHPMag,			MagTexture=Texture2D'VN_UI_Textures.HUD.Ammo.ui_hud_ammo_tt33')
	AmmoUVs(19)=(AmmoClass=WWAmmo_DyakonovGrenade,		MagTexture=Texture2D'VN_UI_Textures.HUD.Ammo.UI_HUD_Ammo_M79_HE')
	AmmoUVs(20)=(AmmoClass=ROAmmo_MD82_Mine,			MagTexture=Texture2D'VN_UI_Textures.HUD.Ammo.UI_HUD_Ammo_RPD_DRUM')
	AmmoUVs(21)=(AmmoClass=WWAmmo_AVSMag,				MagTexture=Texture2D'VN_UI_Textures.HUD.Ammo.UI_HUD_Ammo_SVD')
	AmmoUVs(22)=(AmmoClass=ROAmmo_762x25_PPSHStick,		MagTexture=Texture2D'VN_UI_Textures.HUD.Ammo.UI_HUD_Ammo_PPSH_Stick')
	AmmoUVs(23)=(AmmoClass=WWAmmo_M20Mag,				MagTexture=Texture2D'VN_UI_Textures.HUD.Ammo.UI_HUD_Ammo_MP40')
}
