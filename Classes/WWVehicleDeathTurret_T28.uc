
class WWVehicleDeathTurret_T28 extends ROVehicleDeathPiece;

DefaultProperties
{
	GibMeshesData[0]={(
		TheStaticMesh=None,
		TheSkelMesh=SkeletalMesh'WinterWar_VH_USSR_T-28.Mesh.T-28_Wreck_Turret',
		ThePhysAsset=PhysicsAsset'WinterWar_VH_USSR_T-28.Phy.T-28_Wreck_Turret_Physics',
		DrawScale=1.0f,
		bUseSecondaryGibMeshMITV=FALSE
	)}
}
