
class WWDmgType_MN38Bullet extends RODmgType_SmallArmsBullet
	abstract;

defaultproperties
{
	WeaponShortName="MN38"
	KDamageImpulse=385 // kg * uu/s
	BloodSprayTemplate=ParticleSystem'FX_VN_Impacts.BloodNGore.FX_VN_BloodSpray_Clothes_large'
}