
class WWFighterPlane extends ROSupportAircraft;

var PointLightComponent FlyingLight;
var name PropellerAnim;

var Actor			Target;
var Actor			DelayedKillVictim;
var repnotify bool	FiringAtTarget;
var float			TriggerRadiusSq;

var name					GunfireSockets[4];
var AkComponent				GunfireSoundComponent;
var AkEvent					GunfireSoundComponentEvent;
var	ParticleSystemComponent	GunfireEmitter1, GunfireEmitter2, GunfireEmitter3, GunfireEmitter4;

var WWFighterPlaneManager PlaneManager;

replication
{
	if (bNetDirty && Role == ROLE_Authority)
		Target;
	
	if (bNetDirty)
		FiringAtTarget;
}

simulated event ReplicatedEvent(name VarName)
{
	if (VarName == 'FiringAtTarget')
	{
		if (FiringAtTarget == false)
		{
			// StoppedFiring() is run on server but not clients, so we have to do this as well
			StopFiringEffects();
		}
	}
	else
	{
		super.ReplicatedEvent(VarName);
	}
}

simulated function PostBeginPlay()
{
	super.PostBeginPlay();
	
	Mesh.AttachComponentToSocket(GunfireEmitter1,GunfireSockets[0]);
	Mesh.AttachComponentToSocket(GunfireEmitter2,GunfireSockets[1]);
	Mesh.AttachComponentToSocket(GunfireEmitter3,GunfireSockets[2]);
	Mesh.AttachComponentToSocket(GunfireEmitter4,GunfireSockets[3]);
	
	// We need to spawn a light otherwise the mesh will be pitch black
	Mesh.AttachComponentToSocket(FlyingLight, 'LightSocket');
	
	Mesh.PlayAnim(PropellerAnim,,true);
	
	// Start the sound now so it syncs when we're flying overhead
	if (AmbientSound != none)
	{
		if (AmbientComponent != none)
		{
			AmbientComponent.PlayEvent(AmbientSound);
		}
	}
}

simulated event StartStrike()
{
	Mesh.SetHidden(false);
	
	bBegunStrike = true;
	
	// We don't affect Pawns on the ground, they shouldn't care about us
	// WarnIncomingStrike();
}

simulated function SetTarget(Actor NewTarget)
{
	Target = NewTarget;
	
	`wwdebug("Target:" @ Target,'CMDA');
}

simulated function Tick(float DeltaTime)
{
	super.Tick(DeltaTime);
	
	if (Target == none || !bBegunStrike)
	{
		return;
	}
	
	Velocity = Normal(Target.Location - Location) * Speed;
	
	// https://wiki.beyondunreal.com/Legacy:UnrealScript_Vector_Maths#Rotate_Actor_A_to_face_point_B
	SetRotation(rotator(Target.Location - Location));
	
	if (VSizeSq(Target.Location - Location) < TriggerRadiusSq && !FiringAtTarget)
	{
		FireAtTarget();
		SetTimer(GetFiringTime() * 0.6, false, 'DelayedKillTarget');
		SetTimer(GetFiringTime(), false, 'StoppedFiring');
	}
}

simulated function FireAtTarget()
{
	`wwdebug("",'CMDA');
	
	FiringAtTarget = true;
	
	DelayedKillVictim = Target;
	
	// Clear the target so we don't try to follow it once we've already passed it
	Target = none;
	
	if (GunfireEmitter1 != none)
	{
		GunfireEmitter1.SetActive(true);
	}
	
	if (GunfireEmitter2 != none)
	{
		GunfireEmitter2.SetActive(true);
	}
	
	if (GunfireEmitter3 != none)
	{
		GunfireEmitter3.SetActive(true);
	}
	
	if (GunfireEmitter4 != none)
	{
		GunfireEmitter4.SetActive(true);
	}
	
	if (GunfireSoundComponent != none && !GunfireSoundComponent.IsPlaying())
	{
		GunfireSoundComponent.PlayEvent(GunfireSoundComponentEvent);
	}
}

simulated function DelayedKillTarget()
{
	local vector v;
	
	`wwdebug("",'CMDA');
	
	DelayedKillVictim.TakeDamage(1000, InstigatorController, v, v, class'WWDmgType_FighterPlane');
}

simulated function StoppedFiring()
{
	`wwdebug("",'CMDA');
	
	FiringAtTarget = false;
	bCheckMapBounds = true;
	
	// Set our flight path back to horizontal in case we came from above or below
	SetTimer(0.1, true, 'LevelOut');
	
	StopFiringEffects();
}

simulated function StopFiringEffects()
{
	`wwdebug("",'CMDA');
	
	if (GunfireEmitter1 != none)
	{
		GunfireEmitter1.SetActive(false);
	}
	
	if (GunfireEmitter2 != none)
	{
		GunfireEmitter2.SetActive(false);
	}
	
	if (GunfireEmitter3 != none)
	{
		GunfireEmitter3.SetActive(false);
	}
	
	if (GunfireEmitter4 != none)
	{
		GunfireEmitter4.SetActive(false);
	}
	
	if (GunfireSoundComponent != none && GunfireSoundComponent.IsPlaying())
	{
		GunfireSoundComponent.StopEvents();
	}
}

function float GetFiringTime()
{
	local float FiringTime;
	
	FiringTime = 0.1;
	
	if (WWCarpetBomberAircraft(DelayedKillVictim) != none)
	{
		FiringTime = 2.3;
	}
	else if (WWAerialReconPlane(DelayedKillVictim) != none)
	{
		FiringTime = 1.6;
	}
	
	`wwdebug("Victim:" @ DelayedKillVictim @ "firing for:" @ FiringTime,'CMDA');
	
	return FiringTime;
}

simulated function LevelOut()
{
	local rotator R;
	local vector V;
	local int PitchDelta;
	local int VelocityDelta;
	
	if (IsTimerActive('LevelOut') && Rotation.Pitch == 0 && Velocity.Z == 0)
	{
		ClearTimer('LevelOut');
		return;
	}
	
	R = Rotation;
	V = Velocity;
	
	PitchDelta = 400;
	VelocityDelta = 250;
	
	if (Rotation.Pitch < 0)
	{
		if (abs(Rotation.Pitch) < PitchDelta)
		{
			R.Pitch += abs(Rotation.Pitch);
		}
		else
		{
			R.Pitch += PitchDelta;
		}
	}
	else if (Rotation.Pitch > 0)
	{
		if (Rotation.Pitch < PitchDelta)
		{
			R.Pitch -= Rotation.Pitch;
		}
		else
		{
			R.Pitch -= PitchDelta;
		}
	}
	
	if (Velocity.Z < 0)
	{
		if (abs(Velocity.Z) < VelocityDelta)
		{
			V.Z += abs(Velocity.Z);
		}
		else
		{
			V.Z += VelocityDelta;
		}
	}
	else if (Velocity.Z > 0)
	{
		if (Velocity.Z < VelocityDelta)
		{
			V.Z -= Velocity.z;
		}
		else
		{
			V.Z -= VelocityDelta;
		}
	}
	
	SetRotation(R);
	Velocity = V;
	
	// `wwdebug(self @ "Pitch:" @ Rotation.Pitch @ "Velocity:" @ Velocity.Z,'CMDA');
}

function TakeDamage(int DamageAmount, Controller EventInstigator, vector HitLocation, vector Momentum, class<DamageType> DamageType, optional TraceHitInfo HitInfo, optional Actor DamageCauser) {}

simulated event Landed(vector HitNormal, Actor FloorActor) {}

simulated event HitWall( vector HitNormal, actor Wall, PrimitiveComponent WallComp, optional PhysicalMaterial WallPhysMaterial) {}

simulated function Explode() {}

simulated function ShutDown()
{
	super.ShutDown();
	
	`wwdebug("",'CMDA');
	
	if (PlaneManager != none)
	{
		PlaneManager.RemovePlane(self);
	}
}

defaultproperties
{
	Health=2000
	
	Speed=3500
	
	InboundDelay=2.6
	
	// TriggerRadiusSq=1000000
	// from other vars, 1 meter = 50 units
	// sqrt(1000000) = 1000, / 50 = 20m
	
	TriggerRadiusSq=36000000 // 120m to allow for a realistic firing time, especially on large bombers
	
	FiringAtTarget=false
	
	GunfireSockets(0)=Gun1Socket
	GunfireSockets(1)=Gun2Socket
	GunfireSockets(2)=Gun3Socket
	GunfireSockets(3)=Gun4Socket
	
	AmbientSound=AkEvent'WW_CMD_Spooky.Play_CMD_Skyraider_Plane_Intro'
	
	Begin Object Name=PlaneMesh
		SkeletalMesh=SkeletalMesh'WinterWar_VH_FIN_FOKKER.Mesh.Fokker_DXXI'
		PhysicsAsset=none //PhysicsAsset'VH_VN_ARVN_Skyraider.Phys.Skyraider_Physics'
		AnimSets[0]=AnimSet'VH_VN_ARVN_Skyraider.Animation.AUS_Skyraider_anim'
		RBCollideWithChannels=(Default=FALSE,BlockingVolume=FALSE,GameplayPhysics=FALSE,EffectPhysics=FALSE,Vehicle=FALSE,Untitled1=FALSE,Untitled4=FALSE)
		BlockActors=false
		BlockZeroExtent=false
		BlockRigidBody=false
		BlockNonzeroExtent=false
		CollideActors=false
	End Object
	
	PropellerAnim=propeller_loop
	
	Begin Object class=PointLightComponent name=PlaneLight
		bCastCompositeShadow=False
		LightingChannels=(Dynamic=TRUE,CompositeDynamic=TRUE)
		UseDirectLightMap=FALSE
		Radius=1000.0
	End Object
	Components.Add(PlaneLight)
	FlyingLight=PlaneLight
	
	Begin Object Class=ParticleSystemComponent name=GunfirePSC1
		Template=ParticleSystem'WinterWar_FX.ParticleSystems.FX_Fokker_Tracer'
		bAutoActivate=false
	End Object
	GunfireEmitter1=GunfirePSC1
	
	Begin Object Class=ParticleSystemComponent name=GunfirePSC2
		Template=ParticleSystem'WinterWar_FX.ParticleSystems.FX_Fokker_Tracer'
		bAutoActivate=false
	End Object
	GunfireEmitter2=GunfirePSC2
	
	Begin Object Class=ParticleSystemComponent name=GunfirePSC3
		Template=ParticleSystem'WinterWar_FX.ParticleSystems.FX_Fokker_Tracer'
		bAutoActivate=false
	End Object
	GunfireEmitter3=GunfirePSC3
	
	Begin Object Class=ParticleSystemComponent name=GunfirePSC4
		Template=ParticleSystem'WinterWar_FX.ParticleSystems.FX_Fokker_Tracer'
		bAutoActivate=false
	End Object
	GunfireEmitter4=GunfirePSC4
	
	Begin Object Class=AkComponent name=GunfireComponent
		OcclusionUpdateInterval=1.0
		bStopWhenOwnerDestroyed=true
	End Object
	GunfireSoundComponent=GunfireComponent
	Components.Add(GunfireComponent)
	
	GunfireSoundComponentEvent=AkEvent'WW_CMD_Spooky.Play_CMD_Spooky_BulletFire_Loop'
}
