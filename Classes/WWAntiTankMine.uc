
class WWAntiTankMine extends ROPlantedTrap;

simulated function PostBeginPlay()
{
	super.PostBeginPlay();
	
	ChangeMeshBasedOnTeam();
}

simulated function ChangeMeshBasedOnTeam()
{
	if (GetALocalPlayerController().GetTeamNum() == `AXIS_TEAM_INDEX)
	{
		// If we are a Finnish player looking at this trap, set the ThowableMeshComponent to the 
		// parent weapon's PreviewMesh so we can see where friendly and our own traps are
		`wwdebug("using visible mesh",'M');
		ThrowableMesh.SetSkeletalMesh(SkeletalMesh'WinterWar_WP_FIN_M39.Mesh.FIN_M39_DirtMound');
		ThrowableMesh.SetMaterial(0, MaterialInstanceConstant'WinterWar_WP_FIN_M39.MIC.SnowMound');
		ThrowableMesh.SetScale3D(Vect(4, 3, 2));
		
	}
	else
	{
		// If we are a Soviet player (or a spectator) swap to a below-ground mesh that only Engineers can see
		`wwdebug("using buried mesh",'M');
		ThrowableMesh.SetScale3D(Vect(1, 1, 1));
		ThrowableMesh.SetSkeletalMesh(SkeletalMesh'WinterWar_WP_FIN_M39.Mesh.FIN_M39_Planted');
		ThrowableMesh.SetMaterial(0, class'WWPawn'.default.MD82MICTemplate);
	}
}

simulated function ProcessTouch(Actor Other, Vector HitLocation, Vector HitNormal)
{
	local ROPlayerReplicationInfo ROPRI;
	local ROVehicleBase VehicleVictim;
	
	VehicleVictim = ROVehicleBase(Other);
	
	if (VehicleVictim != none)
	{
		if (!bTriggered)
		{
			ROPRI = ROPlayerReplicationInfo(Pawn(Other).PlayerReplicationInfo);
			
			if (bFriendlyTriggers || ROPRI.Team.TeamIndex != OwningTeam)
			{
				bTriggered = true;
				NetCullDistanceSquared=+100000000.0; // increase distance to 200 meters
				bForceNetUpdate=true;
				NetUpdateFrequency=10;
				bNetDirty = true;
				TriggeredBy = Pawn(Other);
			}
		}
	}
}

defaultproperties
{
	MaxAllowed=8
	FuseLength=0.1
	Damage=200
	DamageRadius=250
	MomentumTransfer=50000
	
	ExplosionSound=			AkEvent'WW_EXP_C4.Play_EXP_C4_Explosion'
	MyExplosionSound=		AkEvent'WW_EXP_C4.Play_EXP_C4_Explosion'
	MyExplosionMediumSound=	AkEvent'WW_EXP_C4.Play_EXP_C4_Explosion'
	
	MyDamageType=class'WWDmgType_ATMine'
	
	TacticalIcon=Texture2D'VN_UI_Textures.HUD.DeathMessage.UI_Kill_Icon_MD82Mine'
	
	bDamageTriggers=false
	bVehicleTriggers=true
	bTriggerOnUntouch=false
	
	Begin Object Name=CollisionCylinder
		CollisionRadius=50
		CollisionHeight=15
		CollideActors=True
		AlwaysLoadOnClient=True
		AlwaysLoadOnServer=True
		Translation=(Z=21.5)
	End Object
	
	Begin Object Name=ThowableMeshComponent
		SkeletalMesh=SkeletalMesh'WinterWar_WP_FIN_M39.Mesh.FIN_M39_Planted'
		bHasPhysicsAssetInstance=false
		MaxDrawDistance=1500
	End Object
}
