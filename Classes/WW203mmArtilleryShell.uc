
class WW203mmArtilleryShell extends ROArtilleryShell;

DefaultProperties
{
	ExplosionSound=AkEvent'WW_CMD_Artillery.Play_CMD_Artillery_EXP_Rocket'
	
	Damage=700
	DamageRadius=1800
	
	ShakeScale=1.5
	MaxSuppressBlurDuration=8.0
	SuppressBlurScalar=2.3
	SuppressAnimationScalar=0.85
	ExplodeExposureScale=0.306
	
	ProjExplosionTemplate=ParticleSystem'FX_VN_Weapons.Explosions.FX_VN_Rocket'
	
	bUseFireSoundLengthAsDelay = true
}
