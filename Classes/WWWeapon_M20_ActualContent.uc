
class WWWeapon_M20_ActualContent extends WWWeapon_M20;

DefaultProperties
{
	ArmsAnimSet=AnimSet'WinterWar_WP_FIN_M20.Anim.FIN_M20_Anims'
	
	Begin Object Name=FirstPersonMesh
		DepthPriorityGroup=SDPG_Foreground
		SkeletalMesh=SkeletalMesh'WinterWar_WP_FIN_M20.Mesh.FIN_M20'
		PhysicsAsset=PhysicsAsset'WinterWar_WP_FIN_M20.Phy.FIN_M20_Physics'
		AnimSets(0)=AnimSet'WinterWar_WP_FIN_M20.Anim.FIN_M20_Anims'
		AnimTreeTemplate=AnimTree'WinterWar_WP_FIN_M20.Anim.FIN_M20_Tree'
		Scale=1.0
		FOV=70
	End Object
	
	Begin Object Name=PickupMesh
		SkeletalMesh=SkeletalMesh'WinterWar_WP_FIN_M20.Mesh.FIN_M20_3rd'
		PhysicsAsset=PhysicsAsset'WinterWar_WP_FIN_M20.Phy.FIN_M20_3rd_Physics'
		CollideActors=true
		BlockActors=true
		BlockZeroExtent=true
		BlockNonZeroExtent=true//false
		BlockRigidBody=true
		bHasPhysicsAssetInstance=false
		bUpdateKinematicBonesFromAnimation=false
		PhysicsWeight=1.0
		RBChannel=RBCC_GameplayPhysics
		RBCollideWithChannels=(Default=TRUE,GameplayPhysics=TRUE,EffectPhysics=TRUE)
		bSkipAllUpdateWhenPhysicsAsleep=TRUE
		bSyncActorLocationToRootRigidBody=true
	End Object
	
	AttachmentClass=class'WWWeapon_M20_Attach'
}
