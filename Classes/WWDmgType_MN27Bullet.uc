
class WWDmgType_MN27Bullet extends RODmgType_SmallArmsBullet
	abstract;

defaultproperties
{
	WeaponShortName="MN27"
	KDamageImpulse=425 // kg * uu/s
	BloodSprayTemplate=ParticleSystem'FX_VN_Impacts.BloodNGore.FX_VN_BloodSpray_Clothes_large'
}