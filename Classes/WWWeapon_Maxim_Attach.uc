
class WWWeapon_Maxim_Attach extends ROWeapAttach_M2_HMG_Tripod;

defaultproperties
{
	MuzzleFlashPSCTemplate=ParticleSystem'FX_VN_Weapons.MuzzleFlashes.FX_VN_MuzzleFlash_1stP_Rifles_split_alwaysFlash'
	
	WeaponClass=class'WWWeapon_Maxim'
	
	ShellEjectPSCTemplate=ParticleSystem'FX_VN_Weapons.ShellEjects.FX_Wep_ShellEject_VC_DShK'
	
	// TracerClass=class'M2BulletTracer'
	TracerFrequency=0
	
	ReloadAnims(0)=Maxim_ReloadEmpty
	ReloadAnims(1)=Maxim_ReloadEmpty
	
	CHR_AnimSet=AnimSet'CHR_Playeranim_Master.Anim.CHR_Tripod_anim'
}
