
class WWWeapon_Maxim_ActualContent extends WWWeapon_Maxim;

DefaultProperties
{
	ArmsAnimSet=AnimSet'WinterWar_WP_SOV_MAXIM.Anim.WP_MaximHands'
	
	Begin Object Name=FirstPersonMesh
		SkeletalMesh=SkeletalMesh'WinterWar_WP_SOV_MAXIM.Mesh.Sov_Maxim'
		PhysicsAsset=PhysicsAsset'WinterWar_WP_SOV_MAXIM.Phy.Sov_Maxim_Physics'
		AnimSets(0)=AnimSet'WinterWar_WP_SOV_MAXIM.Anim.WP_MaximHands'
		AnimTreeTemplate=AnimTree'WinterWar_WP_SOV_MAXIM.Anim.MaximTurretAnimtree'
	End Object
	
	Begin Object Name=FirstPersonArmsMesh
		AnimSets(1)=AnimSet'WinterWar_WP_SOV_MAXIM.Anim.WP_MaximHands'
	End Object
	
	Begin Object Name=AmmoBelt0
		SkeletalMesh=SkeletalMesh'WinterWar_WP_SOV_MAXIM.Mesh.Sov_Maxim_Belt'
		PhysicsAsset=PhysicsAsset'WinterWar_WP_SOV_MAXIM.Phy.Sov_Maxim_Belt_Physics'
		AnimSets.Add(AnimSet'WinterWar_WP_SOV_MAXIM.Anim.WP_MaximHands')
		DepthPriorityGroup=SDPG_Foreground
		bOnlyOwnerSee=true
		MaxAmmoShown=18
	End Object
	AmmoBeltMesh=AmmoBelt0
	
	AttachmentClass=class'WWWeapon_Maxim_Attach'
	
	WeaponFireSnd(DEFAULT_FIREMODE)=(DefaultCue=AkEvent'WW_WEP_RP46.Play_WEP_RP46_Loop_3P', FirstPersonCue=AkEvent'WW_WEP_RP46.Play_WEP_RP46_Loop')
	bLoopingFireSnd(DEFAULT_FIREMODE)=true
	WeaponFireLoopEndSnd(DEFAULT_FIREMODE)=(DefaultCue=AkEvent'WW_WEP_RP46.Play_WEP_RP46_Tail_3P', FirstPersonCue=AkEvent'WW_WEP_RP46.Play_WEP_RP46_Stereo_Tail')
	bLoopHighROFSounds(DEFAULT_FIREMODE)=true
}
