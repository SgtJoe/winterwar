
class WWUISceneTeamSelect extends ROUISceneTeamSelect;

var localized array<string> TeamDescriptions;

function InitializeButtonStyle(ROPlayerController ROPC)
{
	NorthButtonImageEnabled = NorthEnabledLogos[0];
	NorthButtonImageDisabled = NorthDisabledLogos[0];
	NorthButtonImageHighlighted = NorthHighlightedLogos[0];
	
	SouthButtonImageEnabled = SouthEnabledLogos[0];
	SouthButtonImageDisabled = SouthDisabledLogos[0];
	SouthButtonImageHighlighted = SouthHighlightedLogos[0];
	
	TeamLabelButtons[0].SetCaption(class'WWMapInfo'.default.NorthernArmyNames[0]);
	TeamLabelButtons[1].SetCaption(class'WWMapInfo'.default.SouthernArmyNames[0]);
	
	TeamImages[0].SetValue(NorthButtonImageEnabled);
	TeamImages[1].SetValue(SouthButtonImageEnabled);
}

function ShowTeamInfo()
{
	local UIPanel Container;
	Container = UIPanel(FindChild(TeamInfoContainerName, true));
	
	if( TeamInfoTeamIndex == `AXIS_TEAM_INDEX )
	{
		UILabel(Container.FindChild(TeamInfoLabelName, true)).SetValue(TeamDescriptions[0]);
	}
	else
	{
		UILabel(Container.FindChild(TeamInfoLabelName, true)).SetValue(TeamDescriptions[1]);
	}
	
	Container.SetDockParameters(UIFACE_Top, TeamImages[TeamInfoTeamIndex], UIFACE_Top, 0.5, UIPADDINGEVAL_PercentTarget);
	Container.SetDockParameters(UIFACE_Left, TeamImages[TeamInfoTeamIndex], UIFACE_Left, 0.0);
	Container.SetDockParameters(UIFACE_Right, TeamImages[TeamInfoTeamIndex], UIFACE_Right, 0.0);
	Container.SetVisibility(TeamInfoTeamIndex < 2);
}

function bool AllowTeamSwitch()
{
`ifndef(RELEASE)
	return true;
`endif
	return super.AllowTeamSwitch();
}

defaultproperties
{
	NorthEnabledLogos(0)=		MaterialInstanceConstant'WinterWar_UI.TeamSelect.Team_Cutout_FIN_desat'
	NorthDisabledLogos(0)=		MaterialInstanceConstant'WinterWar_UI.TeamSelect.Team_Cutout_FIN_disabled'
	NorthHighlightedLogos(0)=	MaterialInstanceConstant'WinterWar_UI.TeamSelect.Team_Cutout_FIN_highlighted'
	
	SouthEnabledLogos(0)=		MaterialInstanceConstant'WinterWar_UI.TeamSelect.Team_Cutout_USSR_desat'
	SouthDisabledLogos(0)=		MaterialInstanceConstant'WinterWar_UI.TeamSelect.Team_Cutout_USSR_disabled'
	SouthHighlightedLogos(0)=	MaterialInstanceConstant'WinterWar_UI.TeamSelect.Team_Cutout_USSR_highlighted'
}
