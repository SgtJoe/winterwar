
class WWWeapon_MosinBase_Attach extends ROWeapAttach_MN9130_Rifle;

defaultproperties
{
	Begin Object Name=SkeletalMeshComponent0
		AnimSets(0)=AnimSet'WP_VN_3rd_Master.Anim.MN9130_3rd_anim'
		AnimTreeTemplate=AnimTree'WP_VN_3rd_Master.AnimTree.MN9130_3rd_Tree'
		PhysicsAsset=PhysicsAsset'WP_VN_3rd_Master.Phy_Bounds.MN9130_3rd_Bounds_Physics'
		CullDistance=5000
	End Object
	
	WeaponClass=class'ROWeap_MN9130_Rifle'
}
