
class WWVehicleFactory_T20 extends WWVehicleFactory;

defaultproperties
{
	Begin Object Name=SVehicleMesh
		SkeletalMesh=SkeletalMesh'WinterWar_VH_USSR_T-20.Mesh.T-20_PHAT'
	End Object
	
	Begin Object Name=CollisionCylinder
		CollisionHeight=+60.0
		CollisionRadius=+90.0
		Translation=(X=0.0,Y=0.0,Z=10.0)
		bAlwaysRenderIfSelected=true
	End Object
	
	VehicleClass=class'WWVehicle_T20_ActualContent'
}
