
class WWCharCustMannequin extends ROCharCustMannequin
	notplaceable;

var StaticMeshComponent BadgeComponent;
var StaticMesh BadgeMesh;

event PostBeginPlay()
{
	super.PostBeginPlay();
	
	PawnHandlerClass = class'WWPawnHandler';
}

function UpdateMannequin(byte TeamIndex, byte ArmyIndex, bool bPilot, int ClassIndex,  byte HonorLevel, byte TunicID, byte TunicMaterialID, byte ShirtID, byte HeadID, byte HairID, byte HeadgearID, byte HeadgearMatID, byte FaceItemID, byte FacialHairID, byte TattooID, optional bool bMainMenu)
{
	local byte byteDisposal;
	local byte bNoHeadgear;
	local ROMapInfo ROMI;
	local rotator InitialRot;
	
	if( ArmyIndex == 255 )
		return;
	
	InitialRot = mesh.Rotation;
	
	DisplayedCharConfig.TunicMesh = TunicID;
	DisplayedCharConfig.TunicMaterial = TunicMaterialID;
	DisplayedCharConfig.ShirtTexture = ShirtID;
	DisplayedCharConfig.HeadMesh = HeadID;
	DisplayedCharConfig.HairMaterial = HairID;
	DisplayedCharConfig.HeadgearMesh = HeadgearID;
	DisplayedCharConfig.HeadgearMat = HeadgearMatID;
	DisplayedCharConfig.FaceItemMesh = FaceItemID;
	DisplayedCharConfig.FacialHairMesh = FacialHairID;
	DisplayedCharConfig.TattooTex = TattooID;
	
	If(HeadAndArmsMIC == none)
		HeadAndArmsMIC = new class'MaterialInstanceConstant';
	if(BodyMIC == none)
		BodyMIC = new class'MaterialInstanceConstant';
	if(HeadgearMIC == none)
		HeadgearMIC = new class'MaterialInstanceConstant';
	if(HairMIC == none)
		HairMIC = new class'MaterialInstanceConstant';
	
	if( ThirdPersonHeadgearMeshComponent.AttachedToSkelComponent != none )
		Mesh.DetachComponent(ThirdPersonHeadgearMeshComponent);
	if( FaceItemMeshComponent.AttachedToSkelComponent != none )
		Mesh.DetachComponent(FaceItemMeshComponent);
	if( FacialHairMeshComponent.AttachedToSkelComponent != none )
		Mesh.DetachComponent(FacialHairMeshComponent);
	if( ThirdPersonHeadAndArmsMeshComponent.AttachedToSkelComponent != none )
		Mesh.DetachComponent(ThirdPersonHeadAndArmsMeshComponent);
	if( WeaponMeshComponent.AttachedToSkelComponent != none )
		Mesh.DetachComponent(WeaponMeshComponent);
	if( BadgeComponent.bAttached )
		mesh.DetachComponent(BadgeComponent);
	
	if( ClassIndex < 0 || bMainMenu )
	{
		ClassIndex = bPilot ? `RI_TANK_CREW : `RI_NOGEAR;
	}
	
	bIsPilot = bPilot;
	
	TunicMesh = class'WWPawnHandler'.static.GetTunicMeshes(TeamIndex, ArmyIndex, byte(bPilot), TunicID, bNoHeadgear);
	BodyMICTemplate = class'WWPawnHandler'.static.GetBodyMIC(TeamIndex, ArmyIndex, byte(bPilot), TunicID, TunicMaterialID);
	HeadAndArmsMesh = class'WWPawnHandler'.static.GetHeadAndArmsMesh(TeamIndex, ArmyIndex, byte(bPilot), HeadID, byteDisposal);
	FieldgearMesh = class'WWPawnHandler'.static.GetFieldgearMesh(TeamIndex, 0, TunicID, ClassIndex, byteDisposal);
	HeadAndArmsMICTemplate = class'WWPawnHandler'.static.GetHeadMIC(TeamIndex, ArmyIndex, HeadID, TunicID, byte(bPilot));
	HeadgearMesh = class'WWPawnHandler'.static.GetHeadgearMesh(TeamIndex, ArmyIndex, byte(bPilot), HeadID, HairID, HeadgearID, HeadgearMatID, HeadgearMICTemplate, HairMICTemplate, HeadgearAttachSocket, byteDisposal);
	FaceItemMesh = class'WWPawnHandler'.static.GetFaceItemMesh(TeamIndex, ArmyIndex, byte(bPilot), HeadgearID, FaceItemID, FaceItemAttachSocket, byteDisposal);
	FacialHairMesh = class'WWPawnHandler'.static.GetFacialHairMesh(TeamIndex, ArmyIndex, FacialHairID, FacialHairAttachSocket);
	BadgeMesh = class'WWPawnHandler'.static.GetBadgeMesh(TeamIndex, TunicID, ClassIndex, 0);
	
	if (TeamIndex == `AXIS_TEAM_INDEX && TunicID == 4)
	{
		FaceItemMesh = none;
		FacialHairMesh = none;
	}
	
	BodyMIC.SetParent(BodyMICTemplate);
	HeadAndArmsMIC.SetParent(HeadAndArmsMICTemplate);
	HeadgearMIC.SetParent(HeadgearMICTemplate);
	HairMIC.SetParent(HairMICTemplate);
	
	ROMI = ROMapInfo(WorldInfo.GetMapInfo());
	
	if(ROMI != none)
	{
		CompositedBodyMesh = ROMI.GetCachedCompositedPawnMesh(TunicMesh, FieldgearMesh);
	}
	else
	{
		return;
	}
	
	CompositedBodyMesh.Characterization = PlayerHIKCharacterization;
	
	Mesh.ReplaceSkeletalMesh(CompositedBodyMesh);
	
	Mesh.SetMaterial(0, BodyMIC);
	
	/* 1.2.2 New Customization
	if (TeamIndex == `ALLIES_TEAM_INDEX)
	{
		Mesh.SetMaterial(1, BodyMIC);
	}
	*/
	
	Mesh.GenerateAnimationOverrideBones(HeadAndArmsMesh);
	
	ThirdPersonHeadAndArmsMeshComponent.SetSkeletalMesh(HeadAndArmsMesh);
	ThirdPersonHeadAndArmsMeshComponent.SetMaterial(0, HeadAndArmsMIC);
	ThirdPersonHeadAndArmsMeshComponent.SetParentAnimComponent(Mesh);
	ThirdPersonHeadAndArmsMeshComponent.SetShadowParent(Mesh);
	ThirdPersonHeadAndArmsMeshComponent.SetLODParent(Mesh);
	AttachComponent(ThirdPersonHeadAndArmsMeshComponent);
	
	if( HeadgearMesh != none && bNoHeadgear == 0 )
	{
		AttachNewHeadgear(HeadgearMesh);
	}
	
	if( FaceItemID > 0 && FaceItemMesh != none )
	{
		AttachNewFaceItem(FaceItemMesh);
	}
	
	if( FacialHairID > 0 && FacialHairMesh != none )
	{
		AttachNewFacialHair(FacialHairMesh);
	}
	
	if ( BadgeMesh != none )
	{
		AttachBadge(BadgeMesh);
	}
	
	AttachPreviewWeapon(TeamIndex, ArmyIndex);
	
	mesh.SetRotation(InitialRot);
}

function AttachNewHeadgear(SkeletalMesh NewHeadgearMesh)
{
	local SkeletalMeshSocket HeadSocket;
	
	ThirdPersonHeadgearMeshComponent.SetSkeletalMesh(NewHeadgearMesh);
	ThirdPersonHeadgearMeshComponent.SetMaterial(0, HeadgearMIC);
	
	HeadSocket = ThirdPersonHeadAndArmsMeshComponent.GetSocketByName(HeadgearAttachSocket);
	
	if( HeadSocket != none )
	{
		if( mesh.MatchRefBone(HeadSocket.BoneName) != INDEX_NONE )
		{
			ThirdPersonHeadgearMeshComponent.SetShadowParent(mesh);
			ThirdPersonHeadgearMeshComponent.SetLODParent(mesh);
			mesh.AttachComponent( ThirdPersonHeadgearMeshComponent, HeadSocket.BoneName, HeadSocket.RelativeLocation, HeadSocket.RelativeRotation, HeadSocket.RelativeScale);
		}
	}
}

function AttachBadge(StaticMesh NewBadgeMesh)
{
	if ( Mesh.GetSocketByName(class'WWPawn'.default.BadgeAttachSocket) != none )
	{
		BadgeComponent.SetStaticMesh(NewBadgeMesh);
		
		BadgeComponent.SetShadowParent(Mesh);
		Mesh.AttachComponentToSocket(BadgeComponent, class'WWPawn'.default.BadgeAttachSocket);
	}
}

function AttachPreviewWeapon(byte TeamIndex, byte ArmyIndex) {}

defaultproperties
{
	Begin Object class=StaticMeshComponent name=BadgeComponent0
		LightEnvironment=MyLightEnvironment
		CastShadow=FALSE
		MaxDrawDistance=1000
		CollideActors=false
		BlockActors=false
		BlockZeroExtent=false
		BlockNonZeroExtent=false
	End Object
	BadgeComponent=BadgeComponent0
}
