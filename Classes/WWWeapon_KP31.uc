
class WWWeapon_KP31 extends ROWeap_PPSH41_SMG
	abstract;

`include(WinterWar\Classes\WWWeaponPickupMessagesOverride.uci)

defaultproperties
{
	WeaponContentClass.Empty
	WeaponContentClass(0)="WinterWar.WWWeapon_KP31_ActualContent"
	
	AltAmmoLoadouts.Empty
	AmmoContentClassStart=0
	
	RoleSelectionImage.Empty
	RoleSelectionImage(0)=Texture2D'WinterWar_UI.WP_Render.WP_Render_KP31'
	
	TeamIndex=`AXIS_TEAM_INDEX
	
	InvIndex=`WI_KP31
	
	WeaponProjectiles(DEFAULT_FIREMODE)=	class'WWProjectile_KP31Bullet'
	WeaponProjectiles(ALTERNATE_FIREMODE)=	class'WWProjectile_KP31Bullet'
	
	maxRecoilPitch=170//150
	minRecoilPitch=170//150//115
	maxRecoilYaw=80//95
	minRecoilYaw=-80//-35

	RecoilModWhenEmpty = 1.2f
	LagLimit=3.0
	ZoomInTime=0.5
	ZoomOutTime=0.35

	EquipTime=+0.8//1.2
	PutDownTime=+0.66//1.00

	SwayScale=1.35
	
	InstantHitDamage(0)=72
	InstantHitDamage(1)=72
	
	InstantHitDamageTypes(0)=class'WWDmgType_KP31Bullet'
	InstantHitDamageTypes(1)=class'WWDmgType_KP31Bullet'
	
	MuzzleFlashPSCTemplate=ParticleSystem'WinterWar_FX.ParticleSystems.FX_WW_MuzzleFlash_1stP_SMG'
	
	WeaponReloadEmptyMagAnim=KP_reload
	
	AmmoClass=class'ROAmmo_762x25_PPSHDrum'
	MaxAmmoCount=71
	InitialNumPrimaryMags=2
	
	PlayerViewOffset=(X=1.5,Y=4.5,Z=-1.25)
	ShoulderedPosition=(X=0.5,Y=2,Z=-0.5)
	IronSightPosition=(X=-3.0,Y=0,Z=0.0)
	
	SightRanges.Empty
	SightRanges[0]=(SightRange=100,SightPitch=16384,SightPositionOffset=-0.03)
}
