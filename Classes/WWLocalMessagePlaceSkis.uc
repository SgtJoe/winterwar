
class WWLocalMessagePlaceSkis extends ROLocalMessagePlantedItem;

var	localized string SkisPlaceHint;

static function string GetString(optional int Switch, optional bool bPRI1HUD, optional PlayerReplicationInfo RelatedPRI_1, optional PlayerReplicationInfo RelatedPRI_2, optional Object OptionalObject)
{
	return repl(default.SkisPlaceHint, "%FIREKEY%", GetFireKeyName(PlayerController(OptionalObject)));
}
