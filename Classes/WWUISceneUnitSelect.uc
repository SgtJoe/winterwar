
class WWUISceneUnitSelect extends ROUISceneUnitSelect;

event PostInitialize()
{
	super.PostInitialize();
	
	PrimaryWeaponAmmoList = WWUIWidgetWeaponAmmoList(FindChild(PrimaryWeaponAmmoListName, true));
}

function SetPrimaryAmmoImageAndCount(byte DesiredAmmoVariant)
{
	local int PriMags, SecMags, PriImageIndex, SecImageIndex;
	
	if( SelectedPrimaryWeaponClass == none )
	{
		PrimaryWeaponAmmoImages[0].SetVisibility(false);
		PrimaryWeaponAmmoImages[1].SetVisibility(false);
		PrimaryWeaponAmmoLabels[0].SetValue("");
		PrimaryWeaponAmmoLabels[1].SetValue("");
		return;
	}
	else
	{
		PrimaryWeaponAmmoImages[1].SetVisibility(true);
	}
	
	GetPrimaryAmmoDisplayDetails(DesiredAmmoVariant, PriMags, SecMags, PriImageIndex, SecImageIndex);
	
	if( SecMags > 0 )
	{
		PrimaryWeaponAmmoLabels[0].SetValue("x"$PriMags);
		PrimaryWeaponAmmoLabels[1].SetValue("x"$SecMags);
		
		if( PriImageIndex > -1 )
			PrimaryWeaponAmmoImages[0].ImageComponent.SetImage(class'WWHUD'.default.AmmoUVs[PriImageIndex].MagTexture);
		
		if( SecImageIndex > -1 )
			PrimaryWeaponAmmoImages[1].ImageComponent.SetImage(class'WWHUD'.default.AmmoUVs[SecImageIndex].MagTexture);
		
		PrimaryWeaponAmmoImages[0].SetVisibility(true);
		PrimaryWeaponAmmoTooltip.SetValue(class'WWHUD'.default.AmmoUVs[PriImageIndex].AmmoDescription $ "\n" $ class'WWHUD'.default.AmmoUVs[SecImageIndex].AmmoDescription);
	}
	else
	{
		PrimaryWeaponAmmoLabels[0].SetValue("");
		PrimaryWeaponAmmoLabels[1].SetValue("x"$PriMags);
		PrimaryWeaponAmmoImages[0].ImageComponent.SetImage(none);
		
		if( PriImageIndex > -1 )
			PrimaryWeaponAmmoImages[1].ImageComponent.SetImage(class'WWHUD'.default.AmmoUVs[PriImageIndex].MagTexture);
		
		PrimaryWeaponAmmoImages[0].SetVisibility(false);
		PrimaryWeaponAmmoTooltip.SetValue(class'WWHUD'.default.AmmoUVs[PriImageIndex].AmmoDescription);
	}
}

function GetPrimaryAmmoDisplayDetails(byte AmmoVariant, out int PrimaryMagCount, out int SecondaryMagCount, out int PrimaryAmmoIndex, out int SecondaryAmmoIndex)
{
	local int ContentClassIdx;
	local name PriAmmoName, SecAmmoName;
	local class<ROWeapon> WeaponContentClass;
	local array<ROAmmoUV> AmmoUVs;
	
	ContentClassIdx = 255;
	
	if( AmmoVariant > 0 && SelectedPrimaryWeaponContentClass.default.AltAmmoLoadouts.length > 0 )
		ContentClassIdx = SelectedPrimaryWeaponContentClass.default.AltAmmoLoadouts[AmmoVariant - 1].WeaponContentClassIndex;
	
	if( ContentClassIdx < 255 )
		WeaponContentClass = class<ROWeapon>(DynamicLoadObject(SelectedPrimaryWeaponClass.default.WeaponContentClass[ContentClassIdx],class'Class'));
	else
		WeaponContentClass = SelectedPrimaryWeaponContentClass;
	
	if( AmmoVariant > 0 && WeaponContentClass.default.AltAmmoLoadouts.length > 0 && ContentClassIdx == 255 )
	{
		PrimaryMagCount = WeaponContentClass.default.AltAmmoLoadouts[AmmoVariant - 1].InitialNumPrimaryMags;
		SecondaryMagCount = WeaponContentClass.default.AltAmmoLoadouts[AmmoVariant - 1].InitialNumSecondaryMags;
		PriAmmoName = WeaponContentClass.default.AltAmmoLoadouts[AmmoVariant - 1].PrimaryAmmoClass.Name;
		if( WeaponContentClass.default.AltAmmoLoadouts[AmmoVariant - 1].SecondaryAmmoClass != none )
			SecAmmoName = WeaponContentClass.default.AltAmmoLoadouts[AmmoVariant - 1].SecondaryAmmoClass.Name;
	}
	else
	{
		PrimaryMagCount = WeaponContentClass.default.InitialNumPrimaryMags;
		SecondaryMagCount = WeaponContentClass.default.InitialNumSecondaryMags;
		
		if( WeaponContentClass.default.PrimaryAmmoClass != none )
			PriAmmoName = WeaponContentClass.default.PrimaryAmmoClass.Name;
		else
			PriAmmoName = WeaponContentClass.default.AmmoClass.Name;
		
		if( WeaponContentClass.default.SecondaryAmmoClass != none )
			SecAmmoName = WeaponContentClass.default.SecondaryAmmoClass.Name;
	}
	
	AmmoUVs = class'WWHUD'.default.AmmoUVs;
	
	if( PriAmmoName != '' )
		PrimaryAmmoIndex = AmmoUVs.Find('AmmoClass', PriAmmoName);
	
	if( SecAmmoName != '' )
		SecondaryAmmoIndex = AmmoUVs.Find('AmmoClass', SecAmmoName);
}

// Don't play Vietnam tutorials for Winter War
// TODO: Update when we have our own tutorials
function bool ShouldPlayTutorialVideo()
{
	return false;
}

defaultproperties
{
	NorthBackgroundImages[0]=Texture2D'WinterWar_UI.WP_Render.TeamSelect_FIN_UnitSelect'
	SouthBackgroundImages[0]=Texture2D'WinterWar_UI.WP_Render.TeamSelect_SOV_UnitSelect'
	
	PrimaryWeaponAmmoListName=WWUIWidgetWeaponAmmoList_Primary
}
