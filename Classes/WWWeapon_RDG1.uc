
class WWWeapon_RDG1 extends ROWeap_RDG1_Smoke
	abstract;

`include(WinterWar\Classes\WWWeaponPickupMessagesOverride.uci)

defaultproperties
{
	WeaponContentClass(0)="WinterWar.WWWeapon_RDG1_ActualContent"
	
	RoleSelectionImage(0)=Texture2D'VN_UI_Textures.WeaponTex.VN_Weap_RDG1_Smoke'
	
	TeamIndex=`ALLIES_TEAM_INDEX
	
	InvIndex=`WI_RDG1
	
	AmmoClass=class'ROAmmo_RDG1_Smoke'
}
