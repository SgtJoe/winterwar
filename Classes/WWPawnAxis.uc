
class WWPawnAxis extends WWPawn;

simulated event byte ScriptGetTeamNum()
{
	return `AXIS_TEAM_INDEX;
}

defaultproperties
{
	TunicMesh=SkeletalMesh'WinterWar_CHR_FIN.Mesh.FIN_Coat'
	PawnMesh_SV=SkeletalMesh'WinterWar_CHR_FIN.Mesh.FIN_SV'
	FieldgearMesh=SkeletalMesh'WinterWar_CHR_FIN.Mesh.FIN_Gear_Rifleman'
	BodyMICTemplate=MaterialInstanceConstant'WinterWar_CHR_FIN.MIC.FIN_Coat'
	
	BadgeMesh=none
	
	ArmsOnlyMeshFP=SkeletalMesh'WinterWar_CHR_ALL.Mesh.FIN_Hands_Coat'
	
	HeadAndArmsMesh=SkeletalMesh'WinterWar_CHR_FIN.Mesh.FIN_Head_1'
	HeadAndArmsMICTemplate=MaterialInstanceConstant'WinterWar_CHR_FIN.MIC.FIN_Head_1'
	
	HeadgearMesh=SkeletalMesh'WinterWar_CHR_FIN.Mesh.FIN_Headgear_Ushanka'
	HeadgearMICTemplate=MaterialInstanceConstant'WinterWar_CHR_FIN.MIC.FIN_Coat'
	
	Begin Object Name=ROPawnSkeletalMeshComponent
		AnimSets(11)=AnimSet'WinterWar_CHR_ALL.Anim.CHR_Finnish_Unique'
	End Object
	
	bSingleHandedSprinting=true
}
