
class WWWeapon_LahtiSaloranta extends ROWeap_L2A1_LMG
	abstract;

`include(WinterWar\Classes\WWWeaponPickupMessagesOverride.uci)

defaultproperties
{
	WeaponContentClass(0)="WinterWar.WWWeapon_LahtiSaloranta_ActualContent"
	
	RoleSelectionImage(0)=Texture2D'WinterWar_UI.WP_Render.WP_Render_LahtiSaloranta'
	
	TeamIndex=`AXIS_TEAM_INDEX
	
	InvIndex=`WI_LS26
	
	WeaponProjectiles(0)=class'WWProjectile_LS26Bullet'
	WeaponProjectiles(ALTERNATE_FIREMODE)=class'WWProjectile_LS26Bullet'
	FireInterval(0)=+0.12 //~500rpm
	Spread(0)=0.0008 // ~ 3 MOA
	
	AltFireModeType=ROAFT_None
	
	maxRecoilPitch=320//200//280
	minRecoilPitch=290//210//150//330
	maxRecoilYaw=100//100//180
	minRecoilYaw=-90//-180
	minRecoilYawAbsolute=50
	maxDeployedRecoilPitch=40//80
	minDeployedRecoilPitch=40//80
	maxDeployedRecoilYaw=20
	minDeployedRecoilYaw=-20
	minDeployedRecoilYawAbsolute=25
	RecoilRate=0.1
	RecoilMaxYawLimit=500
	RecoilMinYawLimit=65035
	RecoilMaxPitchLimit=1500
	RecoilMinPitchLimit=64785
	RecoilISMaxYawLimit=500
	RecoilISMinYawLimit=65035
	RecoilISMaxPitchLimit=350
	RecoilISMinPitchLimit=65035
	RecoilBlendOutRatio=1
	RecoilViewRotationScale=0.7
	
	RecoilCompensationScale=1.10
	LagLimit=3.0
	ZoomInTime=0.55
	ZoomOutTime=0.4

	EquipTime=+0.87
	PutDownTime=+0.7
	
	InstantHitDamageTypes(0)=class'WWDmgType_LS26Bullet'
	InstantHitDamageTypes(1)=class'WWDmgType_LS26Bullet'
	
	MuzzleFlashPSCTemplate=ParticleSystem'WinterWar_FX.ParticleSystems.FX_WW_MuzzleFlash_1stP_LMG'
	
	AmmoClass=class'WWAmmo_LS26Mag'
	MaxAmmoCount=21
	InitialNumPrimaryMags=8
	NumMagsToResupply=2
	MaxNumPrimaryMags=12
	
	TracerClass=class'WWProjectile_LS26BulletTracer'
	TracerFrequency=5
	
	PlayerViewOffset=(X=6.0,Y=4.5,Z=-1.25)
	ShoulderedPosition=(X=5.0,Y=2,Z=-0.5)
	IronSightPosition=(X=1.0,Y=0.42,Z=0.0)
	
	WeaponReloadEmptyMagAnim=LS_Bipod_reloadempty
	DeployReloadEmptyMagAnim=LS_deploy_reloadempty
	
	CollisionCheckLength=55.45
	
	SwayScale=1.7

	ROBarrelClass=class'WWMGBarrelLS26'
	
	BipodZCheckDist=25.0
	BipodOffset=(X=48.0)
	
	SightRanges.Empty
	SightRanges[0]=(SightRange=200,SightPitch=200,SightSlideOffset=0.1,SightPositionOffset=-0.10)
	
	bStationaryReload=true
}
