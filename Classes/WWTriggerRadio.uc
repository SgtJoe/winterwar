
class WWTriggerRadio extends ROTriggerRadio
	placeable;

function Timer()
{
	local WWPlayerController PC;
	
	if ( !bWaiting )
	{
		if ( SavedUser != none )
		{
			if (  SavedUser.Controller != none )
			{
				PC = WWPlayerController(SavedUser.Controller);
			}
			else if ( ROPawn(SavedUser) != none && SavedUser.DrivenVehicle != none && SavedUser.LastRealViewer != none )
			{
				PC = WWPlayerController(SavedUser.LastRealViewer);
			}
			
			if ( PC != none )
			{
				PC.HitThis(self);
				bWaiting = true;
				SetTimer(10.0, false);
				BackupROPC = none;
			}
		}
		else
		{
			bAvailable = true;
			bWaiting = false;
			if ( BackupROPC != none )
			{
				BackupROPC.ClientClearRadioCallin();
				BackupROPC = none;
			}
		}
	}
	else
	{
		bAvailable = true;
		bWaiting = false;
	}
}

function RequestArty(Pawn PawnUser, byte ArtyType)
{
	local ROPlayerController ROPC;
	local ROVolumeTest RVT;
	local ROPlayerReplicationInfo ROPRI;
	local ROTeamInfo ROTI;
	
	ROPC = ROPlayerController(PawnUser.Controller);
	
	if ( ROPC == none )
	{
		return;
	}
	
	if ( !bEnabled || !bAvailable )
	{
		ROPC.ClientClearRadioCallin();
		return;
	}
	
	SavedUser = none;
	
	ROPRI = ROPlayerReplicationInfo(ROPC.PlayerReplicationInfo);
	ROTI = ROTeamInfo(ROPRI.Team);
	
	if ( ROPRI == none || ROPRI.RoleInfo == none || !ROPRI.RoleInfo.bIsTeamLeader || ROTI == none )
	{
		ROPC.ClientClearRadioCallin();
		return;
	}
	
	if ( ROTI.SavedArtilleryCoords == vect(-999999.0, -999999.0, -999999.0) )
	{
		ROPC.ClientClearRadioCallin();
		ROPC.ReceiveLocalizedMessage(class'ROLocalMessageArtillery', ROAMSG_NoCoords,,,self);
		return;
	}
	
	if ( ROPC != none )
	{
		RVT = Spawn(class'ROVolumeTest',self,, ROTI.SavedArtilleryCoords);
		if ( RVT != none && RVT.IsInNoArtyVolume() )
		{
			ROPC.ClientClearRadioCallin();
			ROPC.ReceiveLocalizedMessage(class'ROLocalMessageArtillery', ROAMSG_InvalidTarget,,,self);
			RVT.Destroy();
			return;
		}
		RVT.Destroy();
	}
	
	if ( ApprovePlayerTeam(PawnUser.GetTeamNum()) )
	{
		SavedUser = PawnUser;
		bAvailable = false;
		
		TH_CommanderSwitchSidesCheck();
		
		if ( SavedUser.Controller != none )
			ROPC = ROPlayerController(SavedUser.Controller);
		
		BackupROPC = ROPC;
		
		`wwdebug(""$ ArtyType, 'CMDA');
		
		switch ( ArtyType )
		{
			case 0: // Finnish Mortars - Soviet Light Artillery
				if ( ROPC != none )
				{
					ROPlayerController(SavedUser.Controller).ReceiveLocalizedMessage(class'ROLocalMessageArtillery', ROAMSG_RequestStrike);
				}
				ROGameInfo(WorldInfo.Game).BroadcastLocalizedVoiceCom(`VOICECOM_RequestAbility1, PawnUser, , , , true, PawnUser.GetTeamNum());
				break;
			
			case 1: // Finnish Artillery - Soviet Heavy Artillery
				if ( ROPC != none )
				{
					ROPlayerController(SavedUser.Controller).ReceiveLocalizedMessage(class'ROLocalMessageArtillery', ROAMSG_RequestStrike);
				}
				ROGameInfo(WorldInfo.Game).BroadcastLocalizedVoiceCom(`VOICECOM_RequestAbility2, PawnUser, , , , true, PawnUser.GetTeamNum());
				break;
			
			case 2: // Soviet Bombers (see RequestAntiAir for Finnish Fighter Planes)
				if ( ROPC != none )
				{
					ROPlayerController(SavedUser.Controller).ReceiveLocalizedMessage(class'ROLocalMessageAirSupport', ROMSG_RequestBomber);
				}
				ROGameInfo(WorldInfo.Game).BroadcastLocalizedVoiceCom(`VOICECOM_RequestAbility3, PawnUser, , , , true, PawnUser.GetTeamNum());
				break;
		}
		
		ROPC.NotifyCommanderAbilityRequested(ArtyType, 12.5 + ROPC.GetTimerTimeForCommanderAbility(ArtyType));
		SetTimer(12.5, false); // Adjust this length for our sound files
	}
	else
	{
		ROPC.ClientClearRadioCallin();
	}
}

defaultproperties
{
	Begin Object Name=StaticMeshComponent0
		StaticMesh=StaticMesh'WinterWar_ENV_PropsMilitary.Mesh.Military_Radio_GermanRadio'
	End Object
}
