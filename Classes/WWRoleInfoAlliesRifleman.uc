
class WWRoleInfoAlliesRifleman extends WWRoleInfoAllies;

DefaultProperties
{
	RoleType=RORIT_Rifleman
	ClassTier=1
	ClassIndex=`RI_RIFLEMAN
	
	Items[RORIGM_Default]={(
		PrimaryWeapons=(class'WWWeapon_MN9130',class'WWWeapon_MN91'),
		
		OtherItems=(class'WWWeapon_RGD33')
	)}
	
	ClassIcon=Texture2D'WinterWar_UI.RoleIcons.RoleIcon_Rifleman'
}
