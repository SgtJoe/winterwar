
class WWVehicleFactory_T26 extends WWVehicleFactory;

defaultproperties
{
	Begin Object Name=SVehicleMesh
		SkeletalMesh=SkeletalMesh'WinterWar_VH_USSR_T-26.Mesh.T-26_PHAT'
	End Object
	
	Begin Object Name=CollisionCylinder
		CollisionHeight=60.0
		CollisionRadius=120.0
		Translation=(X=0.0,Y=0.0,Z=55.0)
		bAlwaysRenderIfSelected=true
	End Object
	
	VehicleClass=class'WWVehicle_T26_ActualContent'
}
