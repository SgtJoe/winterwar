
class WWWeapon_MN9130_Dyakonov_ActualContent extends WWWeapon_MN9130_Dyakonov;

simulated function SetupArmsAnim()
{
	super.SetupArmsAnim();
	
	// ArmsMesh.AnimSets has slots 0-2-3 filled, so we need to back fill slot 1 and then move to slot 4
	ROPawn(Instigator).ArmsMesh.AnimSets[1] = SkeletalMeshComponent(Mesh).AnimSets[0];
	ROPawn(Instigator).ArmsMesh.AnimSets[4] = SkeletalMeshComponent(Mesh).AnimSets[1];
}

defaultproperties
{
	ArmsAnimSet=AnimSet'WinterWar_WP_COMMON.Anim.MNHands_Normal'
	
	Begin Object Name=FirstPersonMesh
		DepthPriorityGroup=SDPG_Foreground
		SkeletalMesh=SkeletalMesh'WinterWar_WP_SOV_DYAKONOV.Mesh.SOV_DYAKONOV'
		PhysicsAsset=PhysicsAsset'WP_VN_VC_MN9130_rifle.Phys.Sov_MN9130_Physics'
		AnimSets(0)=AnimSet'WinterWar_WP_COMMON.Anim.MNHands_Normal'
		AnimSets(1)=AnimSet'WinterWar_WP_SOV_DYAKONOV.Anim.MNHands_Dyakonov'
		AnimTreeTemplate=AnimTree'WinterWar_WP_SOV_DYAKONOV.Anim.Dyakonov_Tree'
		Scale=1.0
		FOV=70
	End Object
	
	Begin Object Name=PickupMesh
		SkeletalMesh=SkeletalMesh'WinterWar_WP_SOV_DYAKONOV.Mesh.SOV_DYAKONOV_3rd'
		PhysicsAsset=PhysicsAsset'WP_VN_3rd_Master.Phy.MN9130_3rd_Master_Physics'
		AnimTreeTemplate=AnimTree'WP_VN_3rd_Master.AnimTree.MN9130_3rd_Tree'
		CollideActors=true
		BlockActors=true
		BlockZeroExtent=true
		BlockNonZeroExtent=true//false
		BlockRigidBody=true
		bHasPhysicsAssetInstance=false
		bUpdateKinematicBonesFromAnimation=false
		PhysicsWeight=1.0
		RBChannel=RBCC_GameplayPhysics
		RBCollideWithChannels=(Default=TRUE,GameplayPhysics=TRUE,EffectPhysics=TRUE)
		bSkipAllUpdateWhenPhysicsAsleep=TRUE
		bSyncActorLocationToRootRigidBody=true
	End Object
	
	AttachmentClass=class'WWWeapon_MN9130_Dyakonov_Attach'
}
