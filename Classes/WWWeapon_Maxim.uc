
class WWWeapon_Maxim extends ROWeap_M2_HMG_Tripod;

simulated function FireAmmunition()
{
	super.FireAmmunition();
	
	if (WorldInfo.NetMode != NM_DedicatedServer && AmmoBeltMesh != None && AmmoCount < 18)
	{
		AmmoBeltMesh.HideBoneByName(Bullets[AmmoCount], PBO_None);
	}
}

defaultproperties
{
	InvIndex=INDEX_NONE
	
	MountWeaponAnimName=Maxim_GetOn
	
	// MAIN FIREMODE
	FiringStatesArray(0)=WeaponFiring
	WeaponFireTypes(0)=EWFT_Custom
	WeaponProjectiles(0)=class'WWProjectile_Maxim'
	FireInterval(0)=+0.1	// 600 RPM
	Spread(0)=0.0002//0.0003
	
	// ALT FIREMODE
	FiringStatesArray(ALTERNATE_FIREMODE)=none
	WeaponFireTypes(ALTERNATE_FIREMODE)=EWFT_Custom
	WeaponProjectiles(ALTERNATE_FIREMODE)=none
	FireInterval(ALTERNATE_FIREMODE)=+0.1	// 600 RPM
	Spread(ALTERNATE_FIREMODE)=0.0002//0.0018
	
	InstantHitDamageTypes(0)=class'WWDmgType_Maxim'
	InstantHitDamageTypes(1)=class'WWDmgType_Maxim'
	
	ShellEjectPSCTemplate=ParticleSystem'FX_VN_Weapons.ShellEjects.FX_Wep_ShellEject_VC_DShK'
	
	WeaponFireAnim(0)=Maxim_Shoot
	WeaponFireAnim(1)=Maxim_Shoot
	WeaponIdleAnims(0)=Maxim_Idle
	WeaponIdleAnims(1)=Maxim_Idle
	WeaponFireSightedAnim(0)=Maxim_Shoot
	WeaponFireSightedAnim(1)=Maxim_Shoot
	WeaponIdleSightedAnims(0)=Maxim_Idle
	WeaponIdleSightedAnims(1)=Maxim_Idle
	WeaponReloadEmptyMagAnim=Maxim_Reloadempty
	WeaponReloadNonEmptyMagAnim=Maxim_Reloadempty
	WeaponAmmoCheckAnim=Maxim_Reloadempty

	WeaponIdleShoulderedAnims(0)=Maxim_Idle
	WeaponIdleShoulderedAnims(1)=Maxim_Idle
	WeaponFireShoulderedAnim(0)=Maxim_Shoot
	WeaponFireShoulderedAnim(1)=Maxim_Shoot
	
	AmmoClass=class'WWAmmo_MaximBelt'
	MaxAmmoCount=250
	bCanReloadNonEmptyMag=false
	
	MuzzleFlashPSCTemplate=ParticleSystem'WinterWar_FX.ParticleSystems.FX_WW_MuzzleFlash_1stP_LMG'
	
	// TracerClass=class'M2BulletTracer'
	TracerFrequency=0
	
	SightRanges.Empty
	SightRanges[0]=(SightRange=200,SightSlideOffset=0.0,SightPositionOffset=0.0,AddedPitch=175)
	SightRanges[1]=(SightRange=300,SightSlideOffset=0.09,SightPositionOffset=-0.022,AddedPitch=165)
	SightRanges[2]=(SightRange=400,SightSlideOffset=0.20,SightPositionOffset=-0.061,AddedPitch=155)
	SightRanges[3]=(SightRange=500,SightSlideOffset=0.31,SightPositionOffset=-0.102,AddedPitch=145)
	SightRanges[4]=(SightRange=600,SightSlideOffset=0.43,SightPositionOffset=-0.139,AddedPitch=135)
	SightRanges[5]=(SightRange=700,SightSlideOffset=0.55,SightPositionOffset=-0.179,AddedPitch=125)
	SightRanges[6]=(SightRange=800,SightSlideOffset=0.69,SightPositionOffset=-0.231,AddedPitch=120)
	SightRanges[7]=(SightRange=900,SightSlideOffset=0.85,SightPositionOffset=-0.289,AddedPitch=110)
	SightRanges[8]=(SightRange=1000,SightSlideOffset=1.01,SightPositionOffset=-0.348,AddedPitch=110)
	
	SuppressionPower=30
	
	Bullets.Empty
	Bullets( 0)=BONE_BELT_01
	Bullets( 1)=BONE_BELT_02
	Bullets( 2)=BONE_BELT_03
	Bullets( 3)=BONE_BELT_04
	Bullets( 4)=BONE_BELT_05
	Bullets( 5)=BONE_BELT_06
	Bullets( 6)=BONE_BELT_07
	Bullets( 7)=BONE_BELT_08
	Bullets( 8)=BONE_BELT_09
	Bullets( 9)=BONE_BELT_10
	Bullets(10)=BONE_BELT_11
	Bullets(11)=BONE_BELT_12
	Bullets(12)=BONE_BELT_13
	Bullets(13)=BONE_BELT_14
	Bullets(14)=BONE_BELT_15
	Bullets(15)=BONE_BELT_16
	Bullets(16)=BONE_BELT_17
	Bullets(17)=BONE_BELT_18
}