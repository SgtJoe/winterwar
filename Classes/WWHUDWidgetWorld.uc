
class WWHUDWidgetWorld extends ROHUDWidgetWorld
	config(Game_WinterWar_Client);

defaultproperties
{
	ObjectiveNeutralIcon=					Texture2D'WinterWar_UI.Objective.64x64.Objective_64_Neutral'
	ObjectiveStatusNorthAttackingNeutral=	Texture2D'WinterWar_UI.Objective.64x64.Objective_64_Neutral_FIN_Taking'
	ObjectiveStatusSouthAttackingNeutral=	Texture2D'WinterWar_UI.Objective.64x64.Objective_64_Neutral_SOV_Taking'
	
	NorthIcons(0)={(
		ObjIconRed=					Texture2D'WinterWar_UI.Objective.64x64.Objective_64_FIN',
		ObjIconBlue=				Texture2D'WinterWar_UI.Objective.64x64.Objective_64_FIN',
		CappingObjRed=				Texture2D'WinterWar_UI.Objective.64x64.Objective_64_FIN_Losing',
		CappingObjBlue=				Texture2D'WinterWar_UI.Objective.64x64.Objective_64_FIN_Losing',
		
		HomeObjIconRed=				Texture2D'WinterWar_UI.Objective.64x64.Objective_64_FINHQ',
		HomeObjIconBlue=			Texture2D'WinterWar_UI.Objective.64x64.Objective_64_FINHQ',
		HomeObjEnemyHeldIconRed=	Texture2D'WinterWar_UI.Objective.64x64.Objective_64_SOVHQ',
		HomeObjEnemyHeldIconBlue=	Texture2D'WinterWar_UI.Objective.64x64.Objective_64_SOVHQ',
		HomeObjNeutralIconRed=		Texture2D'WinterWar_UI.Objective.64x64.Objective_64_FINHQ_Neutral',
		HomeObjNeutralIconBlue=		Texture2D'WinterWar_UI.Objective.64x64.Objective_64_FINHQ_Neutral'
	)}
	
	SouthIcons(0)={(
		ObjIconRed=					Texture2D'WinterWar_UI.Objective.64x64.Objective_64_SOV',
		ObjIconBlue=				Texture2D'WinterWar_UI.Objective.64x64.Objective_64_SOV',
		CappingObjRed=				Texture2D'WinterWar_UI.Objective.64x64.Objective_64_SOV_Losing',
		CappingObjBlue=				Texture2D'WinterWar_UI.Objective.64x64.Objective_64_SOV_Losing',
		
		HomeObjIconRed=				Texture2D'WinterWar_UI.Objective.64x64.Objective_64_SOVHQ',
		HomeObjIconBlue=			Texture2D'WinterWar_UI.Objective.64x64.Objective_64_SOVHQ',
		HomeObjEnemyHeldIconRed=	Texture2D'WinterWar_UI.Objective.64x64.Objective_64_FINHQ',
		HomeObjEnemyHeldIconBlue=	Texture2D'WinterWar_UI.Objective.64x64.Objective_64_FINHQ',
		HomeObjNeutralIconRed=		Texture2D'WinterWar_UI.Objective.64x64.Objective_64_SOVHQ_Neutral',
		HomeObjNeutralIconBlue=		Texture2D'WinterWar_UI.Objective.64x64.Objective_64_SOVHQ_Neutral'
	)}
	
	NorthSelectedTexture=Texture2D'WinterWar_UI.Misc.SquadSelection_FIN'
	SouthSelectedTexture=Texture2D'WinterWar_UI.Misc.SquadSelection_SOV'
	
	EnemyTransportSpottedIcon=Texture2D'VN_UI_Textures.OverheadMap.ui_overheadmap_icons_enemy_tank' 
}
