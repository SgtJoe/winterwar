
class WWProjectile_KP31Bullet extends ROBullet;

defaultproperties
{
	BallisticCoefficient=0.140 // 9mm Finnish - 115gr FMJ
	
	Damage=131
	MyDamageType=class'WWDmgType_KP31Bullet'
	
	Speed=21000 // 420 M/S
	MaxSpeed=21000 // 420 M/S
	
	// RS2. Energy transfer function
	// MAT-49, BHP, Owen, F1, MP40
	VelocityDamageFalloffCurve=(Points=((InVal=95062500,OutVal=1.0),(InVal=380250000,OutVal=0.55)))
}
