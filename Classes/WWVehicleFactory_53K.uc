
class WWVehicleFactory_53K extends WWVehicleFactory;

defaultproperties
{
	Begin Object Name=SVehicleMesh
		SkeletalMesh=SkeletalMesh'WinterWar_VH_USSR_53-K.Mesh.53-K_PHAT'
	End Object
	
	Begin Object Name=CollisionCylinder
		CollisionHeight=+30.0
		CollisionRadius=+20.0
		Translation=(X=-10.0,Y=0.0,Z=35.0)
		bAlwaysRenderIfSelected=true
	End Object
	
	VehicleClass=class'WWVehicle_53K_ActualContent'
}
