
class WWProjectile_DyakonovGrenade extends ROFusedGrenadeProjectile;

defaultproperties
{
	BallisticCoefficient=1.0
	AccelRate=1.0
	
	MyImpactDamageType=class'WWDmgType_DyakonovGrenadeImpact'
	MyExpImpactDamageType=class'WWDmgType_DyakonovGrenade'
	MyDamageType=class'WWDmgType_DyakonovGrenade'
	
	ImpactDamage=200
	Damage=200
	DamageRadius=625
	RadialDamageFalloffExponent=1.0
	MomentumTransfer=1
	Speed=3800 // 76 M/S
	MaxSpeed=3800	// 76 M/S
	MinSpeed=200
	DampenFactorParallel=0.1
	DampenFactorZ=0.2
	// This must be defined in a separate branch of subclasses?
	// DampenFactor=0.9
	
	ShakeScale=2.0
	MaxSuppressBlurDuration=4.5
	SuppressBlurScalar=1.5
	SuppressAnimationScalar=0.6
	ExplodeExposureScale=0.45
	
	ProjExplosionTemplate=ParticleSystem'FX_VN_Weapons.Explosions.FX_VN_Grenade_explosion'
	WaterExplosionTemplate=ParticleSystem'FX_VN_Weapons.Explosions.FX_VN_Grenade_explosion'
	
	ExplosionSound=AkEvent'WW_EXP_Shared.Play_EXP_Grenade_Explosion'
	
	Begin Object Class=StaticMeshComponent Name=ProjectileMesh
		StaticMesh=StaticMesh'WinterWar_WP_SOV_DYAKONOV.Mesh.Dyakonov_Grenade_Projectile'
		MaxDrawDistance=5000
		CollideActors=true
		CastShadow=false
		LightEnvironment=MyLightEnvironment
		BlockActors=false
		BlockZeroExtent=true
		BlockNonZeroExtent=true
		BlockRigidBody=false
		// 90 Degrees = 16383.996 UnrRot
		// http://romerounrealscript.blogspot.com/2012/01/using-rotators-in-unrealscript.html
		Rotation=(Pitch=0, Yaw=-16384, Roll=0)
		Scale=0.01
	End Object
	Components.Add(ProjectileMesh)
	
	FuseTime=5.0f
}
