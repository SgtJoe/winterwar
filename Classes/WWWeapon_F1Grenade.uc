
class WWWeapon_F1Grenade extends ROEggGrenadeWeapon
	abstract;

`include(WinterWar\Classes\WWWeaponPickupMessagesOverride.uci)

defaultproperties
{
	WeaponContentClass(0)="WinterWar.WWWeapon_F1Grenade_ActualContent"
	
	RoleSelectionImage(0)=Texture2D'WinterWar_UI.WP_Render.WP_Render_F1'
	
	WeaponClassType=ROWCT_Grenade
	
	TeamIndex=`ALLIES_TEAM_INDEX
	
	InvIndex=`WI_F1GRENADE
	
	InventoryWeight=0
	Category=ROIC_Grenade
	
	PlayerViewOffset=(X=-1.557,Y=2.077,Z=-1.914)
	
	WeaponPullPinAnim=F1_pullpin
	WeaponPutDownAnim=F1_Putaway
	WeaponEquipAnim=F1_Pullout
	WeaponDownAnim=F1_Down
	WeaponUpAnim=F1_Up
	
	WeaponCrawlingAnims(0)=F1_CrawlF
	WeaponCrawlStartAnim=F1_Crawl_into
	WeaponCrawlEndAnim=F1_Crawl_out
	
	WeaponSprintStartAnim=F1_sprint_into
	WeaponSprintLoopAnim=F1_Sprint
	WeaponSprintEndAnim=F1_sprint_out
	
	WeaponMantleOverAnim=F1_Mantle
	
	WeaponMeleeAnims(0)=F1_Bash
	WeaponMeleeHardAnim=F1_BashHard
	MeleePullbackAnim=F1_Pullback
	MeleeHoldAnim=F1_Pullback_Hold
	
	PrimeSound=AkEvent'WW_EXP_Shared.Play_EXP_Grenade_Safety_Release'
	
	FuzeLength=3.2
	
	MuzzleFlashSocket=none
	
	AmmoClass=class'ROAmmo_M61_Grenade'
	
	FiringStatesArray(0)=WeaponSingleFiring
	WeaponProjectiles(0)=class'WWProjectile_F1Grenade'
	WeaponThrowAnim(0)=F1_throw
	WeaponIdleAnims(0)=F1_idle
	ExplosiveBlindFireRightAnim(0)=F1_R_throw
	ExplosiveBlindFireLeftAnim(0)=F1_L_throw
	ExplosiveBlindFireUpAnim(0)=F1_Up_throw
	
	FiringStatesArray(ALTERNATE_FIREMODE)=WeaponSingleFiring
	WeaponProjectiles(ALTERNATE_FIREMODE)=class'WWProjectile_F1Grenade'
	WeaponThrowAnim(ALTERNATE_FIREMODE)=F1_toss
	WeaponIdleAnims(ALTERNATE_FIREMODE)=F1_idle
	ExplosiveBlindFireRightAnim(ALTERNATE_FIREMODE)=F1_R_toss
	ExplosiveBlindFireLeftAnim(ALTERNATE_FIREMODE)=F1_L_throw
	ExplosiveBlindFireUpAnim(ALTERNATE_FIREMODE)=F1_toss
	
	Weight=0.454	//KG
	MaxAmmoCount=1
	InitialNumPrimaryMags=2
	
	ThrowSpawnModifier=0.525
	TossSpawnModifier=0.4
	
	ThrowingBattleChatterIndex=`BATTLECHATTER_ThrowingGrenade
	
	MinBurstAmount=1
	MaxBurstAmount=1
	BurstWaitTime=2.5
	
	EquipTime=+0.45
	PutDownTime=+0.45
	
	bCanBeQuickThrown=true
}
