
class WWHUDWidgetObjective extends ROHUDWidgetObjective;

defaultproperties
{
	NeutralObjIcons={(
		UpperEdgeMargin=0.22, // 28px
		LowerEdgeMargin=0.22, // 28px
		ObjBorder=		Texture2D'WinterWar_UI.Objective.128x128.Objective_128_Neutral',
		ObjIconWhite=	Texture2D'WinterWar_UI.Objective.128x128.Objective_128_Neutral',
		ObjIconRed=		Texture2D'WinterWar_UI.Objective.128x128.Objective_128_Neutral_FIN_Taking_Progress',
		ObjIconBlue=	Texture2D'WinterWar_UI.Objective.128x128.Objective_128_Neutral_SOV_Taking_Progress',
		CappingObjWhite=Texture2D'WinterWar_UI.Objective.128x128.Objective_128_Neutral_Taking',
		CappingObjRed=	Texture2D'WinterWar_UI.Objective.128x128.Objective_128_Neutral_FIN_Taking',
		CappingObjBlue=	Texture2D'WinterWar_UI.Objective.128x128.Objective_128_Neutral_SOV_Taking'
	)}
	
	NorthObjIcons(0)={(
		UpperEdgeMargin=0.22, // 28px
		LowerEdgeMargin=0.22, // 28px
		ObjBorder=			Texture2D'WinterWar_UI.Objective.128x128.Objective_128_Neutral',
		ObjIconWhite=		Texture2D'WinterWar_UI.Objective.128x128.Objective_128_Neutral',
		ObjIconRed=			Texture2D'WinterWar_UI.Objective.128x128.Objective_128_FIN',
		ObjIconBlue=		Texture2D'WinterWar_UI.Objective.128x128.Objective_128_FIN_Losing_Progress',
		CappingObjWhite=	Texture2D'WinterWar_UI.Objective.128x128.Objective_128_Neutral_Taking',
		CappingObjRed=		Texture2D'WinterWar_UI.Objective.128x128.Objective_128_Neutral_Taking',
		CappingObjBlue=		Texture2D'WinterWar_UI.Objective.128x128.Objective_128_Neutral_Taking',
		HomeObjRedBorder=	Texture2D'WinterWar_UI.Objective.128x128.Objective_128_NeutralHQ'/* Texture2D'WinterWar_UI.Objective.128x128.Objective_128_FINHQ_Neutral' */,
		HomeObjBlueBorder=	Texture2D'WinterWar_UI.Objective.128x128.Objective_128_NeutralHQ'/* Texture2D'WinterWar_UI.Objective.128x128.Objective_128_FINHQ_Neutral' */
	)}
	
	SouthObjIcons(0)={(
		UpperEdgeMargin=0.22, // 28px
		LowerEdgeMargin=0.22, // 28px
		ObjBorder=			Texture2D'WinterWar_UI.Objective.128x128.Objective_128_Neutral',
		ObjIconWhite=		Texture2D'WinterWar_UI.Objective.128x128.Objective_128_Neutral',
		ObjIconRed=			Texture2D'WinterWar_UI.Objective.128x128.Objective_128_SOV_Losing_Progress',
		ObjIconBlue=		Texture2D'WinterWar_UI.Objective.128x128.Objective_128_SOV',
		CappingObjWhite=	Texture2D'WinterWar_UI.Objective.128x128.Objective_128_Neutral_Taking',
		CappingObjRed=		Texture2D'WinterWar_UI.Objective.128x128.Objective_128_Neutral_Taking',
		CappingObjBlue=		Texture2D'WinterWar_UI.Objective.128x128.Objective_128_Neutral_Taking',
		HomeObjRedBorder=	Texture2D'WinterWar_UI.Objective.128x128.Objective_128_NeutralHQ'/* Texture2D'WinterWar_UI.Objective.128x128.Objective_128_SOVHQ_Neutral' */,
		HomeObjBlueBorder=	Texture2D'WinterWar_UI.Objective.128x128.Objective_128_NeutralHQ'/* Texture2D'WinterWar_UI.Objective.128x128.Objective_128_SOVHQ_Neutral' */
	)}
}
