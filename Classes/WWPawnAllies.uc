
class WWPawnAllies extends WWPawn;

simulated event byte ScriptGetTeamNum()
{
	return `ALLIES_TEAM_INDEX;
}

defaultproperties
{
	TunicMesh=SkeletalMesh'WinterWar_CHR_SOV.Mesh.SOV_Coat'
	PawnMesh_SV=SkeletalMesh'WinterWar_CHR_SOV.Mesh.SOV_SV'
	FieldgearMesh=SkeletalMesh'WinterWar_CHR_SOV.Mesh.SOV_Gear_Rifleman'
	BodyMICTemplate=MaterialInstanceConstant'WinterWar_CHR_SOV.MIC.SOV_Coat'
	
	BadgeMesh=StaticMesh'WinterWar_CHR_SOV.Mesh.SOV_RankTabs_Coat_Recruit'
	
	ArmsOnlyMeshFP=SkeletalMesh'WinterWar_CHR_ALL.Mesh.SOV_Hands_Coat'
	
	HeadAndArmsMesh=SkeletalMesh'WinterWar_CHR_SOV.Mesh.SOV_Head_1'
	HeadAndArmsMICTemplate=MaterialInstanceConstant'WinterWar_CHR_SOV.MIC.SOV_Head_1'
	
	HeadgearMesh=SkeletalMesh'WinterWar_CHR_SOV.Mesh.SOV_Headgear_Budenovka2'
	HeadgearMICTemplate=MaterialInstanceConstant'WinterWar_CHR_SOV.MIC.SOV_Budenovka2'
	
	Begin Object Name=ROPawnSkeletalMeshComponent
		AnimSets(11)=AnimSet'WinterWar_CHR_ALL.Anim.CHR_Soviet_Unique'
	End Object
	
	bSingleHandedSprinting=false
}
