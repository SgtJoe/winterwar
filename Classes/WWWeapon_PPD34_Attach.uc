
class WWWeapon_PPD34_Attach extends ROWeapAttach_PPSH41_SMG;

defaultproperties
{
	Begin Object Name=SkeletalMeshComponent0
		SkeletalMesh=SkeletalMesh'WinterWar_WP_SOV_PPD34.Mesh.SOV_PPD34_3rd'
		AnimSets(0)=AnimSet'WP_VN_3rd_Master.Anim.PPSH_UPGD1_3rd_anim'
		PhysicsAsset=PhysicsAsset'WinterWar_WP_SOV_PPD34.Phy.SOV_PPD34_3rd_Bounds_Physics'
		CullDistance=5000
	End Object
	
	WeaponClass=class'WWWeapon_PPD34'
}
