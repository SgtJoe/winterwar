
class WWWeapon_MN38_Attach extends WWWeapon_MosinBase_Attach;

defaultproperties
{
	Begin Object Name=SkeletalMeshComponent0
		SkeletalMesh=SkeletalMesh'WinterWar_WP_SOV_MN38.Mesh.SOV_MN38_3rd'
		PhysicsAsset=PhysicsAsset'WinterWar_WP_SOV_MN38.Phy.MN38_3rd_Bounds_Physics'
	End Object
	
	WeaponClass=class'WWWeapon_MN38'
	
	MuzzleFlashPSCTemplate=ParticleSystem'WinterWar_FX.ParticleSystems.FX_WW_MuzzleFlash_3rdP_M38'
}
