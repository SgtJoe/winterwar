
class WWRoleInfoAlliesEngineer extends WWRoleInfoAllies;

DefaultProperties
{
	RoleType=RORIT_Engineer
	ClassTier=3
	ClassIndex=`RI_SAPPER
	
	Items[RORIGM_Default]={(
		PrimaryWeapons=(class'WWWeapon_MN38'),
		
		OtherItems=(class'WWWeapon_Satchel')
	)}
	
	ClassIcon=Texture2D'VN_UI_Textures.menu.class_icon_sapper'
}
