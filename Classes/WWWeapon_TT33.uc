
class WWWeapon_TT33 extends ROWeap_TT33_Pistol
	abstract;

`include(WinterWar\Classes\WWWeaponPickupMessagesOverride.uci)

defaultproperties
{
	WeaponContentClass(0)="WinterWar.WWWeapon_TT33_ActualContent"
	
	RoleSelectionImage(0)=Texture2D'VN_UI_Textures.WeaponTex.VN_Weap_TT33_Pistol'
	
	TeamIndex=`ALLIES_TEAM_INDEX
	
	InvIndex=`WI_TT33
	
	WeaponProjectiles(0)=class'TT33Bullet'
	
	InstantHitDamageTypes(0)=class'RODmgType_TT33Bullet'
	InstantHitDamageTypes(1)=class'RODmgType_TT33Bullet'
	
	MuzzleFlashPSCTemplate=ParticleSystem'WinterWar_FX.ParticleSystems.FX_WW_MuzzleFlash_1stP_Pistol'
	
	AmmoClass=class'ROAmmo_762x25_TT33Mag'
	
	WeaponReloadEmptyMagAnim=TT33_reloadempty_new
	WeaponReloadEmptyMagIronAnim=TT33_reloadempty_new
	
	WeaponReloadNonEmptyMagAnim=TT33_reloadhalf_new
	WeaponReloadNonEmptyMagIronAnim=TT33_reloadhalf_new
}
