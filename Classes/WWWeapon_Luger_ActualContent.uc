
class WWWeapon_Luger_ActualContent extends WWWeapon_Luger;

DefaultProperties
{
	ArmsAnimSet=AnimSet'WinterWar_WP_FIN_L35.Anim.FIN_L-35_Anims'
	
	Begin Object Name=FirstPersonMesh
		DepthPriorityGroup=SDPG_Foreground
		SkeletalMesh=SkeletalMesh'WinterWar_WP_FIN_LUGER.Mesh.FIN_Luger'
		PhysicsAsset=PhysicsAsset'WinterWar_WP_FIN_L35.Phy.FIN_L-35_Physics'
		AnimSets(0)=AnimSet'WinterWar_WP_FIN_L35.Anim.FIN_L-35_Anims'
		AnimTreeTemplate=AnimTree'WinterWar_WP_FIN_L35.Anim.FIN_L-35_AnimTree'
		Scale=1.0
		FOV=70
	End Object
	
	Begin Object Name=PickupMesh
		SkeletalMesh=SkeletalMesh'WinterWar_WP_FIN_LUGER.Mesh.FIN_Luger_3rd'
		PhysicsAsset=PhysicsAsset'WP_VN_AUS_3rd_Master.Phy.Browning_HP_3rd_Physics'
		CollideActors=true
		BlockActors=true
		BlockZeroExtent=true
		BlockNonZeroExtent=true//false
		BlockRigidBody=true
		bHasPhysicsAssetInstance=false
		bUpdateKinematicBonesFromAnimation=false
		PhysicsWeight=1.0
		RBChannel=RBCC_GameplayPhysics
		RBCollideWithChannels=(Default=TRUE,GameplayPhysics=TRUE,EffectPhysics=TRUE)
		bSkipAllUpdateWhenPhysicsAsleep=TRUE
		bSyncActorLocationToRootRigidBody=true
	End Object
	
	AttachmentClass=class'WWWeapon_Luger_Attach'
	
	WeaponFireSnd(DEFAULT_FIREMODE)=(DefaultCue=AkEvent'WW_WEP_BHP.Play_WEP_BHP_Fire_Single_3P', FirstPersonCue=AkEvent'WW_WEP_BHP.Play_WEP_BHP_Fire_Single')
}
