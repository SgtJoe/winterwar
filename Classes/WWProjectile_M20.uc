
class WWProjectile_M20 extends TT33Bullet;

DefaultProperties
{
	MyDamageType=class'WWDmgType_M20'
	
	Damage=118
	Speed=19000		// 380 m/s
	MaxSpeed=19000  // 380 m/s
}
