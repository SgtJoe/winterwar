
class WWWeapon_AntiTankMine extends ROWeap_MD82_Mine
	abstract;

var const int TrapPlacementSize;
var const int TrapPlacementSizeFar;

`include(WinterWar\Classes\WWWeaponPickupMessagesOverride.uci)

simulated function bool CheckForAdditionalPlacingSpace(vector PlaceLoc, vector HitNormal, vector XAxis, vector YAxis)
{
	local vector SpaceCheckLoc;
	
	SpaceCheckLoc = PlaceLoc + 6 * HitNormal;
	
	if (bDebugWeapon)
	{
		DrawDebugSphere(SpaceCheckLoc,1,8,0,0,255,false);
		DrawDebugLine(SpaceCheckLoc + TrapPlacementSizeFar * XAxis + TrapPlacementSize * YAxis, SpaceCheckLoc + TrapPlacementSizeFar * XAxis - TrapPlacementSize * YAxis, 0, 255, 0);
		DrawDebugLine(SpaceCheckLoc - TrapPlacementSize * XAxis - TrapPlacementSize * YAxis, SpaceCheckLoc - TrapPlacementSize * XAxis + TrapPlacementSize * YAxis, 0, 255, 0);
		DrawDebugLine(SpaceCheckLoc - TrapPlacementSize * XAxis + TrapPlacementSize * YAxis, SpaceCheckLoc + TrapPlacementSizeFar * XAxis + TrapPlacementSize * YAxis, 255, 0, 0);
		DrawDebugLine(SpaceCheckLoc - TrapPlacementSize * XAxis - TrapPlacementSize * YAxis, SpaceCheckLoc + TrapPlacementSizeFar * XAxis - TrapPlacementSize * YAxis, 255, 0, 0);
	}
	
	return	FastTrace(SpaceCheckLoc + TrapPlacementSizeFar * XAxis + TrapPlacementSize * YAxis, SpaceCheckLoc + TrapPlacementSizeFar * XAxis - TrapPlacementSize * YAxis) &&
			FastTrace(SpaceCheckLoc - TrapPlacementSize * XAxis - TrapPlacementSize * YAxis, SpaceCheckLoc - TrapPlacementSize * XAxis + TrapPlacementSize * YAxis) &&
			FastTrace(SpaceCheckLoc - TrapPlacementSize * XAxis + TrapPlacementSize * YAxis, SpaceCheckLoc + TrapPlacementSizeFar * XAxis + TrapPlacementSize * YAxis) &&
			FastTrace(SpaceCheckLoc - TrapPlacementSize * XAxis - TrapPlacementSize * YAxis, SpaceCheckLoc + TrapPlacementSizeFar * XAxis - TrapPlacementSize * YAxis );
}

simulated function PickImpact()
{
	if (Instigator.IsFirstPerson() && ROPlayerController(Instigator.Controller) != none)
	{
		ROPlayerController(Instigator.Controller).ClientPlayForceFeedbackWaveform(class'ROWaveForms'.default.MeleeHit);
	}
}

DefaultProperties
{
	WeaponContentClass(0)="WinterWar.WWWeapon_AntiTankMine_ActualContent"
	
	RoleSelectionImage(0)=Texture2D'WinterWar_UI.WP_Render.WP_Render_M39'
	
	TeamIndex=`AXIS_TEAM_INDEX
	
	InvIndex=`WI_ATMINE
	
	TrapOffset=4.0
	
	bPlaceAnywhere=false
	bPlaceOnHardSurfaces=false
	
	AmmoClass=class'ROAmmo_MD82_Mine'
	
	TrapClass=class'WWAntiTankMine'
	
	MaxAmmoCount=1
	InitialNumPrimaryMags=5
	MaxNumPrimaryMags=8
	MaxAllowedAtOnce=8
	
	PlayerViewOffset=(X=3,Y=4.5,Z=-1.25)
	ShoulderedPosition=(X=3,Y=3.5,Z=-0.5)
	
	WeaponIdleAnims(0)=Punji_idle
	WeaponIdleAnims(ALTERNATE_FIREMODE)=Punji_idle
	WeaponPutDownAnim=Punji_Putaway
	WeaponEquipAnim=Punji_Pullout
	WeaponDownAnim=Punji_Down
	WeaponUpAnim=Punji_Up
	WeaponCrawlingAnims(0)=Punji_CrawlF
	WeaponCrawlStartAnim=Punji_Crawl_into
	WeaponCrawlEndAnim=Punji_Crawl_out
	WeaponSprintStartAnim=Punji_sprint_into
	WeaponSprintLoopAnim=Punji_Sprint
	WeaponSprintEndAnim=Punji_sprint_out
	WeaponMantleOverAnim=Punji_Mantle
	WeaponPlaceTrapAnim=Punji_boobyTrap
	WeaponReadyStartAnim=Punji_idle_to_boobytrap_idle
	WeaponReadyEndAnim=Punji_boobytrap_to_idle
	WeaponReadyLoopAnim=Punji_boobyTrap_idle
	WeaponMeleeAnims(0)=Punji_Bash
	WeaponMeleeHardAnim=Punji_BashHard
	MeleePullbackAnim=Punji_Pullback
	MeleeHoldAnim=Punji_Pullback_Hold
	WeaponSpotEnemyAnim=Punji_SpotEnemy
	
	Begin Object Name=PreviewMeshComponent
		StaticMesh=StaticMesh'WP_VN_VC_PunjiTrap.Mesh.S_DirtMound'
		Scale3D=(X=4,Y=3,Z=2)
	End Object
	
	TrapPlacementSize=30
	TrapPlacementSizeFar=80
}
