
class WWUIWidgetWeaponAmmoList extends ROUIWidgetWeaponAmmoList;

function LoadGridItems(int SwitchVal, optional int SecondaryVal, optional int TertiaryVal)
{
	local int i, NumLoadouts, PriMags, SecMags, PriIndex, SecIndex;
	local class<ROWeapon>	WeaponContentClass;
	
	WeaponContentClass = GetWeaponClass(SwitchVal, true);
	SourceItemList.length = 0;
	
	if( WeaponContentClass != none )
		NumLoadouts = WeaponContentClass.default.AltAmmoLoadouts.length + 1;
	
	for(i=0; i<NumLoadouts; i++)
	{
		SourceItemList.Add(1);
		GetPrimaryAmmoDisplayDetails(i, PriMags, SecMags, PriIndex, SecIndex);
		
		if( PriIndex > -1 )
		{
			SourceItemList[i].ItemThumbnail = class'WWHUD'.default.AmmoUVs[PriIndex].MagTexture;
			SourceItemList[i].ItemDescription = class'WWHUD'.default.AmmoUVs[PriIndex].AmmoDescription;
		}
		
		SourceItemList[i].ItemLabel = "x"$PriMags;
		
		if( SecMags > 0 )
		{
			if( SecIndex > -1 )
			{
				SourceItemList[i].Item2Thumbnail = class'WWHUD'.default.AmmoUVs[SecIndex].MagTexture;
				SourceItemList[i].Item2Description = class'WWHUD'.default.AmmoUVs[SecIndex].AmmoDescription;
			}
			SourceItemList[i].Item2Label = "x"$SecMags;
		}
		SourceItemList[i].SourceArrayIndex = i;
	}
}
