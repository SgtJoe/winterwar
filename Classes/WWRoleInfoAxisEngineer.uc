
class WWRoleInfoAxisEngineer extends WWRoleInfoAxis;

DefaultProperties
{
	RoleType=RORIT_Engineer
	ClassTier=3
	ClassIndex=`RI_SAPPER
	
	Items[RORIGM_Default]={(
		PrimaryWeapons=(class'WWWeapon_MN27',class'WWWeapon_MN91'),
		
		OtherItems=(class'WWWeapon_Kasapanos_FactoryIssue',class'WWWeapon_AntiTankMine',class'WWWeapon_Kasapanos',class'WWWeapon_AntiTankMine'),
		OtherItemsStartIndexForPrimary=(0, 2),
		NumOtherItemsForPrimary=(2, 2)
	)}
	
	ClassIcon=Texture2D'VN_UI_Textures.menu.class_icon_sapper'
}
