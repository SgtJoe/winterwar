
class WWVoicePack_FIN_1 extends WWVoicePack;

defaultproperties
{
	// INF_Attack
	VoiceComs[`VOICECOM_Attack]=(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_Attack'")
	
	// INF_AttackObjective
	VoiceComs[`VOICECOM_SLAttack]=(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_AttackObjective'")
	VoiceComs[`VOICECOM_TLAttack]=(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_AttackObjective'")
	
	// INF_Charging
	VoiceComs[`VOICECOM_Charging]=(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_Charging'")
	
	// INF_Confirm
	VoiceComs[`VOICECOM_Confirm]=	(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_Confirm'")
	VoiceComs[`VOICECOM_SLConfirm]=	(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_Confirm'")
	VoiceComs[`VOICECOM_TLConfirm]=	(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_Confirm'")
	VoiceComs[`VOICECOM_AIConfirm]=	(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_Confirm'")
	
	// INF_DefendObjective
	VoiceComs[`VOICECOM_SLDefend]=(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_DefendObjective'")
	VoiceComs[`VOICECOM_TLDefend]=(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_DefendObjective'")
	
	// INF_EnemyDeath
	VoiceComs[`VOICECOM_EnemyDeath]=			(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_EnemyDeath'")
	VoiceComs[`VOICECOM_EnemyDeath_Hero]=		(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_EnemyDeath'")
	VoiceComs[`VOICECOM_EnemyDeath_LowMorale]=	(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_EnemyDeath'")
	VoiceComs[`VOICECOM_EnemyDeath_Suppressed]=	(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_EnemyDeath'")
	
	// INF_EnemyDeathUnknown
	VoiceComs[`VOICECOM_EnemyDeathUnknown]=(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_EnemyDeathUnknown'")
	
	// INF_EnemySpotted_Engineer
	VoiceComs[`VOICECOM_EnemySpottedAntiTank]=			(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_EnemySpotted_Engineer'")
	VoiceComs[`VOICECOM_EnemySpottedEngineer]=			(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_EnemySpotted_Engineer'")
	VoiceComs[`VOICECOM_EnemySpottedEngineer_Special]=	(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_EnemySpotted_Engineer'")
	VoiceComs[`VOICECOM_EnemySpottedGrenadier]=			(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_EnemySpotted_Engineer'")
	
	// INF_EnemySpotted_Generic
	VoiceComs[`VOICECOM_InfantrySpotted]=		(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_EnemySpotted_Generic'")
	VoiceComs[`VOICECOM_EnemySpottedInfantry]=	(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_EnemySpotted_Generic'")
	VoiceComs[`VOICECOM_EnemySpottedTransport]=	(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_EnemySpotted_Generic'")
	
	// INF_EnemySpotted_MG
	VoiceComs[`VOICECOM_EnemySpottedMGer]=			(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_EnemySpotted_MG'")
	VoiceComs[`VOICECOM_TakingFireMachineGunner]=	(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_EnemySpotted_MG'")
	
	// INF_EnemySpotted_Sniper
	VoiceComs[`VOICECOM_EnemySpottedSniper]=(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_EnemySpotted_Sniper'")
	VoiceComs[`VOICECOM_TakingFireSniper]=	(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_EnemySpotted_Sniper'")
	
	// INF_EnemySpotted_Tank
	VoiceComs[`VOICECOM_EnemySpottedTank]=	(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_EnemySpotted_Tank'")
	VoiceComs[`VOICECOM_TakingFireTank]=	(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_EnemySpotted_Tank'")
	
	// INF_EnemySpotted_Trap
	VoiceComs[`VOICECOM_SpottedExplosive]=(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_EnemySpotted_Generic'")
	
	// INF_FriendlyDeath
	VoiceComs[`VOICECOM_FriendlyDeath]=				(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_FriendlyDeath'")
	VoiceComs[`VOICECOM_FriendlyDeath_LowMorale]=	(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_FriendlyDeath'")
	VoiceComs[`VOICECOM_FriendlyDeath_Hero]=		(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_FriendlyDeath'")
	
	// INF_FriendlyFire
	VoiceComs[`VOICECOM_FriendlyFire]=		(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_FriendlyFire'")
	VoiceComs[`VOICECOM_SawFriendlyFire]=	(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_FriendlyFire'")
	
	// INF_IdleChatter
	VoiceComs[`VOICECOM_IdleSituation1]=					(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_IdleChatter'")
	VoiceComs[`VOICECOM_IdleSituation1_LowMorale]=			(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_IdleChatter'")
	VoiceComs[`VOICECOM_IdleSituation2]=					(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_IdleChatter'")
	VoiceComs[`VOICECOM_IdleSituation2_LowMorale]=			(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_IdleChatter'")
	VoiceComs[`VOICECOM_IdleSituation3]=					(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_IdleChatter'")
	VoiceComs[`VOICECOM_IdleSituation3_LowMorale]=			(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_IdleChatter'")
	VoiceComs[`VOICECOM_IdleSituation3_HighMorale]=			(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_IdleChatter'")
	VoiceComs[`VOICECOM_IdleSituationOfficer]=				(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_IdleChatter'")
	VoiceComs[`VOICECOM_IdleSituationOfficer_LowMorale]=	(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_IdleChatter'")
	VoiceComs[`VOICECOM_IdleSituationOfficer_HighMorale]=	(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_IdleChatter'")
	VoiceComs[`VOICECOM_IdleEnemyLocation]=					(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_IdleChatter'")
	VoiceComs[`VOICECOM_IdleEnemyLocation_LowMorale]=		(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_IdleChatter'")
	VoiceComs[`VOICECOM_IdleEnemyLocationOfficer]=			(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_IdleChatter'")
	VoiceComs[`VOICECOM_IdleEnemyLocationOfficer_LowMorale]=(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_IdleChatter'")
	
	// INF_LosingObjective
	VoiceComs[`VOICECOM_LosingObjective]=					(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_LosingObjective'")
	VoiceComs[`VOICECOM_LosingObjectiveOfficer]=			(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_LosingObjective'")
	VoiceComs[`VOICECOM_LosingObjective_LowMorale]=			(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_LosingObjective'")
	VoiceComs[`VOICECOM_LosingObjectiveOfficer_LowMorale]=	(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_LosingObjective'")
	
	// INF_Negative
	VoiceComs[`VOICECOM_Negative]=(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_Negative'")
	VoiceComs[`VOICECOM_SLReject]=(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_Negative'")
	
	// INF_NoAmmo
	VoiceComs[`VOICECOM_NeedAmmo]=				(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_NoAmmo'")
	VoiceComs[`VOICECOM_LowOnAmmo]=				(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_NoAmmo'")
	VoiceComs[`VOICECOM_LowOnAmmo_LowMorale]=	(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_NoAmmo'")
	VoiceComs[`VOICECOM_OutOfAmmo]=				(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_NoAmmo'")
	VoiceComs[`VOICECOM_OutOfAmmoMGer]=			(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_NoAmmo'")
	
	// INF_Reloading
	VoiceComs[`VOICECOM_Reloading]=				(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_Reloading'")
	VoiceComs[`VOICECOM_Reloading_Suppressed]=	(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_Reloading'")
	
	// INF_RequestArtyCoordinates
	VoiceComs[`VOICECOM_TLRequestArtyCoords]=(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_RequestArtyCoordinates'")
	
	// INF_RequestOrders
	VoiceComs[`VOICECOM_RequestOrders]=		(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_RequestOrders'")
	VoiceComs[`VOICECOM_SLRequestOrders]=	(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_RequestOrders'")
	
	// INF_RequestRadioAbility_1
	VoiceComs[`VOICECOM_RequestAbility1]=(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_RequestRadioAbility_1'")
	VoiceComs[`VOICECOM_RequestAbility2]=(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_RequestRadioAbility_1'")
	
	// INF_RequestRadioAbility_2
	VoiceComs[`VOICECOM_RequestAbility3]=(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_RequestRadioAbility_2'")
	
	// INF_RequestRadioAbility_CancelRequest
	VoiceComs[`VOICECOM_CancelArtillery]=(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_RequestRadioAbility_CancelRequest'")
	
	// INF_RequestRadioAbility_Recon
	VoiceComs[`VOICECOM_CallForReconPlane]=(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_RequestRadioAbility_Recon'")
	
	// INF_RequestSupport_Artillery
	VoiceComs[`VOICECOM_NeedArtillery]=(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_RequestSupport_Artillery'")
	VoiceComs[`VOICECOM_SLRequestArty]=(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_RequestSupport_Artillery'")
	
	// INF_RequestSupport_Engineer
	VoiceComs[`VOICECOM_NeedAntiTank]=	(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_RequestSupport_Engineer'")
	VoiceComs[`VOICECOM_NeedExplosives]=(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_RequestSupport_Engineer'")
	
	// INF_RequestSupport_Generic
	VoiceComs[`VOICECOM_RequestSupport]=(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_RequestSupport_Generic'")
	
	// INF_RequestSupport_MG
	VoiceComs[`VOICECOM_NeedMGSupport]=(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_RequestSupport_MG'")
	
	// INF_RequestSupport_Recon
	VoiceComs[`VOICECOM_NeedRecon]=(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_RequestSupport_Recon'")
	
	// INF_RequestSupport_Smoke
	VoiceComs[`VOICECOM_NeedSmoke]=(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_RequestSupport_Smoke'")
	
	// INF_Retreat
	VoiceComs[`VOICECOM_Retreat]=			(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_Retreat'")
	VoiceComs[`VOICECOM_Retreat_LowMorale]=	(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_Retreat'")
	
	// INF_Sorry
	VoiceComs[`VOICECOM_Sorry]=(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_Sorry'")
	
	// INF_SpawnSpeech
	VoiceComs[`VOICECOM_SpawnAttacking]=			(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_SpawnSpeech'")
	VoiceComs[`VOICECOM_SpawnAttacking_HighMorale]=	(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_SpawnSpeech'")
	VoiceComs[`VOICECOM_SpawnAttacking_LowMorale]=	(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_SpawnSpeech'")
	VoiceComs[`VOICECOM_SpawnDefending]=			(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_SpawnSpeech'")
	VoiceComs[`VOICECOM_SpawnDefending_HighMorale]=	(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_SpawnSpeech'")
	VoiceComs[`VOICECOM_SpawnDefending_LowMorale]=	(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_SpawnSpeech'")
	VoiceComs[`VOICECOM_SpawnNeutral]=				(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_SpawnSpeech'")
	
	// INF_Suppressed
	VoiceComs[`VOICECOM_Suppressed]=			(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_Suppressed'")
	VoiceComs[`VOICECOM_Suppressed_LowMorale]=	(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_Suppressed'")
	VoiceComs[`VOICECOM_Suppressed_Hero]=		(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_Suppressed'")
	
	// INF_Suppressing
	VoiceComs[`VOICECOM_Suppressing]=		(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_Suppressing'")
	VoiceComs[`VOICECOM_Suppressing_Hero]=	(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_Suppressing'")
	
	// INF_TakingFire
	VoiceComs[`VOICECOM_TakingFireInfantry]=	(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_TakingFire'")
	VoiceComs[`VOICECOM_TakingFireUnknown]=		(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_TakingFire'")
	VoiceComs[`VOICECOM_TakingFireUnknown_Hero]=(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_TakingFire'")
	VoiceComs[`VOICECOM_TakeCover]=				(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_TakingFire'")
	
	// INF_TakingObjective
	VoiceComs[`VOICECOM_TakingObjective]=					(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_TakingObjective'")
	VoiceComs[`VOICECOM_TakingObjectiveOfficer]=			(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_TakingObjective'")
	VoiceComs[`VOICECOM_TakingObjective_LowMorale]=			(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_TakingObjective'")
	VoiceComs[`VOICECOM_TakingObjectiveOfficer_LowMorale]=	(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_TakingObjective'")
	
	// INF_Taunts
	VoiceComs[`VOICECOM_Taunt]=(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_Taunts'")
	
	// INF_Thanks
	VoiceComs[`VOICECOM_Thanks]=(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_Thanks'")
	
	// INF_ThrowingGrenade
	VoiceComs[`VOICECOM_ThrowingGrenade]=(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_ThrowingGrenade'")
	
	// INF_ThrowingMolotov
	VoiceComs[`VOICECOM_ThrowingSatchel]=(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_ThrowingMolotov'")
	
	// INF_ThrowingSmoke
	VoiceComs[`VOICECOM_ThrowingSmoke]=(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_ThrowingSmoke'")
	
	// INF_Wounded
	VoiceComs[`VOICECOM_DeathHeart]=	(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_Wounded'")
	VoiceComs[`VOICECOM_DeathStomach]=	(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_Wounded'")
	VoiceComs[`VOICECOM_DeathNeck]=		(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_Wounded'")
	VoiceComs[`VOICECOM_DyingFast]=		(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_Wounded'")
	VoiceComs[`VOICECOM_DyingSlow]=		(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_Wounded'")
	VoiceComs[`VOICECOM_Bandaging]=		(Sound="AkEvent'WW_AUD_VOX_FIN_1.Play_INF_F1_Wounded'")
}
