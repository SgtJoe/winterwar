
class WWWeapon_RGD33 extends ROWeap_Type67_Grenade
	abstract;

`include(WinterWar\Classes\WWWeaponPickupMessagesOverride.uci)

simulated event Tick( float DeltaTime )
{
	super.Tick(DeltaTime);
	
	// This grenade only activates upon throwing, so don't count down CurrentFuzeTime
	if (bPrimed)
	{
		CurrentFuzeTime = FuzeLength;
	}
}

defaultproperties
{
	WeaponContentClass(0)="WinterWar.WWWeapon_RGD33_ActualContent"
	
	RoleSelectionImage(0)=Texture2D'WinterWar_UI.WP_Render.WP_Render_RGD33'
	
	InvIndex=`WI_RGD33
	
	WeaponPullPinAnim=RGD33_Pullpin
	
	WeaponEquipAnim=Granate_Up
	
	FuzeLength=3.5
	
	AmmoClass=class'ROAmmo_Type67_Grenade'
	
	WeaponProjectiles(DEFAULT_FIREMODE)=class'WWProjectile_RGD33'
	WeaponProjectiles(ALTERNATE_FIREMODE)=class'WWProjectile_RGD33'
	
	InitialNumPrimaryMags=1
}
