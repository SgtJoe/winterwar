
class WWVehicle_T26_EarlyWar_ActualContent extends WWVehicle_T26_ActualContent
	placeable;

DefaultProperties
{
	Begin Object Name=ROSVehicleMesh
		SkeletalMesh=SkeletalMesh'WinterWar_VH_USSR_T-26.Mesh.T-26_Rig'
		Materials[0]=MaterialInstanceConstant'WinterWar_VH_USSR_T-26.MIC.Green_T-26_M'
	End Object
	
	DestroyedMats(0)=MaterialInstanceConstant'WinterWar_VH_USSR_T-26.MIC.Green_T-26_WRECK_M'
	
	DestroyedMatsTurret(0)=MaterialInstanceConstant'WinterWar_VH_USSR_T-26.MIC.Green_T-26_WRECK_M'
}
