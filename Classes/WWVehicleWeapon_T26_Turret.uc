
class WWVehicleWeapon_T26_Turret extends WWVehicleWeapon_TankTurret
	abstract;

defaultproperties
{
	WeaponContentClass(0)="WinterWar.WWVehicleWeapon_T26_Turret_ActualContent"
	
	SeatIndex=1
	
	PlayerIronSightFOV=13.0
	
	FiringStatesArray(0)=WeaponSingleFiring
	WeaponFireTypes(0)=EWFT_Projectile
	WeaponProjectiles(0)=class'WWVehicleProjectile_T26_HE'
	FireInterval(0)=+6.0
	Spread(0)=0.0001
	
	FiringStatesArray(ALTERNATE_FIREMODE)=WeaponFiring
	WeaponFireTypes(ALTERNATE_FIREMODE)=EWFT_Projectile
	WeaponProjectiles(ALTERNATE_FIREMODE)=class'WWProjectile_DT_T26'
	FireInterval(ALTERNATE_FIREMODE)=+0.092
	Spread(ALTERNATE_FIREMODE)=0.0007
	
	FireTriggerTags=(T26_Cannon)
	AltFireTriggerTags=(T26_CoaxMG)
	
	VehicleClass=class'WWVehicle_T26'
	
	MainGunProjectiles(MAINGUN_AP_INDEX)=class'WWVehicleProjectile_T26_HE'
	MainGunProjectiles(MAINGUN_HE_INDEX)=class'WWVehicleProjectile_T26_AP'
//	MainGunProjectiles(MAINGUN_SMOKE_INDEX)=class'WWVehicleProjectile_Smoke'
	
	HEAmmoCount=`TANK_AMMO_T26_AP
	APAmmoCount=`TANK_AMMO_T26_HE
//	SmokeAmmoCount=`TANK_AMMO_SMOKE
	
	AmmoClass=class'WWAmmo_DTDrum'
	MaxAmmoCount=63
	bUsesMagazines=true
	InitialNumPrimaryMags=`TANK_AMMO_T26_DT
	
	PenetrationDepth=22.23
	MaxPenetrationTests=3
	MaxNumPenetrations=2
}
