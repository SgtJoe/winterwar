
class WWWeapon_QuadMaxims extends ROWeap_DShK_HMG_Tripod;

var name MaximMuzzleFlashSockets[4];

var ROParticleSystemComponent MuzzleFlashPSC1, MuzzleFlashPSC2, MuzzleFlashPSC3, MuzzleFlashPSC4;

simulated event MuzzleFlashTimer()
{
	if (MuzzleFlashPSC1 != none)
	{
		MuzzleFlashPSC1.DeactivateSystem();
	}
	
	if (MuzzleFlashPSC2 != none)
	{
		MuzzleFlashPSC2.DeactivateSystem();
	}
	
	if (MuzzleFlashPSC3 != none)
	{
		MuzzleFlashPSC3.DeactivateSystem();
	}
	
	if (MuzzleFlashPSC4 != none)
	{
		MuzzleFlashPSC4.DeactivateSystem();
	}
}

simulated event CauseMuzzleFlash()
{
	CauseMuzzleFlashLight();
	
	if (!bHidden || bShowFireFXWhenHidden)
	{
		if ( !bMuzzleFlashAttached )
		{
			AttachMuzzleFlash();
		}
		
		if (MuzzleFlashPSC1 != none)
		{
			if (!MuzzleFlashPSC1.bIsActive || MuzzleFlashPSC1.bWasDeactivated)
			{
				MuzzleFlashPSC1.SetTemplate(MuzzleFlashPSCTemplate);
				SetMuzzleFlashParams(MuzzleFlashPSC1);
				MuzzleFlashPSC1.ActivateSystem(false);
			}
		}
		
		if (MuzzleFlashPSC2 != none)
		{
			if (!MuzzleFlashPSC2.bIsActive || MuzzleFlashPSC2.bWasDeactivated)
			{
				MuzzleFlashPSC2.SetTemplate(MuzzleFlashPSCTemplate);
				SetMuzzleFlashParams(MuzzleFlashPSC2);
				MuzzleFlashPSC2.ActivateSystem(false);
			}
		}
		
		if (MuzzleFlashPSC3 != none)
		{
			if (!MuzzleFlashPSC3.bIsActive || MuzzleFlashPSC3.bWasDeactivated)
			{
				MuzzleFlashPSC3.SetTemplate(MuzzleFlashPSCTemplate);
				SetMuzzleFlashParams(MuzzleFlashPSC3);
				MuzzleFlashPSC3.ActivateSystem(false);
			}
		}
		
		if (MuzzleFlashPSC4 != none)
		{
			if (!MuzzleFlashPSC4.bIsActive || MuzzleFlashPSC4.bWasDeactivated)
			{
				MuzzleFlashPSC4.SetTemplate(MuzzleFlashPSCTemplate);
				SetMuzzleFlashParams(MuzzleFlashPSC4);
				MuzzleFlashPSC4.ActivateSystem(false);
			}
		}
		
		SetTimer(MuzzleFlashDuration,false,'MuzzleFlashTimer');
	}
}

simulated event StopMuzzleFlash()
{
	ClearTimer('MuzzleFlashTimer');
	MuzzleFlashTimer();
	
	if (MuzzleFlashPSC1 != none)
	{
		MuzzleFlashPSC1.DeactivateSystem();
	}
	
	if (MuzzleFlashPSC2 != none)
	{
		MuzzleFlashPSC2.DeactivateSystem();
	}
	
	if (MuzzleFlashPSC3 != none)
	{
		MuzzleFlashPSC3.DeactivateSystem();
	}
	
	if (MuzzleFlashPSC4 != none)
	{
		MuzzleFlashPSC4.DeactivateSystem();
	}
}

simulated function AttachMuzzleFlash()
{
	bMuzzleFlashAttached = true;
	
	if (ROSkeletalMeshComponent(Mesh) != none)
	{
		if (MuzzleFlashPSCTemplate != none)
		{
			MuzzleFlashPSC1 = new(Outer) class'ROParticleSystemComponent';
			MuzzleFlashPSC1.bAutoActivate = false;
			MuzzleFlashPSC1.SetDepthPriorityGroup(Mesh.DepthPriorityGroup);
			MuzzleFlashPSC1.SetFOV(ROSkeletalMeshComponent(Mesh).FOV);
			
			MuzzleFlashPSC2 = new(Outer) class'ROParticleSystemComponent';
			MuzzleFlashPSC2.bAutoActivate = false;
			MuzzleFlashPSC2.SetDepthPriorityGroup(Mesh.DepthPriorityGroup);
			MuzzleFlashPSC2.SetFOV(ROSkeletalMeshComponent(Mesh).FOV);
			
			MuzzleFlashPSC3 = new(Outer) class'ROParticleSystemComponent';
			MuzzleFlashPSC3.bAutoActivate = false;
			MuzzleFlashPSC3.SetDepthPriorityGroup(Mesh.DepthPriorityGroup);
			MuzzleFlashPSC3.SetFOV(ROSkeletalMeshComponent(Mesh).FOV);
			
			MuzzleFlashPSC4 = new(Outer) class'ROParticleSystemComponent';
			MuzzleFlashPSC4.bAutoActivate = false;
			MuzzleFlashPSC4.SetDepthPriorityGroup(Mesh.DepthPriorityGroup);
			MuzzleFlashPSC4.SetFOV(ROSkeletalMeshComponent(Mesh).FOV);
			
			ROSkeletalMeshComponent(Mesh).AttachComponentToSocket(MuzzleFlashPSC1, default.MaximMuzzleFlashSockets[0]);
			ROSkeletalMeshComponent(Mesh).AttachComponentToSocket(MuzzleFlashPSC2, default.MaximMuzzleFlashSockets[1]);
			ROSkeletalMeshComponent(Mesh).AttachComponentToSocket(MuzzleFlashPSC3, default.MaximMuzzleFlashSockets[2]);
			ROSkeletalMeshComponent(Mesh).AttachComponentToSocket(MuzzleFlashPSC4, default.MaximMuzzleFlashSockets[3]);
		}
	}
}
simulated function DetachMuzzleFlash()
{
	bMuzzleFlashAttached = false;
	
	if (ROSkeletalMeshComponent(Mesh) != none)
	{
		if (MuzzleFlashPSC1 != none)
		{
			ROSkeletalMeshComponent(Mesh).DetachComponent( MuzzleFlashPSC1 );
		}
		
		if (MuzzleFlashPSC2 != none)
		{
			ROSkeletalMeshComponent(Mesh).DetachComponent( MuzzleFlashPSC2 );
		}
		
		if (MuzzleFlashPSC3 != none)
		{
			ROSkeletalMeshComponent(Mesh).DetachComponent( MuzzleFlashPSC3 );
		}
		
		if (MuzzleFlashPSC4 != none)
		{
			ROSkeletalMeshComponent(Mesh).DetachComponent( MuzzleFlashPSC4 );
		}
	}
	
	MuzzleFlashPSC1 = none;
	MuzzleFlashPSC2 = none;
	MuzzleFlashPSC3 = none;
	MuzzleFlashPSC4 = none;
}

simulated function SetFOV( float NewFOV )
{
	super.SetFOV(NewFOV);
	
	if (MuzzleFlashPSC1 != none)
	{
		MuzzleFlashPSC1.SetFOV(NewFOV);
	}
	
	if (MuzzleFlashPSC2 != none)
	{
		MuzzleFlashPSC2.SetFOV(NewFOV);
	}
	
	if (MuzzleFlashPSC3 != none)
	{
		MuzzleFlashPSC3.SetFOV(NewFOV);
	}
	
	if (MuzzleFlashPSC4 != none)
	{
		MuzzleFlashPSC4.SetFOV(NewFOV);
	}
}

simulated function MoveWeaponToWorld()
{
	if (ROPawn(Instigator) != none)
	{
		MuzzleFlashPSC1.SetDepthPriorityGroup(SDPG_World);
		MuzzleFlashPSC2.SetDepthPriorityGroup(SDPG_World);
		MuzzleFlashPSC3.SetDepthPriorityGroup(SDPG_World);
		MuzzleFlashPSC4.SetDepthPriorityGroup(SDPG_World);
	}
	
	super.MoveWeaponToWorld();
}

simulated function MoveWeaponToForeground()
{
	if (ROPawn(Instigator) != none)
	{
		MuzzleFlashPSC1.SetDepthPriorityGroup(SDPG_Foreground);
		MuzzleFlashPSC2.SetDepthPriorityGroup(SDPG_Foreground);
		MuzzleFlashPSC3.SetDepthPriorityGroup(SDPG_Foreground);
		MuzzleFlashPSC4.SetDepthPriorityGroup(SDPG_Foreground);
	}
	
	super.MoveWeaponToForeground();
}

simulated function PlayReload(float ReloadDuration)
{
	super.PlayReload(ReloadDuration);
	
	if (Instigator.IsHumanControlled() && Instigator.IsLocallyControlled())
	{
		WWPlayerController(Instigator.Controller).ReceiveLocalizedMessage(class'WWLocalMessageGameReloadingMaxim');
	}
}

simulated state Reloading
{
	simulated function BeginState(name PreviousStateName)
	{
		super.BeginState(PreviousStateName);
		
		if (Role == ROLE_Authority)
		{
			ROGameInfo(WorldInfo.Game).HandleBattleChatterEvent(Instigator, `BATTLECHATTER_Reloading);
		}
	}
}

defaultproperties
{
	InvIndex=INDEX_NONE
	
	AmmoBeltSocket=AmmoBeltSocket
	
	FiringStatesArray(0)=WeaponFiring
	WeaponFireTypes(0)=EWFT_Custom
	WeaponProjectiles(0)=class'WWProjectile_QuadMaxim'
	FireInterval(0)=+0.1 // 600rpm
	Spread(0)=0.000175
	
	InstantHitDamageTypes(0)=class'WWDmgType_QuadMaxim'
	InstantHitDamageTypes(1)=class'WWDmgType_QuadMaxim'
	
	WeaponIdleAnims(0)=DshK_Ironsight_Idle
	WeaponIdleAnims(1)=DshK_Ironsight_Idle
	WeaponIdleSightedAnims(0)=DshK_Ironsight_Idle
	WeaponIdleSightedAnims(1)=DshK_Ironsight_Idle
	WeaponIdleShoulderedAnims(0)=DshK_Ironsight_Idle
	WeaponIdleShoulderedAnims(1)=DshK_Ironsight_Idle
	
	WeaponReloadEmptyMagAnim=DshK_Idle
	WeaponReloadNonEmptyMagAnim=DshK_Idle
	WeaponAmmoCheckAnim=DshK_Idle
	
	AmmoClass=class'WWAmmo_MaximBox'
	MaxAmmoCount=350
	
	bCanReloadNonEmptyMag=true
	
	MaximMuzzleFlashSockets(0)=Muzzle_Maxim1
	MaximMuzzleFlashSockets(1)=Muzzle_Maxim2
	MaximMuzzleFlashSockets(2)=Muzzle_Maxim3
	MaximMuzzleFlashSockets(3)=Muzzle_Maxim4
	
	MuzzleFlashPSCTemplate=ParticleSystem'WinterWar_FX.ParticleSystems.FX_WW_MuzzleFlash_1stP_LMG'
	
	bShowFireFXWhenHidden=true
	
	TracerFrequency=0
	
	SightRanges.Empty
	SightRanges[0]=(SightRange=200,SightSlideOffset=0.0,SightPositionOffset=-0.000,AddedPitch=10)
	
	SuppressionPower=50
	
	WeaponFireSnd(DEFAULT_FIREMODE)=(DefaultCue=AkEvent'WW_WEP_M2_Browning.Play_WEP_M2_Browning_Loop_3P', FirstPersonCue=AkEvent'WW_WEP_M2_Browning.Play_WEP_M2_Browning_Auto_LP')
	bLoopingFireSnd(DEFAULT_FIREMODE)=true
	bLoopHighROFSounds(DEFAULT_FIREMODE)=true
	
	WeaponFireLoopEndSnd(DEFAULT_FIREMODE)=(DefaultCue=AkEvent'WW_WEP_M2_Browning.Play_WEP_M2_Browning_Tail_3P', FirstPersonCue=AkEvent'WW_WEP_M2_Browning.Play_WEP_M2_Browning_Auto_Tail')
}

