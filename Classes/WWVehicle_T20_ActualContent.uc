
class WWVehicle_T20_ActualContent extends WWVehicle_T20
	placeable;

DefaultProperties
{
	Begin Object Name=ROSVehicleMesh
		SkeletalMesh=SkeletalMesh'WinterWar_VH_USSR_T-20.Mesh.T-20_Rig'
		LightingChannels=(Dynamic=TRUE,Unnamed_1=TRUE,bInitialized=TRUE)
		AnimTreeTemplate=AnimTree'WinterWar_VH_USSR_T-20.Anim.T-20_AnimTree'
		PhysicsAsset=PhysicsAsset'WinterWar_VH_USSR_T-20.Phy.T-20_Rig_Physics'
	End Object
	
	DestroyedSkeletalMesh=SkeletalMesh'WinterWar_VH_USSR_T-20.Mesh.T-20_Wreck'
	DestroyedPhysicsAsset=PhysicsAsset'WinterWar_VH_USSR_T-20.Phy.T-20_Wreck_Physics'
	
	DestroyedMats(0)=MaterialInstanceConstant'WinterWar_VH_USSR_T-20.MIC.T-20_WRECK_M'
	
	SeatProxies(0)={(
		TunicMeshType=SkeletalMesh'CHR_VN_US_Army.Mesh.US_Tunic_Long_Mesh',
		HeadGearMeshType=SkeletalMesh'CHR_VN_US_Headgear.Mesh.US_headgear_var1',
		HeadAndArmsMeshType=SkeletalMesh'CHR_VN_US_Heads.Mesh.US_Head1_Mesh',
		HeadphonesMeshType=none,
		HeadAndArmsMICTemplate=MaterialInstanceConstant'CHR_VN_US_Heads.Materials.M_US_Head_01_Long_INST',
		BodyMICTemplate=MaterialInstanceConstant'CHR_VN_US_Army.Materials.M_US_Tunic_Long_INST',
		SeatIndex=0,
		PositionIndex=0
	)}
	
	SeatProxies(1)={(
		TunicMeshType=SkeletalMesh'CHR_VN_US_Army.Mesh.US_Tunic_Long_Mesh',
		HeadGearMeshType=SkeletalMesh'CHR_VN_US_Headgear.Mesh.US_headgear_var1',
		HeadAndArmsMeshType=SkeletalMesh'CHR_VN_US_Heads.Mesh.US_Head1_Mesh',
		HeadphonesMeshType=none,
		HeadAndArmsMICTemplate=MaterialInstanceConstant'CHR_VN_US_Heads.Materials.M_US_Head_01_Long_INST',
		BodyMICTemplate=MaterialInstanceConstant'CHR_VN_US_Army.Materials.M_US_Tunic_Long_INST',
		SeatIndex=1,
		PositionIndex=0
	)}
	
	SeatProxies(2)={(
		TunicMeshType=SkeletalMesh'CHR_VN_US_Army.Mesh.US_Tunic_Long_Mesh',
		HeadGearMeshType=SkeletalMesh'CHR_VN_US_Headgear.Mesh.US_headgear_var1',
		HeadAndArmsMeshType=SkeletalMesh'CHR_VN_US_Heads.Mesh.US_Head1_Mesh',
		HeadphonesMeshType=none,
		HeadAndArmsMICTemplate=MaterialInstanceConstant'CHR_VN_US_Heads.Materials.M_US_Head_01_Long_INST',
		BodyMICTemplate=MaterialInstanceConstant'CHR_VN_US_Army.Materials.M_US_Tunic_Long_INST',
		SeatIndex=2,
		PositionIndex=0
	)}
	
	SeatProxies(3)={(
		TunicMeshType=SkeletalMesh'CHR_VN_US_Army.Mesh.US_Tunic_Long_Mesh',
		HeadGearMeshType=SkeletalMesh'CHR_VN_US_Headgear.Mesh.US_headgear_var1',
		HeadAndArmsMeshType=SkeletalMesh'CHR_VN_US_Heads.Mesh.US_Head1_Mesh',
		HeadphonesMeshType=none,
		HeadAndArmsMICTemplate=MaterialInstanceConstant'CHR_VN_US_Heads.Materials.M_US_Head_01_Long_INST',
		BodyMICTemplate=MaterialInstanceConstant'CHR_VN_US_Army.Materials.M_US_Tunic_Long_INST',
		SeatIndex=3,
		PositionIndex=0
	)}
	
	SeatProxies(4)={(
		TunicMeshType=SkeletalMesh'CHR_VN_US_Army.Mesh.US_Tunic_Long_Mesh',
		HeadGearMeshType=SkeletalMesh'CHR_VN_US_Headgear.Mesh.US_headgear_var1',
		HeadAndArmsMeshType=SkeletalMesh'CHR_VN_US_Heads.Mesh.US_Head1_Mesh',
		HeadphonesMeshType=none,
		HeadAndArmsMICTemplate=MaterialInstanceConstant'CHR_VN_US_Heads.Materials.M_US_Head_01_Long_INST',
		BodyMICTemplate=MaterialInstanceConstant'CHR_VN_US_Army.Materials.M_US_Tunic_Long_INST',
		SeatIndex=4,
		PositionIndex=0
	)}
	
	SeatProxies(5)={(
		TunicMeshType=SkeletalMesh'CHR_VN_US_Army.Mesh.US_Tunic_Long_Mesh',
		HeadGearMeshType=SkeletalMesh'CHR_VN_US_Headgear.Mesh.US_headgear_var1',
		HeadAndArmsMeshType=SkeletalMesh'CHR_VN_US_Heads.Mesh.US_Head1_Mesh',
		HeadphonesMeshType=none,
		HeadAndArmsMICTemplate=MaterialInstanceConstant'CHR_VN_US_Heads.Materials.M_US_Head_01_Long_INST',
		BodyMICTemplate=MaterialInstanceConstant'CHR_VN_US_Army.Materials.M_US_Tunic_Long_INST',
		SeatIndex=5,
		PositionIndex=0
	)}
	
	SeatProxyAnimSet=AnimSet'WinterWar_VH_USSR_T-20.Anim.CHR_UC_Anim_Master'
	
	Begin Object Class=AudioComponent Name=EngineStartupAudioComponent
		SoundCue=SoundCue'WinterWar_VH_USSR_COMMON_SFX.Audio.UC_Movement_Engine_Start_Exhaust_Cue'
	End Object
	EngineStartupAudio=EngineStartupAudioComponent
	Components.Add(EngineStartupAudioComponent)
	
	Begin Object Class=AudioComponent Name=EngineRunAudioComponent
		SoundCue=SoundCue'WinterWar_VH_USSR_COMMON_SFX.Audio.UC_Movement_Engine_Run_Exhaust_Cue'
	End Object
	EngineRunAudio=EngineRunAudioComponent
	Components.Add(EngineRunAudioComponent)
	
	Begin Object Class=AudioComponent Name=EngineShiftUpAudioComponent
		SoundCue=SoundCue'WinterWar_VH_USSR_COMMON_SFX.Audio.UC_Movement_Engine_Exhaust_ShiftUp_Cue'
	End Object
	EngineShiftUpAudio=EngineShiftUpAudioComponent
	Components.Add(EngineShiftUpAudioComponent)
	
	Begin Object Class=AudioComponent Name=EngineShiftDownAudioComponent
		SoundCue=SoundCue'WinterWar_VH_USSR_COMMON_SFX.Audio.Panzer_Movement_Engine_Exhaust_ShiftDown_Cue'
	End Object
	EngineShiftDownAudio=EngineShiftDownAudioComponent
	Components.Add(EngineShiftDownAudioComponent)
	
	Begin Object Class=AudioComponent Name=EngineShutdownAudioComponent
		SoundCue=SoundCue'WinterWar_VH_USSR_COMMON_SFX.Audio.UC_Movement_Engine_Stop_Cue'
	End Object
	EngineShutdownAudio=EngineShutdownAudioComponent
	Components.Add(EngineShutdownAudioComponent)
	
	
	Begin Object Class=AudioComponent Name=TracksDestroyedAudioComponent
		SoundCue=SoundCue'WinterWar_VH_USSR_COMMON_SFX.Audio.UC_Movement_Engine_Stop_Cue'
	End Object
	TracksDestroyedAudio=TracksDestroyedAudioComponent
	Components.Add(TracksDestroyedAudioComponent)
	
	
	Begin Object Class=AudioComponent name=HullMGSoundComponent
		bShouldRemainActiveIfDropped=true
		bStopWhenOwnerDestroyed=true
		SoundCue=SoundCue'WinterWar_VH_USSR_COMMON_SFX.Audio.MG_DP28_Tank_Fire_Loop_M_Cue'
	End Object
	MGAmbientSound=HullMGSoundComponent
	Components.Add(HullMGSoundComponent)
	
	Begin Object Class=AudioComponent Name=HullMGReloadSoundComponent
		SoundCue=SoundCue'WinterWar_VH_USSR_COMMON_SFX.Audio.Reload_DT'
	End Object
	MGReloadSound=HullMGReloadSoundComponent
	Components.Add(HullMGReloadSoundComponent)
	
	MGStopSound=SoundCue'WinterWar_VH_USSR_COMMON_SFX.Audio.MG_DP28_Tank_Fire_Loop_Tail_M_Cue'
}
