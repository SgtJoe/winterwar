
class WWWeapon_MN9130_Dyakonov extends WWWeapon_MosinBase
	abstract;

var /* repnotify */ int GrenadeCount;
var /* repnotify */ bool GrenadeLoaded/* , CurrentlyLoading */;

var(Animations) name LoadGrenadeAnim;

var class<Projectile> GrenadeProjectile;

var localized string GrenadesRemainingString;

var vector GrenadeViewPosition;

/* replication
{
	if (bNetDirty && bNetOwner && Role == ROLE_Authority)
		GrenadeCount, GrenadeLoaded;
} */

// Bayonet functionality is used to attach the launcher
simulated function SwapBayonetData()
{
	if (bUsingBayonet)
	{
		CollisionCheckLength = default.CollisionCheckLength + WeaponBayonetLength;
		SwayStaminaMod = default.SwayStaminaMod * WeaponBayonetSpreadScale;
		ShoulderedSpreadMod = default.ShoulderedSpreadMod * WeaponBayonetSpreadScale;
		HippedSpreadMod = default.HippedSpreadMod * WeaponBayonetSpreadScale;
		Spread[0] = default.Spread[0] * WeaponBayonetSpreadScale;
		Spread[1] = default.Spread[1] * WeaponBayonetSpreadScale;
		InstantAttachBayonet();
		
		DisplayRemainingGrenades();
	}
	else
	{
		CollisionCheckLength = default.CollisionCheckLength;
		SwayStaminaMod = default.SwayStaminaMod;
		ShoulderedSpreadMod = default.ShoulderedSpreadMod;
		HippedSpreadMod = default.HippedSpreadMod;
		Spread[0] = default.Spread[0];
		Spread[1] = default.Spread[1];
		InstantDetachBayonet();
		
		GrenadeLoaded = false;
	}
}

// Don't allow melee with the launcher attached (until we can sync the 3rd person anims)
simulated function bool UseMeleeFireMode()
{
	return !bUsingBayonet;
}

// SwitchFireMode is used to load a grenade
simulated exec function SwitchFireMode()
{
	if (ROPawn(Instigator) != none && (ROPawn(Instigator).bBandaging || ROPawn(Instigator).bDisarmingTrap))
	{
		return;
	}
	
	if (!bUsingBayonet)
	{
		`wwdebug("Launcher not attached",'D');
		return;
	}
	
	if (GrenadeLoaded)
	{
		`wwdebug("Grenade already loaded",'D');
		return;
	}
	
	`wwdebug(""$ GrenadeCount @ GrenadeLoaded,'D');
	
	StopFire(DEFAULT_FIREMODE);
	
	if (GrenadeCount <= 0)
	{
		DisplayRemainingGrenades();
		ROGameInfo(WorldInfo.Game).HandleBattleChatterEvent(Instigator, `BATTLECHATTER_OutOfAmmo);
		return;
	}
	
	if (bUsingSights)
	{
		if (Role == ROLE_Authority)
		{
			ServerZoomOut(false);
		}
		else
		{
			ZoomOut(false, default.ZoomOutTime);
		}
	}
	
	GrenadeLoaded = false;
	
	PlayAnimation(LoadGrenadeAnim, SkeletalMeshComponent(Mesh).GetAnimLength(LoadGrenadeAnim));
	
	// This is now set via AnimNotify, to prevent loading the grenade if the anim is canceled
	// SetTimer(SkeletalMeshComponent(Mesh).GetAnimLength(LoadGrenadeAnim) * 0.95, false, 'LoadGrenade');
}

simulated function LoadGrenade()
{
	GrenadeLoaded = true;
	
	`wwdebug(""$ GrenadeCount @ GrenadeLoaded,'D');
	
	ServerLoadGrenade();
}

reliable server function ServerLoadGrenade()
{
	GrenadeLoaded = true;
	
	`wwdebug(""$ GrenadeCount @ GrenadeLoaded,'D');
}

function DisplayRemainingGrenades()
{
	PlayerController(Instigator.Controller).ClientMessage(GrenadeCount @ GrenadesRemainingString, 'Warning');
}

simulated function ZoomIn(bool bAnimateTransition, float ZoomTimeToGo)
{
	if( bAnimateTransition )
	{
		ZoomInTime=ZoomTimeToGo;
		
		if( bZoomingOut )
		{
			bZoomingOut=false;
			bZoomOutInterrupted=true;
			ZoomTime=ZoomInTime - ZoomTime;
			ZoomPartialTime=ZoomTime;
			ZoomStartOffset=PlayerViewOffset;
			ZoomRotStartOffset=ZoomRotInterp;
		}
		else
		{
			ZoomTime=ZoomInTime;
			ZoomStartOffset=PlayerViewOffset;
		}
		
		if (bUsingBayonet && GrenadeLoaded)
		{
			`wwdebug("Grenade Loaded",'D');
			ZoomTargetOffset=GrenadeViewPosition;
		}
		else
		{
			ZoomTargetOffset=IronSightPosition;
		}
		
		bZoomingIn=true;
	}
	
	if (WorldInfo.NetMode != NM_DedicatedServer && ROPlayerController(Instigator.Controller) != none)
	{
		ROPlayerController(Instigator.Controller).HandleIronSightZoom(true, PlayerIronSightFOV, ZoomTime, WeaponProficiencyDDFSettleTransitionDuration, WeaponProficiencyDDFFocusBlendRate);
		SetSway(true);
	}
	
	bShouldered = false;
	bShoulderingIn = false;
	bShoulderingOut = false;
	bUsingSights = true;
	bCachedUsingSights = bUsingSights;
	
	if (ROPawn(Instigator) != none)
	{
		// This should work but for some reason doesn't
		/* if (bUsingBayonet && GrenadeLoaded)
		{
			ROPawn(Instigator).HeavyWeaponAction();
		}
		else
		{
			ROPawn(Instigator).HeavyWeaponActionComplete();
		} */
		
		ROPawn(Instigator).SetIronSightAnims(true);
	}
}

simulated function FireAmmunition()
{
	local vector StartTrace, EndTrace, RealStartLoc, AimDir;
	local ImpactInfo TestImpact;
	local Projectile SpawnedProjectile;
	
	super.FireAmmunition();
	
	if (!bUsingBayonet)
	{
		`wwdebug("Launcher not attached",'D');
		return;
	}
	
	if (!GrenadeLoaded || GrenadeCount <= 0 || GrenadeProjectile == none)
	{
		`wwdebug("No grenade loaded",'D');
		return;
	}
	
	// Only spawn the actual projectile on the server
	if (Role == ROLE_Authority)
	{
		StartTrace = Instigator.GetWeaponStartTraceLocation();
		AimDir = Vector(GetAdjustedAim( StartTrace ));
		RealStartLoc = GetPhysicalFireStartLoc(AimDir);
		
		if (StartTrace != RealStartLoc)
		{
			EndTrace = StartTrace + AimDir * GetTraceRange();
			TestImpact = CalcWeaponFire(StartTrace, EndTrace);
			AimDir = Normal(TestImpact.HitLocation - RealStartLoc);
		}
		
		SpawnedProjectile = Spawn(GetGrenadeClass(), Self,, RealStartLoc);
		
		if (SpawnedProjectile != None && !SpawnedProjectile.bDeleteMe)
		{
			SpawnedProjectile.Init(AimDir);
		}
	}
	
	// Update these vars on both
	`wwdebug(""$ GrenadeCount @ GrenadeLoaded,'D');
	
	GrenadeCount--;
	GrenadeLoaded = false;
	
	`wwdebug(""$ GrenadeCount @ GrenadeLoaded,'D');
	
	DisplayRemainingGrenades();
}

simulated function class<Projectile> GetGrenadeClass()
{
	return GrenadeProjectile;
}

defaultproperties
{
	WeaponContentClass(0)="WinterWar.WWWeapon_MN9130_Dyakonov_ActualContent"
	
	RoleSelectionImage(0)=Texture2D'WinterWar_UI.WP_Render.WP_Render_MN9130'
	
	TeamIndex=`ALLIES_TEAM_INDEX
	
	InvIndex=`WI_DYAKONOV
	
	WeaponProjectiles(DEFAULT_FIREMODE)=	class'MN9130Bullet'
	WeaponProjectiles(ALTERNATE_FIREMODE)=	class'MN9130Bullet'
	
	GrenadeProjectile=class'WWProjectile_DyakonovGrenade'
	
	Spread(DEFAULT_FIREMODE)=	0.0007// ~2.5 MOA
	Spread(ALTERNATE_FIREMODE)=	0.0007// ~2.5 MOA
	
	InstantHitDamageTypes(0)=class'RODmgType_MN9130Bullet'
	InstantHitDamageTypes(1)=class'RODmgType_MN9130Bullet'
	
	ShoulderedPosition=(X=1.5,Y=2,Z=-0.75)
	IronSightPosition=(X=-3,Y=0,Z=0)
	GrenadeViewPosition=(X=3.0,Y=1.6,Z=-5.5)
	
	GrenadeCount=5
	
	LoadGrenadeAnim=9130_LoadGrenade
	
	bCanThrow=false
	
	bDebugWeapon=false
	
	SightRanges.Empty
	SightRanges[0]=(SightRange=100,SightPitch=-175,SightSlideOffset=0.05,SightPositionOffset=-0.1)
}
