
class WWVehicleDeathTurret_T26 extends ROVehicleDeathPiece;

DefaultProperties
{
	GibMeshesData[0]={(
		TheStaticMesh=None,
		TheSkelMesh=SkeletalMesh'WinterWar_VH_USSR_T-26.Mesh.T-26_Wreck_Turret',
		ThePhysAsset=PhysicsAsset'WinterWar_VH_USSR_T-26.Phy.T-26_Wreck_Turret_Physics',
		DrawScale=1.0f,
		bUseSecondaryGibMeshMITV=FALSE
	)}
}
