
class WWHUDWidgetWeapon extends ROHUDWidgetWeapon;

function UpdateWidget()
{
	super.UpdateWidget();
	
	HUDComponents[ROWC_TunnelSpawnIcon].bVisible = false;
	HUDComponents[ROWC_TunnelSpawnCount].bVisible = false;
	HUDComponents[ROWC_DigTunnelIcon].bVisible = false;
	HUDComponents[ROWC_DigTunnelTextP1].bVisible = false;
	HUDComponents[ROWC_DigTunnelTextP2].bVisible = false;
	HUDComponents[ROWC_DigTunnelTextP3].bVisible = false;
	
	if (ROVehicle(PlayerOwner.Pawn) != none || ROWeaponPawn(PlayerOwner.Pawn) != none)
	{
		HUDComponents[ROWC_GrenadeIcon].bVisible = false;
		HUDComponents[ROWC_GrenadeCount].bVisible = false;
		HUDComponents[ROWC_FireModeIcon].bVisible = false;
	}
}

simulated function string GetWeaponNameByIndex(int WeaponIndex, optional bool bIsMagazine = false)
{
	local class<ROWeapon> ROWClass;
	ROWClass = class'WWHUDWidgetKillMessages'.static.GetWeaponClassByIndex(WeaponIndex);
	
	if ( ROWClass != none )
	{
		if ( bIsMagazine )
		{
			return ROWClass.default.ShortDisplayName @ MagazineText;
		}
		
		return ROWClass.default.ShortDisplayName;
	}
	else
	{
		if ( WeaponIndex == `ROII_Bandage )
		{
			return BandageResupplyText;
		}
		else if ( WeaponIndex == `ROII_CarriedAmmo )
		{
			return CarriedMGAmmoResupplyText;
		}
	}
	
	return "";
}

simulated function Texture2D GetMagTexByWeapIndex(int WeaponIndex)
{
	local class<ROWeapon> ROWClass;
	ROWClass = class'WWHUDWidgetKillMessages'.static.GetWeaponClassByIndex(WeaponIndex);
	
	if ( ROWClass != none )
	{
		return GetMagTexByWeapClass(ROWClass);
	}
	else
	{
		if ( WeaponIndex == `ROII_Bandage )
		{
			return BandageResupplyTex;
		}
		else if ( WeaponIndex == `ROII_CarriedAmmo )
		{
			return CarriedMGAmmoResupplyTex;
		}
	}
	
	return none;
}
