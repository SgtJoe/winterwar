
class WWPawnAlliesTanker extends WWPawnAllies;

function OneSecondLoopingTimer()
{
	if (WWVehicleTank(DrivenVehicle) != none || WWVehicleTank(ROWeaponPawn(DrivenVehicle).MyVehicle) != none)
	{
		return;
	}
	
	super.OneSecondLoopingTimer();
}

defaultproperties
{
	TunicMesh=SkeletalMesh'WinterWar_CHR_SOV.Mesh.SOV_Tanker'
	PawnMesh_SV=SkeletalMesh'WinterWar_CHR_SOV.Mesh.SOV_Tanker'
	FieldgearMesh=SkeletalMesh'WinterWar_CHR_SOV.Mesh.SOV_Gear_Tanker'
	BodyMICTemplate=MaterialInstanceConstant'WinterWar_CHR_SOV.MIC.SOV_Tanker'
	
	BadgeMesh=none
	
	ArmsOnlyMeshFP=SkeletalMesh'WinterWar_CHR_ALL.Mesh.Sov_Hands_Tanker'
	
	HeadAndArmsMesh=SkeletalMesh'WinterWar_CHR_SOV.Mesh.SOV_Head_8'
	HeadAndArmsMICTemplate=MaterialInstanceConstant'WinterWar_CHR_SOV.MIC.SOV_Head_8_Tanker'
	
	HeadgearMesh=SkeletalMesh'WinterWar_CHR_SOV.Mesh.SOV_Headgear_Tanker'
	HeadgearMICTemplate=MaterialInstanceConstant'WinterWar_CHR_SOV.MIC.SOV_Tanker'
	
	bIsPilot=true
}
