
class WWVoicePack extends ROVoicePack;

defaultproperties
{
	// VoiceComs[`VOICECOM_Burning]=(Sound="AkEvent'WW_VOX_VC_Voice1.Play_VC_Soldier_1_DeathByFire'")
	
	VoiceComs[`VOICECOM_TankCannonReloaded]=(Priority=Speech_Command)
	VoiceComs[`VOICECOM_TankBailOut]=(Type=ROVCT_Vehicle,MessageClass=none,Priority=Speech_OnFire,VoiceMessageFrequencyPriority=7)
}
