
class WWWeapon_Binoculars extends ROItem_Binoculars
	abstract;

`include(WinterWar\Classes\WWWeaponPickupMessagesOverride.uci)

DefaultProperties
{
	WeaponContentClass(0)="WinterWar.WWWeapon_Binoculars_ActualContent"
	
	InvIndex=`WI_Binocs
}
