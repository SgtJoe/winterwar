
class WWProjectile_M32Grenade extends Type67GrenadeProjectile;

defaultproperties
{
	MyDamageType=class'WWDmgType_M32Grenade'
	
	Begin Object Name=ThowableMeshComponent
		SkeletalMesh=SkeletalMesh'WinterWar_WP_FIN_M32_GRENADE.Mesh.FIN_M32_Grenade_3rd'
		PhysicsAsset=PhysicsAsset'WP_VN_3rd_Projectile.Phy.Type67_Projectile_3rd_Physics'
	End Object
}
