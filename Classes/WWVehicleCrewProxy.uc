
class WWVehicleCrewProxy extends VehicleCrewProxy;

simulated function ReplaceProxyMeshWithPawn(ROPawn InPawn)
{
	if (InPawn != none)
	{
		Mesh.ReplaceSkeletalMesh(InPawn.CompositedBodyMesh);
		
		Mesh.GenerateAnimationOverrideBones(InPawn.HeadAndArmsMesh);
		
		ThirdPersonHeadAndArmsMeshComponent.ReplaceSkeletalMesh(InPawn.HeadAndArmsMesh);
		ThirdPersonHeadAndArmsMeshComponent.SetParentAnimComponent(Mesh);
		ThirdPersonHeadAndArmsMeshComponent.SetShadowParent(Mesh);
		
		AttachNewInfantryHeadgear(InPawn);
		
		if (HeadAndArmsMIC == none)
			HeadAndArmsMIC = new class'MaterialInstanceConstant';
		if (BodyMIC == none)
			BodyMIC = new class'MaterialInstanceConstant';
		
		HeadAndArmsMIC.SetParent(InPawn.HeadAndArmsMICTemplate);
		BodyMIC.SetParent(InPawn.BodyMICTemplate);
		
		ThirdPersonHeadAndArmsMeshComponent.SetMaterial(0, HeadAndArmsMIC);
		
		Mesh.SetMaterial(0, BodyMIC);
		
		if (GetTeamNum() == `ALLIES_TEAM_INDEX)
		{
			Mesh.SetMaterial(1, BodyMIC);
		}
	}
}
