
class WWWeapon_PPD34_ActualContent extends WWWeapon_PPD34;

DefaultProperties
{
	ArmsAnimSet=AnimSet'WinterWar_WP_SOV_PPD34.Anim.SOV_PPD34_Anims'
	
	Begin Object Name=FirstPersonMesh
		DepthPriorityGroup=SDPG_Foreground
		SkeletalMesh=SkeletalMesh'WinterWar_WP_SOV_PPD34.Mesh.SOV_PPD34'
		PhysicsAsset=PhysicsAsset'WinterWar_WP_SOV_PPD34.Phy.SOV_PPD34_Physics'
		AnimSets(0)=AnimSet'WinterWar_WP_SOV_PPD34.Anim.SOV_PPD34_Anims'
		AnimTreeTemplate=AnimTree'WinterWar_WP_SOV_PPD34.Anim.SOV_PPD34_Tree'
		Scale=1.0
		FOV=70
	End Object
	
	Begin Object Name=PickupMesh
		SkeletalMesh=SkeletalMesh'WinterWar_WP_SOV_PPD34.Mesh.SOV_PPD34_3rd'
		PhysicsAsset=PhysicsAsset'WinterWar_WP_SOV_PPD34.Phy.SOV_PPD34_3rd_Physics'
		CollideActors=true
		BlockActors=true
		BlockZeroExtent=true
		BlockNonZeroExtent=true//false
		BlockRigidBody=true
		bHasPhysicsAssetInstance=false
		bUpdateKinematicBonesFromAnimation=false
		PhysicsWeight=1.0
		RBChannel=RBCC_GameplayPhysics
		RBCollideWithChannels=(Default=TRUE,GameplayPhysics=TRUE,EffectPhysics=TRUE)
		bSkipAllUpdateWhenPhysicsAsleep=TRUE
		bSyncActorLocationToRootRigidBody=true
	End Object
	
	AttachmentClass=class'WWWeapon_PPD34_Attach'
}
