
class WWWeapon_LahtiSaloranta_Attach extends ROWeapAttach_L2A1_LMG;

defaultproperties
{
	Begin Object Name=SkeletalMeshComponent0
		SkeletalMesh=SkeletalMesh'WinterWar_WP_FIN_LS_M26.Mesh.FIN_LahtiSaloranta_3rd'
		AnimTreeTemplate=AnimTree'WP_VN_AUS_3rd_Master.AnimTree.L2A1_3rd_Tree'
		AnimSets(0)=AnimSet'WP_VN_AUS_3rd_Master.Animation.L2A1_3rd_Anim'
		PhysicsAsset=PhysicsAsset'WP_VN_AUS_3rd_Master.Phy_Bounds.L2A1_3rd_Bounds_Physics'
		CullDistance=5000
	End Object
	
	WeaponClass=class'WWWeapon_LahtiSaloranta'
	
	TracerClass=class'L2A1BulletTracer'
	TracerFrequency=5
	
	ReloadAnims(0)=CH_Reload_Half
	CH_ReloadAnims(0)=CH_Reload_Half
	Prone_ReloadAnims(0)=Prone_Reload_Half
}
