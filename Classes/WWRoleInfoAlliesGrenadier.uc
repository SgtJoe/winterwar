
class WWRoleInfoAlliesGrenadier extends WWRoleInfoAllies;

DefaultProperties
{
	RoleType=RORIT_Support
	ClassTier=2
	ClassIndex=`RI_GRENADIER
	
	Items[RORIGM_Default]={(
		PrimaryWeapons=(class'WWWeapon_MN9130_Dyakonov'),
		
		SecondaryWeapons=(class'WWWeapon_NagantRevolver')
	)}
	
	bAllowPistolsInRealism=true
	bBotSelectable=false
	
	ClassIcon=Texture2D'WinterWar_UI.RoleIcons.RoleIcon_Grenadier'
}
