
class WWProjectile_RGD33 extends Type67GrenadeProjectile;

defaultproperties
{
	ExplosionSound=AkEvent'WW_EXP_Shared.Play_EXP_Grenade_Explosion'
	
	MyDamageType=class'WWDmgType_RGD33'
	
	Begin Object Name=ThowableMeshComponent
		SkeletalMesh=SkeletalMesh'WinterWar_WP_SOV_RGD33.Mesh.SOV_RGD33_3rd'
	End Object
}
