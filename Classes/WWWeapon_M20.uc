
class WWWeapon_M20 extends ROProjectileWeapon
	abstract;

var name RoundBoneNames[50];

var name MagFollowerSkelControlName;

var SkelControlBase MagFollowerSkelControl;

var int UpcomingMagAmmoCount;

simulated event PostInitAnimTree(SkeletalMeshComponent SkelComp)
{
	super.PostInitAnimTree(SkelComp);
	
	if(MagFollowerSkelControlName != 'none')
	{
		MagFollowerSkelControl = SkelComp.FindSkelControl(MagFollowerSkelControlName);
	}
}

simulated function FireAmmunition()
{
	super.FireAmmunition();
	UpdateRounds(false);
}

simulated function TimeAmmoCheck()
{
	super.TimeAmmoCheck();
	UpdateRounds(false);
}

private reliable client function SendNextMagAmmoCount(int NewMagCount)
{
	UpcomingMagAmmoCount = NewMagCount;
}

simulated function TimeReloading()
{
	if( Role == ROLE_Authority )
	{
		UpcomingMagAmmoCount = AmmoArray.Length > 0 ? AmmoArray[GetNextMagIndex(AmmoCount <= 0)] : 0;
		SendNextMagAmmoCount(UpcomingMagAmmoCount);
	}
	
	UpdateRounds(false);
	
	super.TimeReloading();
}

simulated function UpdateRoundsTimeReloading()
{
	UpdateRounds(true);
}

simulated function UpdateRounds(bool bReloadingAnimNotify)
{
	local SkeletalMeshComponent SkelMesh;
	local int i, NumRoundsInMag;
	
	SkelMesh = SkeletalMeshComponent(Mesh);
	
	NumRoundsInMag = bReloadingAnimNotify ? UpcomingMagAmmoCount : AmmoCount /* - 1 */;
	
	if(SkelMesh != none)
	{
		for(i = 0; i < MaxAmmoCount /* - 1 */; ++i)
		{
			if(i >= NumRoundsInMag)
			{
				SkelMesh.HideBoneByName(RoundBoneNames[i], PBO_Disable);
			}
			else
			{
				SkelMesh.UnHideBoneByName(RoundBoneNames[i]);
			}
		}
	}
	
	if(MagFollowerSkelControl != none)
	{
		MagFollowerSkelControl.SetSkelControlStrength( 1 - FPctByRange(NumRoundsInMag, 0, MaxAmmoCount /* - 1 */), 0.f );
	}
}

`include(WinterWar\Classes\WWWeaponPickupMessagesOverride.uci)

defaultproperties
{
	WeaponContentClass(0)="WinterWar.WWWeapon_M20_ActualContent"
	
	RoleSelectionImage(0)=Texture2D'WinterWar_UI.WP_Render.WP_Render_M20'
	
	WeaponClassType=ROWCT_SMG
	
	TeamIndex=`AXIS_TEAM_INDEX
	
	Category=ROIC_Primary
	Weight=4.6 //KG
	InventoryWeight=2
	
	InvIndex=`WI_M20
	
	PlayerIronSightFOV=55
	
	FiringStatesArray(0)=WeaponFiring
	WeaponFireTypes(0)=EWFT_Custom
	WeaponProjectiles(0)=class'WWProjectile_M20'
	FireInterval(0)=+0.1 // 600 RPM
	DelayedRecoilTime(0)=0.0
	Spread(0)=0.0022 // 8 MOA
	
	FiringStatesArray(ALTERNATE_FIREMODE)=none
	WeaponProjectiles(ALTERNATE_FIREMODE)=none
	FireInterval(ALTERNATE_FIREMODE)=+0.13
	Spread(ALTERNATE_FIREMODE)=0
	
	WeaponDryFireSnd=AkEvent'WW_WEP_Shared.Play_WEP_Generic_Dry_Fire'
	
	PreFireTraceLength=1250 //25 Meters
	
	// AI
	MinBurstAmount=1
	MaxBurstAmount=15
	
	// Recoil
	maxRecoilPitch=160//170
	minRecoilPitch=160//170//140
	maxRecoilYaw=60
	minRecoilYaw=-80

	RecoilModWhenEmpty = 1.13f
	LagLimit=3.0
	ZoomInTime=0.3
	ZoomOutTime=0.25

	SwayScale=1.15
	
	RecoilRate=0.09//0.06
	RecoilMaxYawLimit=500
	RecoilMinYawLimit=65035
	RecoilMaxPitchLimit=750
	RecoilMinPitchLimit=64785
	RecoilISMaxYawLimit=600//500
	RecoilISMinYawLimit=65035
	RecoilISMaxPitchLimit=500
	RecoilISMinPitchLimit=65035
	RecoilBlendOutRatio=1
	minRecoilYawAbsolute=25
	
	InstantHitDamage(0)=60
	InstantHitDamage(1)=60
	
	InstantHitDamageTypes(0)=class'WWDmgType_M20'
	InstantHitDamageTypes(1)=class'WWDmgType_M20'
	
	MuzzleFlashSocket=MuzzleFlashSocket
	MuzzleFlashPSCTemplate=ParticleSystem'WinterWar_FX.ParticleSystems.FX_WW_MuzzleFlash_1stP_SMG'
	MuzzleFlashDuration=0.33
	MuzzleFlashLightClass=class'ROGame.RORifleMuzzleFlashLight'
	
	ShellEjectSocket=ShellEjectSocket
	ShellEjectPSCTemplate=ParticleSystem'FX_VN_Weapons.ShellEjects.FX_Wep_ShellEject_VC_PPSH'
	
	bHasIronSights=true
	
	WeaponEquipAnim=M20_pullout
	WeaponPutDownAnim=M20_putaway
	
	WeaponUpAnim=M20_pullout//M20_Up
	WeaponDownAnim=M20_putaway//M20_Down
	
	WeaponFireAnim(0)=M20_shoot
	WeaponFireAnim(1)=M20_shoot
	WeaponFireLastAnim=M20_shoot_last
	
	WeaponFireShoulderedAnim(0)=M20_shoot
	WeaponFireShoulderedAnim(1)=M20_shoot
	WeaponFireLastShoulderedAnim=M20_shoot_last
	
	WeaponFireSightedAnim(0)=M20_shoot
	WeaponFireSightedAnim(1)=M20_shoot
	WeaponFireLastSightedAnim=M20_shoot_last
	
	WeaponIdleAnims(0)=M20_idle
	WeaponIdleAnims(1)=M20_idle
	WeaponIdleShoulderedAnims(0)=M20_idle
	WeaponIdleShoulderedAnims(1)=M20_idle
	
	WeaponIdleSightedAnims(0)=M20_iron_idle
	WeaponIdleSightedAnims(1)=M20_iron_idle
	
	WeaponCrawlingAnims(0)=M20_Crawl
	WeaponCrawlStartAnim=M20_Crawl_into
	WeaponCrawlEndAnim=M20_Crawl_out
	
	WeaponReloadNonEmptyMagAnim=M20_reloadhalf
	WeaponReloadEmptyMagAnim=M20_reloadempty
	
	WeaponAmmoCheckAnim=M20_ammocheck
	
	WeaponSprintStartAnim=M20_sprint_into
	WeaponSprintLoopAnim=M20_sprint
	WeaponSprintEndAnim=M20_sprint_out
	
	Weapon1HSprintStartAnim=M20_ger_sprint_into
	Weapon1HSprintLoopAnim=M20_ger_sprint
	Weapon1HSprintEndAnim=M20_ger_sprint_out
	
	WeaponMantleOverAnim=M20_Mantle
	
	WeaponSpotEnemyAnim=M20_idle//M20_SpotEnemy
	WeaponSpotEnemySightedAnim=M20_iron_idle//M20_SpotEnemy_ironsight
	
	WeaponMeleeAnims(0)=M20_Bash
	WeaponMeleeHardAnim=M20_BashHard
	MeleePullbackAnim=M20_Pullback
	MeleeHoldAnim=M20_Pullback_Hold
	
	BoltControllerNames[0]=BoltSlide_M20
	
	MagFollowerSkelControlName=FeedControl
	
	RoundBoneNames(0)= B49
	RoundBoneNames(1)= B48
	RoundBoneNames(2)= B47
	RoundBoneNames(3)= B46
	RoundBoneNames(4)= B45
	RoundBoneNames(5)= B44
	RoundBoneNames(6)= B43
	RoundBoneNames(7)= B42
	RoundBoneNames(8)= B41
	RoundBoneNames(9)= B40
	RoundBoneNames(10)=B39
	RoundBoneNames(11)=B38
	RoundBoneNames(12)=B37
	RoundBoneNames(13)=B36
	RoundBoneNames(14)=B35
	RoundBoneNames(15)=B34
	RoundBoneNames(16)=B33
	RoundBoneNames(17)=B32
	RoundBoneNames(18)=B31
	RoundBoneNames(19)=B30
	RoundBoneNames(20)=B29
	RoundBoneNames(21)=B28
	RoundBoneNames(22)=B27
	RoundBoneNames(23)=B26
	RoundBoneNames(24)=B25
	RoundBoneNames(25)=B24
	RoundBoneNames(26)=B23
	RoundBoneNames(27)=B22
	RoundBoneNames(28)=B21
	RoundBoneNames(29)=B20
	RoundBoneNames(30)=B19
	RoundBoneNames(31)=B18
	RoundBoneNames(32)=B17
	RoundBoneNames(33)=B16
	RoundBoneNames(34)=B15
	RoundBoneNames(35)=B14
	RoundBoneNames(36)=B13
	RoundBoneNames(37)=B12
	RoundBoneNames(38)=B11
	RoundBoneNames(39)=B10
	RoundBoneNames(40)=B9
	RoundBoneNames(41)=B8
	RoundBoneNames(42)=B7
	RoundBoneNames(43)=B6
	RoundBoneNames(44)=B5
	RoundBoneNames(45)=B4
	RoundBoneNames(46)=B3
	RoundBoneNames(47)=B2
	RoundBoneNames(48)=B1
	RoundBoneNames(49)=B0
	
	ReloadMagazinEmptyCameraAnim=CameraAnim'1stperson_Cameras.Anim.Camera_MP40_reloadempty'
	
	EquipTime=+0.66//0.75
	
	bDebugWeapon = false
	
	ISFocusDepth=28
	
	// Ammo
	AmmoClass=class'WWAmmo_M20Mag'
	MaxAmmoCount=50
	bUsesMagazines=true
	InitialNumPrimaryMags=3
	bPlusOneLoading=false
	bCanReloadNonEmptyMag=true
	PenetrationDepth=15
	MaxPenetrationTests=3
	MaxNumPenetrations=2
	PerformReloadPct=0.85f
	
	PlayerViewOffset=(X=7.0,Y=6.0,Z=-4.0)
	ShoulderedPosition=(X=7.0,Y=5.5,Z=-3.5)
	IronSightPosition=(X=5.0,Y=0,Z=0.0)
	
	bUsesFreeAim=true
	
	FreeAimHipfireOffsetX=30
	
	SuppressionPower=5
	
	Begin Object Class=ForceFeedbackWaveform Name=ForceFeedbackWaveformShooting1
		Samples(0)=(LeftAmplitude=30,RightAmplitude=30,LeftFunction=WF_Constant,RightFunction=WF_Constant,Duration=0.100)
	End Object
	WeaponFireWaveForm=ForceFeedbackWaveformShooting1
	
	CollisionCheckLength=36.5
	
	FireCameraAnim[0]=CameraAnim'1stperson_Cameras.Anim.Camera_PPSh_Shoot'
	FireCameraAnim[1]=CameraAnim'1stperson_Cameras.Anim.Camera_PPSh_Shoot'
	
	WeaponFireSnd(DEFAULT_FIREMODE)=(DefaultCue=AkEvent'WW_WEP_Owen.Play_WEP_Owen_Single_3P', FirstPersonCue=AkEvent'WW_WEP_Owen.Play_WEP_Owen_Fire_Single')
}
