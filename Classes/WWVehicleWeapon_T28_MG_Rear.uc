
class WWVehicleWeapon_T28_MG_Rear extends WWVehicleWeapon_T28_MG
	abstract;

function BeginReload()
{
	local WWVehicle_T28 T;
	T = WWVehicle_T28(MyVehicle);
	
	TimeReloading();
	
	if (T != none)
	{
		if (T.RearMGReloadCount < 255)
		{
			T.RearMGReloadCount++;
		}
		else
		{
			T.RearMGReloadCount = 0;
		}
	}
}

defaultproperties
{
	WeaponContentClass(0)="WinterWar.WWVehicleWeapon_T28_MG_Rear_ActualContent"
	
	SeatIndex=`T28_MG_T_R
	
	FireTriggerTags=(T28_RearMG)
}
