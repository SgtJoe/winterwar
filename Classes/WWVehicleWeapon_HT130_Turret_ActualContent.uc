
class WWVehicleWeapon_HT130_Turret_ActualContent extends WWVehicleWeapon_HT130_Turret
	HideDropDown;

simulated function StartLoopingFireSound(byte FireModeNum)
{
	if (FireModeNum < bLoopingFireSnd.Length && bLoopingFireSnd[FireModeNum] && !ShouldForceSingleFireSound())
	{
		bPlayingLoopingFireSnd = true;
		
		ROPawn(ROWeaponPawn(Instigator).Driver).SetWeaponAmbientSound(WeaponFireSnd[FireModeNum].DefaultCue, WeaponFireSnd[FireModeNum].FirstPersonCue);
	}
}

simulated function StopLoopingFireSound(byte FireModeNum)
{
	if (bPlayingLoopingFireSnd)
	{
		ROPawn(ROWeaponPawn(Instigator).Driver).SetWeaponAmbientSound(None);
		
		if (FireModeNum < WeaponFireLoopEndSnd.Length)
		{
			WeaponPlayFireSound(WeaponFireLoopEndSnd[FireModeNum].DefaultCue, WeaponFireLoopEndSnd[FireModeNum].FirstPersonCue);
		}
		
		bPlayingLoopingFireSnd = false;
	}
}

DefaultProperties
{
	WeaponFireSnd(DEFAULT_FIREMODE)=(DefaultCue=AkEvent'WW_WEP_Flamethrower.Play_WEP_Flamethrower_Start_3P', FirstPersonCue=AkEvent'WW_WEP_Flamethrower.Play_WEP_Flamethrower_Start_3P')
	WeaponFireLoopEndSnd(DEFAULT_FIREMODE)=(DefaultCue=AkEvent'WW_WEP_Flamethrower.Play_WEP_Flamethrower_End_3P', FirstPersonCue=AkEvent'WW_WEP_Flamethrower.Play_WEP_Flamethrower_End_3P')
	bLoopingFireSnd(DEFAULT_FIREMODE)=true
	bLoopHighROFSounds(DEFAULT_FIREMODE)=true
	
	BarTexture=Texture2D'ui_textures.Textures.button_128grey'
}
