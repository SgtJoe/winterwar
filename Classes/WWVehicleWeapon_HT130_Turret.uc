
class WWVehicleWeapon_HT130_Turret extends WWVehicleWeapon_TankTurret
	abstract;

var int FlameProjectileCount;

replication
{
	if (bNetDirty && bNetOwner && Role==ROLE_Authority)
		FlameProjectileCount;
}

simulated exec function SwitchFireMode() {}

simulated function bool AllowFiring(byte FireModeNum=0)
{
	if( FireModeNum == ALTERNATE_FIREMODE && (IsTimerActive('ReloadComplete') || bCoaxialReloading) )
	{
		return false;
	}
	
	return super(ROVehicleWeapon).AllowFiring(FireModeNum);
}

simulated function class<Projectile> GetProjectileClass()
{
	if ( CurrentFireMode == DEFAULT_FIREMODE )
	{
		return WeaponProjectiles[DEFAULT_FIREMODE];
	}
	
	return super.GetProjectileClass();
}

function bool GiveInitialAmmo(optional bool bResupplyOnly)
{
	local bool bResuppliedCannon, bSuperResupplied;
	bMainCannonLoaded = true;
	
	if (FlameProjectileCount < default.FlameProjectileCount)
	{
		FlameProjectileCount = default.FlameProjectileCount;
		bResuppliedCannon = true;
	}
	
	bSuperResupplied = super(ROVehicleWeapon).GiveInitialAmmo();
	
	return (bSuperResupplied || bResuppliedCannon);
}

simulated function int GetMainGunAmmoCount( optional byte MainGunAmmoIndex = -1)
{
	return FlameProjectileCount;
}

simulated function bool HasMainGunAmmo( byte MainGunAmmoIndex )
{
	return FlameProjectileCount > 0;
}

simulated function bool HasAnyAmmo()
{
	if (super(ROVehicleWeapon).HasAnyAmmo())
	{
		return true;
	}
	
	if (FlameProjectileCount > 0)
	{
		return true;
	}
	
	return false;
}

function ConsumeMainGunAmmo( byte AmmoIndex )
{
	FlameProjectileCount = Max(0, FlameProjectileCount - 1);
}

simulated function HandleVehicleRecoil() {}

defaultproperties
{
	WeaponContentClass(0)="WinterWar.WWVehicleWeapon_HT130_Turret_ActualContent"
	
	SeatIndex=1
	
	PlayerIronSightFOV=13.0
	
	FiringStatesArray(0)=WeaponFiring
	WeaponFireTypes(0)=EWFT_Projectile
	WeaponProjectiles(0)=class'WWVehicleProjectile_HT130_Flames'
	FireInterval(0)=+0.07
	Spread(0)=0.002
	
	FireCameraAnim(0)=none
	
	WeaponDryFireSnd=AkEvent'WW_WEP_Flamethrower.Play_WEP_Flamethrower_DryFire'
	
	FiringStatesArray(ALTERNATE_FIREMODE)=WeaponFiring
	WeaponFireTypes(ALTERNATE_FIREMODE)=EWFT_Projectile
	WeaponProjectiles(ALTERNATE_FIREMODE)=class'WWProjectile_DT_HT130'
	FireInterval(ALTERNATE_FIREMODE)=+0.092
	Spread(ALTERNATE_FIREMODE)=0.0007
	
	AltFireTriggerTags=(T70CoaxMG)
	
	VehicleClass=class'WWVehicle_T26'
	
	// 30 seconds of fuel = 400 units
	FlameProjectileCount=1200
	
	AmmoClass=class'WWAmmo_DTDrum'
	MaxAmmoCount=63
	bUsesMagazines=true
	InitialNumPrimaryMags=`TANK_AMMO_T26_DT
	
	PenetrationDepth=22.23
	MaxPenetrationTests=3
	MaxNumPenetrations=2
}
