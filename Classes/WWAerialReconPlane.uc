
class WWAerialReconPlane extends ROAerialReconPlane
	abstract;

var PhysicsAsset DefaultPhysicsAsset;
var PhysicsAsset CrashingPhysicsAsset;

var PointLightComponent FlyingLight;

var bool MarkedForDeath;

var float WorldMinX;
var float WorldMinY;
var float WorldMaxX;
var float WorldMaxY;

simulated function PostBeginPlay()
{
	super.PostBeginPlay();
	
	// We need to spawn a light otherwise the mesh will be pitch black
	Mesh.AttachComponentToSocket(FlyingLight, 'LightSocket');
	
	// Enable normal collision for hit detection
	SkeletalMeshComponent(CollisionComponent).SetPhysicsAsset(DefaultPhysicsAsset, true);
}

simulated function bool IsInMapBounds()
{
	local array<Actor> MapBounds;
	
	if (FindActorsOfClass(class'ROVolumeMapBounds', MapBounds))
	{
		ROVolumeMapBounds(MapBounds[0]).GetBounds(WorldMinX, WorldMinY, WorldMaxX, WorldMaxY);
		
		if (Location.X > WorldMinX &&
			Location.X < WorldMaxX &&
			Location.Y > WorldMinY &&
			Location.Y < WorldMaxY)
		{
			`wwdebug("true",'CMDA');
			return true;
		}
	}
	
	`wwdebug("false",'CMDA');
	return false;
}

function TakeDamage(int DamageAmount, Controller EventInstigator, vector HitLocation, vector Momentum, class<DamageType> DamageType, optional TraceHitInfo HitInfo, optional Actor DamageCauser)
{
	local float DamageMod;
	local int StartingHealth;
	local bool bTeamKill;
	
	if (bExploded && !CanRemoveWreckage(DamageType))
	{
		return;
	}
	else if (bExploded && CanRemoveWreckage(DamageType))
	{
		`wwdebug("destroyed by" @ DamageType.name,'CMDA');
		Lifespan = 0.1;
		return;
	}
	
	DamageMod = 1.0;
	StartingHealth = Health;
	
	`wwdebug("taking" @ DamageType.name @ "in" @ HitInfo.BoneName,'CMDA');
	
	if (class<WWDmgType_FighterPlane>(DamageType) != none)
	{
		Health = 0; // Health - (default.Health * 0.7); // -70% of base health per fighter run
	}
	else
	{
		if (HitInfo.BoneName == 'Engine')
		{
			if (DamageType.default.VehicleDamageScaling >= 1.0 || (class<RODamageType>(DamageType) != none && class<RODamageType>(DamageType).default.AirVehicleDamageScaling >= 1.0))
			{
				DamageMod = 7;
			}
			else
			{
				DamageMod = 2;
			}
		}
		else
		{
			if (DamageType.default.VehicleDamageScaling >= 1.0 || (class<RODamageType>(DamageType) != none && class<RODamageType>(DamageType).default.AirVehicleDamageScaling >= 1.0))
			{
				DamageMod = 2;
			}
		}
		
		Health -= DamageAmount * DamageMod;
	}
	
	if ( WorldInfo.NetMode != NM_DedicatedServer )
	{
		UpdatedHealth();
	}
	
	if (Health >= 0 && !bExploded)
	{
		`wwdebug("health:" @ Max(0, Health),'CMDA');
	}
	
	if ( StartingHealth > 0 && Health <= 0 )
	{
		KilledBy = Controller(DamageCauser);
		
		// Swap to a smaller collision model to make the plane seem buried in the ground
		SkeletalMeshComponent(CollisionComponent).SetPhysicsAsset(CrashingPhysicsAsset, true);
		
		SetBase(none);
		SetPhysics(PHYS_RigidBody);
		
		AerialReconDuration = 0;
		
		Velocity = (CurrentLocation - LastLocation) / LastDeltaTime;
		
		// Set a life span in case we don't hit anything so we're not just falling into the void forever
		LifeSpan = 15.0;
		
		if (EventInstigator.GetTeamNum() == TeamIndex)
		{
			bTeamKill = true;
		}
		
		bFadeOutAudio = false;
		
		ROGameInfo(WorldInfo.Game).ScoreReconKill(EventInstigator, bTeamKill);
	}
}

function UpdateAerialRecon()
{
	// Don't allow planes to keep reporting once they've been shot down
	if (Health > 0)
	{
		super.UpdateAerialRecon();
	}
}

function bool CanRemoveWreckage(class<DamageType> DamageType)
{
	return (class<RODmgType_Satchel>(DamageType) != none ||
			class<RODmgTypeArtillery>(DamageType) != none);
}

simulated event Landed(vector HitNormal, Actor FloorActor) {}

simulated event HitWall( vector HitNormal, actor Wall, PrimitiveComponent WallComp, optional PhysicalMaterial WallPhysMaterial) {}

simulated function Explode()
{
	if( !bExploded )
	{
		if( Role == ROLE_Authority )
			bRepExploded = true;
		
		if (MapHasEngineers())
		{
			// If the map has engineers, set infinite lifespan so they can blow us up with satchels
			LifeSpan = 0.0;
		}
		else
		{
			// If there's no way to get rid of our wreckage, just destroy ourself after a while
			LifeSpan = 180.0;
		}
		
		SetPhysics(PHYS_None);
		
		Mesh.StopAnim();
		
		// Enable normal collision again so players don't clip into the mesh
		SkeletalMeshComponent(CollisionComponent).SetPhysicsAsset(DefaultPhysicsAsset, true);
		
		// The lighting is fine when on the ground
		Mesh.DetachComponent(FlyingLight);
		
		if ( AmbientSound != None )
		{
			if ( AmbientComponent != None )
			{
				AmbientComponent.StopEvents();
			}
		}
		
		bExploded = true;
		
		if ( SmokeEmitter != None )
		{
			SmokeEmitter.DeactivateSystem();
		}
		
		if ( WorldInfo.NetMode != NM_DedicatedServer )
		{
			if ( ExplosionSound != none )
			{
				PlayAkEvent(ExplosionSound, true);
			}
			
			PlayVehicleExplosionEffect(ExplosionTemplate, true);
		}
		
		if( CrashDamage > 0 && CrashDamageRadius > 0 )
		{
			if ( Role == ROLE_Authority && WorldInfo.NetMode != NM_Client )
				MakeNoise(1.0);
			HurtRadius(CrashDamage, CrashDamageRadius, CrashDamageType, 50000, Location,, KilledBy);
		}
		
		if( Role == ROLE_Authority )
		{
			ROGameInfo(WorldInfo.Game).DestroyAerialReconPlane(TeamIndex, true);
			
			if( CallingController != none )
				CallingController.bSentRadioContactMessage = false;
		}
	}
}

function bool MapHasEngineers()
{
	local int i;
	
	for (i = 0; i < ROMapInfo(WorldInfo.GetMapInfo()).NorthernRoles.Length; i++)
	{
		if (ROMapInfo(WorldInfo.GetMapInfo()).NorthernRoles[i].RoleInfoClass.default.RoleType == RORIT_Engineer)
		{
			return true;
		}
	}
	
	for (i = 0; i < ROMapInfo(WorldInfo.GetMapInfo()).SouthernRoles.Length; i++)
	{
		if (ROMapInfo(WorldInfo.GetMapInfo()).SouthernRoles[i].RoleInfoClass.default.RoleType == RORIT_Engineer)
		{
			return true;
		}
	}
	
	return false;
}

defaultproperties
{
	Begin Object Class=AnimNodeSequence Name=AnimNodeSeq0
	bCauseActorAnimEnd=true
	End Object
	
	Begin Object Class=DynamicLightEnvironmentComponent Name=MyLightEnvironment
		bIsVehicleLightEnvironment=true
		bSynthesizeSHLight=true
	End Object
	Components.Add(MyLightEnvironment)
	
	Begin Object Class=SkeletalMeshComponent Name=PlaneMesh
		AnimSets[0]=AnimSet'VH_VN_US_Recon.Animation.O1BirdDog_anim'
		PhysicsAsset=PhysicsAsset'WinterWar_VH_USSR_RECON.Phy.SOV_ReconPlane_CrashingPhy'
		Animations=AnimNodeSeq0
		RBChannel=RBCC_Vehicle
		RBCollideWithChannels=(Default=TRUE,BlockingVolume=TRUE,GameplayPhysics=TRUE,EffectPhysics=TRUE,Vehicle=TRUE,Untitled1=TRUE,Untitled4=TRUE)
		BlockActors=true
		BlockZeroExtent=true
		BlockRigidBody=true
		BlockNonzeroExtent=true
		CollideActors=true
		bForceDiscardRootMotion=true
		bUseSingleBodyPhysics=1
		bNotifyRigidBodyCollision=true
		ScriptRigidBodyCollisionThreshold=10.0
		LightEnvironment=MyLightEnvironment
	End Object
	Components.Add(PlaneMesh)
	
	Begin Object class=PointLightComponent name=PlaneLight
		bCastCompositeShadow=False
		LightingChannels=(Dynamic=TRUE,CompositeDynamic=TRUE)
		UseDirectLightMap=FALSE
		Radius=1000.0
	End Object
	Components.Add(PlaneLight)
	
	Mesh=PlaneMesh
	CollisionComponent=PlaneMesh
	FlyingLight=PlaneLight
	
	MarkedForDeath=false
}
