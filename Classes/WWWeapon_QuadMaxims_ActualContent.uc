
class WWWeapon_QuadMaxims_ActualContent extends WWWeapon_QuadMaxims;

DefaultProperties
{
	ArmsAnimSet=AnimSet'WinterWar_VH_USSR_ZIS.Anim.WP_QuadMaximhands'
	
	Begin Object Name=FirstPersonMesh
		SkeletalMesh=SkeletalMesh'WinterWar_VH_USSR_ZIS.Mesh.ZIS_QuadMaxims'
		PhysicsAsset=PhysicsAsset'WP_VN_VC_DshK_HMG.Phy.VC_DshK_Physics'
		AnimSets(0)=AnimSet'WinterWar_VH_USSR_ZIS.Anim.WP_QuadMaximhands'
		AnimTreeTemplate=AnimTree'WP_VN_VC_DshK_HMG.Animation.DshKTurretAnimtree'
	End Object
	
	Begin Object Name=FirstPersonArmsMesh
		AnimSets(1)=AnimSet'WinterWar_VH_USSR_ZIS.Anim.WP_QuadMaximhands'
	End Object
	
	/* Begin Object Name=AmmoBelt0
		SkeletalMesh=
		DepthPriorityGroup=SDPG_Foreground
		bOnlyOwnerSee=true
	End Object
	AmmoBeltMesh=AmmoBelt0 */
	
	AttachmentClass=class'WWWeapon_QuadMaxims_Attach'
}
