
class WWPosedPawn extends RODebugPosedPlayer
	hidecategories(RODebugPosedPlayer);

var transient WWPawn SpawnedPawn;

enum TeamEnum
{
	Axis,
	Allies
};

enum RoleEnum
{
	RI_RIFLEMAN,
	RI_ELITE_RIFLEMAN,
	RI_ASSAULT,
	RI_MACHINE_GUNNER,
	RI_SNIPER,
	RI_SAPPER,
	RI_COMMANDER,
	RI_TANK_CREW,
	RI_SQUAD_LEADER
};

enum PosesCommonEnum
{
	Hip_idle_rifle			<DisplayName=Standing Rifle>,
	Iron_idle_rifle			<DisplayName=Standing Rifle Aim>,
	JogF_rifle				<DisplayName=Standing Rifle Run>,
	Hip_idle_MG				<DisplayName=Standing MG>,
	JogF_MG					<DisplayName=Standing MG Run>,
	Hip_idle_pistol			<DisplayName=Standing Pistol>,
	Iron_idle_pistol		<DisplayName=Standing Pistol Aim>,
	jogF_pistol				<DisplayName=Standing Pistol Run>,
	Hip_idle_nade			<DisplayName=Standing Item>,
	Iron_idle_nade			<DisplayName=Standing Item Aim>,
	jogF_nade				<DisplayName=Standing Item Run>,
	CH_Hip_idle_rifle		<DisplayName=Crouch Rifle>,
	CH_Iron_idle_rifle		<DisplayName=Crouch Rifle Aim>,
	CH_Hip_idle_MG			<DisplayName=Crouch MG>,
	CH_Iron_idle_MG			<DisplayName=Crouch MG Aim>,
	CH_Hip_idle_pistol		<DisplayName=Crouch Pistol>,
	CH_Iron_idle_pistol		<DisplayName=Crouch Pistol Aim>,
	CH_Hip_idle_nade		<DisplayName=Crouch Item>,
	CH_Iron_idle_nade		<DisplayName=Crouch Item Aim>,
	Prone_Iron_idle_rifle	<DisplayName=Prone Rifle>,
	Prone_Iron_idle_MG		<DisplayName=Prone MG>,
	Prone_Iron_idle_pistol	<DisplayName=Prone Pistol>,
	prone_idle_nade			<DisplayName=Prone Item>
};

var() TeamEnum			PawnTeam;
var() RoleEnum			PawnRole;
var() int					PawnHonorLevel<ClampMin=0|ClampMax=99>;
var() PosesCommonEnum		PosesCommon;
var() class<ROWeapon>		WeaponClass;

var(WWPosedPawnAdvancedOptions) string				PoseOverride;
var(WWPosedPawnAdvancedOptions) AnimSet				PoseOverrideAnimSet;
var(WWPosedPawnAdvancedOptions) bool				DisableRightHandIK;
var(WWPosedPawnAdvancedOptions) bool				DisableLeftHandIK;
var(WWPosedPawnAdvancedOptions) bool				DisablePhysics;

function string GetPawnType()
{
	if (PawnTeam == Axis)
	{
		return "WinterWar.WWPawnAxis";
	}
	else if (PawnRole == RI_TANK_CREW)
	{
		return "WinterWar.WWPawnAlliesTanker";
	}
	else
	{
		return "WinterWar.WWPawnAllies";
	}
}

function name GetPoseAnim()
{
	local string retVal;
	
	if (PoseOverride != "")
	{
		retVal = PoseOverride;
	}
	else
	{
		retVal = string(PosesCommon);
	}
	
	return name(retVal);
}

function string GetPoseWeapon()
{
	local string retVal;
	
	if (Left(WeaponClass, 2) ~= "RO")
	{
		retVal = "ROGameContent."$string(WeaponClass.name);
	}
	else if (Left(WeaponClass, 2) ~= "WW")
	{
		retVal = "WinterWar."$string(WeaponClass.name);
	}
	else
	{
		return string(self.default.WeaponClass);
	}
	
	return retVal;
}

DefaultProperties
{
	DisableRightHandIK=false
	DisableLeftHandIK=false
	
	PawnHonorLevel=99
	
	WeaponClass="WinterWar.WWWeapon_MN91_ActualContent"
}
