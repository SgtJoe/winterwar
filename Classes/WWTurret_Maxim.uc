
class WWTurret_Maxim extends ROTurret_M2_HMG
	placeable;

simulated function UnHideBulletsNotify()
{
	WWWeapon_Maxim(MyWeapon).UnHideBulletsNotify();
}

defaultproperties
{
	OwningTeam=`ALLIES_TEAM_INDEX
	
	YawLimit=(X=-3000,Y=3000)
	
	Begin Object Name=CollisionCylinder
		CollisionHeight=25.000000
		CollisionRadius=25.000000 // Adjusting this makes the Use area larger, but also blocks pawns from getting close to it(tough balance)
	End Object
	
	Begin Object Name=WeaponSkeletalMeshComponent
		SkeletalMesh=SkeletalMesh'WinterWar_WP_SOV_MAXIM.Mesh.SOV_Maxim_3rd'
		AnimTreeTemplate=AnimTree'WinterWar_WP_SOV_MAXIM.Anim.MaximTurretAnimtree_3rd'
		AnimSets(0)=AnimSet'WinterWar_WP_SOV_MAXIM.Anim.Maxim_3rd_anim'
		PhysicsAsset=PhysicsAsset'WinterWar_WP_SOV_MAXIM.Phy.Sov_Maxim_Physics'
		CollideActors=true
		BlockActors=true
		BlockZeroExtent=true
		BlockNonZeroExtent=true
	End Object
	
	DefaultInventoryContentClass(0)="WinterWar.WWWeapon_Maxim_ActualContent"
	
	RootBoneName=ROOTBONE
	PivotBoneName=Maxim_Traverse
	AttachSocketName=AttachSocket
	PlayerRefSocketName=PlayerRef
	CameraSocketName=PlayerCam
	CamPivotSocketName=PlayerCamPivot
	IronSightsCameraSocketName=PlayerCamIS
	IronSightsCamPivotSocketName=PlayerCamPivotIS
	DuckedCameraSocketName=PlayerCamDuck
	LeftHandBoneHandleName=LeftHand
	RightHandBoneHandleName=RightHand
	ChestIKSocketName=ChestIKSocket
	MuzzleFlashSocket=MuzzleFlashSocket
	
	MoveToAnim=Maxim_goto
	DismountAnim=Maxim_getoff
	StandingAnim=Maxim_Idle
	DuckingAnim=Maxim_Duck_Idle
}