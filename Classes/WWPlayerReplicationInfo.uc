
class WWPlayerReplicationInfo extends ROPlayerReplicationInfo;

simulated function ClientInitialize(Controller C)
{
	local ROPlayerController ROPC;
	local bool bNewOwner;
	
	bNewOwner = (Owner != C);
	Super.ClientInitialize(C);
	
	if (bNewOwner)
	{
		self.UsedNames.Length = 0;
	}
	
	ROPC = ROPlayerController(C);
	if ( bNewOwner && ROPC != None && LocalPlayer(ROPC.Player) != None )
	{
		ClientInitializeUnlocks();
	}
	
	PawnHandlerClass = class'WWPawnHandler';
}
