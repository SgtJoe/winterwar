
class WWVoicePack_RUS_C extends ROAnnouncerPack;

DefaultProperties
{
	// CMD_RadioAbilityConfirm
	RadioComs[EROARC_Ability1Confirm]=(Sound=AkEvent'WW_AUD_VOX_RUS_C.Play_CMD_RUS_RadioAbilityConfirm')
	RadioComs[EROARC_Ability2Confirm]=(Sound=AkEvent'WW_AUD_VOX_RUS_C.Play_CMD_RUS_RadioAbilityConfirm')
	RadioComs[EROARC_Ability3Confirm]=(Sound=AkEvent'WW_AUD_VOX_RUS_C.Play_CMD_RUS_RadioAbilityConfirm')
	
	// CMD_RadioAbilityConfirm_CancelRequest
	RadioComs[EROARC_StrikeCancel]=(Sound=AkEvent'WW_AUD_VOX_RUS_C.Play_CMD_RUS_RadioAbilityConfirm_CancelRequest')
}
