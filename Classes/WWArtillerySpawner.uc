
class WWArtillerySpawner extends ROArtillerySpawner;

var class<ROArtilleryShell> ShellClass;

simulated function PostBeginPlay()
{
	local int StrikeTemp;
	local WWMapInfo MI;
	
	super(Actor).PostBeginPlay();
	
	InstigatorController = Controller(Owner);
	
	MI = WWMapInfo(WorldInfo.GetMapInfo());
	
	if (InstigatorController != none)
	{
		OwningTeam =	InstigatorController.GetTeamNum();
		ArtilleryType =	Clamp(WWPlayerController(InstigatorController).SavedArtyType, 0, 1);
		BatterySize =	MI.GetBatterySizeWW(OwningTeam, ArtilleryType);
		SalvoAmount =	MI.GetSalvoAmountWW(OwningTeam, ArtilleryType);
		SpreadAmount =	MI.GetSpreadAmountWW(OwningTeam, ArtilleryType);
		StrikeTemp =	MI.GetStrikeDelayWW(OwningTeam, ArtilleryType);
		SalvoInterval =	MI.GetSalvoIntervalWW(OwningTeam, ArtilleryType);
		ShellClass =	MI.GetShellClassWW(OwningTeam, ArtilleryType);
	}
	else
	{
		bNoGameTracking = true;
	}
	
	if (!bNoGameTracking)
	{
		ROTeamInfo(WorldInfo.GRI.Teams[OwningTeam]).ActiveArtillerySpawner = self;
		ROTeamInfo(WorldInfo.GRI.Teams[OwningTeam]).RemainingSalvos = SalvoAmount + 1;
	}
	
	if (FRand() < 0.5)
	{
		StrikeDelay =  StrikeTemp + (StrikeTemp * (Frand() * 0.15));
	}
	else
	{
		StrikeDelay =  StrikeTemp - (StrikeTemp * (Frand() * 0.15));
	}
	
	ArtilleryShellClassesAvailNorth.length = 0;
	ArtilleryShellClassesAvailSouth.length = 0;
	
	if (OwningTeam == `AXIS_TEAM_INDEX)
	{
		ArtilleryShellClassesAvailNorth.AddItem(ShellClass);
	}
	else
	{
		ArtilleryShellClassesAvailSouth.AddItem(ShellClass);
	}
	/*
	`wwdebug("Team" @ OwningTeam @ "Arty" @ ArtilleryType,'CMDA');
	`wwdebug("BatterySize" @ BatterySize,'CMDA');
	`wwdebug("SalvoAmount" @ SalvoAmount,'CMDA');
	`wwdebug("SpreadAmount" @ SpreadAmount,'CMDA');
	`wwdebug("StrikeDelay" @ StrikeDelay,'CMDA');
	`wwdebug("SalvoInterval" @ SalvoInterval,'CMDA');
	
	if (OwningTeam == `AXIS_TEAM_INDEX)
	{
		`wwdebug("ShellType" @ ArtilleryShellClassesAvailNorth[0],'CMDA');
	}
	else
	{
		`wwdebug("ShellType" @ ArtilleryShellClassesAvailSouth[0],'CMDA');
	}
	*/
	StartBarrage();
}

simulated function TryRefundAbilityUse()
{
	if (Role == ROLE_Authority)
	{
		if (SalvoCounter < 1 && SpawnCounter < 1)
		{
			if (WorldInfo != none && ROGameReplicationInfo(WorldInfo.GRI) != none)
			{
				if (ArtilleryType < 1)
				{
					ROGameReplicationInfo(WorldInfo.GRI).NextAbilityOneTime[SpawnTeam] = CachedArtyTime;
				}
				else
				{
					ROGameReplicationInfo(WorldInfo.GRI).NextAbilityTwoTime[SpawnTeam] = CachedArtyTime;
				}
			}
		}
	}
}
